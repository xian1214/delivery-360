package host.exp.exponent;
import com.facebook.react.bridge.Callback;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;

import android.os.SystemClock;
import android.posapi.PosApi;
import android.posapi.PosApi.OnCommEventListener;
import java.io.UnsupportedEncodingException;
import android.app.Activity;
import android.content.Context;
import java.io.File;
import java.io.FileWriter;
import java.util.Arrays;
import java.lang.String;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.util.Log;

import java.io.InputStream;
import java.net.URL;
import java.net.HttpURLConnection;

public class PrintManager extends ReactContextBaseJavaModule{


    private Context context;
    private PosApi mPosApi;
    private Activity mActivity = null;

    public PrintManager(ReactApplicationContext reactContext){
        super(reactContext);

        context = reactContext;
        try {
            FileWriter localFileWriterOn = new FileWriter(new File("/proc/gpiocontrol/set_sam"));
            localFileWriterOn.write("1");
            localFileWriterOn.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    OnCommEventListener mCommEventListener = new OnCommEventListener() {

        @Override
        public void onCommState(int cmdFlag, int state, byte[] resp, int respLen) {
            // TODO Auto-generated method stub
            switch(cmdFlag){
                case PosApi.POS_INIT:
                    if(state==PosApi.COMM_STATUS_SUCCESS){
                        //Toast.makeText(getApplicationContext(), "Initialization success", Toast.LENGTH_SHORT).show();
                        System.out.println("---------------POS API INITIALIZE IS SUCCESS");
                    }else {
                        System.out.println("---------------POS API INITIALIZE IS FAILED----");
                        //Toast.makeText(getApplicationContext(), "Failed to initialize", Toast.LENGTH_SHORT).show();
                    }
                    break;
            }
        }


    };
    @Override
    public String getName(){
        return "PrintManager";
    }
    @ReactMethod
    public void printText(boolean isLocalFile, String str,  String imgPath, Callback callback){

        mPosApi = PosApi.getInstance(context);
        mPosApi.setOnComEventListener(mCommEventListener);
                
        int x = 0;
        Log.e("Logo", "got X before");
        try {
            x = mPosApi.initDeviceEx("/dev/ttyMT2");
        }catch(Exception e) {
            //Log.e("poserrwatch","error")
            e.printStackTrace();
        }
         
        Log.e("Logo", "got X");
        StringBuilder sb = new StringBuilder();
        sb.append("       --Delivery Details--     ");
        sb.append(str);
        sb.append("\n");
        sb.append("\n");
        byte[] text=null;
        try {
            text = sb.toString().getBytes("GBK");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

//        Bitmap bitmap = BitmapFactory.decodeFile("/sdcard/Android/data/com.kelvin.driver/files/signature.bmp");
        Exception watcher = null;
        Bitmap bitmap = null;
        Bitmap resBmp = null;
        if(!isLocalFile){
            try {
                InputStream in = new java.net.URL(imgPath).openStream();
                bitmap = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                watcher = e;
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
        }
        else{
            bitmap = BitmapFactory.decodeFile(imgPath);
        }
        resBmp = Bitmap.createScaledBitmap(bitmap,300,200,false);
        byte[] mData = bitmap2PrinterBytes(resBmp);
        int i = 0;
        for(i=0;i<mData.length;i++){
            if(mData[i]!=0){
                int pp = 30;
            }
        }


        mPosApi.printText(60,text,text.length);
        SystemClock.sleep(1000);

        mPosApi.printImage(20,20,300,200,mData);
        SystemClock.sleep(2000);



        StringBuilder signatureStick = new StringBuilder();
        signatureStick.append("        SIGNATURE      ");
        signatureStick.append("\n");
        signatureStick.append("------------------------");
        signatureStick.append("\n");
        signatureStick.append("\n");
        signatureStick.append("     ");
        signatureStick.append("\n");

        byte[] text2=null;
        try {
            text2 = signatureStick.toString().getBytes("GBK");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        mPosApi.printText(60,text2,text2.length);
        mPosApi.closeDev();
        
        String greeting = "Finished";
        callback.invoke(greeting);
    }
    public void onCatalystInstanceDestroy(){
        try {
            FileWriter localFileWriterOn = new FileWriter(new File("/proc/gpiocontrol/set_sam"));
            localFileWriterOn.write("0");
            localFileWriterOn.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
     }
     public  static  byte[] bitmap2PrinterBytes (Bitmap bitmap){
         
        int width = bitmap.getWidth();
        int height = bitmap.getHeight();
        //Log.v("hello", "height?:"+height);
        int startX = 0;
        int startY = 0;
        int offset = 0;
        int scansize = width;
        int writeNo = 0;
        int rgb=0;
        int colorValue = 0;
        int[] rgbArray = new int[offset + (height - startY) * scansize
                                + (width - startX)];
        bitmap.getPixels(rgbArray, offset, scansize, startX, startY,
            width, height);

        int iCount = (height % 8);
        if (iCount > 0) {
        iCount = (height / 8) + 1;
        } else {
        iCount = (height / 8);
        }

        byte [] mData = new byte[iCount*width];

        //Log.v("hello", "myiCount?:"+iCoun t);
        for (int l = 0; l <= iCount - 1; l++) {
        //Log.v("hello", "iCount?:"+l);
        //Log.d("hello", "l?:"+l);
        for (int i = 0; i < width; i++) {
            int rowBegin = l * 8;
            //Log.v("hello", "width?:"+i);
            int tmpValue = 0;
            int leftPos = 7;
            int newheight = ((l + 1) * 8 - 1);
            //Log.v("hello", "newheight?:"+newheight);
            for (int j = rowBegin; j <=newheight; j++) {
            //Log.v("hello", "width?:"+i+"  rowBegin?:"+j);
            if (j >= height) {
            colorValue = 0;
            } else {
            rgb = rgbArray[offset + (j - startY)* scansize + (i - startX)];
            if (rgb == -1) {
            colorValue = 0;
            } else {
            colorValue = 1;
            }
            }
            //Log.d("hello", "rgbArray?:"+(offset + (j - startY)
            //  * scansize + (i - startX)));
            //Log.d("hello", "colorValue?:"+colorValue);
            tmpValue = (tmpValue + (colorValue << leftPos));
            leftPos = leftPos - 1;     

            }
            mData[writeNo]=(byte) tmpValue;
            writeNo++;
        }
        }

        return mData;
        }
}