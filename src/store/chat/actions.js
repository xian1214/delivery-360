import * as types from './actionTypes'
import firebaseService from '../../service/firebaseService'
import Cache from '../../utils/cache'
import * as CONST from '../../components/constant'
const FIREBASE_REF_MESSAGES = firebaseService.database().ref('ErpMessages')
const FIREBASE_REF_MESSAGES_LIMIT = 20
import api from "../../service/api";
import moment from 'moment'
export const sendMessage = (channel,id, message, uri, voice,actionID,cb) => {
  return (dispatch) => {
    //console.log('cache.currentUser....on chat actino...>>',Cache.currentUser)
    dispatch(chatMessageLoading())
    //console.log('channel on send message', channel)
    let createdAt = new Date().getTime()
    let chatMessage = {
      text: message,
      createdAt: createdAt,
      user: {
        role:"Driver",
        name:Cache.currentUser.user.UserName,
        avatar:Cache.currentUser.user.Avarta
      },
      actionID,
      dnNumber:channel,
      image: uri,
      voice,
    }
    
    let chatRoom = firebaseService.database().ref(channel)
    //console.log('1111111111-----------1',chatMessage)
    chatRoom.push().set(chatMessage, (error) => {
      if (error) {
        dispatch(chatMessageError(error.message))
        cb(error,null)
      } else {
        cb(null,true)
        var data = {
          Text: message,
          Time: moment(),
          Name: Cache.currentUser.user.UserName
        }
        //console.log('1111111111-----------2',chatMessage)
        api.lastMessageUpdate(id, data, (error, res) => {
          if (error) {
            //console.log('1111111111-----------3',chatMessage)
            Alert.alert('Upload new message failure')
            return
          }
          //console.log('1111111111-----------4',chatMessage)
            api.incrementUnreadCnt(id, CONST.DRIVER_USER,message, (error,res)=>{
                if(error){
                  //console.log('1111111111-----------5',chatMessage)
                  Alert.alert('Unread message increment failure')
                  return
                }
            })
            '1111111111-----------6'
            //console.log("arrived chat info res is ",res)
        })
        dispatch(chatMessageSuccess())
      }
    })
  }
}

export const updateMessage = text => {
  return (dispatch) => {
    dispatch(chatUpdateMessage(text))
  }
}

export const loadMessages = (channel,length,cb) => {
  let chatRoom = firebaseService.database().ref(channel)
  return (dispatch) => {
    chatRoom.limitToLast(length).on('value', (snapshot) => {
      cb(null,null)
      dispatch(loadMessagesSuccess(snapshot.val()))
    }, (errorObject) => {
      cb(null,null)
      dispatch(loadMessagesError(errorObject.message))
    })
  }
}
export function plusUnreadMsgCnt(){
  //console.log('message redux 1')
  return dispatch =>{
    //console.log('message redux 2')
    dispatch({type:types.PLUS_UNREAD_CNT, status:types.SUCCESS});
  }
}
export function setUnreadMsgCnt(cnt){
  //console.log('message redux 1')
  return dispatch =>{
    //console.log('message redux 2')
    dispatch({type:types.SET_UNREAD_CNT,unreadCnt:cnt, status:types.SUCCESS});
  }
}

export function resetUnreadMsgCnt(){
  return dispatch =>{
    dispatch({type:types.RESET_UNREAD_CNT, status:types.SUCCESS});
  }
}
const chatMessageLoading = () => ({
  type: types.CHAT_MESSAGE_LOADING
})

const chatMessageSuccess = () => ({
  type: types.CHAT_MESSAGE_SUCCESS
})

const chatMessageError = error => ({
  type: types.CHAT_MESSAGE_ERROR,
  error
})

const chatUpdateMessage = text => ({
  type: types.CHAT_MESSAGE_UPDATE,
  text
})

const loadMessagesSuccess = messages => ({
  type: types.CHAT_LOAD_MESSAGES_SUCCESS,
  messages
})

const loadMessagesError = error => ({
  type: types.CHAT_LOAD_MESSAGES_ERROR,
  error
})
