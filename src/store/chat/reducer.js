import * as types from './actionTypes'

const initialState = {
  sending: false,
  sendingError: null,
  message: '',
  messages: {},
  loadMessagesError: null,
  unreadCnt:0,
}

const chat = (state = initialState, action) => {
  switch(action.type) {
    case types.CHAT_MESSAGE_LOADING:
      return { ...state, sending: true, sendingError: null }
    case types.CHAT_MESSAGE_ERROR:
      return { ...state, sending: false, sendingError: action.error }
    case types.CHAT_MESSAGE_SUCCESS:
      return { ...state, sending: false, sendingError: null, message: '' }
    case types.CHAT_MESSAGE_UPDATE:
      return { ...state, sending: false, message: action.text, sendingError: null }
    case types.CHAT_LOAD_MESSAGES_SUCCESS:
      return { ...state, messages: action.messages, loadMessagesError: null }
    case types.CHAT_LOAD_MESSAGES_ERROR:
      return { ...state, messages: null, loadMessagesError: action.error }
    case types.PLUS_UNREAD_CNT:
      //console.log('message redux 3')
      return { ...state, unreadCnt:state.unreadCnt+1,type:types.PLUS_UNREAD_CNT, loadMessagesError: action.error }
    case types.RESET_UNREAD_CNT:
    //console.log('message redux 4')
      return { ...state,  unreadCnt:0, loadMessagesError: action.error }
    case types.SET_UNREAD_CNT:
    //console.log('message redux 5')
      return { ...state,  unreadCnt:action.unreadCnt, loadMessagesError: action.error }
    default:
      return state
  }
}

export default chat
