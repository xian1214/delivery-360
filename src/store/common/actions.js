import * as types from './actionTypes';
import api from '../../service/api'
import Cache from '../../utils/cache'
import moment from 'moment'
import * as commonStyles from '../../styles/styles'

import {
  Alert
} from "react-native";

export function setHeading(heading){
  return dispatch =>{
    dispatch({type:types.SET_HEADING, heading});
  }
}

// export function setLocation(location){
//   ////console.log('updating location on actions.....',location)
//   return dispatch =>{
//     dispatch({type:types.SET_LOCATION, location, status:types.SUCCESS});
//   }
// }

export function refreshStats(){
  return dispatch =>{
    dispatch({type:types.REFRESH_STATS});
  }
}


export function setVehicleDetail(vehicleDetail){
  ////console.log('updating location on actions.....',location)
  return dispatch =>{
    dispatch({type:types.SET_VEHICLE_DETAIL, vehicleDetail, status:types.SUCCESS});
  }
}
export function setCustomerSignature(localSignature){
  return dispatch =>{
    //console.log('signatureUri....on actions.Signature---on actions dispatch', localSignature)
    dispatch({type:types.SET_LOCAL_SIGNATURE, localSignature, status:types.SUCCESS});
  }
}
export function setShelfLifeAlerts(flags){
  ////console.log('dispatching alerts...',flags)
  return dispatch =>{
    dispatch({type:types.SET_ALERT_SHELFLIFE, value:flags});
  }
}
export function setVelocityMileage(currentVal){
  if(!!currentVal){
    return dispatch =>{
      dispatch({type:types.SET_VELOCITY, value:currentVal.Velocity, status:types.SUCCESS});
      dispatch({type:types.SET_MILEAGE, value:currentVal.Mileage, status:types.SUCCESS});
    }
  }
  
} 
export function setTargetSpeed(speed){
  return dispatch =>{
    dispatch({type:types.SET_TARGET_SPEED,targetSpeed,status:types.SUCCESS})
  }
}
export function receivedNotification(notification){
  return dispatch=>{
    dispatch({type:types.RECEIVED_NOTIFICATION, notification});
  }
}

export function changeQueueState(value){
  return dispatch=>{
    
    if ( value ){
      api.queueIn((err,res)=>{
         //console.log('arrived queuein error.Message------',err.Message)
        if (err==null){
          Cache.currentUser.user.QueueNo = res
          dispatch({type:types.CHANGE_QUEUE_STATE, value});
        }
        else{
          if(!!err.Message){
            Alert.alert(err.Message)
          }
        }
        
        return

      })
    }else{
      dispatch({type:types.CHANGE_QUEUE_STATE, value});
      api.queueDrop((err,res)=>{
        // //console.log(err)
        if (err==null){
          
        }
      })
    }
    
  }
}

export function manualUpdate(currentDelivery){
  return dispatch=>{
    dispatch({type:types.UPDATE_CURRENT_DELIVERY, value:currentDelivery, status:types.SUCCESS})
  }
}

export function updateCurrentDelivery(id){
  //console.log('id on acitons.js >>',id)
  return dispatch=>{
    dispatch({type:types.UPDATE_CURRENT_DELIVERY, status:types.LOADING})
    if (!!!id){
      api.getInprocessDelivery((err, deliveries)=>{
        if ( err == null ){
          if (deliveries == null || deliveries.length==0){
            //console.log("this displayes null on delivery note.....!!!!!!!::deliveries",deliveries)
            dispatch({type:types.UPDATE_CURRENT_DELIVERY, value:null, status:types.SUCCESS})
            return;
          }
          
          api.getDelivery(deliveries[0].DeliveryNoteID, (err, res)=>{
            if (err == null ){
              //console.log("AAAA",(!!res)?'DN is not null '+res.DeliveryNoteNumber:'DN is null',id)
              dispatch({type:types.UPDATE_CURRENT_DELIVERY, value:res, status:types.SUCCESS})
            }else{
              //console.log("BBBB",id)
              dispatch({type:types.UPDATE_CURRENT_DELIVERY, status:types.FAILED})
            }
          })
        }else{
          dispatch({type:types.UPDATE_CURRENT_DELIVERY, status:types.FAILED})
        }
      })
    }else{
      api.getDelivery(id, (err, res)=>{
        if (err == null ){
          console.log('cccc',res.DeliveryNoteNumber,id)
          dispatch({type:types.UPDATE_CURRENT_DELIVERY, value:res, status:types.SUCCESS})
        }else{
          //console.log('dddd',id)
          dispatch({type:types.UPDATE_CURRENT_DELIVERY, value:res, status:types.FAILED})
        }
      })
    }
    
  }
}

export function getTodayUpcomingDeliveryCount(){
  return dispatch=>{
    api.getDeliveries(
      Cache.currentUser.user.UserID, 
      false,
      1,
      1,
      moment().format('YYYY-MM-DD') + ' 00:00:00',
      moment().format('YYYY-MM-DD') + ' 23:59:59',
      (err, res)=>{
        //console.log('todayCount', err)
        if ( err == null ){
          dispatch({type:types.GET_TODAY_UPCOMING_COUNT, count:res.TotalNumberOfRecords})
        }
      }
    )
  }
}

export function getFutureUpcomingDeliveryCount(){
  return dispatch=>{
    api.getDeliveries(
      Cache.currentUser.user.UserID, 
      false,
      1,
      1,
      moment().format('YYYY-MM-DD') + ' 00:00:00',
      null,
      (err, res)=>{
        //console.log('otherCount', err)
        if ( err == null ){
          dispatch({type:types.GET_FUTURE_UPCOMING_COUNT, count:res.TotalNumberOfRecords})
        }
      }
    )
  }
}

export function getUpcomingDeliveries(page, pageSize){
  return dispatch=>{
    dispatch({type:types.GET_UPCOMING_DELIVERIES_MORE, status:types.LOADING})
    api.getDeliveries(
      Cache.currentUser.user.UserID, 
      false,
      page,
      pageSize,
      null,
      null,
      (err, res)=>{
        if ( err == null ){
          dispatch({type:types.GET_UPCOMING_DELIVERIES_MORE, items:res.Results, status: types.SUCCESS})
        }else{
          dispatch({type:types.GET_UPCOMING_DELIVERIES_MORE, status:types.FAILED})
        }
      }
    )
  }
}

export function getUpcomingAllDeliveries(readCnt){
  return dispatch=>{
    dispatch({type:types.GET_UPCOMING_DELIVERIES_REFRESH, status:types.LOADING})
    api.getDeliveries(
      Cache.currentUser.user.UserID, 
      false,
      1,
      readCnt,
      null,//moment().format('YYYY-MM-DD') + ' 00:00:00',
      null,
      (err, res)=>{
        //console.log('upcoming Deliveries', err)
        if ( err == null ){
          dispatch({type:types.GET_UPCOMING_DELIVERIES_REFRESH, items:res.Results, status: types.SUCCESS})
        }else{
          dispatch({type:types.GET_UPCOMING_DELIVERIES_REFRESH, status:types.FAILED})
        }
      }
    )
  }
}

export function getCompletedDeliveries(page, pageSize){
  
  return dispatch=>{
    dispatch({type:types.GET_COMPLETED_DELIVERIES_MORE, status:types.LOADING})
    api.getDeliveries(
      Cache.currentUser.user.UserID, 
      true,
      page,
      pageSize,
      null,
      null,
      (err, res)=>{
        if ( err == null ){
          dispatch({type:types.GET_COMPLETED_DELIVERIES_MORE, items:res.Results, status: types.SUCCESS})
        }else{
          dispatch({type:types.GET_COMPLETED_DELIVERIES_MORE, status:types.FAILED})
        }
      }
    )
  }
}

export function getCompletedAllDeliveries(pageSize){
  return dispatch=>{
    dispatch({type:types.GET_COMPLETED_DELIVERIES_REFRESH, status:types.LOADING})
    api.getDeliveries(
      Cache.currentUser.user.UserID, 
      true,
      1,
      pageSize,
      null,
      null,
      (err, res)=>{
        if ( err == null ){
          dispatch({type:types.GET_COMPLETED_DELIVERIES_REFRESH, items:res.Results, status: types.SUCCESS})
        }else{
          dispatch({type:types.GET_COMPLETED_DELIVERIES_REFRESH, status:types.FAILED})
        }
      }
    )
  }
}

export function setInternetConnection(status){
  return dispatch=>{
    dispatch({type:types.SET_INTERNET_CONNECTION_STATUS, value:status})
  }
}

export function updateBatteryInfo(status){
  return dispatch=>{
    dispatch({type:types.UPDATE_BATTERY_INFO, value:status})
  }
}

// export function getDeliveries(){
//   return dispatch=>{
//     dispatch({type:types.GET_DELIVERIES, status:types.LOADING})
//     api.getDeliveries(( err, res) => {
//       if (err == null) {
//         Cache.deliveries = res.Results
//         let upcoming = [], completed=[], currentItem=null;
//         res.Results.map(item => {
//           if ((item.User && item.User.UserID == Cache.currentUser.user.UserID)&&item.ProjectID!=null) {
//             if ( item.Status=='004005' || item.Status == '004002'){
//               upcoming.push(item)
//               if ( item.OTWDatetime ){
//                 currentItem = item
//               }
//             }
//             if ( item.Status=='004003'){
//               completed.push(item)
//             }
//           }
//         });
//         completed.reverse()
//         dispatch({type:types.GET_DELIVERIES, upcomingDeliveries:upcoming, completedDeliveries:completed, currentItem, status:types.SUCCESS})
//       }else{
//         dispatch({type:types.GET_DELIVERIES, status:types.FAILED})
//       }
//     });
//   }
// }

