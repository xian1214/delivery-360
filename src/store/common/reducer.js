import * as types from "./actionTypes";
import * as commonStyles from '../../styles/styles'

const initialState = {
  type: null,
  heading:null,
  location: null,
  backupLocation:null,
  notification: null,
  hasConnection: true,
  upcomingDeliveries:[],
  completedDeliveries:[],
  queueIn: false,
  currentDelivery:null,
  vehicleDetail:null,
  internetConnection:true,
  batteryInfo:{},
  shelfLifeAlerts:[],
  todayUpcomingCount:0,
  otherUpcomingCount:0,
  localSignature:null,
};

export default function common(state = initialState, action = {}) {
  switch (action.type) {

    case types.SET_HEADING:
      return {
        ...state,
        type:types.SET_HEADING,
        heading: action.heading
      }

    case types.SET_LOCATION:
      return {
        ...state,
        type: types.SET_LOCATION,
        location: action.location,
        status: action.status
      }
    case types.SET_BACKUP_LOCATION:
    return {
      ...state,
      type: types.SET_LOCATION,
      backupLocation: action.location,
      status: action.status
    }

    // case types.PLUS_UNREAD_CNT:
    // return {
    //   ...state,
    //   type: types.PLUS_UNREAD_CNT,
    //   unreadMsgCnt: unreadMsgCnt+1,
    //   status: action.status
    // }

    // case types.RESET_UNREAD_CNT:
    // return {
    //   ...state,
    //   type: types.RESET_UNREAD_CNT,
    //   unreadMsgCnt: 0,
    //   status: action.status
    // }

    case types.SET_LOCATION_MODE:
    return {
      ...state,
      type: types.SET_LOCATION_MODE,
      locationMode: action.isMobileMode,
      status: action.status
    }
    case types.SET_VEHICLE_DETAIL:
      return {
        ...state,
        type: types.SET_VEHICLE_DETAIL,
        vehicleDetail: action.vehicleDetail,
        status: action.status
      }
    case types.SET_LOCAL_SIGNATURE:
      //console.log('signatureUri....on actions.Signature---on reducer',action.localSignature)
      return {
        ...state,
        type: types.SET_LOCAL_SIGNATURE,
        localSignature: action.localSignature,
        status: action.status
      }

    case types.RECEIVED_NOTIFICATION:
      return {
        ...state,
        type: types.RECEIVED_NOTIFICATION,
        notification: action.notification
      }

    case types.GET_TODAY_UPCOMING_COUNT:
      return {
        ...state,
        type: types.GET_TODAY_UPCOMING_COUNT,
        todayUpcomingCount: action.count
      }

    case types.GET_FUTURE_UPCOMING_COUNT:
      return {
        ...state,
        type: types.GET_FUTURE_UPCOMING_COUNT,
        otherUpcomingCount: action.count
      }

    
    case types.GET_UPCOMING_DELIVERIES_REFRESH:
      // if ( action.status == types.SUCCESS && state.headingcurrentDelivery == null ){
      //   action.items.map((item)=>{
      //     if ( item.OTWDatetime && item.Status != commonStyles.DELIVERY_STATUS_FINISH){
      //       state.currentDelivery = item
      //     }
      //   })
      // }
      return {
        ...state,
        type: types.GET_UPCOMING_DELIVERIES_REFRESH,
        status: action.status,
        upcomingDeliveries: action.status == types.SUCCESS?action.items:state.upcomingDeliveries,
      }

    // case types.GET_COMPLETED_DELIVERIES_MORE:
    //   state.type = types.GET_COMPLETED_DELIVERIES
    //   state.status = action.status
    //   if ( action.status == types.SUCCESS ){
    //     let mapping = []
    //     state.completedDeliveries.map((item)=>mapping[item.DeliveryNoteID]=true)
    //     action.items.map((item)=>{
    //       if ( !mapping[item.DeliveryNoteID] ){
    //         state.completedDeliveries.push(item)
    //       }
    //     })
    //   }
    //   return JSON.parse(JSON.stringify(state))

    case types.GET_COMPLETED_DELIVERIES_REFRESH:
      // if ( action.status == types.SUCCESS && state.currentDelivery == null ){
      //   action.items.map((item)=>{
      //     if ( item.OTWDatetime && item.Status != commonStyles.DELIVERY_STATUS_FINISH){
      //       state.currentDelivery = item
      //     }
      //   })
      // }
      return {
        ...state,
        type: types.GET_COMPLETED_DELIVERIES_REFRESH,
        status: action.status,
        completedDeliveries:action.status == types.SUCCESS?action.items:state.completedDeliveries,
      }
      
    // case types.GET_DELIVERIES:
    //   state.type = types.GET_DELIVERIES;
    //   if ( action.status== types.SUCCESS ){
    //     state.upcomingDeliveries = action.upcomingDeliveries
    //     let mapping = []
    //     state.upcomingDeliveries.map((item)=>mapping[item.DeliveryNoteID]=true)
    //     action.upcomingDeliveries.map((item)=>{
    //       if ( !mapping[item.DeliveryNoteID] ){
    //         state.upcomingDeliveries.push(item)
    //       }
    //     })
    //     state.completedDeliveries = action.completedDeliveries
    //     state.currentDelivery = action.currentItem
    //   }
    //   state.status = action.status
    //   return JSON.parse(JSON.stringify(state))

    case types.CHANGE_QUEUE_STATE:
      return {
        ...state,
        type: types.CHANGE_QUEUE_STATE,
        queueIn: action.value
      }

    case types.UPDATE_CURRENT_DELIVERY:
    console.log('this.props.currentDelivery on reducer',action.status, !!action.value, !!state.currentDelivery)
      return {
        ...state,
        type: types.UPDATE_CURRENT_DELIVERY,
        currentDelivery: action.status==types.SUCCESS?action.value:state.currentDelivery,
        status: action.status
      }
      
    case types.SET_INTERNET_CONNECTION_STATUS:
      return {
        ...state,
        types:types.SET_INTERNET_CONNECTION_STATUS,
        internetConnection: action.value
      }
    case types.UPDATE_BATTERY_INFO:
      return {
        ...state,
        types:types.UPDATE_BATTERY_INFO,
        batteryInfo: action.value
      }
    case types.REFRESH_STATS:
    return {
      ...state,
      type:types.REFRESH_STATS,
      
    }
    case types.SET_ALERT_SHELFLIFE:
      ////console.log('returning -------- on reducer.js')
      return {
        ...state,
        type:types.SET_ALERT_SHELFLIFE,
        shelfLifeAlerts: action.value
      }
    default:
      return state;
  }
}
