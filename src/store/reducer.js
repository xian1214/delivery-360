import { combineReducers } from 'redux'

import session from './session'
import chat from './chat'
import common from './common'

export default combineReducers({
  session,
  chat,
  common
})
