"use strict";

import React, { PureComponent } from "react";

import {
  StyleSheet,
  View,
  Platform,
  Modal,
  TouchableOpacity,
  Text,
  StatusBar,
  Image,
  ScrollView,
  FlatList,
  RefreshControl,
  Alert,
} from "react-native";


import * as commonColors from "../styles/colors";
import * as commonStyles from "../styles/styles";
import { screenWidth, screenHeight } from "../styles/styles";

import * as actionTypes from "../store/common/actionTypes";

import { bindActionCreators } from "redux";
import { connect } from "react-redux";
// import * as actions from "../redux/actions";
import * as actions from '../store/common/actions'
import * as session from '../store/session/actions'

import api from "../service/api";
import Cache from "../utils/cache";
import { Ionicons, MaterialCommunityIcons } from "@expo/vector-icons";
import { Actions } from "react-native-router-flux";
import HeaderBar from "../components/header";
import UtilService from '../utils/utils'
import InformationBar from '../components/informationBar'
import moment from 'moment'
var gHandler = null
function isSameDate(A, B) {
  if (
    A.getDate() == B.getDate() &&
    A.getMonth() == B.getMonth() &&
    A.getFullYear() == B.getFullYear()
  )
    return true;
  return false;
}

const Item=({icon, name, content, isGrey})=>(
    <View style={{flexDirection:'row', alignItems:'center', marginTop:5}}>
        <Ionicons name={icon} size={16} color={'grey'} style={{width:20}}/>
        <Text style={{fontSize:13, color:'grey', marginLeft:8, width:50}}>{name}</Text>
        <Text style={{fontSize:15, color:isGrey?'grey':'#333', fontWeight:'bold', flex:1}}>{content}</Text>
    </View>
)

const Card = ({ card, isToday, onPress, type }) => (
  <TouchableOpacity
    //onPress={() => onPress()}
    style={[
      styles.cardContainer,
      { borderColor: commonColors.green }
    ]}
  >
    <View style={{padding:15, marginTop:type>0?40:0,flexDirection:'row'}}>
      <Text style={{color:'#777', marginTop:5}}># {card.DeliveryNoteNumber}</Text>
      <Text style={{flex:1}}></Text>
      <Text style={{color:'black', marginTop:5}}>{moment(card.ETD).format("YYYY/MM/DD")}</Text>
    </View>
    <View style={{flexDirection:'row', backgroundColor:commonColors.cardColor3, alignItems:'center', height:30, width:'100%', marginTop:8, paddingHorizontal:20}}>
            <Text style={{color:'grey', flex:1}}>Item</Text>
            <Text style={{color:'grey', width:80}}>Delivered</Text>
    </View>
    {
      card.DeliveryNoteItems.map((item, index)=>{
        return(
            <View key={index} style={{flexDirection:'row', alignItems:'center', height:30, width:'100%', marginTop:8, paddingHorizontal:20}}>
                <Text style={{color:'grey', flex:1}}>{item.Item.Name}</Text>
                <Text style={{color:'grey', width:80}}>{item.DeliveredQuantity} {item.Item.Uom?item.Item.Uom.Name:''}</Text>
            </View>
        )
      })
    }
  </TouchableOpacity>
);

const CardList = ({ cards, onPress, currentID, currentDelivery, isRefreshing, onRefresh }) => (
  <View style={{ flex: 1 }}>
    <FlatList
      refreshControl={
        <RefreshControl
            refreshing={isRefreshing}
            onRefresh={() => onRefresh()}
            tintColor={'grey'}
        />
      }
      initialNumToRender={10}
      onEndReachedThreshold={0.1}
      onEndReached={gHandler.onEndReached.bind(gHandler)}
      data={cards}
      keyExtractor={item=>''+item.DeliveryNoteID}
      renderItem={({item, index})=>{
        let card = item
        let isShowDate = true;
        if (index > 0) {
          let A = new Date(card.CompleteTime), B = new Date(cards[index - 1].CompleteTime);
          if ( isSameDate(A, B) ) isShowDate=false
        }
        return (
          <View style={{ paddingHorizontal: 20, paddingVertical: 5 }} >
            {(isShowDate && (new Date(card.CompleteTime)).getFullYear()>2000) && <Text style={styles.date}>{UtilService.getDateTime(card.CompleteTime)}</Text>}
            <Card 
              card={card} 
              isToday={isSameDate(new Date(card.CompleteTime), new Date())} 
              onPress={() => onPress(card)} 
              type={currentID!=card.DeliveryNoteID?0:(currentDelivery.Status==commonStyles.DELIVERY_STATUS_RETURNING && currentDelivery.ReturnWarehouse!=null?2:1)}
            />
            
          </View>
        );
      }}
    />
  </View>
);
class Completed extends PureComponent {
  constructor(props) {
    super(props);
    this.state={
      isRefreshing: false,
      completedDeliveries:[]
    }
    gHandler = this
    this.readCnt = 10
  }

    componentDidMount(){
      //this.props.actions.getCompletedAllDeliveries(10)
      //setTimeout(()=>{//console.log('this.props.actions:::ioooooooooooooooooooooooooo',this.props.completedDeliveries)},1000)
      var start = moment(this.props.startDate).format("YYYY-MM-DD" + ' 00:00:00')
      var end = moment(this.props.endDate).format("YYYY-MM-DD" + ' 23:59:59')
      //console.log('cache.currentUser.user.UserID--- on completed summary',Cache.currentUser.user.UserID)
      api.getCompletedDeliveriesByTime(start,end,  Cache.currentUser.user.UserID, (error,res)=>{
        if(error){
          return
        }
        
        this.setState({completedDeliveries:res})
      })
      
    }
    onEndReached(){
    
    }
    onRefresh(){
    
    }
  
    componentWillReceiveProps(nextProps){
      if ( nextProps.actionType == actionTypes.GET_COMPLETED_DELIVERIES_REFRESH ){
        
        if (nextProps.status==actionTypes.LOADING){
          this.setState({isRefreshing:true})
        }
        if (nextProps.status==actionTypes.FAILED){
          this.setState({isRefreshing:false})
          Alert.alert('Failed to fetch deliveries')
        }
        if (nextProps.status==actionTypes.SUCCESS){
          this.setState({isRefreshing:false})
          if ( this.props.currentDelivery ){
            this.props.actions.updateCurrentDelivery()
          }
  
        }
      }
    }

  render() {
    let {currentDelivery, internetConnection} = this.props
    return (
      <View style={styles.container}>
        <InformationBar iConnection={internetConnection} currentDelivery={currentDelivery}/>
        <HeaderBar title={"Delivery Summary"} />
        <CardList
          cards={this.state.completedDeliveries}
          isRefreshing={this.state.isRefreshing}
          onRefresh={()=>this.onRefresh()}
          currentDelivery = {this.props.currentDelivery}
          currentID = {this.props.currentDelivery?this.props.currentDelivery.DeliveryNoteID:null}
        />
       
      </View>
    );
  }
}

export default connect(
  state => ({
    actionType: state.common.type,
    status: state.common.status,
    currentDelivery: state.common.currentDelivery,
    completedDeliveries:state.common.completedDeliveries,
    internetConnection: state.common.internetConnection
  }),
  dispatch => ({
    actions: bindActionCreators({...actions,...session}, dispatch)
  })
)(Completed);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop:24, 
    backgroundColor: "rgb(48,48,48)"
  },
  cardContainer: {
    elevation: 3,
    borderRadius: 8,
    backgroundColor: "white",
    // padding: 10,
    borderWidth: 3,
    borderColor: commonColors.blue
  },
  date: {
    fontSize: 13,
    color: "white",
    marginVertical: 5,
    fontWeight: "bold"
  }
});
