"use strict";

import React, { PureComponent } from "react";

import {
  StyleSheet,
  View,
  Platform,
  Modal,
  TouchableOpacity,
  Text,
  StatusBar,
  Image,
  TextInput,
  ScrollView,
  Alert,
  ActivityIndicator,
} from "react-native";


import * as commonColors from "../styles/colors";
import * as commonStyles from "../styles/styles";
import { screenWidth, screenHeight } from "../styles/styles";
import * as CONST from '../components/constant'
import * as actionTypes from "../store/common/actionTypes";

import { bindActionCreators } from "redux";
import { connect } from "react-redux";
// import * as actions from "../redux/actions";
import * as actions from '../store/common/actions'
import * as session from '../store/session/actions'

import {ImagePicker, Permissions, ImageManipulator} from 'expo'

import api from "../service/api";
import Cache from "../utils/cache";
import { Ionicons, MaterialCommunityIcons } from "@expo/vector-icons";
import { Actions } from "react-native-router-flux";
import HeaderBar from '../components/header'
import async from 'async'
import UtilService from '../utils/utils'
import { WaitingModal } from "../components/waiting";
import InformationBar from '../components/informationBar'
import moment from 'moment'
import * as config from '../config'



const Warning=({icon,name, content, isGrey})=>(
    <View style={{flexDirection:'row',alignItems:'center', marginTop:5}}>
        <Ionicons name={icon} size={16} color={'red'} style={{width:20}}/>
        <Text style={{fontSize:13, color:'grey'}}>{name+' '}</Text>
        <Text style={{fontSize:12, color:isGrey?'black':'black', fontWeight:'bold'}}>{content}</Text>
    </View>
  )

  class DeliverySecondReport extends PureComponent {
    constructor(props) {
        super(props);

        this.state = {
            projectName:'Project Name',
            DONumber:'RT02342',
            clientName: 'Austin',
            clientAddress: 'No.1, Jalan Bukit Bintang, Kuala Lumpur',
            items:[
                {title:'Cement', quantity:'100kg', delivered:'100kg'},
                {title:'Steel', quantity:'800kg', delivered:'800kg'}
            ],
            isConfirmed: false,
            showSignModal: false,
            signatureUri:null,
            showConfirmModal: false,
            qrScanData: null,
            subRequests:[],
            // startUnloadingTime: this.props.currentDelivery.UnloadingTime?UtilService.getDateTime(this.props.currentDelivery.UnloadingTime):null,
            // doneUnloadingTime: this.props.currentDelivery.UnloadedTime?UtilService.getDateTime(this.props.currentDelivery.UnloadedTime):null,
            isWaiting: false,
            UnloadedTimeStarted: false,
            unloadingStarted: false,
            qacView:null,
            pictures:[
                {image:null, area:'', comment:'',
                    DestinationLatitude:this.props.location.coords.latitude,
                    DestinationLongitude:this.props.location.coords.longitude
                },
            ],
            currentDelivery:null,
            customer:{}
        };
        this.convert=[]
    }

    componentDidMount(){
        if ( !this.props.internetConnection ){
            alert('You can not see report in offline mode!')
            Actions.pop()
            return;
        }
        this.setState({isWaiting: true})
      api.getDelivery(this.props.currentDelivery.DeliveryNoteID, (err,res)=>{
        console.log('arrived customer res=>',res)
        if ( err == null ){
          api.getCustomer(res.CustomerID,(error,res)=>{
                if(error){
                    return
                }
            this.setState({customer:res})
          })
          this.state.subRequests = res.DeliveryNoteItems
          this.setState({subRequests:[...this.state.subRequests]})
          if(this.state.subRequests.length==0 || this.state.subRequests==null){
              this.setState({isWaiting:false})
              return;
          }
          this.state.subRequests.map((request, index)=>{
              api.getCheckResultsByDnItem(request.DeliveryNoteItemID, (error,res)=>{
                  this.setState({isWaiting:false})
                  if(error==null){
                      request.checkResults = res
                  }
              })
          })
          this.setState({currentDelivery:res})
          this.setState({pictures:res.DeliveryReportPhotos, signatureUri:res.Signature})
          if(!!res.Signature){
              this.setState({isConfirmed:true})
          }
        }else{
            this.setState({isWaiting:false})
        }
      })
    }

    updateQacItems(){
        api.getQacItems(this.props.currentDelivery.DeliveryNoteID, (err, res)=>{
            if ( err == null ){
                this.setState({qacView:res})
            }
        })
    }

    submit(){
        let {currentDelivery, actions} = this.props
        if ( currentDelivery == null ){
            alert('Failed!')
            return;
        }
        api.createAlert({
            Type: commonStyles.SignOff,
            Level: CONST.EVENT_LEVEL,
            Message: '',
            ReferenceID: currentDelivery.DeliveryNoteID,
            UserID: currentDelivery.User.UserID
        }, (err1, res1)=>{
            actions.changeQueueState(false)
            if ( Cache.hasInternetConnection ){
                actions.updateCurrentDelivery(currentDelivery.DeliveryNoteID)
            }else{
                currentDelivery.IsSignOff = true
                actions.manualUpdate(currentDelivery)
            }
            if(Cache.hasInternetConnection){
                var tempAfterPhoto = ''
                this.state.pictures.map((item)=>{
                    if(!!item.image){
                        tempAfterPhoto = item.image.uri
                    }
                    api.createPhoto(currentDelivery.DeliveryNoteID, {
                        DeliveryNoteID: currentDelivery.DeliveryNoteID,
                        Area: item.area,
                        Remark: item.comment,
                        AfterPhoto: tempAfterPhoto,
                        DestinationLatitude:item.DestinationLatitude,
                        DestinationLongitude:item.DestinationLongitude
                    }, (err, res)=>{
                
                    })
                })
            }
            else{
                this.state.pictures.map((item)=>{
                    api.createPhoto(currentDelivery.DeliveryNoteID, {
                        DeliveryNoteID: currentDelivery.DeliveryNoteID,
                        Area: item.area,
                        Remark: item.comment,
                        AfterPhoto: item.image
                    }, (err, res)=>{
                
                    })
                })
            }

            
        })

        
        
        this.setState({showConfirmModal:true})
    }

    renderConfirmModal(){
        return (
            <Modal
            visible={this.state.showConfirmModal}
            transparent={true}
            onRequestClose={() => {}}
            >
            <View style={styles.modalContainer}>
                <View style={[styles.modal,{width:300}]}>
                <Text style={{color:'#333', margin:30}}>{Cache.getLang('message.delivery_completed')}</Text>
                <View style={{height:40, flexDirection:'row', width:'100%', borderBottomRightRadius:5, borderBottomLeftRadius:5, overflow:'hidden', borderTopWidth:1, borderTopColor:'#ccc'}}>
                    <TouchableOpacity style={{width:50}} onPress={()=>{
                        this.setState({showConfirmModal:false})
                        Actions.LastScreen({data:this.props.currentDelivery, signature:[]})                        
                    }} style={{flex:1, alignItems:'center', justifyContent:'center', backgroundColor:'rgb(240,250,253)'}}>
                    <Text style={{color:commonColors.theme, fontSize:13, fontWeight:'bold'}}>Done</Text>
                    </TouchableOpacity>
                </View>
                </View>
            </View>
            </Modal>
        );
    }

    onConfirm(){
        if ( this.state.isConfirmed ){
            //   this.setState({showConfirmModal: true})
            this.submit()
        }else{
                Actions.Signature({data: this.props.currentDelivery, done:(qrScanData, signatureUri)=>{
                    this.setState({isConfirmed:true, qrScanData, signatureUri:null})
                    let image = {
                        uri: signatureUri,
                        type: "image/png",
                        name: "file.png"
                    };
                    this.props.actions.setCustomerSignature(signatureUri);
                    api.uploadFile(image, (err, res)=>{
                        if ( err == null ){
                            // this.setState({Signature:res.filePath, signatureUri})
                            this.setState({signatureUri:res.filePath})
                            api.uploadSign(this.props.currentDelivery.DeliveryNoteID, 
                                (Cache.hasInternetConnection?res.filePath: res), (err, res)=>{
                                if ( err ){
                                    Alert.alert('Failed!')
                                }
                            })
                        }else{
                            Alert.alert('Failed.')
                        }
                    })
                }})
        }
    }

    async takePicture(index){
        let res = await Permissions.askAsync(Permissions.CAMERA)
        if ( res.status ==='granted'){
            let {status} = await Permissions.askAsync(Permissions.CAMERA_ROLL)
            if ( status === 'granted' ){
                let image = await ImagePicker.launchCameraAsync({
                    quality:0.3
                })
                if ( !image.cancelled ){
                    this.setState({isWaiting:true})
                    const manipResult = await ImageManipulator.manipulateAsync(
                        image.uri,
                        [{resize:{width:768}}],
                        { format: 'jpeg', compress:0.6 }
                    );
                    api.uploadImage(manipResult.uri, (err, res)=>{
                        if ( err == null ){
                            if (Cache.hasInternetConnection){
                                this.state.pictures[index].image={uri:res.filePath}
                                this.state.pictures[index].DestinationLatitude= this.props.location.coords.latitude
                                this.state.pictures[index].DestinationLongitude= this.props.location.coords.longitude
                            }else{
                                this.state.pictures[index].image=res
                                this.state.pictures[index].DestinationLatitude= this.props.location.coords.latitude
                                this.state.pictures[index].DestinationLongitude= this.props.location.coords.longitude
                                this.convert[res] = {uri:manipResult.uri}
                            }
                            
                            this.setState({pictures:[...this.state.pictures]}) 
                        }     
                        this.setState({isWaiting:false})   
                    })

                    
                }
                
            }
        }
    }


    addPicture(){
        this.state.pictures.push({image:null, area:'', comment:'',   
                DestinationLatitude:this.props.location.coords.latitude,
                DestinationLongitude:this.props.location.coords.longitude
            })
        this.setState({pictures:[...this.state.pictures]})
    }



    startUnloading(){
        let {currentDelivery} = this.props
        if ( !!currentDelivery&&currentDelivery.startUnloadingTime || this.state.unloadingStarted ) return
        this.setState({unloadingStarted:true}, ()=>{
            api.createAlert({
                Type: commonStyles.Unloading,
                Level: 3,
                Message: 'started unloading...',
                ReferenceID: this.props.currentDelivery.DeliveryNoteID,
                UserID: this.props.currentDelivery.User.UserID
            }, (err1, res1)=>{
                if ( Cache.hasInternetConnection ){
                    this.props.actions.updateCurrentDelivery(this.props.currentDelivery.DeliveryNoteID)
                }else{
                    this.props.currentDelivery.UnloadingTime= new Date()
                    this.props.actions.manualUpdate(this.props.currentDelivery)
                }
                this.setState({unloadingStarted:false})
            })
        })
    }
  
    doneUnloading(){
        if ( this.props.currentDelivery.UnloadingTime == null || this.props.currentDelivery.UnloadedTime ) return;
        this.setState({UnloadedTimeStarted:true})
        api.createAlert({
            Type: commonStyles.Unloaded,
            Level: 3,
            Message: 'done to unload...',
            ReferenceID: this.props.currentDelivery.DeliveryNoteID,
            UserID: this.props.currentDelivery.User.UserID
        }, (err1, res1)=>{
            if ( Cache.hasInternetConnection ){
                this.props.actions.updateCurrentDelivery(this.props.currentDelivery.DeliveryNoteID)
            }else{
                this.props.currentDelivery.UnloadedTime = new Date()
                this.props.actions.manualUpdate(this.props.currentDelivery)
            }
            this.setState({UnloadedTimeStarted:false})
        })
    }

    clickChecklist(item, index){
        if ( !this.state.isConfirmed ) {
            Actions.CheckList({item, index, update:()=>{
                if(!!this.props.currentDelivery){
                    this.setState({subRequests:this.props.currentDelivery.DeliveryNoteItems})
                }
                else{
                    Alert.alert('Current delivery is null')
                }                
            }})
        }
    }

    showAll(){
        if ( this.state.showIndex == -2 ){
            this.setState({showIndex:-1})
        }else{
            this.setState({showIndex:-2})
        }
    }

    showIndex(index){
        if ( this.state.showIndex == index ){
            this.setState({showIndex:-1})
        }else{
            this.setState({showIndex:index})
        }
    }

    addItem(){
        Actions.AddItem({update:()=>{
            this.updateQacItems()
        }})
    }


    renderDetail(index){
        let {checkResults} = this.state.subRequests[index]
        if ( !checkResults ) return null
          return(
              <View>
                  {checkResults.map((result, index)=>{
                      return(
                          <View key={index} style={{paddingHorizontal:20, marginTop:10}}>
                            <Text style={{color:'#ccc', fontSize:13}}>{result.CheckListItem.Name}</Text>
                            <Text style={{color:'grey', fontSize:14, marginTop:5}}>{result.Value}</Text>
                            <ScrollView horizontal={true}>
                                {result.Images!=null&&result.Images!=''&&result.Images.split(',').map((item, idx)=>{
                                    return(
                                        <TouchableOpacity key={idx} onPress={()=>Actions.ShowImage({uri:item})}>
                                            <Image source={{uri:item}} style={{width:60, height:60, marginTop:10, marginRight:10}}/>
                                        </TouchableOpacity>
                                    )
                                })}
                                
                            </ScrollView>
                            <View style={{width:'100%', height:1, backgroundColor:'#ccc', marginTop:10}}/>
                          </View>
                      )
                  })}
              </View>
          )
      }
    

  renderQacDetail(item){

    return(
        <View>
            {item.CheckResults.map((result, index)=>{
                result.ImageArray = result.Images.split(',')
                return(
                    <View key={index} style={{paddingHorizontal:20}}>
                      <Text style={{color:'#ccc', fontSize:13, marginTop:10}}>{result.CheckItemName}</Text>
                      <Text style={{color:'grey', fontSize:14, marginTop:5}}>{result.Value}</Text>
                      
                      <View style={{width:'100%', height:1, backgroundColor:'#ccc', marginTop:10}}/>
                    </View>
                )
            })}
        </View>
    )
  }

  editQacItem(qacItem){
      Actions.AddItem({item:qacItem, update:()=>{
          this.updateQacItems()
      }})
  }
  renderRemark(){
    return(
        <View>
        <View style={{width:'100%', backgroundColor:'rgb(228,228,233)', paddingVertical:8, paddingHorizontal:15, elevation:4, marginTop:10}}>
                <Text style={{color:'grey', fontSize:13}}>Remarks</Text>
            </View>
            <View style={{backgroundColor:'white', elevation:4}}>
                {this.state.pictures.map((item, index)=>{
                    return(
                        <View key={index} style={{flexDirection:'row', paddingHorizontal:20, paddingVertical:10}}>
                            {item.AfterPhoto==null&&<View style={{borderColor:'rgb(0,185,237)', borderWidth:2, alignItems:'center', justifyContent:'center', width:60, height:60}}>
                                {/* <Ionicons name={'ios-camera'} color={commonColors.theme} size={30}/> */}
                                <Text style={{fontSize:8,color:'#333'}}>No Photo</Text>
                            </View>}
                            {item.AfterPhoto!=null&&<TouchableOpacity onPress={()=>Actions.ShowImage({uri:config.SERVICE_FILE_URL+item.AfterPhoto})} style={{width:60, height:60}}>
                                <Image source={{uri:config.SERVICE_FILE_URL+item.AfterPhoto}} style={{flex:1, resizeMode:'cover'}}/>
                            </TouchableOpacity>}
                            {/* <Text style={{flex:1}}>{'afterphoto-url:'+config.SERVICE_FILE_URL+'/'+item.AfterPhoto}</Text> */}
                            <View style={{marginLeft:15, flex:1}}>
                                
                                <View style={{marginTop:10, paddingRight:20}}>
                                    <View style={{flexDirection:'row', alignItems:'flex-end'}}>
                                        <Text style={{fontSize:13, color:'grey'}}>Area</Text>
                                        <Text style={{fontSize:15, color:'black', marginLeft:10}}>{item.Area}</Text>
                                        <View style={{flex:1}}></View>
                                        {
                                            !!item.DestinationLatitude && !!item.DestinationLongitude &&
                                            <TouchableOpacity onPress={()=> Actions.PlacePicker({destination:{latitude:item.DestinationLatitude||0, longitude:item.DestinationLongitude||0}})} 
                                                style={{width:40,justifyContent:'center',alignItems:'flex-end'}}>
                                                <Ionicons name={'md-pin'} size={30} color={'rgb(0,185,237)'}/>
                                            </TouchableOpacity>
                                        }
                                        
                                    </View>
                                    <Text style={{fontSize:15, color:'black', marginTop:10}}>{item.Remark}</Text>
                                </View>
                            </View>
                        </View>
                    )
                })}
            </View>
        </View>
    )
    
  }
  render() {
      //if ( this.props.currentDelivery == null ) return null;
      //let {DeliveryNoteNumber, IssueTime} = this.props.currentDelivery
      let {internetConnection} = this.props
      let {currentDelivery} = this.state
      if(!!currentDelivery){
        var issued2SignedOffTimeSpan = UtilService.getTimeSpanBySec(currentDelivery.IssueTime,currentDelivery.SignOffTime)
      }
    if(!!!currentDelivery){
        return(
            <View style={{flex:1,alignItems:'center',justifyContent:'center'}}>
                <Text style={{color:'blue',fontWeight:'bold',fontSize:20}}>{this.state.statusText}</Text>
                <ActivityIndicator size={150}/>
            </View>
        )
    }
    return (
      <View style={styles.container}>
        <InformationBar iConnection={internetConnection} currentDelivery={currentDelivery}/>   
        <HeaderBar title={!!currentDelivery&&currentDelivery.DeliveryNoteNumber} 
            back={()=>Actions.pop()} 
            rightElement={(
                <View style={{flexDirection:'row', alignItems:'center'}}>
                    <TouchableOpacity  onPress={()=>Actions.AlertListByDn({dnID:currentDelivery.DeliveryNoteID})} style={{padding:10}}>
                        <Ionicons name={'md-warning'} size={24} color={'white'}/>
                    </TouchableOpacity>
                    <TouchableOpacity style={{marginLeft:15, padding:10}} onPress={()=>Actions.Chat({channel:currentDelivery})}>
                        <Ionicons name={'md-chatboxes'} size={24} color={'white'}/>
                    </TouchableOpacity>
                </View>
            )}
        />
        <ScrollView style={{flex:1}}>
            <View>
                <View style={{flexDirection:'row', alignItems:'center', height:40, width:'100%', paddingHorizontal:20, backgroundColor:'rgb(228,228,233)', elevation:4}}>
                    <Text style={{color:'grey', flex:1}}>{'Bill To'}</Text>
                </View>
                <View style={{paddingHorizontal:20,paddingVertical:10}}>
                    <Text style={{fontSize:18,fontWeight:'bold',marginBottom:10}}>{this.state.customer.Name}</Text>
                    <Text style={{flex:1}}>{this.state.customer.Address}</Text>
                </View>
            </View> 
            <View>
                <View style={{flexDirection:'row', alignItems:'center', height:40, width:'100%', paddingHorizontal:20, backgroundColor:'rgb(228,228,233)', elevation:4}}>
                    <Text style={{color:'grey', flex:1}}>{'Deliver To'}</Text>
                </View>
                <View style={{paddingHorizontal:20,paddingVertical:10}}>
                    <Text style={{fontSize:18,fontWeight:'bold',marginBottom:10}}>{this.state.customer.Name}</Text>
                    <Text style={{flex:1}}>{this.state.customer.Address}</Text>
                </View>
            </View> 
            <View>
                <View style={{flexDirection:'row', alignItems:'center', height:40, width:'100%', paddingHorizontal:20, backgroundColor:'rgb(228,228,233)', elevation:4}}>
                    <Text style={{color:'grey', flex:1}}>{'Details'}</Text>
                </View>
                <View style={{padding:20}}>
                <View style={{flexDirection:'row'}}>
                    <View style={{width:'50%',color:'grey'}}><Text style={{color:'grey'}}>Purchase Order No.</Text></View>
                    <View style={{width:'50%',color:'grey'}}><Text style={{color:'grey'}}>Seal No.</Text></View>
                </View>
                <View style={{flexDirection:'row',marginBottom:10}}>
                    <View style={{width:'50%',color:'black'}}><Text>{}</Text></View>
                    <View style={{width:'50%',color:'black'}}><Text>{}</Text></View>
                </View>
                <View style={{flexDirection:'row'}}>
                    <View style={{width:'50%',color:'grey'}}><Text style={{color:'grey'}}>Vehicle No.</Text></View>
                    <View style={{width:'50%',color:'grey'}}><Text style={{color:'grey'}}>Payment Term</Text></View>
                </View>
                <View style={{flexDirection:'row',marginBottom:10}}>
                    <View style={{width:'50%',color:'black'}}><Text>{currentDelivery.Vehicle.LicensePlate}</Text></View>
                    <View style={{width:'50%',color:'black'}}><Text>{}</Text></View>
                </View>
                <View style={{flexDirection:'row'}}>
                    <View style={{width:'50%',color:'grey'}}><Text style={{color:'grey'}}>Delivery Terms</Text></View>
                    <View style={{width:'50%',color:'grey'}}><Text style={{color:'grey'}}>Delivery Order No.</Text></View>
                </View>
                <View style={{flexDirection:'row',marginBottom:10}}>
                    <View style={{width:'50%',color:'black'}}><Text>Delivered</Text></View>
                    <View style={{width:'50%',color:'black'}}><Text>{this.state.currentDelivery.DeliveryNoteNumber}</Text></View>
                </View>
            </View> 
            </View> 
           
            <View>
                <View style={{flexDirection:'row', alignItems:'center', height:40, width:'100%', paddingHorizontal:20, backgroundColor:'rgb(228,228,233)', elevation:4}}>
                    <Text style={{color:'grey', flex:1}}>{'Delivery Items'}</Text>
                </View>
                <View style={{padding:20}}>
                {
                   this.state.subRequests.map((element,index)=>{
                    return(
                        <View key={'subRequest=='+index}>
                            <Text style={{fontWeight:'bold',fontSize:18,marginBottom:8}}>{element.Item.Name}</Text>
                            <View style={{flexDirection:'row'}}>
                                <View style={{width:'50%',color:'grey'}}><Text style={{color:'grey'}}>Packing Type</Text></View>
                                <View style={{width:'50%',color:'grey'}}><Text style={{color:'grey'}}>Quantity</Text></View>
                            </View>
                            <View style={{flexDirection:'row',marginBottom:10}}>
                                <View style={{width:'50%',color:'black'}}><Text>{}</Text></View>
                                <View style={{width:'50%',color:'black'}}><Text>{element.PlannedQuantity-element.ReturnedQuantity}</Text></View>
                            </View>
                            <View style={{flexDirection:'row'}}>
                                <View style={{width:'50%',color:'grey'}}><Text style={{color:'grey'}}>{'Gross Weight'}</Text></View>
                                <View style={{width:'50%',color:'grey'}}><Text style={{color:'grey'}}>{'Tare Weigth'}</Text></View>
                            </View>
                            <View style={{flexDirection:'row',marginBottom:10}}>
                                <View style={{width:'50%',color:'black'}}><Text>{}</Text></View>
                                <View style={{width:'50%',color:'black'}}><Text>{}</Text></View>
                            </View>
                            <View style={{flexDirection:'row'}}>
                                <View style={{width:'50%',color:'grey'}}><Text style={{color:'grey'}}>Time Out</Text></View>
                                <View style={{width:'50%',color:'grey'}}><Text style={{color:'grey'}}>Delivered Qty</Text></View>
                            </View>
                            <View style={{flexDirection:'row',marginBottom:10}}>
                                <View style={{width:'50%',color:'black'}}><Text>{}</Text></View>
                                <View style={{width:'50%',color:'black'}}><Text>{element.DeliveredQuantity}</Text></View>
                            </View>
                            {   
                                !!element.Item.ShelfLifeTime && element.Item.ShelfLifeTime>0 &&
                                <Warning 
                                    icon={"md-warning"}
                                    name = {'Shelf life exceeded by'}
                                    content={(issued2SignedOffTimeSpan/60).toFixed(0) + "min(s)"}
                                    isGrey
                                />
                            }
                        </View>
                        )
                   })
                   
                }
                
            </View> 
            {this.renderRemark()}
            </View>
            <Text style={{color:'grey',fontWeight:'bold', fontSize:15, marginLeft:15, marginTop:20}}>
                {Cache.getLang('label.report_agree')}
            </Text>
            <View style={{height:30}}/>
            {this.state.isConfirmed&&this.state.signatureUri&&<Image source={{uri:config.SERVICE_FILE_URL+this.state.signatureUri}} style={{width:'100%', height:250, resizeMode:'contain'}}/>}
            {this.state.isConfirmed&&this.state.qrScanData&&<View style={{flexDirection:'row', justifyContent:'center'}}>
                <Text style={{color:'#ccc', fontSize:13}}>Acceptor:</Text>
                <Image source={{uri:this.state.acceptorUri}} style={{width:30, height:30, borderRadius:15, margin:8}}/>
                <Text style={{color:'#333', fontWeight:'bold', fontSize:15}}>{this.state.acceptorName}</Text>
            </View>}
            <View style={{height:20}}/>
            <View style={{width:100}}><Text>{' '}</Text></View> 
            
        </ScrollView>
        <WaitingModal isWaiting={this.state.isWaiting}/>
      </View>
    );
  }
}

export default connect(
  state => ({
    //currentDelivery: state.common.currentDelivery,
    internetConnection: state.common.internetConnection,
    location:state.common.location
  }),
  dispatch => ({
    actions: bindActionCreators(actions, dispatch)
  })
)(DeliverySecondReport);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop:24,
    backgroundColor: 'white'
  },
  input: {
    fontSize: 14,
    color: commonColors.title,
    height: 36,
    borderRadius: 3,
    marginBottom: 3,
    paddingHorizontal: 10,
    backgroundColor: commonColors.inputColor
  },
  modalContainer: {
    flex: 1,
    backgroundColor: "rgba(0, 0, 0,0.5)",
    alignItems: "center",
    justifyContent: "center"
  },
  modal: {
    width:360,
    borderRadius: 5,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "white",
    elevation:3,
  },
});
