"use strict";

import React, { PureComponent } from "react";

import {
  StyleSheet,
  View,
  Platform,
  Modal,
  TouchableOpacity,
  Text,
  StatusBar,
  Image,
  TextInput,
  Alert
} from "react-native";


import * as commonColors from "../styles/colors";
import * as commonStyles from "../styles/styles";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import * as actions from '../store/common/actions'
import * as session from '../store/session/actions'
import api from "../service/api";
import Cache from "../utils/cache";
import { Ionicons, MaterialCommunityIcons } from "@expo/vector-icons";
import { Actions } from "react-native-router-flux";
import HeaderBar from "../components/header";
import UtilService from '../utils/utils'
import InformationBar from '../components/informationBar'
import * as config from '../config'

function isSameDate(A, B) {
  if (
    A.getDate() == B.getDate() &&
    A.getMonth() == B.getMonth() &&
    A.getFullYear() == B.getFullYear()
  )
    return true;
  return false;
}

class ItemReturn extends PureComponent {
  constructor(props) {
    super(props);
    this.state={
      returnedQty:0,
      disabled:false,
      isRefreshing: false,
      isWaiting: false,
      returnReason:'',
      showPromptModal:false,
      showExceedModal:false
    }
    this.reasonCases = [
      "Customer Not Available",
      "Customer Out Of Town",
      "Customer Cancel Delivery",
      "Customer Reject Item",
      "Vehicle Broke Down",
      "Wrong Item"
    ]
  }

  componentDidMount(){
   
    if(!!this.props.item)
    {
      //console.log('..............renderisbeing.................')
      this.setState({returnedQty:this.props.item.ReturnedQuantity})
    }    
  }

  componentWillReceiveProps(nextProps){
    
  }
  done(index){
    
    if(this.state.returnedQty>this.props.item.PlannedQuantity){
      this.setState({showExceedModal:true})
      return
    }
    this.setState({showPromptModal:true})
    this.setState({returnReason:this.reasonCases[index]})
    
   
    
  }

  renderIndicator() {
    if ( Actions.currentScene != 'Main') return null
    return (
      <Modal
        visible={this.state.isWaiting}
        transparent={true}
        onRequestClose={() => {}}
      >
        <View style={styles.indicatorContainer}>
          <View style={styles.indicator}>
            <ActivityIndicator size="large" />
          </View>
        </View>
      </Modal>
    );
  }
  render() {
    let {returnedQty} = this.state
    let {item} = this.props
    let {Item} = item.Item
    return (
      <View style={styles.container}>
        <HeaderBar title={"Item Return ("+ ((!!item && !!item.Item)?item.Item.Name:'')+")"}/>
        {/* <Text>{'returned Qty='+returnedQty}</Text> */}
        <View style={{flexDirection:'row',alignItems:'center',padding:20,width:'60%'}}>
              {/* <View style={{flex:1}}> */}
                <Text style={{fontSize:16, color:'grey', width:60}}>{'Return'}</Text>
                <TextInput
                    ref="DONumber"
                    autoCapitalize="none"
                    autoCorrect={false}
                    placeholder=""
                    placeholderTextColor={commonColors.placeholderText}
                    textAlign="left"
                    style={[styles.input,{marginLeft:15, flex:1}]}
                    underlineColorAndroid="transparent"
                    returnKeyType={"next"}
                    value={returnedQty}
                    keyboardType = "decimal-pad"
                    onChangeText={text =>{
                          // this.setState({ unit: text.replace(/\t/g, "") })
                          var temp = Number(text.replace(/\t/g, ""))
                          //console.log("returnedQty is on change...",temp)
                          //this.state.returnedQty = temp
                          if(temp>item.PlannedQuantity){
                            this.setState({showExceedModal:true})
                          }
                          this.setState({returnedQty:temp})
                      }                  
                    }
                />
                 <Text>{' < '+ item.PlannedQuantity} {item.Item.Uom.Name}</Text>
        </View>
        <View style={{width:'100%',height:40,justifyContent:'center',alignItems:'flex-start'}}>
              <Text style={{marginLeft:20,fontSize:12,color:'grey'}}>
                  Return Reason
              </Text>
        </View>    
        {
          this.reasonCases.map((item,index)=>{
              return(
                <View key={index}>
                  <TouchableOpacity style={{backgroundColor:'white'}} onPress={()=>{this.done(index)}}>
                    <View style={{width:'100%',height:70,justifyContent:'center',alignItems:'flex-start'}}>
                        <Text style={styles.reasonTextStyle}>
                        {this.reasonCases[index]}
                        </Text>
                    </View>    
                    <View style={{height:1,width:'100%',backgroundColor:'rgb(30,30,30)'}}></View>
                  </TouchableOpacity>
                </View>
              )
          })
        }
        {this.renderIndicator()}
        {this.renderModal2()}
        {this.renderWarningModal()}
      </View>
    );
  }
  confirmReturn(){
    
    this.setState({showPromptModal:false})
    let {item} = this.props
    let {returnedQty} = this.state
    if(returnedQty>item.PlannedQuantity){
      Alert.alert("Return Qty can't be more than planned qty")
    }
    item.ReturnReason = this.state.returnReason
    item.ReturnedQuantity = returnedQty
    //console.log('this.state.returnedQty->item', item.ReturnedQuantity)
    this.setState({isWaiting:true})
    api.updateDeliveryNoteItem(item.DeliveryNoteItemID,item,(error,res)=>{
      if(error){
        this.setState({isWaiting:false})
      }
       this.setState({isWaiting:false})
       var str = item.ReturnedQuantity + ' is returned'
       //Alert.alert(str)
       this.props.update(returnedQty)
       Actions.pop()
    })
  }
  renderModal2() {
    return (
      <Modal
        visible={this.state.showPromptModal}
        transparent={true}
        onRequestClose={() => {}}
      >
        <View style={styles.modalContainer}>
          <View style={styles.modal}>
            <View style={{flex:1, padding:15, width:'100%', justifyContent:'center'}}>
              <Text style={{fontSize:13, fontWeight:'bold', color:'grey',margin:10, textAlign:'center'}}>{"You are returning item\nReason:"+"\""+this.state.returnReason+"\""}</Text>
            </View>
            <View style={{height:36, flexDirection:'row', width:'100%', borderBottomRightRadius:5, borderBottomLeftRadius:5, overflow:'hidden', borderTopWidth:1, borderTopColor:'#ccc'}}>
              <TouchableOpacity onPress={()=>this.setState({showPromptModal:false})} style={{flex:1, alignItems:'center', justifyContent:'center', backgroundColor:'rgb(249,249,249)'}}>
                <Text style={{color:commonColors.theme, fontSize:13}}>Cancel</Text>
              </TouchableOpacity>
              <TouchableOpacity onPress={()=>{this.confirmReturn()}} style={{flex:1, alignItems:'center', justifyContent:'center', backgroundColor:commonColors.button}}>
                <Text style={{color:'white', fontSize:13, fontWeight:'bold'}}>Confirm</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </Modal>
    );
  }

  renderWarningModal() {
    return (
      <Modal
        visible={this.state.showExceedModal}
        transparent={true}
        onRequestClose={() => {}}
      >
        <View style={styles.modalContainer}>
          <View style={styles.modal}>
            <View style={{flex:1, justifyContent:'center', alignItems:'center'}}>
              <Text style={{fontSize:14, color:'grey',margin:10, textAlign:'center'}}>{'Return qty is exceeded'}</Text>
            </View>
            <View style={{height:40, flexDirection:'row', width:'100%', borderBottomRightRadius:5, borderBottomLeftRadius:5, overflow:'hidden', borderTopWidth:1, borderTopColor:'#ccc'}}>
              <TouchableOpacity onPress={()=>this.setState({showExceedModal:false})} style={{flex:1, alignItems:'center', justifyContent:'center', backgroundColor:'rgb(249,249,249)'}}>
                <Text style={{color:commonColors.theme, fontSize:13}}>Cancel</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </Modal>
    );
  }
}

export default connect(
  state => ({
    actionType: state.common.type,
    status: state.common.status,
    currentDelivery: state.common.currentDelivery,
    completedDeliveries:state.common.completedDeliveries,
    internetConnection: state.common.internetConnection
  }),
  dispatch => ({
    actions: bindActionCreators({...actions,...session}, dispatch)
  })
)(ItemReturn);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop:24, 
    backgroundColor:'white'
  },
  indicatorContainer: {
    flex: 1,
    backgroundColor: "rgba(0, 0, 0,0.5)",
    alignItems: "center",
    justifyContent: "center"
  },
  cardContainer: {
    elevation: 3,
    borderRadius: 8,
    backgroundColor: "white",
    // padding: 10,
    borderWidth: 3,
    borderColor: commonColors.blue
  },
  date: {
    fontSize: 13,
    color: "white",
    marginVertical: 5,
    fontWeight: "bold"
  },
  username: {
    fontSize: 18,
    fontWeight: "bold",
    color: "white",
    marginLeft:15,
    flex:1,
  },
  reasonTextStyle:{
    color:'#777',
    fontSize:20, 
    fontWeight:'bold', 
    marginLeft:20
  },
  input: {
    width:50,
    fontSize: 14,
    color: commonColors.title,
    height: 40,
    borderRadius: 3,
    marginBottom: 3,
    marginRight:10,
    paddingHorizontal: 10,
    backgroundColor: commonColors.inputColor
  },
  modalContainer: {
    flex: 1,
    backgroundColor: "rgba(0, 0, 0,0.5)",
    alignItems: "center",
    justifyContent: "center"
  },
  modal: {
    width: 260,
    height: 160,
    borderRadius: 5,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "white",
    elevation:3,
  },
});
