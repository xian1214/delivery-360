"use strict";

import React, { PureComponent } from "react";

import {
  StyleSheet,
  View,
  Platform,
  Modal,
  TouchableOpacity,
  Text,
  StatusBar,
  TextInput,
  Image,
  Alert,
} from "react-native";

import { Ionicons, FontAwesome } from "@expo/vector-icons";
import HeaderBar from '../components/header'
import Cache from '../utils/cache'
import { Actions } from "react-native-router-flux";
import * as actionTypes from "../store/common/actionTypes";

import { bindActionCreators } from "redux";
import { connect } from "react-redux";
// import * as actions from "../redux/actions";
import * as actions from '../store/common/actions'
import * as session from '../store/session/actions'

class Offline extends PureComponent {
  
  report(){
    if ( this.props.currentDelivery == null || !this.props.currentDelivery.OTWStatus ){
      Alert.alert('There is no delivery on the way!')
      return;
    }
    Actions.DeliveryReport({data:this.props.currentDelivery})
  }
  render() {
    return (
      <View style={styles.container}>
        <HeaderBar title={'Delivery'} NoBackButton={true}
          rightElement={(
            <TouchableOpacity onPress={()=>this.report()}>
              <Text style={{color:'white', fontSize:12}}>REPORT</Text>
            </TouchableOpacity>
          )}
        />
        <View style={{flex:1, alignItems:'center', justifyContent:'center'}}>
            <View>
                <FontAwesome name={'wifi'} color='rgb(185, 185,200)' size={100}/>
                <View style={{position:'absolute', right:0, bottom:20, height:40, width:40, borderRadius:20, borderWidth:5, borderColor:'white', backgroundColor:'rgb(185,185,200)', justifyContent:'center', alignItems:'center'}}>
                    <Ionicons name={'md-help'} color={'white'} size={24}/>
                </View>
            </View>
            <Text style={{fontSize:20, color:'rgb(132,132,150)', fontWeight:'bold', marginTop:20}}>
                No Internet Connection
            </Text>
            <Text style={{fontSize:13, color:'rgb(186, 185,200)', marginTop:20}}>Please check your internet settings.</Text>
        </View>
      </View>
    );
  }
}

export default connect(
  state => ({
    status: state.common.status,
    actionType: state.common.type,
    currentDelivery: state.common.currentDelivery
  }),
  dispatch => ({
    actions: bindActionCreators({...actions,...session}, dispatch)
  })
)(Offline);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop:24,
    backgroundColor: 'rgb(250,250,250)'
  },
  
});
