"use strict";

import React, { PureComponent } from "react";

import {
  StyleSheet,
  View,
  Platform,
  Modal,
  TouchableOpacity,
  Text,
  StatusBar,
  Image,
  ScrollView,
  FlatList,
  RefreshControl,
  Alert,
} from "react-native";


import * as commonColors from "../styles/colors";
import * as commonStyles from "../styles/styles";
import { screenWidth, screenHeight } from "../styles/styles";

import * as actionTypes from "../store/common/actionTypes";

import { bindActionCreators } from "redux";
import { connect } from "react-redux";
// import * as actions from "../redux/actions";
import * as actions from '../store/common/actions'
import * as session from '../store/session/actions'

import api from "../service/api";
import Cache from "../utils/cache";
import { Ionicons, MaterialCommunityIcons } from "@expo/vector-icons";
import { Actions } from "react-native-router-flux";
import HeaderBar from "../components/header";
import UtilService from '../utils/utils'
import InformationBar from '../components/informationBar'
var gHandler = null
function isSameDate(A, B) {
  if (
    A.getDate() == B.getDate() &&
    A.getMonth() == B.getMonth() &&
    A.getFullYear() == B.getFullYear()
  )
    return true;
  return false;
}

const Item=({icon, name, content, isGrey})=>(
    <View style={{flexDirection:'row', alignItems:'center', marginTop:5}}>
        <Ionicons name={icon} size={16} color={'grey'} style={{width:20}}/>
        <Text style={{fontSize:13, color:'grey', marginLeft:8, width:50}}>{name}</Text>
        <Text style={{fontSize:15, color:isGrey?'grey':'#333', fontWeight:'bold', flex:1}}>{content}</Text>
    </View>
)

const Card = ({ card, isToday, onPress, type }) => (
  <TouchableOpacity
    onPress={() => onPress()}
    style={[
      styles.cardContainer,
      { borderColor: commonColors.green }
    ]}
  >
    {type>0&&<View style={{position:'absolute', top:0, left:0, backgroundColor:commonColors.green, width:'100%', height:40, justifyContent:'center', paddingHorizontal:15}}>
      <Text style={{color:'white'}}>{type==1?'Signed Off...':'Returning...'}</Text>
    </View>}
    <View style={{padding:15, marginTop:type>0?40:0}}>
      {isToday&&card.Project&&<Text style={{fontWeight:'bold', color:'#333'}}>{card.Project.Name}</Text>}
      <Text style={{color:'#777', marginTop:5}}># {card.DeliveryNoteNumber}</Text>
      {isToday&&<Item icon={'md-calendar'} name={'Date'} content={UtilService.getDateTime(card.CompleteTime)}/>}
      {isToday&&<Item icon={'md-radio-button-off'} name={'From'} content={card.Warehouse.Address} isGrey/>}
      {isToday&&<Item icon={'md-pin'} name={'To'} content={card.DestinationAddress} isGrey/>}
      {/* {isToday&&<Text style={{marginTop:10, color:'grey'}}>Approx. {card.dist}km</Text>} */}
    </View>
  </TouchableOpacity>
);

const CardList = ({ cards, onPress, currentID, currentDelivery, isRefreshing, onRefresh }) => (
  <View style={{ flex: 1 }}>
    <FlatList
      refreshControl={
        <RefreshControl
            refreshing={isRefreshing}
            onRefresh={() => onRefresh()}
            tintColor={'grey'}
        />
      }
      initialNumToRender={10}
      onEndReachedThreshold={0.1}
      onEndReached={gHandler.onEndReached.bind(gHandler)}
      data={cards}
      keyExtractor={item=>''+item.DeliveryNoteID}
      renderItem={({item, index})=>{
        let card = item
        let isShowDate = true;
        if (index > 0) {
          let A = new Date(card.CompleteTime), B = new Date(cards[index - 1].CompleteTime);
          if ( isSameDate(A, B) ) isShowDate=false
        }
        return (
          <View style={{ paddingHorizontal: 20, paddingVertical: 5 }} >
            {(isShowDate && (new Date(card.CompleteTime)).getFullYear()>2000) && <Text style={styles.date}>{UtilService.getDateTime(card.CompleteTime)}</Text>}
            <Card 
              card={card} 
              isToday={isSameDate(new Date(card.CompleteTime), new Date())} 
              onPress={() => onPress(card)} 
              type={currentID!=card.DeliveryNoteID?0:(currentDelivery.Status==commonStyles.DELIVERY_STATUS_RETURNING && currentDelivery.ReturnWarehouse!=null?2:1)}
            />
            
          </View>
        );
      }}
    />
  </View>
);
class Completed extends PureComponent {
  constructor(props) {
    super(props);
    this.state={
      isRefreshing: false,
    }
    gHandler = this
    this.readCnt = 10
  }

    componentDidMount(){
      this.props.actions.getCompletedAllDeliveries(10)
      //setTimeout(()=>{//console.log('this.props.actions:::ioooooooooooooooooooooooooo',this.props.completedDeliveries)},1000)

      
    }
    onEndReached(){
     //Alert.alert('End reached')
     this.readCnt+=10
     this.props.actions.getCompletedAllDeliveries(this.readCnt)
    }
    onRefresh(){
      if ( this.props.status != actionTypes.LOADING){
        this.props.actions.getCompletedAllDeliveries(10)
      }
    }
  
    componentWillReceiveProps(nextProps){
      if ( nextProps.actionType == actionTypes.GET_COMPLETED_DELIVERIES_REFRESH ){
        
        if (nextProps.status==actionTypes.LOADING){
          this.setState({isRefreshing:true})
        }
        if (nextProps.status==actionTypes.FAILED){
          this.setState({isRefreshing:false})
          Alert.alert('Failed to fetch deliveries')
        }
        if (nextProps.status==actionTypes.SUCCESS){
          this.setState({isRefreshing:false})
          if ( this.props.currentDelivery ){
            this.props.actions.updateCurrentDelivery()
          }
  
        }
      }
    }

  render() {
    let {currentDelivery, internetConnection} = this.props
    return (
      <View style={styles.container}>
        <InformationBar iConnection={internetConnection} currentDelivery={currentDelivery}/>
        <HeaderBar title={"Completed Deliveries"} />
        <CardList
          cards={this.props.completedDeliveries}
          onPress={item => {
            if (this.props.currentDelivery && item.DeliveryNoteID == this.props.currentDelivery.DeliveryNoteID){
              if ( this.props.currentDelivery.Status == commonStyles.DELIVERY_STATUS_RETURNING && this.props.currentDelivery.ReturnWarehouse!=null ) {
                Actions.Navigation()
              }else{
                Actions.LastScreen({data:item})
              }
              return;
            }
            Actions.CompletedDeliveryDetail({ data: item })
          }}
          isRefreshing={this.state.isRefreshing}
          onRefresh={()=>this.onRefresh()}
          currentDelivery = {this.props.currentDelivery}
          currentID = {this.props.currentDelivery?this.props.currentDelivery.DeliveryNoteID:null}
        />
       
      </View>
    );
  }
}

export default connect(
  state => ({
    actionType: state.common.type,
    status: state.common.status,
    currentDelivery: state.common.currentDelivery,
    completedDeliveries:state.common.completedDeliveries,
    internetConnection: state.common.internetConnection
  }),
  dispatch => ({
    actions: bindActionCreators({...actions,...session}, dispatch)
  })
)(Completed);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop:24, 
    backgroundColor: "rgb(48,48,48)"
  },
  cardContainer: {
    elevation: 3,
    borderRadius: 8,
    backgroundColor: "white",
    // padding: 10,
    borderWidth: 3,
    borderColor: commonColors.blue
  },
  date: {
    fontSize: 13,
    color: "white",
    marginVertical: 5,
    fontWeight: "bold"
  }
});
