"use strict";

import React, { PureComponent } from "react";

import {
  StyleSheet,
  View,
  Platform,
  Modal,
  TouchableOpacity,
  Text,
  StatusBar,
  Image,
  TextInput,
  ScrollView,
  Alert,
  ActivityIndicator
} from "react-native";


import * as commonColors from "../styles/colors";
import * as commonStyles from "../styles/styles";
import { screenWidth, screenHeight } from "../styles/styles";

import * as actionTypes from "../store/common/actionTypes";

import { bindActionCreators } from "redux";
import { connect } from "react-redux";
// import * as actions from "../redux/actions";
import * as actions from '../store/common/actions'
import * as session from '../store/session/actions'

import api from "../service/api";
import Cache from "../utils/cache";
import { Ionicons, MaterialCommunityIcons, MaterialIcons } from "@expo/vector-icons";
import { Actions } from "react-native-router-flux";
import HeaderBar from '../components/header'
import async from 'async'
import UtilService from '../utils/utils'
import * as config from '../config'

class AsyncScreen extends PureComponent {
  constructor(props) {
    super(props);
    this.stop = false
    this.state={
        total: 0,
        currentNumber: 0,
        currentAction:''
    }
  }

  async componentDidMount(){
      if ( !Cache.hasInternetConnection ){
          alert('You can not async data on offline mode!')
          Actions.pop()
          return;
      }
    let apiList = await api.getStoredApis();
    if ( apiList.length == 0 ) {
        alert('You have already uploaded data to server!')
        Actions.pop();
        return;
    }
    Alert.alert(
        'Information',
        'Do you want to upload data to server?',
        [
          {text: 'Cancel', onPress: () => Actions.pop(), style: 'cancel'},
          {text: 'OK', onPress: async() => {
              let apiList = await api.getStoredApis();
              //console.log('apiList',apiList)
              this.setState({total:apiList.length})
              async.mapSeries(apiList, (item, cb)=>{
                   
                    if ( this.stop ){
                        cb('stop')
                        return;
                    }
                    //console.log('item', item)
                    this.setState({currentNumber:this.state.currentNumber+1})
                    this.setState({currentAction:item.sub_url})
                    if ( item.sub_url == 'uploadSign' ){
                        api.uploadSign(item.method, apiList[item.json_data].result.filePath, cb)
                        return;
                    }
                    if ( item.sub_url == 'updateQacItem' ){
                        item.json_data[0].CheckResults.map((checkResults, index)=>{
                            let images = checkResults.Images.split(',')
                            let converted = []
                            images.map((image)=>converted.push(config.SERVICE_FILE_URL+apiList[image].result.filePath))
                            checkResults.Images = converted.join()
                        })
                        api.updateQacItem(item.json_data, cb)
                        return;
                    }
                    if ( item.sub_url == 'createPhoto' ){
                        ////console.log('createPhoto', item)
                        if ( typeof (item.json_data.AfterPhoto) ==='number' )
                            item.json_data.AfterPhoto = apiList[item.json_data.AfterPhoto].result.filePath
                        //console.log('createPhoto json---on async', item.json_data)
                        api.createPhoto(item.json_data.DeliveryNoteID, item.json_data, cb)
                        return;
                    }
                    if ( item.sub_url == 'updateCheckResult'){
                        let images = item.json_data.Images.split(',')
                        //console.log('images', images)
                        let converted = []
                        images.map((image)=>{
                            if ( image == '' ) return;
                            if ( image.length < 4 ){
                                converted.push(config.SERVICE_FILE_URL+apiList[image].result.filePath)
                            }else{
                                converted.push(image)
                            }
                        })
                        //console.log('converted', converted)
                        item.json_data.Images = converted.join()
                        //console.log('------->', item.json_data)
                        api.updateCheckResult(item.json_data, (err, res)=>{
                            //console.log(err, res)
                            cb( err, res)
                        })
                        return;
                    }
                    if ( item.sub_url == 'uploadImage' ){
                        api.uploadImage(item.json_data, (err, res)=>{
                            item.result = res;
                            cb(err, res)
                        })
                        return;
                    }
                    if ( item.sub_url == 'uploadFile' ){
                        api.uploadFile(item.json_data, (err, res)=>{
                            item.result = res;
                            cb(err, res)
                        })
                        return;
                    }
                    api.baseApi(item.sub_url, item.method, item.json_data, cb)
              }, (err, results)=>{
                if ( err == null ){
                    api.putApis([])
                    this.props.actions.updateCurrentDelivery()
                }
                Actions.pop()
              })
          }},
        ],
        { cancelable: false }
      )
      
  }

  render() {
    return (
      <View style={styles.container}>
        <ActivityIndicator size="large"/>
        <Text style={{marginTop:10, color:'white'}}>Synchronize data to server...({this.state.currentNumber}/{this.state.total})</Text>
        <Text style={{marginTop:10, color:'white'}}>{this.state.currentAction}</Text>
        <TouchableOpacity style={{borderRadius: 35, width:70, height:70, alignItems:'center', justifyContent:'center', backgroundColor:'red', marginTop:50, elevation:4}}>
            <View style={{height:30, width:30, backgroundColor:'white'}}/>
        </TouchableOpacity>
      </View>
    );
  }
}

export default connect(
  state => ({
    internetConnection: state.common.internetConnection
  }),
  dispatch => ({
    actions: bindActionCreators({...actions,...session}, dispatch)
  })
)(AsyncScreen);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop:24,
    backgroundColor: 'black',
    justifyContent:'center',
    alignItems:'center'
  },
});
