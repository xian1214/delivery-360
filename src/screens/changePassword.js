"use strict";

import React, { PureComponent } from "react";

import {
  StyleSheet,
  View,
  Platform,
  Modal,
  TouchableOpacity,
  Text,
  StatusBar,
  Image,
  TextInput,
  Alert
} from "react-native";


import * as commonColors from "../styles/colors";
import * as commonStyles from "../styles/styles";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import * as actions from '../store/common/actions'
import * as session from '../store/session/actions'
import api from "../service/api";
import Cache from "../utils/cache";
import { Ionicons, MaterialCommunityIcons } from "@expo/vector-icons";
import { Actions } from "react-native-router-flux";
import HeaderBar from "../components/header";
import UtilService from '../utils/utils'
import InformationBar from '../components/informationBar'
import * as config from '../config'

function isSameDate(A, B) {
  if (
    A.getDate() == B.getDate() &&
    A.getMonth() == B.getMonth() &&
    A.getFullYear() == B.getFullYear()
  )
    return true;
  return false;
}

class ChangePassword extends PureComponent {
  constructor(props) {
    super(props);
    this.state={
      isRefreshing: false,
      isWaiting:false,
      username: Cache.currentUser.user.FirstName+' '+Cache.currentUser.user.LastName,
      avatar: {uri:config.SERVICE_FILE_URL+Cache.currentUser.user.Avarta},
      avatarUrl:Cache.currentUser.user.Avatar,
      avatarInstance: {uri:config.SERVICE_FILE_URL+Cache.currentUser.user.Avarta},
    }
  }

  componentDidMount(){
    //console.log('current user on profile ----> >>>',Cache.currentUser.user)
  }

  componentWillReceiveProps(nextProps){
    
  }
  done(){
    let {newPassword,confirmPassword} = this.state
    if(newPassword!=confirmPassword){
      Alert.alert('Please confirm your new password again!')
      return
    }
    var data = {
      OldPassword:this.state.curPassword,
      Password:this.state.newPassword,
      Username:Cache.currentUser.user.Username
    }
    api.updatePassword(data,(error,res)=>{
        this.setState({isWaiting:true})
        if(error){
          Alert.alert('Password change failed')
          this.setState({isWaiting:false})
          return
        }
        this.setState({isWaiting:false})
        Alert.alert('Your password is changed!')
    })
  }

  renderIndicator() {
    if ( Actions.currentScene != 'Main') return null
    return (
      <Modal
        visible={this.state.isWaiting}
        transparent={true}
        onRequestClose={() => {}}
      >
        <View style={styles.indicatorContainer}>
          <View style={styles.indicator}>
            <ActivityIndicator size="large" />
          </View>
        </View>
      </Modal>
    );
  }
  render() {
    let {avatarUrl,avatarInstance} = this.state
    return (
      <View style={styles.container}>
        <HeaderBar title={"Change Password"}  rightElement={(
            <TouchableOpacity disabled={this.state.disabled} onPress={()=>this.done()}>
              <Ionicons name="md-checkmark" size={24} color={'white'}/>
            </TouchableOpacity>
          )}/>
        <View style={{flexDirection:'row',alignItems:'center',padding:20}}>
                <Text style={{fontSize:13, color:'grey', width:100}}>{'Current\nPassword'}</Text>
                <TextInput
                    ref="DONumber"
                    autoCapitalize="none"
                    autoCorrect={false}
                    placeholder=""
                    placeholderTextColor={commonColors.placeholderText}
                    textAlign="left"
                    style={[styles.input,{marginLeft:15, flex:1}]}
                    underlineColorAndroid="transparent"
                    returnKeyType={"next"}
                    secureTextEntry={true}
                    value={this.state.curPassword}
                    onChangeText={text =>{
                        this.setState({curPassword:text})
                    }}
                />
        </View>
        <View style={{flexDirection:'row',alignItems:'center',paddingHorizontal:20,paddingBottom:20}}>
                <Text style={{fontSize:13, color:'grey', width:100}}>{'New\nPassword'}</Text>
                <TextInput
                    ref="DONumber"
                    autoCapitalize="none"
                    autoCorrect={false}
                    placeholder=""
                    placeholderTextColor={commonColors.placeholderText}
                    textAlign="left"
                    style={[styles.input,{marginLeft:15, flex:1}]}
                    underlineColorAndroid="transparent"
                    secureTextEntry={true}
                    returnKeyType={"next"}
                    value={this.state.newPassword}
                    onChangeText={text =>{
                        this.setState({newPassword:text})
                    }}
                />
        </View>
        <View style={{flexDirection:'row',alignItems:'center',paddingHorizontal:20}}>
                <Text style={{fontSize:13, color:'grey', width:100}}>{'Confirm New\nPassword'}</Text>
                <TextInput
                    ref="DONumber"
                    autoCapitalize="none"
                    autoCorrect={false}
                    placeholder=""
                    placeholderTextColor={commonColors.placeholderText}
                    textAlign="left"
                    secureTextEntry={true}
                    style={[styles.input,{marginLeft:15, flex:1}]}
                    underlineColorAndroid="transparent"
                    returnKeyType={"next"}
                    value={this.state.confirmPassword}
                    onChangeText={text =>{
                        this.setState({confirmPassword:text})
                    }}
                />
        </View>
        {this.renderIndicator()}
      </View>
    );
  }
}

export default connect(
  state => ({
    actionType: state.common.type,
    status: state.common.status,
    currentDelivery: state.common.currentDelivery,
    completedDeliveries:state.common.completedDeliveries,
    internetConnection: state.common.internetConnection
  }),
  dispatch => ({
    actions: bindActionCreators({...actions,...session}, dispatch)
  })
)(ChangePassword);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop:24, 
    backgroundColor:'white'
  },
  cardContainer: {
    elevation: 3,
    borderRadius: 8,
    backgroundColor: "white",
    // padding: 10,
    borderWidth: 3,
    borderColor: commonColors.blue
  },
  input: {
    fontSize: 14,
    color: commonColors.title,
    height: 40,
    borderRadius: 3,
    marginBottom: 3,
    paddingHorizontal: 10,
    backgroundColor: commonColors.inputColor
  },
  date: {
    fontSize: 13,
    color: "white",
    marginVertical: 5,
    fontWeight: "bold"
  },
  username: {
    fontSize: 18,
    fontWeight: "bold",
    color: "white",
    marginLeft:15,
    flex:1,
  }
});
