"use strict";

import React, { PureComponent } from "react";

import {
  StyleSheet,
  View,
  Platform,
  Modal,
  TouchableOpacity,
  Text,
  StatusBar,
  Image,
  ScrollView,
  RefreshControl,
  Alert,
} from "react-native";

import * as commonColors from "../styles/colors";
import * as commonStyles from "../styles/styles";
import { screenWidth, screenHeight } from "../styles/styles";

import * as actionTypes from "../redux/actionTypes";

import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import * as actions from "../redux/actions";

import api from "../service/api";
import Cache from "../utils/cache";
import { Ionicons, MaterialCommunityIcons } from "@expo/vector-icons";
import { Actions } from "react-native-router-flux";
import HeaderBar from "../components/header";
import UtilService from '../utils/utils'
import InformationBar from '../components/informationBar'
import moment from "moment";

class CheckinList extends PureComponent {
  constructor(props) {
    super(props);

    this.state = {
        checkinList:[],
        items:[],
        months:[
            0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11
        ],
        currentMonth:0,
    };
  }

  componentDidMount(){
      this.fetchData(0)
  }

  fetchData(index){
      this.setState({currentMonth:index})
      let from = new Date()
      let to = new Date()
      from.setDate(1);
      from.setMonth(from.getMonth() - index+1);
      to.setDate(1);
      to.setMonth(to.getMonth() - index);
      
      api.getCheckinList(from, to, (err, res)=>{
          if ( err == null ){
            let items=[], len = 0
            res.map((item)=>{
              if ( items[len]==undefined ) items.push({items:[]})
              let buck = items[len].items
              if (buck.length==0||(new Date(buck[0].CheckInAt)).getDate()==(new Date(item.CheckInAt)).getDate()) buck.push(item)
              else {
                len = len + 1;
                items.push({items:[item]})
              }
            })
            //console.log('arrived res on checkInList....',res)
            this.setState({checkinList:res, items})
          }
      })
  }

  render() {
    let {currentDelivery, upcomingDeliveries, internetConnection} = this.props
    return (
      <View style={styles.container}>
        <InformationBar iConnection={internetConnection} currentDelivery={currentDelivery}/>   
        <HeaderBar title={"Check In/Out Time"} />
        <View style={{height:80, width:'100%', borderBottomColor:'#ccc', borderBottomWidth:1}}>
            <ScrollView
                horizontal={true}
                showsHorizontalScrollIndicator={false}
                >
                <View style={{padding:10, alignItems:'center', justifyContent:'center', flexDirection:'row'}}>
                {this.state.months.map((month, index)=>{
                    let d = new Date()
                    d = d.setMonth(d.getMonth()-index)
                    return(
                        <TouchableOpacity onPress={()=>this.fetchData(index)} key={index} 
                            style={{width:60, height:60, elevation:2, borderRadius:4, margin:3, alignItems:'center', justifyContent:'center', backgroundColor:this.state.currentMonth==index?commonColors.button:'white'}}>
                            <Text style={{fontSize:16, color:index==this.state.currentMonth?'white':'#777', fontWeight:'bold'}}>{moment(d).format('MMM')}</Text>
                            <Text style={{fontSize:12, color:index==this.state.currentMonth?'white':'#aaa', fontWeight:'bold'}}>{moment(d).format('YYYY')}</Text>
                        </TouchableOpacity>
                    )
                })}
                </View>
            </ScrollView>
        </View>
        <ScrollView>
            <View style={{padding:10}}>
                {this.state.items.map((item, index)=>{
                    
                    return (
                        <View key={index} style={{flexDirection:'row', borderBottomColor:'#ccc', borderBottomWidth:1, padding:8}}>
                            <View style={{alignItems:'center'}}>
                                <Text style={{fontSize:16, color:'#777', fontWeight:'bold'}}>{moment(new Date(item.items[0].CheckInAt)).format('DD MMM')}</Text>
                                <Text style={{fontSize:12, color:'#aaa', fontWeight:'bold'}}>{moment(new Date(item.items[0].CheckInAt)).format('YYYY')}</Text>
                            </View>
                            <View style={{marginLeft:20}}>
                                {item.items.map((it, index)=>(
                                  <Text key={index} style={{fontSize:14, color:'#999'}}>{moment(new Date(it.CheckInAt)).format('HH:mm A')} - {it.CheckOutAt?moment(new Date(it.CheckOutAt)).format('HH:mm A'):''}</Text>
                                ))}
                            </View>
                        </View>
                    )
                })}
            </View>
        </ScrollView>
      </View>
    );
  }
}

export default connect(
  state => ({
    actionType: state.common.type,
    status: state.common.status,
    upcomingDeliveries: state.common.upcomingDeliveries,
    currentDelivery: state.common.currentDelivery,
    internetConnection: state.common.internetConnection
  }),
  dispatch => ({
    actions: bindActionCreators(actions, dispatch)
  })
)(CheckinList);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop:24,
    backgroundColor: "rgb(250,250,250)"
  },
});
