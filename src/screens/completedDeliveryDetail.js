"use strict";

import React, { PureComponent } from "react";

import {
  StyleSheet,
  View,
  Platform,
  Modal,
  TouchableOpacity,
  Text,
  StatusBar,
  Image,
  Linking,
  NativeModules
} from "react-native";


import * as commonColors from "../styles/colors";
import * as commonStyles from "../styles/styles";
import { screenWidth, screenHeight } from "../styles/styles";

import * as actionTypes from "../store/common/actionTypes";

import { bindActionCreators } from "redux";
import { connect } from "react-redux";
// import * as actions from "../redux/actions";
import * as actions from '../store/common/actions'
import * as session from '../store/session/actions'

import api from "../service/api";
import UtilService from '../utils/utils'
import Cache from "../utils/cache";
import { Ionicons, MaterialCommunityIcons } from "@expo/vector-icons";
import { Actions } from "react-native-router-flux";
import HeaderBar from '../components/header'
import {MapView} from 'expo'
import * as CONST from '../components/constant'
const PrintManager = NativeModules.PrintManager;
import * as config from '../config'

const Item = ({ icon, name, content, isGrey }) => (
    <View style={{ flexDirection: "row", alignItems: "center", marginTop: 5 }}>
      <Ionicons name={icon} size={16} color={"grey"} style={{ width: 20 }} />
      <Text style={{ fontSize: 13, color: "grey", marginLeft: 8, width: 50 }}>
        {name}
      </Text>
      <Text
        style={{
          fontSize: 15,
          color: isGrey ? "grey" : "#333",
          fontWeight: "bold",
          flex:1
        }}
      >
        {content}
      </Text>
    </View>
  );

class CompletedDeliveryDetail extends PureComponent {
  constructor(props) {
    super(props);

    this.state = {
        items:[]
    };
  }

  onChat(){
    Actions.Chat({channel:this.props.data})
  }
  print(){
    var str = "";
    this.props.data.DeliveryNoteItems.map((item, index)=>{    
      str+="\n"
      str+="\n"
      str+="    "+ item.Item.Name + "      "
      str+=item.DeliveredQuantity+"\n"
    })
    var text = "John";
    var imgPath = config.SERVICE_FILE_URL+this.props.data.Signature
    var isLocalFile = false
    // PrintManager.printText(false,str.toString(),imgPath.toString(),(greeting)=>{
    //           //console.log('arrved string>>>>>>',greeting)
    // })
    try{
          PrintManager.printText(false,str.toString(),imgPath.toString(),(greeting)=>{
            //console.log('arrved string>>>>>>',greeting)
      })
    }catch(e){
      Alert.alert(JSON.stringify(e))
    }
        
  }
  render() {
    let {Project, DeliveryNoteNumber, Warehouse, DestinationAddress} = this.props.data
    return (
      <View style={styles.container}>
        <HeaderBar 
          title={'Completed Delivery'}
          rightElement={(
            <TouchableOpacity onPress={()=>Actions.AlertListByDn({dnID:this.props.data.DeliveryNoteID})}>
              <Ionicons name="md-warning" size={24} color={'white'}/>
            </TouchableOpacity>
          )}
        />
        <View style={{width:'100%', elevation:5, padding:15}}>
            {Project&&<Text style={{fontSize:18, color:'#333', fontWeight:'bold'}}>{Project.Name}</Text>}
            <Text style={{fontSize:16, color:'grey', marginTop:5}}>{DeliveryNoteNumber}</Text>
            <View style={{height:20}}/>
            <Item icon={"md-radio-button-off"} name={"From"} content={Warehouse.Address} isGrey />
            <Item icon={"md-pin"} name={"To"} content={DestinationAddress} isGrey />
            {/* <Text style={{fontSize:16, color:'grey', fontWeight:'bold', marginTop:8}}>{Contact.ContactName}</Text>
            <Text style={{fontSize:16, color:'grey', fontWeight:'bold',marginTop:5}}>{UtilService.getAddress1(Contact) + ' ' + UtilService.getAddress2(Contact)}</Text> */}
            <View style={{flexDirection:'row', marginTop:15}}>
                <Text style={{fontSize:13, color:'grey'}}>Completed Time</Text>
                <Text style={{fontSize:14, color:'#777', fontWeight:'bold', marginLeft:20}}>{UtilService.getDateTime(this.props.data.CompleteTime)}</Text>
            </View>
        </View>

        
        <View style={{marginBottom:20, elevation:4, shadowColor:'black', shadowOffset:{width:4, height:4}, shadowOpacity:0.3, shadowRadius:4}}>
            <View style={{flexDirection:'row', backgroundColor:'rgb(228,228,233)', alignItems:'center', height:34, width:'100%', marginTop:8, paddingHorizontal:20}}>
                <Text style={{color:'grey', flex:1}}>Item</Text>
                <Text style={{color:'grey', width:120}}>Delivered Qty</Text>
            </View>
            {this.props.data.DeliveryNoteItems.map((item, index)=>{
                return(
                    <View key={index} style={{flexDirection:'row', alignItems:'center', height:34, width:'100%',paddingVertical:10, paddingHorizontal:20, backgroundColor:index%2==0?'white':'rgb(249,249,249)'}}>
                        <Text style={{color:'grey', flex:1}}>{item.Item.Name}</Text>
                        <Text style={{color:'grey', width:120}}>{(this.props.data.DeliveryNoteType!=CONST.DELIVERY_TYPE_QAC)? item.DeliveredQuantity:1} {item.Item.Uom.Name}</Text>
                    </View>
                )
            })}
        </View>
        <View style={{width:'100%', alignItems:'center', marginTop:20}}> 
            <TouchableOpacity onPress={()=>Actions.CompletedSecondReport({currentDelivery:this.props.data})} style={{borderRadius:25, borderWidth:2,height:50, borderColor:commonColors.theme, width:280, flexDirection:'row', alignItems:'center', justifyContent:'center'}}>
              <Ionicons name="ios-list" size={24} color={commonColors.theme}/>
              <Text style={{fontSize:15, color:commonColors.theme, fontWeight:'bold', marginLeft:20}}>
                DELIVERY REPORT
              </Text>
            </TouchableOpacity>
        </View>
        <View style={{width:'100%', alignItems:'center', marginTop:20}}> 
            <TouchableOpacity onPress={()=>this.print()} style={{borderRadius:25, borderWidth:2,height:50, borderColor:commonColors.theme, width:140, flexDirection:'row', alignItems:'center', justifyContent:'center'}}>
              <Text style={{fontSize:15, color:'blue', fontWeight:'bold', marginLeft:20}}>
                  PRINT
              </Text>
            </TouchableOpacity>
        </View>
        <View style={{position:'absolute', bottom:0, left:0, width:'100%', alignItems:'center', backgroundColor:'transparent', padding:15, flexDirection:'row'}}>
            <View style={{flex:1}}/>
            <TouchableOpacity onPress={()=>this.onChat()} style={{borderRadius:25, width:50, height:50, backgroundColor:commonColors.green, alignItems:'center', justifyContent:'center', marginLeft:15}}>
                <Ionicons name={'ios-chatboxes'} size={30} color={'white'}/>
            </TouchableOpacity>
        </View>
      </View>
    );
  }
}

export default connect(
  state => ({}),
  dispatch => ({
    actions: bindActionCreators({...actions,...session}, dispatch)
  })
)(CompletedDeliveryDetail);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop:24, 
    backgroundColor: 'rgb(249,249,249)'
  },

});
