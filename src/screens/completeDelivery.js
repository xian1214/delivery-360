"use strict";

import React, { PureComponent } from "react";

import {
  StyleSheet,
  View,
  Platform,
  Modal,
  TouchableOpacity,
  Text,
  StatusBar,
  Image,
  Linking
} from "react-native";


import * as commonColors from "../styles/colors";
import * as commonStyles from "../styles/styles";
import { screenWidth, screenHeight } from "../styles/styles";

import * as actionTypes from "../store/common/actionTypes";

import { bindActionCreators } from "redux";
import { connect } from "react-redux";
// import * as actions from "../redux/actions";
import * as actions from '../store/common/actions'
import * as session from '../store/session/actions'

import api from "../service/api";
import UtilService from '../utils/utils'
import Cache from "../utils/cache";
import { Ionicons, MaterialCommunityIcons } from "@expo/vector-icons";
import { Actions } from "react-native-router-flux";
import HeaderBar from '../components/header'
import {MapView} from 'expo'


const Item = ({ icon, name, content, isGrey }) => (
    <View style={{ flexDirection: "row", alignItems: "center", marginTop: 5 }}>
      <Ionicons name={icon} size={16} color={"grey"} style={{ width: 20 }} />
      <Text style={{ fontSize: 13, color: "grey", marginLeft: 8, width: 50 }}>
        {name}
      </Text>
      <Text
        style={{
          fontSize: 15,
          color: isGrey ? "grey" : "#333",
          fontWeight: "bold"
        }}
      >
        {content}
      </Text>
    </View>
  );

class DeliveryDetail extends PureComponent {
  constructor(props) {
    super(props);

    this.state = {
        projectName:'Project Name',
        DONumber:'RT02342',
        clientName: 'Austin',
        clientAddress: 'No.1, Jalan Bukit Bintang, Kuala Lumpur',
        items:[
            {title:'Cement', quantity:'100kg'},
            {title:'Steel', quantity:'800kg'}
        ]
    };
  }

  onChat(){
      Actions.Chat({channel:this.props.data})
  }

  report(){
    Actions.DeliveryReport({data:this.props.data})
  }

  render() {
    let {Project, DeliveryNoteNumber, Contact, DeliveryNoteItems    } = this.props.data
    return (
      <View style={styles.container}>
        <HeaderBar title={'Complete Delivery'}/>
        <View style={{width:'100%', elevation:5, padding:15}}>
            {Project&&<Text style={{fontSize:18, color:'#333', fontWeight:'bold'}}>{Project.Name}</Text>}
            <Text style={{fontSize:16, color:'grey', marginTop:5}}>{DeliveryNoteNumber}</Text>
            <View style={{height:20}}/>
            <Item icon={"md-radio-button-off"} name={"From"} content={Cache.company.CompanyName} isGrey />
            <Item icon={"md-pin"} name={"To"} content={Contact.ContactName} isGrey />
            {/* <Text style={{fontSize:16, color:'grey', fontWeight:'bold', marginTop:8}}>{Contact.ContactName}</Text>
            <Text style={{fontSize:16, color:'grey', fontWeight:'bold',marginTop:5}}>{UtilService.getAddress1(Contact) + ' ' + UtilService.getAddress2(Contact)}</Text> */}
            <View style={{flexDirection:'row', marginTop:15}}>
                {/* <Text style={{fontSize:13, color:'grey'}}>Approx. 42km, ETA 2:40 PM</Text> */}
                <Text style={{fontSize:13, color:'grey'}}>Arrival Time</Text>
                <Text style={{fontSize:14, color:'#777', fontWeight:'bold', marginLeft:20}}>{UtilService.getDateTime(this.props.arrivalTime)}</Text>
            </View>
        </View>

        
        <View style={{marginBottom:70, elevation:4, shadowColor:'black', shadowOffset:{width:4, height:4}, shadowOpacity:0.3, shadowRadius:4}}>
            <View style={{flexDirection:'row', backgroundColor:commonColors.cardColor3, alignItems:'center', height:34, width:'100%', marginTop:8, paddingHorizontal:20}}>
                <Text style={{color:'grey', flex:1}}>Item</Text>
                <Text style={{color:'grey', width:80}}>Quantity</Text>
            </View>
            {this.props.data.DeliveryNoteItems.map((item, index)=>{
                return(
                    <View key={index} style={{flexDirection:'row', alignItems:'center', height:34, width:'100%',paddingVertical:10, paddingHorizontal:20, backgroundColor:index%2==0?'white':'rgb(249,249,249)'}}>
                        <Text style={{color:'grey', flex:1}}>{item.Item.Name}</Text>
                        <Text style={{color:'grey', width:80}}>{item.Item.PlannedQuantity}</Text>
                    </View>
                )
            })}
        </View>
        <View style={{position:'absolute', bottom:0, left:0, width:'100%', alignItems:'center', backgroundColor:'transparent', padding:15, flexDirection:'row'}}>
            <TouchableOpacity onPress={()=>this.report()} style={{height:50, flex:1, alignItems:'center', justifyContent:'center', backgroundColor:commonColors.theme, borderRadius:8, elevation:5}}>
                <Text style={{fontWeight:'bold', color:'white', fontSize:16}}>Delivery Report</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={()=>this.onChat()} style={{borderRadius:25, width:50, height:50, backgroundColor:commonColors.green, alignItems:'center', justifyContent:'center', marginLeft:15}}>
                <Ionicons name={'ios-chatboxes'} size={30} color={'white'}/>
            </TouchableOpacity>
        </View>
      </View>
    );
  }
}

export default connect(
  state => ({}),
  dispatch => ({
    actions: bindActionCreators({...actions,...session}, dispatch)
  })
)(DeliveryDetail);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop:24, 
    backgroundColor: 'rgb(249,249,249)'
  },

});
