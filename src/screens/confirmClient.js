"use strict";

import React, { PureComponent } from "react";

import {
  StyleSheet,
  View,
  Platform,
  Modal,
  TouchableOpacity,
  Text,
  StatusBar,
  Image,
  ScrollView,
  TextInput,
  ActivityIndicator
} from "react-native";

// import TabNavigator from "react-native-tab-navigator";

import * as commonColors from "../styles/colors";
import * as commonStyles from "../styles/styles";
import { screenWidth, screenHeight } from "../styles/styles";
import * as actions from '../store/common/actions'
import api from "../service/api";
import Cache from "../utils/cache";
import { Ionicons, Feather } from "@expo/vector-icons";
import { Actions } from "react-native-router-flux";
import { SlideButton, SlideDirection } from '../components/sliderButton';
import {Constant,Svg} from 'expo'
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import HeaderBar from '../components/header'

const TextRow = ({title,content})=>(
    <View style={{flexDirection:'row',padding:10}}>
        <Text style={{flex:1,color:'grey',fontSize:18,fontWeight:'bold'}}>{title}</Text>
        <Text style={{flex:1,color:'black',fontSize:18,fontWeight:'bold'}}>{content}</Text>
    </View>
);
class ConfirmClient extends PureComponent {
  constructor(props) {
    super(props);

    this.state = {
        projectName:'Project Name',
        DONumber:'RT02342',
        clientName: 'Austin',
        clientAddress: 'No.1, Jalan Bukit Bintang, Kuala Lumpur',
        items:[],
        signatureArrays:[],
        example:'',
        dnID:'',
        responsible:'',
        customerID:''
    };
  }

  confirm(){
      Actions.pop();
  }
  componentDidMount(){
      var strList = this.props.qrdata.split(',')
      if(strList.length==4){
        this.setState({dnID:strList[0]}) 
        this.setState({responsible:strList[1]})
        this.setState({customerID:strList[2]})

      }
  }
  render() {
    if(!!!this.props.currentDelivery){
      <View>
        <Text>{'Current Delivery is not defined...'}</Text>
      </View>
    }
    return (
      // <ScrollView>
      <View style={styles.container}>
        <HeaderBar title={'Client Confirmation'}/>
        <View style={{justifyContent:'center',padding:20,marginBottom:20}}>
          <Text style={{fontSize:12,fontWeight:'bold',marginBottom:10,textDecorationLine:'underline'}}>Current Delivery Information</Text>
          <TextRow title={'DeliveryNumber'} content={this.props.currentDelivery.DeliveryNoteNumber}/>
          <TextRow title={'Driver ID'} content={this.props.currentDelivery.Responsible}/>
          <TextRow title={'Customer ID'} content={this.props.currentDelivery.CustomerID}/>
        </View>
        <View style={{justifyContent:'center',padding:20}}>
          <Text style={{fontSize:12,fontWeight:'bold',marginBottom:10,textDecorationLine:'underline'}}>Detected Delivery Information</Text>
          <TextRow title={'DeliveryNoteID'} content={this.state.dnID}/>
          <TextRow title={'Driver ID'} content={this.state.responsible}/>
          <TextRow title={'Customer ID'} content={this.state.customerID}/>
        </View>
        <View style={{flex:1,backgroundColor:'#bbb',borderRadius:10,margin:10}}>
          {
            (!!this.props.currentDelivery &&
              this.props.currentDelivery.DeliveryNoteID == this.state.dnID && 
              this.props.currentUser.user.CustomerID == this.state.customerID) &&
            <View style={{flex:1,padding:10,alignItems:'center',justifyContent:'center',backgroundColor:'transparent'}}>
              <Text style={{color:'blue',fontSize:38,fontWeight:'bold'}}>{'Client is correct!'}</Text>
            </View>
          }
          {
            !((!!this.props.currentDelivery &&
              this.props.currentDelivery.DeliveryNoteID == this.state.dnID && 
              this.props.currentUser.user.CustomerID == this.state.customerID)) &&
            <View style={{flex:1,padding:10,alignItems:'center',justifyContent:'center',backgroundColor:'transparent'}}>
              <Text style={{color:'red',fontSize:38,fontWeight:'bold'}}>{'Client is incorrect!'}</Text>
            </View>
          }
        </View>
      </View>
    );
  }
}
export default connect(
  state => ({
    currentDelivery: state.common.currentDelivery,
    internetConnection: state.common.internetConnection
  }),
  dispatch => ({
    actions: bindActionCreators({...actions}, dispatch)
  })
)(ConfirmClient);
const styles = StyleSheet.create({
  container: {
    paddingTop:(Platform.OS==="android")?24:0,
    flex: 1,
    backgroundColor: 'rgb(249,249,249)'
  },
  signContainer: {
    alignItems: "center",
    justifyContent: "center",
    padding: 20
  },
});
