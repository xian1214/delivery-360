"use strict";

import React, { PureComponent } from "react";

import {
  StyleSheet,
  View,
  Platform,
  Modal,
  TouchableOpacity,
  Text,
  StatusBar,
  Image,
  TextInput,
  ScrollView,
  Alert,
  FlatList,
  RefreshControl
} from "react-native";


import * as commonColors from "../styles/colors";
import * as commonStyles from "../styles/styles";
import { screenWidth, screenHeight } from "../styles/styles";

import * as actionTypes from "../store/common/actionTypes";

import { bindActionCreators } from "redux";
import { connect } from "react-redux";
// import * as actions from "../redux/actions";
import * as actions from '../store/common/actions'
import * as session from '../store/session/actions'

import api from "../service/api";
import Cache from "../utils/cache";
import { Ionicons, MaterialCommunityIcons, MaterialIcons } from "@expo/vector-icons";
import { Actions } from "react-native-router-flux";
import HeaderBar from '../components/header'
import async from 'async'
import UtilService from '../utils/utils'
import * as CONST from '../components/constant'
import moment from 'moment'
var timeout = null
var gHandler = null

class AlertListByVehicle extends PureComponent {
  constructor(props) {
    super(props);
    
    this.state = {
        alertList:[],
        allocatedDN:null
    };
    gHandler = this
    this.pageSize = 20;
    this.onEndReachedCalledDuringMomentum = true;
  }

  componentDidMount(){
    if ( !this.props.internetConnection ) {
      Alert.alert('You can not see alert list on offline mode!')
      Actions.pop()
      return;
    }
    this.refresh()
 
  }
  onEndReached(){
    if(!this.onEndReachedCalledDuringMomentum){
        this.pageSize+=20
        this.refresh();
        this.onEndReachedCalledDuringMomentum = true;
    }
  }
  onRefresh(){
    console.log('onRefresh is started.....')
    this.pageSize = 20
    this.refresh()
  }
  refresh(){
    this.setState({isRefreshing:true})
    api.getAlerts(this.props.dnID,(err, res)=>{
        this.setState({isRefreshing:false})
        if ( err == null ){
          if(res.length==0){
            Alert.alert('No Alerts')
          }
          else{
            console.log('arrived res',res.Results.length)
            this.setState({alertList:res.Results})
            this.state.allocatedDN = res[0].ReferenceID
          }
        }
    })
      
  }
  render() {
    return (
      <View style={styles.container}>
        <HeaderBar title='Alerts'
        />
        <FlatList
           refreshControl={
              <RefreshControl
                  refreshing={this.state.isRefreshing}
                  onRefresh={() => gHandler.onRefresh()}
                  tintColor={'grey'}
              />
            }
            initialNumToRender={10}
            onEndReachedThreshold={0.5}
            onMomentumScrollBegin = {()=>{this.onEndReachedCalledDuringMomentum=false;}}
            onEndReached={gHandler.onEndReached.bind(gHandler)}
            data={this.state.alertList}
            // keyExtractor={this._keyExtractor}
            keyExtractor={item=>'event-'+item.EventID}
            renderItem={({item, index})=>{
              return (
                <View key={index} style={{backgroundColor:index%2==0?'#f4f4f5':'#f9f9f9'}}>
                  <Text style={{color:'black',fontWeight:'bold',paddingLeft:15,paddingTop:10,fontSize:16}}>  {((!!item.DeliveryNote)?item.DeliveryNote.DeliveryNoteNumber:item.ImeiNo)}</Text> 
                  <View  style={{flexDirection:'row', alignItems:'center', paddingVertical:10}}>
                            {item.Message!='Miss Turn'&&
                            item.Message!='Traffic'&&
                            item.Message!='Toilet'&& 
                            item.Message!='Meal'&&<View style={{backgroundColor:(item.Level==CONST.ALERT_LEVEL)?'red':'green', height:40, width:40, borderRadius:20, alignItems:'center', justifyContent:'center', marginLeft:15}}>
                              <MaterialCommunityIcons name="truck" size={20} color={"white"} />
                            </View>}
                    {item.Message=='Miss Turn'&&<TouchableOpacity onPress={()=>this.reroute(1)} style={{backgroundColor:commonColors.brown, height:40, width:40, borderRadius:20, alignItems:'center', justifyContent:'center', marginLeft:15}}>
                      <MaterialIcons name="warning" color={'white'} size={20}/>
                    </TouchableOpacity>}
                    {item.Message=='Traffic'&&<TouchableOpacity onPress={()=>this.reroute(2)} style={{backgroundColor:commonColors.pink, height:40, width:40, borderRadius:20, alignItems:'center', justifyContent:'center', marginLeft:15}}>
                      <MaterialIcons name="traffic" color={'white'} size={20}/>
                    </TouchableOpacity>}
                    {item.Message=='Toilet'&&<TouchableOpacity onPress={()=>this.reroute(3)} style={{backgroundColor:commonColors.blue, height:40, width:40, borderRadius:20, alignItems:'center', justifyContent:'center', marginLeft:15}}>
                      <MaterialIcons name="wc" color={'white'} size={20}/>
                    </TouchableOpacity>}
                    {item.Message=='Meal'&&<TouchableOpacity onPress={()=>this.reroute(4)} style={{backgroundColor:commonColors.green, height:40, width:40, borderRadius:20, alignItems:'center', justifyContent:'center', marginLeft:15}}>
                      <MaterialIcons name="restaurant" color={'white'} size={20}/>
                    </TouchableOpacity>}
                        <View style={{marginLeft:15, justifyContent:'center'}}>
                          <Text style={{fontSize:16, color:'grey'}}>{UtilService.getAlarmText(item)}</Text>
                          <Text style={{fontSize:13, color:'#333', fontWeight:'bold', marginTop:5}}>{ moment(new Date(item.OpenDate)).format('YYYY/MM/DD hh:mm:ss A')}</Text>
                        </View>
                  </View>
                </View>
              );
          }}
        />
      </View>
    );
  }
}

export default connect(
  state => ({
    currentDelivery:state.common.currentDelivery,
    internetConnection: state.common.internetConnection
  }),
  dispatch => ({
    actions: bindActionCreators({...actions,...session}, dispatch)
  })
)(AlertListByVehicle);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    paddingTop: Platform.OS === 'android' ? 24 : 0,
  },
  input: {
    fontSize: 14,
    color: commonColors.title,
    height: 36,
    borderRadius: 3,
    marginBottom: 3,
    paddingHorizontal: 10,
    backgroundColor: commonColors.inputColor
  },
  modalContainer: {
    flex: 1,
    backgroundColor: "rgba(0, 0, 0,0.5)",
    alignItems: "center",
    justifyContent: "center"
  },
  modal: {
    width:360,
    borderRadius: 5,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "white",
    elevation:3,
  },
});
