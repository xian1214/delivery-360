"use strict";

import React, { PureComponent } from "react";

import {
  StyleSheet,
  View,
  Platform,
  Modal,
  TouchableOpacity,
  Text,
  StatusBar,
  Image,
  TextInput,
  ScrollView,
  Alert
} from "react-native";


import * as commonColors from "../styles/colors";
import * as commonStyles from "../styles/styles";
import { screenWidth, screenHeight } from "../styles/styles";

import * as actionTypes from "../redux/actionTypes";

import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import * as actions from "../redux/actions";

import api from "../service/api";
import Cache from "../utils/cache";
import * as config from '../config'
import { Ionicons, MaterialCommunityIcons } from "@expo/vector-icons";
import { Actions } from "react-native-router-flux";
import HeaderBar from '../components/header'
import async from 'async'
import {ImagePicker, Permissions, ImageManipulator} from 'expo'
import InformationBar from '../components/informationBar'
import { WaitingModal } from '../components/waiting';
import UtilService from "../utils/utils";

class AddItem extends PureComponent {
  constructor(props) {
    super(props);

    this.state = {
        items:[
            {Name:'Descriptoin'},
            {Name:'Temperature'},
            {Name:'Humidirty'},
        ],
        disabled: false,
        item:null,
        isWaiting: false,
        license:'',
        DONumber:'',
        driver:null,
    };
    this.convert = []
  }

  componentDidMount(){
    // //console.log(this.props.item)
    if ( this.props.item == undefined ){
      api.getQacItemTemplate((err, res)=>{
      // //console.log(err, res)
        if ( err == null ){
          this.setState({item:res})
        }
      })
    }else{
      api.getUsers(1, 50, (err, res)=>{
        if ( err == null ){
          let drivers = []
          res.Results.map((driver)=>drivers[driver.UserID]=driver)
          let notes = this.props.item.Note==null?[]:this.props.item.Note.split(',')
          this.props.item.CheckResults.map((item)=>{
            let ImageArray=(item.Images==null||item.Images=='')?null:item.Images.split(',')
            item.ImageArray = ImageArray.map(async(image)=>{
              if ( image.length < 4 ) {
                this.convert[Number(image)] = await UtilService.getLocalStringData(image)
                return Number(image)
              }
              return image
            })
          })
          
          this.setState({item:this.props.item, DONumber:notes[0], driver:drivers[notes[1]], license:notes[2], })
        }
        
      })
      
    }
    
  }


  async takePicture(index){
    let res = await Permissions.askAsync(Permissions.CAMERA)
    if ( res.status ==='granted'){
        let {status} = await Permissions.askAsync(Permissions.CAMERA_ROLL)
        if ( status === 'granted' ){
            
            let image = await ImagePicker.launchCameraAsync({
                quality:0.3
            })
            if ( !image.cancelled ){
                this.setState({isWaiting:true})
                const manipResult = await ImageManipulator.ImageManipulator(
                  image.uri,
                  [{resize:{width:768}}],
                  { format: 'jpeg', compress:0.6 }
                );
                api.uploadImage(manipResult.uri, (err, res)=>{
                  if ( err == null ){
                    if ( this.state.item.CheckResults[index].ImageArray == undefined ){
                      this.state.item.CheckResults[index].ImageArray = []
                    }
                    if ( Cache.hasInternetConnection ){
                      this.state.item.CheckResults[index].ImageArray.push(config.SERVICE_FILE_URL+res.filePath)
                    }else{
                      this.state.item.CheckResults[index].ImageArray.push(res)
                      this.convert[res] = manipResult.uri
                    }
                    
                    this.setState({item:{...this.state.item}})
                  }     
                  this.setState({isWaiting:false})   
                })
            }
            
        }
    }
  }

  showPicture(index, idx){
    Actions.ShowImage({uri:this.state.item.CheckResults[index].ImageArray[idx]})
  }

  selectDriver(){
    Actions.DriverList({selectDriver:(driver)=>{
      this.setState({driver})
    }})
  }

  done(){
    if ( this.state.DONumber == '' || this.state.driver == null || this.state.license =='' ){
      Alert.alert('Please fill the input with correct info!')
      return;
    }
    // //console.log(this.state.driver)
    this.state.item.Note = this.state.DONumber+','+this.state.driver.UserID+','+this.state.license
    this.state.item.DeliveryNoteID = this.props.currentDelivery.DeliveryNoteID
    this.state.item.ItemID = 10
    this.state.item.CheckResults.map((son, index)=>{
      if ( son.ImageArray != undefined ) son.Images = son.ImageArray.join()
      else son.Images = ''
    })
    api.updateQacItem([this.state.item], (err, res)=>{
      // //console.log('QacItem',err, res)
      if ( err == null ){
        this.props.update()
        Actions.pop()
      }
    })
  }

  render() {
      let {internetConnection, currentDelivery} = this.props
      // if ( this.state.item == null ) return null;
    return (
      <View style={styles.container}>
        <InformationBar iConnection={internetConnection} currentDelivery={currentDelivery}/>        
        <HeaderBar title={'Add Item'} back={()=>Actions.pop()}
          rightElement={(
            <TouchableOpacity disabled={this.state.disabled} onPress={()=>this.done()}>
              <Ionicons name="md-checkmark" size={24} color={'white'}/>
            </TouchableOpacity>
          )}
        />
        <ScrollView style={{padding:15}}>
            <View style={{flexDirection:'row', paddingVertical:5, alignItems:'center'}}>
                <Text style={{fontSize:13, color:'grey', width:100}}>{'Delivery Order\nNumber'}</Text>
                <TextInput
                    ref="DONumber"
                    autoCapitalize="none"
                    autoCorrect={false}
                    placeholder="Delivery Order Number"
                    placeholderTextColor={commonColors.placeholderText}
                    textAlign="left"
                    style={[styles.input,{marginLeft:15, flex:1}]}
                    underlineColorAndroid="transparent"
                    returnKeyType={"next"}
                    value={this.state.DONumber}
                    onChangeText={text =>{
                        this.setState({DONumber:text})
                    }}
                />
            </View>

            <View style={{flexDirection:'row', paddingVertical:5, alignItems:'center'}}>
                <Text style={{fontSize:13, color:'grey', width:100}}>Driver Name</Text>
                <TouchableOpacity onPress={()=>this.selectDriver()} style={{flexDirection:'row', marginLeft:15, flex:1, height:40, backgroundColor:commonColors.inputColor, borderRadius:3, alignItems:'center', paddingHorizontal:10}}>
                    {(this.state.driver==null||this.state.driver.Avatar==null)&&<Image source={commonStyles.face} style={{width:32, height:32, borderRadius:16}}/>}
                    {this.state.driver!=null&&this.state.driver.Avatar!=null&&<Image source={{uri:config.SERVICE_FILE_URL+this.state.driver.Avatar}} style={{width:32, height:32, borderRadius:16}}/>}
                    {this.state.driver==null&&<Text style={{fontSize:13, color:'grey', marginHorizontal:20, flex:1}}>Driver Name</Text>}
                    {this.state.driver!=null&&<Text style={{fontSize:13, color:'grey', marginHorizontal:20, flex:1}}>{this.state.driver.FirstName+' '+this.state.driver.LastName}</Text>}
                    <Ionicons name="md-arrow-dropdown" size={20} color={commonColors.button}/>
                </TouchableOpacity>
            </View>

            <View style={{flexDirection:'row', paddingVertical:5, alignItems:'center'}}>
                <Text style={{fontSize:13, color:'grey', width:100}}>{'Truck License\nPlat'}</Text>
                <TextInput
                    ref="license"
                    autoCapitalize="none"
                    autoCorrect={false}
                    placeholder="Truck License Plat"
                    placeholderTextColor={commonColors.placeholderText}
                    textAlign="left"
                    style={[styles.input,{marginLeft:15, flex:1}]}
                    underlineColorAndroid="transparent"
                    returnKeyType={"next"}
                    value={this.state.license}
                    onChangeText={text =>{
                        this.setState({license:text})
                    }}
                />
            </View>
            <View style={{width:'100%', height:1, backgroundColor:'#ccc', marginVertical:15}}/>
            <Text style={{fontSize:14, color:'grey', marginBottom:8, fontWeight:'bold'}}>Check List</Text>
            {this.state.item&&this.state.item.CheckResults.map((item, index)=>{
                return (
                    <View key={index} style={{paddingVertical:15, borderBottomColor:'#ccc', borderBottomWidth:1}}>
                        <Text style={{fontSize:13, color:'grey', marginBottom:8}}>{item.CheckItemName}</Text>
                        <TextInput
                            ref="remarks"
                            autoCapitalize="none"
                            autoCorrect={false}
                            placeholder="Remarks"
                            placeholderTextColor={commonColors.placeholderText}
                            textAlign="left"
                            style={styles.input}
                            underlineColorAndroid="transparent"
                            returnKeyType={"next"}
                            value={item.Value}
                            onChangeText={text =>{
                                item.Value = text.replace(/\t/g, "")
                                this.setState({ item: {...this.state.item}  })
                            }}
                        />
                        
                          <ScrollView horizontal={true}>
                            <View style={{flexDirection:'row'}}>
                              {item.ImageArray&&item.ImageArray.map( (img, idx)=>{
                                let uri = img
                                if ( typeof img === 'number' ) {
                                  uri = this.convert[img]
                                }
                                return (
                                  <TouchableOpacity key={idx} onPress={()=>this.showPicture(index, idx)} style={{width:90, height:90, marginTop:5, marginRight:5}}>
                                    <Image source={{uri}} style={{flex:1, resizeMode:'cover', backgroundColor:'grey'}}/>
                                  </TouchableOpacity>)
                                })}
                              <TouchableOpacity onPress={()=>this.takePicture(index)} style={{borderColor:commonColors.theme, borderWidth:2, alignItems:'center', justifyContent:'center', width:90, height:90, marginTop:5, marginRight:5}}>
                                  <Ionicons name={'ios-camera'} color={commonColors.theme} size={30}/>
                              </TouchableOpacity>
                            </View>
                          </ScrollView>
                        

                        
                    </View>
                )
            })}
            <View style={{height:50}}/>
        </ScrollView>
        <WaitingModal isWaiting={this.state.isWaiting || this.state.item == null}/>
      </View>
    );
  }
}

export default connect(
  state => ({
    currentDelivery:state.common.currentDelivery,
    internetConnection: state.common.internetConnection
  }),
  dispatch => ({
    actions: bindActionCreators(actions, dispatch)
  })
)(AddItem);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop:24,
    backgroundColor: 'white'
  },
  input: {
    fontSize: 14,
    color: commonColors.title,
    height: 40,
    borderRadius: 3,
    marginBottom: 3,
    paddingHorizontal: 10,
    backgroundColor: commonColors.inputColor
  },
  modalContainer: {
    flex: 1,
    backgroundColor: "rgba(0, 0, 0,0.5)",
    alignItems: "center",
    justifyContent: "center"
  },
  modal: {
    width:360,
    borderRadius: 5,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "white",
    elevation:3,
  },
});
