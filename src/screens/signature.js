"use strict";

import React, { PureComponent } from "react";

import {
  StyleSheet,
  View,
  Platform,
  Modal,
  TouchableOpacity,
  Text,
  StatusBar,
  Image,
  ScrollView,
  RefreshControl,
  Alert
} from "react-native";


import * as commonColors from "../styles/colors";
import * as commonStyles from "../styles/styles";
import { screenWidth, screenHeight } from "../styles/styles";
import * as config from '../config'
import * as actionTypes from "../store/common/actionTypes";

import { bindActionCreators } from "redux";
import { connect } from "react-redux";
// import * as actions from "../redux/actions";
import * as actions from '../store/common/actions'
import * as session from '../store/session/actions'

import api from "../service/api";
import Cache from "../utils/cache";
import { Ionicons, MaterialCommunityIcons } from "@expo/vector-icons";
import { Actions } from "react-native-router-flux";
import HeaderBar from "../components/header";
import UtilService from '../utils/utils'
// import ExpoPixi from 'expo-pixi';
import SignatureCapture from 'react-native-signature-capture'
import {Permissions} from 'expo'

class Signature extends PureComponent {
  constructor(props) {
    super(props);

    this.state = {
        qrScanData: null,
        acceptor:null
    };
    this.acceptorID = 0
  }

  async componentDidMount(){
    let {status} = await Permissions.askAsync(Permissions.CAMERA_ROLL)
    if ( status !== 'granted' ) Actions.pop()
  }

  done(){
    this.refs.sign.saveImage();
    // //console.log('done')
    // this.makeSignImage((signatureUri)=>{
    //   this.props.done(this.state.qrScanData, signatureUri)
    //   Actions.pop()
    // })
  }

  handleQR(data){
      this.setState({qrScanData:data})
      //Alert.alert(data)
      //Alert.alert('QR scanned',data)
      var strList = data.split(',')
      if(strList.length==4){
        this.setState({dnID:strList[0]}) 
        this.setState({responsible:strList[1]})
        this.setState({customerID:strList[2]})
        this.acceptorID = strList[3]
      }
      api.getUser(this.acceptorID,(error,res)=>{
        //console.log('arrived res on scan signature1',res)
        if(error){
          return
        }
        if(res.CustomerID==this.props.currentDelivery.CustomerID){
          //console.log('arrived res on scan signature2',res)
          Alert.alert('Correct Acceptor')
          this.setState({acceptor:res})
        }
        else{
          //console.log('arrived res on scan signature3',res)
          Alert.alert('Incorrect Acceptor')
          return
        }        
      })
  }
  scanQR(){
    ////console.log('-------this.props.data on signqture',this.props.data)
    if ( this.props.data.IsInGeofence != true ){
      Alert.alert('You are out of geofence area. \nYou can scan QRcode \nin Geofence area')
      //return;
    }
    Actions.ScanQRCode({getQRCode:({data})=>{
        this.handleQR(data)
    }})
  }

  // makeSignImage = async (cb) => {
  //   const options = {
  //     format: 'png', /// PNG because the view has a clear background
  //     compress: 0.1, /// Low quality works because it's just a line
  //   };

  //   const image = await this.sketch.glView.takeSnapshotAsync()
  //   cb(image.uri)
  // };

  undoLastLine() {
    this.refs.sign.resetImage();
  }

  _onSaveEvent(result) {
    this.props.done(this.state.qrScanData, Platform.OS==='android'?'file://'+result.pathName:result.pathName)
    Actions.pop()
  }
  _onDragEvent() {
      // //console.log("dragged");
  }

  render() {
    let {acceptor} = this.state
    return (
      <View style={styles.container}>
        <HeaderBar title={Cache.getLang('label.customer_signature')} rightElement={(
            <TouchableOpacity onPress={()=>this.done()} style={{padding:10}}>
                <Ionicons name="md-checkmark" size={20} color={'white'}/>
            </TouchableOpacity>
        )}/>
            {acceptor==null&&<View>
                <Text style={{fontSize:14, color:'grey', margin:15}}>{Cache.getLang('label.report_agree')}</Text>
                <View style={{width:'100%', alignItems:'center', justifyContent:'center', marginTop:20}}>
                    <TouchableOpacity onPress={()=>this.scanQR()} style={{height:36, width:200, borderRadius:18, borderWidth:2, borderColor:commonColors.theme, alignItems:'center', justifyContent:'center', flexDirection:'row'}}>
                        <MaterialCommunityIcons name={'qrcode-scan'} size={18} color={commonColors.theme}/>
                        <Text style={{fontSize:15, fontWeight:'bold', color:commonColors.theme, marginLeft:10}}>{Cache.getLang('label.scan_qr')}</Text>
                    </TouchableOpacity>
                </View>
            </View>}
            {acceptor!=null&&<View style={{flexDirection:'row', alignItems:'center', paddingHorizontal:15, marginTop:15}}>
                <Text style={{fontSize:14, color:'grey',marginRight:10}}>Acceptor:</Text>
                {/* <Image source={commonStyles.face1} style={{width:30, height:30, borderRadius:15, marginLeft:10}}/> */}
                {
                  (acceptor.Avatar==null || acceptor.Avatar==="")&&
                  <Image source={commonStyles.noUser} style={{width:50, height:50, borderRadius:25}}/>
                }
                {
                  !(acceptor.Avatar==null || acceptor.Avatar=="")&&
                  <Image source={{uri:config.SERVICE_FILE_URL+acceptor.Avatar}} style={{width:50, height:50, borderRadius:25}}/>
                }
                <Text style={{fontSize:14, color:'#333', marginLeft:10, flex:1}}>{acceptor.FirstName+' '+acceptor.LastName}</Text>
                <TouchableOpacity onPress={()=>this.scanQR()} style={{height:36, width:36, alignItems:'center', justifyContent:'center'}}>
                    <MaterialCommunityIcons name={'qrcode-scan'} size={18} color={commonColors.theme}/>
                </TouchableOpacity>
            </View>}
            <View style={{marginTop:20, flex:1}}>
                <View style={{flexDirection:'row', paddingHorizontal:15, alignItems:'center'}}>
                    <Text style={{color:'grey', flex:1}}>{Cache.getLang('label.sign_in')}</Text>
                    <TouchableOpacity onPress={()=>this.undoLastLine()} style={{padding:5}}>
                        <Ionicons name='md-refresh-circle' color={commonColors.theme} size={32}/>
                    </TouchableOpacity>
                </View>
                <View style={{flex:1}}>
                    {/* <ExpoPixi.Signature 
                        style={{flex:1, marginTop:10, backgroundColor:commonColors.cardColor3}}
                        ref={ref => this.sketch = ref}
                    /> */}
                    <SignatureCapture
                      style={{flex:1}}
                      ref="sign"
                      onSaveEvent={this._onSaveEvent.bind(this)}
                      onDragEvent={this._onDragEvent.bind(this)}
                      saveImageFileInExtStorage={true}
                      showNativeButtons={false}
                      maxStrokeWidth={12}
                      minStrokeWidth={5}
                      showTitleLabel={false}
                      viewMode={"portrait"} />
                </View>
            </View>
      </View>
    );
  }
}

export default connect(
  state => ({
    actionType: state.common.type,
    status: state.common.status,
    upcomingDeliveries: state.common.upcomingDeliveries,
    currentDelivery: state.common.currentDelivery
  }),
  dispatch => ({
    actions: bindActionCreators({...actions,...session}, dispatch)
  })
)(Signature);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop:24,
    backgroundColor: "rgb(250,250,250)"
  },
});
