import React from 'react';
import {
  StyleSheet,
  View,
  Text,
  Dimensions,
  TouchableOpacity,
  Platform,
  Alert
} from 'react-native';

import MapView, { MAP_TYPES, Polygon, ProviderPropType } from 'react-native-maps';
import HeaderBar from '../components/header'
import {GooglePlacesAutocomplete}  from 'react-native-google-places-autocomplete';
const { width, height } = Dimensions.get('window');
import { Actions } from "react-native-router-flux";
import { connect } from "react-redux";
import * as actions from "../redux/actions";
import { bindActionCreators } from "redux";
import * as commonColors from "../styles/colors";
import googleService from '../service/googleService'
import api from '../service/api'

const ASPECT_RATIO = width / height;
const LATITUDE = 37.78825;
const LONGITUDE = -122.4324;
const LATITUDE_DELTA = 0.0922;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;
let id = 0;
const homePlace = { description: 'Home', geometry: { location: { lat: 48.8152937, lng: 2.4597668 } }};
const workPlace = { description: 'Work', geometry: { location: { lat: 48.8496818, lng: 2.2940881 } }};
class PolygonCreator extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      initialRegion: {
        latitude: LATITUDE,
        longitude: LONGITUDE,
        latitudeDelta: LATITUDE_DELTA,
        longitudeDelta: LONGITUDE_DELTA,
      },
      polygons: [],
      circles:[],
      B_polygons:[],
      B_circles:[],
      rects:[],
      editing: null,
      creatingHole: false,
      tempDrawingObjects:{
        rectObjects:[],
        circleObjects:[],
        polygonObjects:[]
      },
      targetPos:{
        latitude:this.props.location.coords.latitude,
        longitude:this.props.location.coords.longitude
      },
      curPos:{
        latitude:this.props.location.coords.latitude,
        longitude:this.props.location.coords.longitude
      }

    };
    this.lastRegion = {
      latitude: LATITUDE,
      longitude: LONGITUDE,
      latitudeDelta: LATITUDE_DELTA,
      longitudeDelta: LONGITUDE_DELTA,
    },
    this.mapView = null
    this.bounds = []
  }
  
  finish() {
    const { polygons, editing } = this.state;
    this.setState({
      polygons: [...polygons, editing],
      editing: null,
      creatingHole: false,
    });
  }

  uploadLocation(){

    let currentDelivery = this.props.data
      api.postLocation({
        DeliveryRouteID: currentDelivery.DeliveryRouteID,
        DeliveryNoteID: currentDelivery.DeliveryNoteID,
        UserID: currentDelivery.User.UserID,
        Lat:this.state.curPos.latitude,
        Long:this.state.curPos.longitude,
      },(err, res) => {
       
      });
  }
  
  clear() {
    const { polygons, editing } = this.state;
    this.setState({
      polygons: [],
      circles:[],
      editing: null,
      creatingHole: false,
    });
    this.bounds=[]
  }

  getRoute(){
    // //console.log('current', this.props.currentDelivery)
    let {StartLat, StartLong, EndLat, EndLong, PathData} = this.props.data.DeliveryRoute
    if (this.props.data.IsSignOff){
      StartLat = this.props.data.ReturnRoute.StartLat
      StartLong = this.props.data.ReturnRoute.StartLong
      EndLat = this.props.data.ReturnRoute.EndLat
      EndLong = this.props.data.ReturnRoute.EndLong
      PathData = this.props.data.ReturnRoute.PathData
    }
    let {index} = JSON.parse(PathData)
    googleService.getTripPath(
      StartLat+','+StartLong,
      EndLat+','+EndLong,
      (err, res)=>{
        if ( err == null ){
          // //console.log('length', res[index].path.length)
          this.setState({coordinates:res[index].path, index, duration:res[index].duration, distance:res[index].distance})
          this.mapView.fitToCoordinates(res[index].path, {
            edgePadding: {
              right: width / 20,
              bottom: height / 20,
              left: width / 20,
              top: height / 20
            }
          });
        }
      })
  }
  componentWillUnmount(){
    this.props.actions.setLocationMode(true)
    //console.log('will unmount on polygon Creator... .......')
  }
  componentDidMount(){
    this.currentPlace = { description: 'Current Place', geometry: { location: { lat: this.props.location.coords.latitude, lng: this.props.location.coords.longitude} }};
    //console.log('this.props.data.Project.Geofence------- on PolygonCreator',this.props.data.Project.Geofence)
     var projectGeofence = this.props.data.Project.Geofence
     var branchGeofence = this.props.data.Warehouse.Geofence
     
     this.parseGeofence(projectGeofence)
     this.parseBranchGeofence(branchGeofence)
     this.parseDeliveryGeofence(this.props.data)

     this.applyBounds(projectGeofence)
     this.applyBounds(branchGeofence)
     this.setBounds()

     let myRouteTimer = setInterval(()=>{
        if ( this.props.data && this.props.data.DeliveryRoute ){
          this.getRoute()
          clearInterval(myRouteTimer)
        }
      }, 100)
      //console.log('location mode 111111--->',this.props.locationMode) 
      this.props.actions.setLocationMode(false)
      setTimeout(()=>{
        //console.log('location mode 2222--->',this.props.locationMode) 
      },500)
   
      // let postLocationTimer = setInterval(()=>{
      // if(!this.props.actions.locationMode){
      //     this.uploadLocation()
      //   }
      // },20000)  
      
      
  }
  parseBranchGeofence(str){
    //console.log('str on parseGeofence...',str)
    if(str=='' || str==null) return
    try{
      var obj = JSON.parse(str);

      obj.polygonObjects.forEach(coordinates => {
        var polygonCoordinates=[] 
        coordinates.forEach((coordinate)=>{
            var vertex = {
              latitude:coordinate.lat,
              longitude:coordinate.lng
            }
            polygonCoordinates.push(vertex)
         })
         var newPolygon ={
           coordinates:polygonCoordinates
         }
         this.state.B_polygons.push(newPolygon)
      });
      //console.log('this.state.polygons on parse',this.state.polygons)
      obj.circleObjects.forEach(circle => {
        this.state.B_circles.push(circle)
      });
      //console.log('obj on parseGeofence....',obj)
      this.setState({
        B_polygons:[...this.state.B_polygons],
        B_circles:[...this.state.B_circles]
      })
    }
    catch(e){
      Alert.alert('Parsing error')
    }
  }
  parseDeliveryGeofence(geofence){
    
  }
  isDrawingObjEmpty(){
    if(this.state.circles.length == 0 && this.state.polygons.length==0 && this.state.rects.length==0){
      return true
    }
    else{
      return false
    }
  }
  parseGeofence(str){
    //console.log('str on parseGeofence...',str)
    if(str=='' || str==null) return
    try{
      var obj = JSON.parse(str);

      obj.polygonObjects.forEach(coordinates => {
        var polygonCoordinates=[] 
        coordinates.forEach((coordinate)=>{
            var vertex = {
              latitude:coordinate.lat,
              longitude:coordinate.lng
            }
            polygonCoordinates.push(vertex)
         })
         var newPolygon ={
           coordinates:polygonCoordinates
         }
         this.state.polygons.push(newPolygon)
      });
      //console.log('this.state.polygons on parse',this.state.polygons)
      obj.circleObjects.forEach(circle => {
        this.state.circles.push(circle)
      });
      //console.log('obj on parseGeofence....',obj)
      this.setState({
        polygons:[...this.state.polygons],
        circles:[...this.state.circles]
      })
    }
    catch(e){
      Alert.alert('Parsing error')
    }
  }
  applyBounds(geofence){
      
        var tempDrawingObjects
        if(!!geofence){
            //console.log('Geofence exist yes')
            tempDrawingObjects = JSON.parse(geofence)
        }
        else{
            //console.log('Geofence exist no')
            tempDrawingObjects={}
            tempDrawingObjects.rectObjects=[]
            tempDrawingObjects.circleObjects=[]
            tempDrawingObjects.polygonObjects=[]
        }
      
        var tempRects = tempDrawingObjects.rectObjects
        var tempCircles = tempDrawingObjects.circleObjects
        var tempPolygons = tempDrawingObjects.polygonObjects
        var testArray = []
        
        //console.log('tempDrawingObjects on geofenceedit',tempDrawingObjects)
        // if(tempRects.length>0){
        //     var i,j
        //     for(i=0;i<tempRects.length;i++){
        //         var rect = tempRects[i]
        //         var point1 = new google.maps.LatLng(rect.north,rect.east)
        //         var point2 = new google.maps.LatLng(rect.south,rect.west)
        //         this.bounds.extend(point1)
        //         this.bounds.extend(point2)
        //         testArray.push(point1.lng())
        //         testArray.push(point2.lng())
        //     }
        // }
        //console.log('this.tempCircles on didmount', tempCircles)
        if(tempCircles.length>0){
            tempCircles.forEach((circle)=>{
                var circleBounds = this.get4PointsAroundCircumference(circle.latitude,circle.longitude,circle.radius/1000)
                this.bounds.push(circleBounds[0])
                this.bounds.push(circleBounds[1])
                this.bounds.push(circleBounds[2])
                this.bounds.push(circleBounds[3])
            })
        }
        
      
      if(tempPolygons.length>0){
            var i,j
            for(i =0;i<tempPolygons.length;i++){
                var tempPolygon = tempPolygons[i]
                
                for(j=0;j<tempPolygon.length;j++){
                    var point = {
                      latitude:tempPolygon[j].lat,
                      longitude:tempPolygon[j].lng
                    }    
                    this.bounds.push(point)  
                }
            }
        }
      
    }
  setBounds(){
    let myInterval = setInterval(()=>{
            if ( this.mapView ) {
                clearInterval(myInterval)
              if(this.bounds.length==0) return
              this.mapView.fitToCoordinates(this.bounds)
          }
      }, 500)
  }
  createHole() {
    const { editing, creatingHole } = this.state;
    if (!creatingHole) {
      this.setState({
        creatingHole: true,
        editing: {
          ...editing,
          holes: [
            ...editing.holes,
            [],
          ],
        },
      });
    } else {
      const holes = [...editing.holes];
      if (holes[holes.length - 1].length === 0) {
        holes.pop();
        this.setState({
          editing: {
            ...editing,
            holes,
          },
        });
      }
      this.setState({ creatingHole: false });
    }
  }

  onPress(e) {
    
    // if(!!this.props.showMode){
    //   return
    // }
    //this.lastRegion=region
    //this.setState({region:this.lastRegion})
    const { editing, creatingHole } = this.state;
    if (!editing) {
      this.setState({
        editing: {
          id: id++,
          coordinates: [e.nativeEvent.coordinate],
          holes: [],
        },
      });
      //this.setState({region:this.lastRegion})
    }
    else if (!creatingHole) {
      this.setState({
        editing: {
          ...editing,
          coordinates: [
            ...editing.coordinates,
            e.nativeEvent.coordinate,
          ],
        },
      });
    } 
    else {
      // const holes = [...editing.holes];
      // holes[holes.length - 1] = [
      //   ...holes[holes.length - 1],
      //   e.nativeEvent.coordinate,
      // ];
      this.setState({
        editing: {
          ...editing,
          id: id++, // keep incrementing id to trigger display refresh
          coordinates: [
            ...editing.coordinates,
          ],
          //holes,
        },
      });
    }
  }
  addCircle(e){
    if(!!this.props.showMode){
      return;
    }
    let {circles} = this.state
    var newCircle = {
       latitude:e.nativeEvent.coordinate.latitude,
       longitude:e.nativeEvent.coordinate.longitude,
       radius:this.props.radius
    }
    this.setState({
      circles: [...circles, newCircle]
    })
    
  }
  done(){
      if(this.state.editing!=null){
        Alert.alert(
          'Geofence',
          "You didn't finish polygon edition",
          [
            {text: 'Cancel', onPress: () =>{return;}, style: 'cancel'},
            {text: 'OK', onPress: () => this.goBack()},
          ],
          { cancelable: false }
        )
      }else{
        this.goBack()
      }
     
  }
  goBack(){
    this.state.polygons.forEach(polygon => {
      var temp=[]
      polygon.coordinates.forEach(coordinate => {
        temp.push({lat:coordinate.latitude,lng:coordinate.longitude})
      });
      this.state.tempDrawingObjects.polygonObjects.push(temp)
    });

    this.state.circles.forEach(circle => {
      var ele = {latitude:circle.latitude,longitude:circle.longitude,radius:this.props.radius}
      this.state.tempDrawingObjects.circleObjects.push(ele)
    });

    var str = JSON.stringify(this.state.tempDrawingObjects);
    this.props.updateGeofence(str)
    Actions.pop()
  }
  onRegionChange(region) {
    //console.log('region on onregionChange for watch',region)
    this.lastRegion=region
    //this.setState({region})
  }
  render() {
    const mapOptions = {
      scrollEnabled: true,
    };

    // if (this.state.editing) {
    //   mapOptions.scrollEnabled = false;
    //   mapOptions.onPanDrag = e => this.onPress(e);
    // }

    return (
      <View style={{flex:1,paddingTop: Platform.OS === 'android' ? 24 : 0}}>
      <HeaderBar 
          title={'Geofence Editor'}
          rightIcon={"md-checkmark"}
          rightCallback={() => this.done()}
          forbidAutoBack = {false}
      />
      <GooglePlacesAutocomplete
          placeholder="Search"
          minLength={2} // minimum length of text to search
          autoFocus={false}
          returnKeyType={'search'} // Can be left out for default return key https://facebook.github.io/react-native/docs/textinput.html#returnkeytype
          listViewDisplayed="false" // true/false/undefined
          fetchDetails={true}
          renderDescription = {row=>row.description}
          onPress={(data, details = null) => {
            //console.log('autocomplete fitting....')
            var res = {
              latitude: Number(details.geometry.location.lat),
              longitude: Number(details.geometry.location.lng),
              latitudeDelta: this.lastRegion.latitudeDelta,
              longitudeDelta: this.lastRegion.longitudeDelta,
            }
            setTimeout(()=>{
              this.setState({region:res})
            },500)
            
          }}
          getDefaultValue={() => {
            return ''; // text input default value
          }}
          query={{
            // available options: https://developers.google.com/places/web-service/autocomplete
            key: 'AIzaSyC6RQ7uqe7qaRdcAO7Nz8gx7R_mbjkUHkA',
            //language: 'en', // language of the results
            //types: '(cities)', // default: 'geocode'
          }}
          styles={{
            description: {
              fontWeight: 'bold',
            },
            predefinedPlacesDescription: {
              color: '#1faadb',
            },
            container: {
              flex:null
            }
          }}
          //currentLocation={true} // Will add a 'Current location' button at the top of the predefined places list
          //currentLocationLabel="Current location"
          nearbyPlacesAPI="GooglePlacesSearch" // Which API to use: GoogleReverseGeocoding or GooglePlacesSearch
          GoogleReverseGeocodingQuery={{
            // available options for GoogleReverseGeocoding API : https://developers.google.com/maps/documentation/geocoding/intro
          }}
          GooglePlacesSearchQuery={{
            // available options for GooglePlacesSearch API : https://developers.google.com/places/web-service/search
            //rankby: 'distance',
            //types: 'food',
          }}
          filterReverseGeocodingByTypes={[
            //'locality',
            //'administrative_area_level_3',
          ]} // filter the reverse geocoding results by types - ['locality', 'administrative_area_level_3'] if you want to display only cities
          predefinedPlaces={[this.currentPlace]}
          debounce={200}
        />
      <View style={styles.container}>
        <MapView
          provider={this.props.provider}
          style={styles.map}
          // onLayout = {this.onMapLayout}  
          onRegionChange = {(e)=>{this.onRegionChange(e)}}
          ref={(el) => (this.mapView = el)}
          mapType={MAP_TYPES.STANDARD}
          initialRegion = {this.state.initialRegion}
          region = {this.state.region}
          //onPress={e => this.onPress(e)}
          //onLongPress={e=>this.addCircle(e)}
          {...mapOptions}
        >
          {this.state.coordinates&&<MapView.Polyline
              coordinates={this.state.coordinates}
              strokeColor={commonColors.theme}
              fillColor={commonColors.theme}
              strokeWidth={5}
          />}
          { 
            (!!this.props.target) &&
            <MapView.Marker 
              coordinate={this.props.target}
              //onDragEnd={(e) => {this.setState({ curPos: e.nativeEvent.coordinate });this.setRegion(e.nativeEvent.coordinate)}}
            />
          }
          {this.state.polygons.map((polygon,index) => (
            <Polygon
              key={'polygon-'+index}
              coordinates={polygon.coordinates}
              //holes={polygon.holes}
              strokeColor="#F00"
              fillColor="rgba(255,0,0,0.5)"
              strokeWidth={2}
            />
          ))}
           {this.state.B_polygons.map((polygon,index) => (
            <Polygon
              key={'polygon-'+index}
              coordinates={polygon.coordinates}
              //holes={polygon.holes}
              strokeColor="#F00"
              fillColor="rgba(0,255,0,0.2)"
              strokeWidth={2}
            />
          ))}
          {this.state.editing && (
            <Polygon
              key={this.state.editing.id}
              coordinates={this.state.editing.coordinates}
              //holes={this.state.editing.holes}
              strokeColor="#000"
              fillColor="rgba(255,0,0,0.5)"
              strokeWidth={2}
            />
          )}
          {this.state.circles.map((circle,index) => (
            <MapView.Circle
                key = {'circle-'+index}
                center = {{ latitude: circle.latitude || 30, longitude: circle.longitude || 120 }}
                radius = { circle.radius }
                strokeColor = "#4F6D7A"
                fillColor="rgba(255,0,0,0.2)"
                strokeWidth = { 2 }
            />))
          } 
             {this.state.B_circles.map((circle,index) => (
            <MapView.Circle
                key = {'circle-'+index}
                center = {{ latitude: circle.latitude || 30, longitude: circle.longitude || 120 }}
                radius = { circle.radius }
                strokeColor = "#4F6D7A"
                fillColor="rgba(0,255,0,0.2)"
                strokeWidth = { 2 }
            />))
          } 
           <MapView.Circle
                key = {'circle-d'+3}
                center = {{ latitude: Number(this.props.data.DestinationLatitude) || 30, longitude: Number(this.props.data.DestinationLongitude) || 120 }}
                radius = { 500 }
                strokeColor = "#4F6D7A"
                fillColor="rgba(0,0,255,0.2)"
                strokeWidth = { 2 }
            />
          <MapView.Marker draggable
            coordinate={this.state.targetPos}
            onDragEnd={(e) => this.setState({ curPos: e.nativeEvent.coordinate })}
          />
        </MapView>
        <View style={styles.buttonContainer}>
        
            <TouchableOpacity
              onPress={() => this.uploadLocation()}
              style={[styles.bubble, styles.button]}
            >
              <Text style={{color:'rgb(0,255,0)'}}>Finish</Text>
            </TouchableOpacity>
        
          <Text style={{color:'rgb(0,0,255)'}}>{this.state.curPos.latitude},{this.state.curPos.longitude}</Text>
         
        </View>
      </View>
     </View>
    );
  }
  get4PointsAroundCircumference(latitude, longitude, radius) {
    const earthRadius = 6378.1 //Km
    const lat0 = latitude + (-radius / earthRadius) * (180 / Math.PI)
    const lat1 = latitude + (radius / earthRadius) * (180 / Math.PI)
    const lng0 = longitude + (-radius / earthRadius) * (180 / Math.PI) / Math.cos(latitude * Math.PI / 180);
    const lng1 = longitude + (radius / earthRadius) * (180 / Math.PI) / Math.cos(latitude * Math.PI / 180);
  
    return [{
        latitude: lat0,
        longitude: longitude
      }, //bottom
      {
        latitude: latitude,
        longitude: lng0
      }, //left
      {
        latitude: lat1,
        longitude: longitude
      }, //top
      {
        latitude: latitude,
        longitude: lng1
      } //right
    ]
  }
}

PolygonCreator.propTypes = {
  provider: ProviderPropType,
};

export default connect(
  state => ({
    alerts:state.common.alerts,
    location:state.common.location,
    locationMode:state.common.locationMode
  }),
  dispatch => ({
    actions: bindActionCreators(actions, dispatch)
  })
)(PolygonCreator);

const styles = StyleSheet.create({
  container: {
    //...StyleSheet.absoluteFillObject,
    flex:1,
    justifyContent: 'flex-end',
    alignItems: 'center',

  },
  map: {
    ...StyleSheet.absoluteFillObject,
  },
  bubble: {
    backgroundColor: 'rgba(50,50,50,0.7)',
    paddingHorizontal: 18,
    paddingVertical: 12,
    borderRadius: 20,
  },
  latlng: {
    width: 200,
    alignItems: 'stretch',
  },
  button: {
    width: 80,
    paddingHorizontal: 12,
    alignItems: 'center',
    marginHorizontal: 10,
  },
  buttonContainer: {
    flexDirection: 'row',
    marginVertical: 20,
    backgroundColor: 'transparent',
  },
});

//export default PolygonCreator;
