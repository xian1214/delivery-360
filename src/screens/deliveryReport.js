"use strict";

import React, { PureComponent } from "react";

import {
  StyleSheet,
  View,
  Platform,
  Modal,
  TouchableOpacity,
  Text,
  StatusBar,
  Image,
  TextInput,
  ScrollView,
  Alert,
  ActivityIndicator,
} from "react-native";


import * as commonColors from "../styles/colors";
import * as commonStyles from "../styles/styles";
import { screenWidth, screenHeight } from "../styles/styles";
import * as CONST from '../components/constant'
import * as actionTypes from "../store/common/actionTypes";

import { bindActionCreators } from "redux";
import { connect } from "react-redux";
// import * as actions from "../redux/actions";
import * as actions from '../store/common/actions'
import * as session from '../store/session/actions'

import {ImagePicker, Permissions, ImageManipulator} from 'expo'

import api from "../service/api";
import Cache from "../utils/cache";
import { Ionicons, MaterialCommunityIcons } from "@expo/vector-icons";
import { Actions } from "react-native-router-flux";
import HeaderBar from '../components/header'
import async from 'async'
import UtilService from '../utils/utils'
import { WaitingModal } from "../components/waiting";
import InformationBar from '../components/informationBar'
import moment from 'moment'
import * as config from '../config'



class DeliveryReport extends PureComponent {
    constructor(props) {
        super(props);

        this.state = {
            projectName:'Project Name',
            DONumber:'RT02342',
            clientName: 'Austin',
            clientAddress: 'No.1, Jalan Bukit Bintang, Kuala Lumpur',
            items:[
                {title:'Cement', quantity:'100kg', delivered:'100kg'},
                {title:'Steel', quantity:'800kg', delivered:'800kg'}
            ],
            pictures:[
                {image:null, area:'', comment:'',
                    DestinationLatitude:this.props.location.coords.latitude,
                    DestinationLongitude:this.props.location.coords.longitude
                },
            ],
            isConfirmed: false,
            showSignModal: false,
            signatureUri:null,
            showConfirmModal: false,
            qrScanData: null,
            subRequests:this.props.currentDelivery.DeliveryNoteItems,
            // startUnloadingTime: this.props.currentDelivery.UnloadingTime?UtilService.getDateTime(this.props.currentDelivery.UnloadingTime):null,
            // doneUnloadingTime: this.props.currentDelivery.UnloadedTime?UtilService.getDateTime(this.props.currentDelivery.UnloadedTime):null,
            isWaiting: false,
            UnloadedTimeStarted: false,
            unloadingStarted: false,
            qacView:null
        };

        this.convert=[]
    }

    // onChat(){
    //     Actions.Chat({channel:this.props.currentDelivery.DeliveryNoteNumber})
    // }

    componentDidMount(){
       let {currentDelivery} = this.props;
       
       if(!!currentDelivery&&currentDelivery.DeliveryNoteType==2){
           this.updateQacItems()
       }
       let {subRequests} = this.state
       this.IntervalForBar = setInterval(()=>{
           let subRequests = [...this.state.subRequests]
           this.setState({subRequests})           
       },3000)
    }

    updateQacItems(){
        api.getQacItems(this.props.currentDelivery.DeliveryNoteID, (err, res)=>{
            // //console.log(err, res)
            if ( err == null ){
                this.setState({qacView:res})
            }
        })
    }

    submit(){
        let {currentDelivery, actions} = this.props
        if ( currentDelivery == null ){
            alert('Failed!')
            return;
        }
        api.createAlert({
            Type: commonStyles.SignOff,
            Level: CONST.EVENT_LEVEL,
            Message: '',
            ReferenceID: currentDelivery.DeliveryNoteID,
            UserID: currentDelivery.User.UserID
        }, (err1, res1)=>{
            actions.changeQueueState(false)
            if ( Cache.hasInternetConnection ){
                actions.updateCurrentDelivery(currentDelivery.DeliveryNoteID)
            }else{
                currentDelivery.IsSignOff = true
                actions.manualUpdate(currentDelivery)
            }
            if(Cache.hasInternetConnection){
                var tempAfterPhoto = ''
                this.state.pictures.map((item)=>{
                    if(!!item.image){
                        tempAfterPhoto = item.image.uri
                    }
                    api.createPhoto(currentDelivery.DeliveryNoteID, {
                        DeliveryNoteID: currentDelivery.DeliveryNoteID,
                        Area: item.area,
                        Remark: item.comment,
                        AfterPhoto: tempAfterPhoto,
                        DestinationLatitude:item.DestinationLatitude,
                        DestinationLongitude:item.DestinationLongitude
                    }, (err, res)=>{
                            console.log('update---deliveryphoto--',res)
                    })
                })
            }
            else{
                this.state.pictures.map((item)=>{
                    api.createPhoto(currentDelivery.DeliveryNoteID, {
                        DeliveryNoteID: currentDelivery.DeliveryNoteID,
                        Area: item.area,
                        Remark: item.comment,
                        AfterPhoto: item.image
                    }, (err, res)=>{
                
                    })
                })
            }

            
        })

        
        
        this.setState({showConfirmModal:true})
    }

    renderConfirmModal(){
        return (
            <Modal
            visible={this.state.showConfirmModal}
            transparent={true}
            onRequestClose={() => {}}
            >
            <View style={styles.modalContainer}>
                <View style={[styles.modal,{width:300}]}>
                <Text style={{color:'#333', margin:30}}>{Cache.getLang('message.delivery_completed')}</Text>
                <View style={{height:40, flexDirection:'row', width:'100%', borderBottomRightRadius:5, borderBottomLeftRadius:5, overflow:'hidden', borderTopWidth:1, borderTopColor:'#ccc'}}>
                    <TouchableOpacity style={{width:50}} onPress={()=>{
                        this.setState({showConfirmModal:false})
                        Actions.LastScreen({data:this.props.currentDelivery, signature:[]})                        
                    }} style={{flex:1, alignItems:'center', justifyContent:'center', backgroundColor:'rgb(240,250,253)'}}>
                    <Text style={{color:commonColors.theme, fontSize:13, fontWeight:'bold'}}>Done</Text>
                    </TouchableOpacity>
                </View>
                </View>
            </View>
            </Modal>
        );
    }
    pictureSave(cb){
        var currentDelivery = this.props.currentDelivery
        if(Cache.hasInternetConnection){
            var tempAfterPhoto = ''
            this.state.pictures.map((item,index)=>{
                if(!!item.image){
                    tempAfterPhoto = item.image.uri
                }
                api.createPhoto(currentDelivery.DeliveryNoteID, {
                    DeliveryNoteID: currentDelivery.DeliveryNoteID,
                    Area: item.area,
                    Remark: item.comment,
                    AfterPhoto: tempAfterPhoto,
                    DestinationLatitude:item.DestinationLatitude,
                    DestinationLongitude:item.DestinationLongitude
                }, (err, res)=>{
                        console.log('update---deliveryphoto--',res)
                        if(index==this.state.pictures.length-1){
                            cb(err,res)
                        }
                })
            })
        }
        else{
            this.state.pictures.map((item)=>{
                api.createPhoto(currentDelivery.DeliveryNoteID, {
                    DeliveryNoteID: currentDelivery.DeliveryNoteID,
                    Area: item.area,
                    Remark: item.comment,
                    AfterPhoto: item.image
                }, (err, res)=>{
                    console.log('update---deliveryphoto--',res)
                    if(index==this.state.pictures.length-1){
                        cb(err,res)
                    }
                })
            })
        }
    }
    onConfirm(){
        // var currentDelivery = this.props.currentDelivery
        // console.log('this.state.pictures',this.state.pictures)
        // this.state.pictures.map((item,index)=>{
        //     api.createPhoto(currentDelivery.DeliveryNoteID, {
        //         DeliveryNoteID: currentDelivery.DeliveryNoteID,
        //         Area: item.area,
        //         Remark: item.comment,
        //         AfterPhoto: item.image
        //     }, (err, res)=>{
        //         console.log('res.photoes--->',res)
        //         if(index==this.state.pictures.length-1){
        //             Actions.DeliverySecondReport({data:this.props.currentDelivery})
        //         }
        //     })
        // })
        
        // return
        this.pictureSave((error,res)=>{
            console.log('error,res',error,res)
            this.setState({isConfirmed:true})
            Actions.DeliverySecondReport({data:this.props.currentDelivery})
        })

        return
        if ( this.state.isConfirmed ){
            this.submit()
        }else{
                Actions.Signature({data: this.props.currentDelivery, done:(qrScanData, signatureUri)=>{
                    this.setState({isConfirmed:true, qrScanData, signatureUri:null})
                    let image = {
                        uri: signatureUri,
                        type: "image/png",
                        name: "file.png"
                    };

                    //console.log('signatureUri....on actions.Signature---on delivery report',signatureUri)
                    this.props.actions.setCustomerSignature(signatureUri);
                    api.uploadFile(image, (err, res)=>{
                        if ( err == null ){
                            this.setState({Signature:res.filePath, signatureUri})
                            api.uploadSign(this.props.currentDelivery.DeliveryNoteID, 
                                (Cache.hasInternetConnection?res.filePath: res), (err, res)=>{
                                if ( err ){
                                    Alert.alert('Failed!')
                                }
                            })
                        }else{
                            Alert.alert('Failed.')
                        }
                    })
                }})
        }
    }

    async takePicture(index){
        let res = await Permissions.askAsync(Permissions.CAMERA)
        if ( res.status ==='granted'){
            let {status} = await Permissions.askAsync(Permissions.CAMERA_ROLL)
            if ( status === 'granted' ){
                let image = await ImagePicker.launchCameraAsync({
                    quality:0.3
                })
                if ( !image.cancelled ){
                    this.setState({isWaiting:true})
                    const manipResult = await ImageManipulator.manipulateAsync(
                        image.uri,
                        [{resize:{width:768}}],
                        { format: 'jpeg', compress:0.6 }
                    );
                    api.uploadImage(manipResult.uri, (err, res)=>{
                        if ( err == null ){
                            if (Cache.hasInternetConnection){
                                this.state.pictures[index].image={uri:res.filePath}
                                this.state.pictures[index].DestinationLatitude= this.props.location.coords.latitude
                                this.state.pictures[index].DestinationLongitude= this.props.location.coords.longitude
                            }else{
                                //console.log('arrived res on upload image..this.props.location.coords...',this.props.location.coords)
                                this.state.pictures[index].image=res
                                this.state.pictures[index].DestinationLatitude= this.props.location.coords.latitude
                                this.state.pictures[index].DestinationLongitude= this.props.location.coords.longitude
                                this.convert[res] = {uri:manipResult.uri}
                            }
                            
                            this.setState({pictures:[...this.state.pictures]}) 
                        }     
                        this.setState({isWaiting:false})   
                    })

                    
                }
                
            }
        }
    }


    addPicture(){
        this.state.pictures.push({image:null, area:'', comment:'',   
                DestinationLatitude:this.props.location.coords.latitude,
                DestinationLongitude:this.props.location.coords.longitude
            })
        this.setState({pictures:[...this.state.pictures]})
    }



    startUnloading(){
        let {currentDelivery} = this.props
        if ( !!currentDelivery&&currentDelivery.startUnloadingTime || this.state.unloadingStarted ) return
        this.setState({unloadingStarted:true}, ()=>{
            api.createAlert({
                Type: commonStyles.Unloading,
                Level: 3,
                Message: 'started unloading...',
                ReferenceID: this.props.currentDelivery.DeliveryNoteID,
                UserID: this.props.currentDelivery.User.UserID
            }, (err1, res1)=>{
                if ( Cache.hasInternetConnection ){
                    this.props.actions.updateCurrentDelivery(this.props.currentDelivery.DeliveryNoteID)
                }else{
                    this.props.currentDelivery.UnloadingTime= new Date()
                    this.props.actions.manualUpdate(this.props.currentDelivery)
                }
                this.setState({unloadingStarted:false})
            })
        })
    }
  
    doneUnloading(){
        if ( this.props.currentDelivery.UnloadingTime == null || this.props.currentDelivery.UnloadedTime ) return;
        this.setState({UnloadedTimeStarted:true})
        api.createAlert({
            Type: commonStyles.Unloaded,
            Level: 3,
            Message: 'done to unload...',
            ReferenceID: this.props.currentDelivery.DeliveryNoteID,
            UserID: this.props.currentDelivery.User.UserID
        }, (err1, res1)=>{
            if ( Cache.hasInternetConnection ){
                this.props.actions.updateCurrentDelivery(this.props.currentDelivery.DeliveryNoteID)
            }else{
                this.props.currentDelivery.UnloadedTime = new Date()
                this.props.actions.manualUpdate(this.props.currentDelivery)
            }
            this.setState({UnloadedTimeStarted:false})
        })
    }

    clickChecklist(item, index){
        if ( !this.state.isConfirmed ) {
            Actions.CheckList({item, index, update:()=>{
                //console.log("on delivery report ",(!!this.props.currentDelivery)?'PASS':'NOPASS')
                if(!!this.props.currentDelivery){
                    this.setState({subRequests:this.props.currentDelivery.DeliveryNoteItems})
                }
                else{
                    Alert.alert('Current delivery is null')
                }                
            }})
        }
    }

    showAll(){
        if ( this.state.showIndex == -2 ){
            this.setState({showIndex:-1})
        }else{
            this.setState({showIndex:-2})
        }
    }

    showIndex(index){
        if ( this.state.showIndex == index ){
            this.setState({showIndex:-1})
        }else{
            this.setState({showIndex:index})
        }
    }

    addItem(){
        Actions.AddItem({update:()=>{
            this.updateQacItems()
        }})
    }


    renderDetail(index){
        let {results, before} = this.state.subRequests[index]
        if ( !results || !before ) return null
        return(
            <View>
                {/* <Text style={{color:'grey', fontSize:14, marginLeft:20,}}>Description of cement</Text> */}
                {results.map((result, index)=>{
                    return(
                        <View key={index} style={{paddingHorizontal:20}}>
                            <Text style={{color:'#ccc', fontSize:13}}>{before[index].Name}</Text>
                            <Text style={{color:'grey', fontSize:14, marginTop:5}}>{result.Value}</Text>
                            <Image source={{uri:result.Images}} style={{width:60, height:60, marginTop:10}}/>
                            <View style={{width:'100%', height:1, backgroundColor:'#ccc', marginVertical:10}}/>
                        </View>
                    )
                })}
            </View>
        )
    }

  renderQacDetail(item){

    return(
        <View>
            {item.CheckResults.map((result, index)=>{
                result.ImageArray = result.Images.split(',')
                return(
                    <View key={index} style={{paddingHorizontal:20}}>
                      <Text style={{color:'#ccc', fontSize:13, marginTop:10}}>{result.CheckItemName}</Text>
                      <Text style={{color:'grey', fontSize:14, marginTop:5}}>{result.Value}</Text>
                      
                      <View style={{width:'100%', height:1, backgroundColor:'#ccc', marginTop:10}}/>
                    </View>
                )
            })}
        </View>
    )
  }

  editQacItem(qacItem){
      Actions.AddItem({item:qacItem, update:()=>{
          this.updateQacItems()
      }})
  }

  render() {
      //if ( this.props.currentDelivery == null ) return null;
      //let {DeliveryNoteNumber, IssueTime} = this.props.currentDelivery
      let {internetConnection, currentDelivery} = this.props
    return (
      <View style={styles.container}>
        <InformationBar iConnection={internetConnection} currentDelivery={currentDelivery}/>   
        <HeaderBar title={!!currentDelivery&&currentDelivery.DeliveryNoteNumber} 
            back={()=>{console.log('good to back');Actions.pop()}} 
            rightElement={(
                <View style={{flexDirection:'row', alignItems:'center'}}>
                    <TouchableOpacity  onPress={()=>Actions.AlertListByDn({dnID:currentDelivery.DeliveryNoteID})} style={{padding:10}}>
                        <Ionicons name={'md-warning'} size={24} color={'white'}/>
                    </TouchableOpacity>
                    <TouchableOpacity style={{marginLeft:15, padding:10}} onPress={()=>Actions.Chat({channel:currentDelivery})}>
                        <Ionicons name={'md-chatboxes'} size={24} color={'white'}/>
                    </TouchableOpacity>
                </View>
            )}
        />
        <ScrollView style={{marginBottom:70}}>
        <View>
            {!this.state.isConfirmed&&<View>
                <View style={{flexDirection:'row', alignItems:'center', padding:15}}>
                    <Text style={{fontWeight:'bold', color:'grey', fontSize:16, flex:1}}>{Cache.getLang('label.start_unloading')}</Text>
                    <Text style={{fontSize:15, color:'#333', width:150}}>{!!currentDelivery&&currentDelivery.UnloadingTime?moment(currentDelivery.UnloadingTime).format('hh:mm A'):'-'}</Text>
                    <TouchableOpacity onPress={()=>this.startUnloading()} style={{width:30, height:30, borderRadius:15, backgroundColor:(!!currentDelivery&&currentDelivery.UnloadingTime)?commonColors.theme:'white', borderColor:commonColors.theme, borderWidth:((!!currentDelivery)&&currentDelivery.startUnloadingTime)?0:1, alignItems:'center', justifyContent:'center'}}>
                        {!!currentDelivery&&currentDelivery.UnloadingTime&&<Ionicons name={'md-checkmark'} size={20} color={'white'}/>}
                        {!!currentDelivery&&currentDelivery.UnloadingTime==null&&this.state.unloadingStarted &&
                        <ActivityIndicator/>}
                    </TouchableOpacity>
                </View>
                <View style={{flexDirection:'row', alignItems:'center', padding:15}}>
                    <Text style={{fontWeight:'bold', color:'grey', fontSize:16, flex:1}}>{Cache.getLang('label.done_unloading')}</Text>
                    <Text style={{fontSize:15, color:'#333', width:150}}>{!!currentDelivery&&currentDelivery.UnloadedTime?moment(currentDelivery.UnloadedTime).format('hh:mm A'):'-'}</Text>
                    <TouchableOpacity onPress={()=>this.doneUnloading()} style={{width:30, height:30, borderRadius:15, backgroundColor:(!!currentDelivery&&currentDelivery.UnloadedTime)?commonColors.theme:'white', borderColor:commonColors.theme, borderWidth:((!!currentDelivery)&&currentDelivery.doneUnloadingTime)?0:1, alignItems:'center', justifyContent:'center'}}>
                        {!!currentDelivery&&currentDelivery.UnloadedTime&&<Ionicons name={'md-checkmark'} size={20} color={'white'}/>}
                        {!!currentDelivery&&currentDelivery.UnloadedTime==null&&this.state.UnloadedTimeStarted &&
                        <ActivityIndicator/>}
                    </TouchableOpacity>
                </View>
            </View>}
            <View style={{flexDirection:'row', alignItems:'center', height:40, width:'100%', paddingHorizontal:20, backgroundColor:'rgb(228,228,233)', elevation:4}}>
                <Text style={{color:'grey', flex:1}}>{Cache.getLang('label.item')}</Text>
                <Text style={{color:'grey', width:100, textAlign:'right'}}>{Cache.getLang('label.quantity')}</Text>
                {this.state.isConfirmed&&<TouchableOpacity onPress={()=>this.showAll()} style={{marginLeft:80}}>
                    <Ionicons name={"ios-arrow-down"} size={24} color={'rgb(80,118,177)'} style={{width:20}}/>
                </TouchableOpacity>}
                {!this.state.isConfirmed&&<Text style={{color:'grey', width:100, textAlign:'right'}}>{Cache.getLang('label.checklist')}</Text>}
            </View>
            {!!currentDelivery&&currentDelivery.DeliveryNoteType!=CONST.DELIVERY_TYPE_QAC&&this.state.subRequests.map((item, index)=>{
                var shelfTime = item.Item.ShelfLifeTime * 60
                var issued2CurrentTimeSpan = UtilService.getPassTimeBySecondFrom(currentDelivery.IssueTime) //s
                var overTimeSpan = issued2CurrentTimeSpan-shelfTime
                var dueTimeString = UtilService.convertToHourMinSec(Math.abs(overTimeSpan))
                var progressPercent = 0
                var localTime = UtilService.getLocalTime(currentDelivery.IssueTime)
                if(overTimeSpan<0){
                    progressPercent = UtilService.getTimeSpanPercent(issued2CurrentTimeSpan,shelfTime)
                }
                else{
                    progressPercent = UtilService.getTimeSpanPercent(overTimeSpan,shelfTime)
                }
               
                return(
                    <View key={index} style={{width:'100%'}}>
                        <View style={{flexDirection:'row', alignItems:'center', width:'100%', paddingHorizontal:20, paddingVertical:8, backgroundColor:index%2==0?'white':'#eee', elevation:4}}>
                            <View style={{flex:1}}>
                                <View style={{flexDirection:'row', alignItems:'center'}}>
                                    <Text style={{color:'black', flex:1, fontWeight:'bold'}}>{item.Item.Name}</Text>
                                    <Text style={{color:'grey', width:100}}>{item.PlannedQuantity-item.ReturnedQuantity} {(!!item.Item.Uom)?item.Item.Uom.Name:''}</Text>
                                </View>
                                {!this.state.isConfirmed && (!!item.Item.ShelfLifeTime) && item.Item.ShelfLifeTime>0 && <View style={{flexDirection:'row', alignItems:'center', marginTop:5}}>
                                    <View>
                                        <Text style={{fontSize:13, color:'grey'}}>{Cache.getLang('label.shelflife')}</Text>
                                        <Text style={{fontSize:13, color:'black', fontWeight:'bold'}}>{Math.floor(item.Item.ShelfLifeTime/60)} hr {item.Item.ShelfLifeTime%60} min</Text>
                                    </View>
                                    <View style={{flexDirection:'row', alignItems:'center', width:'100%', paddingHorizontal:20}}>
                                        <View style={{flex:1, height:3, backgroundColor:'#eee'}}>
                                            {
                                                !!currentDelivery && currentDelivery.IssueTime &&
                                                    <View style={{width:progressPercent+'%', backgroundColor:(overTimeSpan<0)?commonColors.blue:commonColors.red, height:3}}></View>
                                                    // <View style={{width:'50%', backgroundColor:commonColors.blue, height:3}}></View>
                                            }
                                        </View>
                                        <Text style={{color:(overTimeSpan>0)?'red':'grey', marginLeft:10, marginRight:100}}>{((overTimeSpan>0)?'+ ':'') + dueTimeString}</Text>
                                    </View>
                                </View>}
                            </View>
                            <TouchableOpacity onPress={()=>!this.state.isConfirmed?this.clickChecklist(item, index):this.showIndex(index)}>
                                <Ionicons name={this.state.isConfirmed?
                                    ((this.state.showIndex == index || this.state.showIndex==-2)?"ios-arrow-up":"ios-arrow-down")
                                    :"md-list"} size={24} color={'rgb(80,118,177)'} style={{width:20}}/>
                            </TouchableOpacity>
                        </View>
                        {(this.state.showIndex == index || this.state.showIndex==-2)&&this.renderDetail(index)}
                    </View>
                )
            })}
            {!!this.props.currentDelivery && this.props.currentDelivery.DeliveryNoteType==CONST.DELIVERY_TYPE_QAC&&<View style={{width:'100%'}}>
                <View style={{width:'100%'}}>
                {
                    this.state.qacView&&this.state.qacView.QacItems.map((item, index)=>{
                        return(
                            <View key={index} style={{width:'100%'}}>
                                <View style={{flexDirection:'row', alignItems:'center', width:'100%', paddingHorizontal:20, paddingVertical:8, backgroundColor:index%2==0?'white':'#eee', elevation:4}}>
                                    <View style={{flex:1}}>
                                        <View style={{flexDirection:'row', alignItems:'center'}}>
                                            <Text style={{color:'black', flex:1, fontWeight:'bold'}}>{item.Note?item.Note.split(',')[0]:''}</Text>
                                            <Text style={{color:'grey', width:100}}>1</Text>
                                        </View>
                                    </View>
                                    <TouchableOpacity onPress={()=>!this.state.isConfirmed?this.editQacItem(item):this.showIndex(index)}>
                                        <Ionicons name={this.state.isConfirmed?
                                            ((this.state.showIndex == index || this.state.showIndex==-2)?"ios-arrow-up":"ios-arrow-down")
                                            :"md-list"} size={24} color={'rgb(80,118,177)'} style={{width:20}}/>
                                    </TouchableOpacity>
                                </View>
                                {(this.state.showIndex == index || this.state.showIndex==-2)&&this.renderQacDetail(item)}
                            </View>
                        )
                    })
                }
                </View>
                <View>
                    {!this.state.isConfirmed&&<View style={{width:'100%', alignItems:'center', justifyContent:'center', marginTop:20}}>
                        <TouchableOpacity onPress={()=>this.addItem()} style={{height:36, width:200, borderRadius:18, borderWidth:2, borderColor:'rgb(0,185,237)', alignItems:'center', justifyContent:'center', flexDirection:'row'}}>
                            <Ionicons name={'md-add'} size={20} color={'rgb(0,185,237)'}/>
                            <Text style={{fontSize:16, fontWeight:'bold', color:'rgb(0,185,237)', marginLeft:10}}>{Cache.getLang('label.add_another')}</Text>
                        </TouchableOpacity>
                    </View>}
                </View>
            </View>}
            </View>
            <View style={{width:'100%', backgroundColor:'rgb(228,228,233)', paddingVertical:8, paddingHorizontal:15, elevation:4, marginTop:10}}>
                <Text style={{color:'grey', fontSize:13}}>{Cache.getLang('label.remarks')}</Text>
            </View>
            <View style={{backgroundColor:'white', elevation:4}}>
                {this.state.pictures && this.state.pictures.map((item, index)=>{
                    let image = item.image;
                    if(!!image&& (!!image.uri)){
                        var temp = {
                            uri:config.SERVICE_FILE_URL+image.uri
                        }
                        image = temp
                    }                                      
                    if ( typeof item.image === 'number' ){
                        image = this.convert[item.image]
                    }
                    return(
                        <View key={index} style={{flexDirection:'row', paddingHorizontal:20, paddingVertical:10}}>
                            {item.image==null&&<TouchableOpacity onPress={()=>this.takePicture(index)} style={{borderColor:'rgb(0,185,237)', borderWidth:2, alignItems:'center', justifyContent:'center', width:90, height:90}}>
                                <Ionicons name={'ios-camera'} color={commonColors.theme} size={30}/>
                            </TouchableOpacity>}
                            {item.image!=null&&<TouchableOpacity onPress={()=>this.takePicture(index)} style={{width:90, height:90}}>
                                <Image source={image} style={{flex:1, resizeMode:'cover'}}/>
                            </TouchableOpacity>}
                            <View style={{marginLeft:15, flex:1}}>
                                {!this.state.isConfirmed&&<View>
                                    <View style={{height:3}}/>
                                    <TextInput
                                        ref="area"
                                        autoCapitalize="none"
                                        autoCorrect={false}
                                        placeholder={Cache.getLang('label.area')}
                                        placeholderTextColor={commonColors.placeholderText}
                                        textAlign="left"
                                        style={styles.input}
                                        underlineColorAndroid="transparent"
                                        returnKeyType={"next"}
                                        value={item.area}
                                        onChangeText={text =>{
                                            item.area = text.replace(/\t/g, "")
                                            this.setState({ pictures: [...this.state.pictures]  })
                                        }}
                                    />
                                    <View style={{height:10}}/>
                                    <TextInput
                                        ref="comment"
                                        autoCapitalize="none"
                                        autoCorrect={false}
                                        placeholder={Cache.getLang('label.comment')}
                                        placeholderTextColor={commonColors.placeholderText}
                                        textAlign="left"
                                        style={styles.input}
                                        underlineColorAndroid="transparent"
                                        value={item.comment}
                                        onChangeText={text =>{
                                            item.comment = text.replace(/\t/g, "")
                                            this.setState({ pictures: [...this.state.pictures]  })
                                        }}
                                    />
                                </View>}
                                {this.state.isConfirmed&&<View style={{marginTop:10, paddingRight:20}}>
                                    <View style={{flexDirection:'row', alignItems:'flex-end'}}>
                                        <Text style={{fontSize:13, color:'grey'}}>Area</Text>
                                        <Text style={{fontSize:15, color:'black', marginLeft:10}}>{item.area}</Text>
                                    </View>
                                    <Text style={{fontSize:15, color:'black', marginTop:10}}>{item.comment}</Text>
                                </View>}
                            </View>
                            <TouchableOpacity onPress={()=> Actions.PlacePicker({destination:{latitude:item.DestinationLatitude, longitude:item.DestinationLongitude}})} 
                                style={{width:40,justifyContent:'center',alignItems:'flex-end'}}>
                                <Ionicons name={'md-pin'} size={30} color={'rgb(0,185,237)'}/>
                            </TouchableOpacity>
                        </View>
                    )
                })}
            </View>
                        {!this.state.isConfirmed&&<View style={{width:'100%', alignItems:'center', justifyContent:'center', marginTop:20}}>
                <TouchableOpacity onPress={()=>this.addPicture()} style={{height:36, width:200, borderRadius:18, borderWidth:2, borderColor:'rgb(0,185,237)', alignItems:'center', justifyContent:'center', flexDirection:'row'}}>
                    <Ionicons name={'md-add'} size={20} color={'rgb(0,185,237)'}/>
                    <Text style={{fontSize:16, fontWeight:'bold', color:'rgb(0,185,237)', marginLeft:10}}>{Cache.getLang('label.add_another')}</Text>
                </TouchableOpacity>
            </View>}
            <Text style={{color:'grey',fontWeight:'bold', fontSize:15, marginLeft:15, marginTop:20}}>
                {Cache.getLang('label.report_agree')}
            </Text>
            {this.state.isConfirmed&&this.state.signatureUri&&<Image source={{uri:this.state.signatureUri}} style={{width:'100%', height:250, resizeMode:'contain'}}/>}
            {this.state.isConfirmed&&this.state.qrScanData&&<View style={{flexDirection:'row', justifyContent:'center'}}>
                <Text style={{color:'#ccc', fontSize:13}}>Acceptor:</Text>
                <Image source={{uri:this.state.acceptorUri}} style={{width:30, height:30, borderRadius:15, margin:8}}/>
                <Text style={{color:'#333', fontWeight:'bold', fontSize:15}}>{this.state.acceptorName}</Text>
            </View>}
            <View style={{height:20}}/>
        </ScrollView>
        <View style={{position:'absolute', bottom:0, left:0, width:'100%', alignItems:'center', backgroundColor:'transparent', padding:15, flexDirection:'row'}}>
            <TouchableOpacity onPress={()=>this.onConfirm()} style={{height:50, flex:1, alignItems:'center', justifyContent:'center', backgroundColor:'rgb(60,118, 177)', borderRadius:25, elevation:5}}>
                <Text style={{fontWeight:'bold', color:'white', fontSize:16}}>{this.state.isConfirmed?Cache.getLang('label.submit'):Cache.getLang('label.customer_signature')}</Text>
            </TouchableOpacity>
        </View>
        {/* {this.renderSignModal()} */}
        {this.renderConfirmModal()}
        <WaitingModal isWaiting={this.state.isWaiting}/>
      </View>
    );
  }
}

export default connect(
  state => ({
    currentDelivery: state.common.currentDelivery,
    internetConnection: state.common.internetConnection,
    location:state.common.location
  }),
  dispatch => ({
    actions: bindActionCreators(actions, dispatch)
  })
)(DeliveryReport);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop:24,
    backgroundColor: 'white'
  },
  input: {
    fontSize: 14,
    color: commonColors.title,
    height: 36,
    borderRadius: 3,
    marginBottom: 3,
    paddingHorizontal: 10,
    backgroundColor: commonColors.inputColor
  },
  modalContainer: {
    flex: 1,
    backgroundColor: "rgba(0, 0, 0,0.5)",
    alignItems: "center",
    justifyContent: "center"
  },
  modal: {
    width:360,
    borderRadius: 5,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "white",
    elevation:3,
  },
});
