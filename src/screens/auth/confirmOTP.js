"use strict";

import React, { PureComponent } from "react";
import {
  StyleSheet,
  Text,
  View,
  Image,
  TextInput,
  TouchableOpacity,
  Keyboard,
  TouchableWithoutFeedback,
  Alert,
  ActivityIndicator,
  Modal,
  ImageBackground
} from "react-native";

import { Actions } from "react-native-router-flux";

import * as commonStyles from "../../styles/styles";
import * as commonColors from "../../styles/colors";
import api from "../../service/api";
import HeaderBar from "../../components/header";
import Cache from '../../utils/cache'


export default class ConfirmOTP extends PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      otp: this.props.otp || "",
      rightCallback: null,
      isWaiting: false,
      limit:30,
    };
  }

  componentDidMount() {
    this.hasMounted = true
    this.countDown()
  }

  countDown(){
    let limit = 30
    var interval = setInterval(()=>{
      this.hasMounted&&this.setState({limit})
      if ( limit == 0 || !this.hasMounted ){
        clearInterval(interval)
      }
      limit = limit - 1;
    }, 1000)
  }

  componentWillUnmount() {
    this.hasMounted = false
  }

  onForgotPassword() {
    Actions.Forgot();
  }

  onCreateAccount() {
    Actions.Register();
  }

  onToggleConfirmPassword() {}

  goBack() {
    Actions.Intro();
  }

  submit(){
      Keyboard.dismiss()
      if ( this.state.otp==''){
        Alert.alert('Wrong OTP Number!')
        return;
      }
      this.setState({ isWaiting: true });
      api.confirmOTP(Cache.PhoneNumber, this.state.otp, Cache.pushToken, (err,res)=>{
        // //console.log(err, res)
        if ( err == null ){
          Actions.Main()
        }else{
          setTimeout(()=>Alert.alert('Wrong Authentication!'), 500)
        }
        this.setState({ isWaiting: false });
      })
      
  }

  resend(){
    this.setState({ isWaiting: true });
    api.login(Cache.clientID, Cache.PhoneNumber, Cache.Password, (err, res) => {
      if ( err == null ){
        setTimeout(()=>Alert.alert('Resent successfully!'), 500)
      }else{

      }
      this.setState({ isWaiting: false });
      this.countDown()
    });
  }

  renderIndicator() {
    return (
      <Modal
        visible={this.state.isWaiting}
        transparent={true}
        onRequestClose={() => {}}
      >
        <View style={styles.indicatorContainer}>
          <View style={styles.indicator}>
            <ActivityIndicator size="large" />
          </View>
        </View>
      </Modal>
    );
  }

  render() {
    return (
      <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
        <ImageBackground source={commonStyles.background} style={styles.container}>
          <HeaderBar title={"First Time Sign In"} />
          <View style={{marginTop:20, alignItems:'center'}}>
            <Text
              style={{
                color: "white",
                fontWeight: "bold",
                fontSize: 18,
                textAlign: "center",
                paddingHorizontal: 40
              }}
            >
              Enter the OTP code sent to your mobile number via SMS.
            </Text>
            <View style={styles.inputWrapper}>
              <TextInput
                ref="otp"
                autoCapitalize="none"
                autoCorrect={false}
                placeholder="OTP Code"
                placeholderTextColor={commonColors.placeholderText}
                textAlign="center"
                style={styles.input}
                underlineColorAndroid="transparent"
                keyboardType="phone-pad"
                value={this.state.otp}
                onChangeText={text =>
                  this.setState({ otp: text.replace(/\t/g, "") })
                }
              />
            </View>

            <TouchableOpacity
              activeOpacity={0.5}
              style={styles.loginButtonWrapper}
              onPress={() => this.submit()}
            >
              <View style={styles.buttonLogin}>
                <Text style={styles.textButton}>Submit</Text>
              </View>
            </TouchableOpacity>
          </View>

          <View style={{marginTop:50}}>
            <View style={styles.bottomContentWrap}>
              {this.state.limit<=0&&<TouchableOpacity onPress={()=>{this.resend()}}>
                <Text style={{ color: 'rgba(14, 115, 240, 0.5)', fontWeight: "bold" }}>
                  Resend OTP
                </Text>
              </TouchableOpacity>}
              {this.state.limit>0&&<View>
                <Text style={{fontSize:14, color:'rgba(14, 115, 240, 0.5)', fontWeight:'bold'}}>
                  Resend Again In {this.state.limit}S
                </Text>
              </View>}
            </View>
          </View>
          {this.renderIndicator()}
        </ImageBackground>
      </TouchableWithoutFeedback>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop:24,
    backgroundColor: "rgb(48, 48, 48)"
  },
  logoWrapper: {
    flex: 2,
    justifyContent: "center",
    alignItems: "center",
    marginTop: 60
  },
  logoContent: {
    width: 200,
    height: 120,
    resizeMode: "contain"
  },
  descriptionContainer: {
    marginTop: 20,
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  },
  inputContainer: {
    marginTop: 20,
    flex: 4,
    alignItems: "center"
  },
  bottomContainer: {
    flex: 4,
    justifyContent: "flex-start"
  },
  bottomContentWrap: {
    // flex: 1,
    justifyContent: "flex-start",
    alignItems: "center",
    paddingTop: 10
  },
  textTitle: {
    top: 20,
    color: "rgb(0, 0, 0)",
    //fontFamily: 'Blanch',
    fontSize: 20,
    backgroundColor: "transparent"
  },
  textDescription: {
    color: "rgb(0, 0, 0)",
    //fontFamily: 'OpenSans-Semibold',
    fontSize: 12,
    paddingTop: 5,
    backgroundColor: "transparent"
  },
  textInvite: {
    color: "rgb(0, 0, 0)",
    //fontFamily: 'Open Sans',
    fontSize: 12,
    paddingVertical: 5,
    backgroundColor: "transparent"
  },
  imageEye: {
    width: 20,
    height: 13
  },
  eyeButtonWrapper: {
    justifyContent: "center",
    alignItems: "center",
    padding: 10,
    position: "absolute",
    right: 40
  },
  inputWrapper: {
    justifyContent: "center",
    alignItems: "center",
    alignSelf: "stretch",
    marginTop: 40
  },
  input: {
    fontSize: 14,
    color: 'white',
    height: 50,
    alignSelf: "stretch",
    marginHorizontal: 40,
    // borderColor: '#aaa',
    // borderWidth: 1,
    borderRadius: 3,
    marginBottom: 3,
    paddingHorizontal: 40,
    backgroundColor: 'rgba(100,100,100, 0.5)'
  },
  loginButtonWrapper: {
    marginTop: 16,
    alignSelf: "stretch"
  },
  buttonWrapper: {
    marginTop: 16,
    alignItems: "center"
  },
  buttonLogin: {
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: 'rgba(14, 115, 240, 0.5)',
    borderRadius: 25,
    marginHorizontal: 40,
    height: 50
  },
  textButton: {
    color: "#fff",
    //fontFamily: 'Open Sans',
    // fontWeight: 'bold',
    fontSize: 16,
    backgroundColor: "transparent"
  },
  textTitleButton: {
    color: "rgb(0, 0, 0)",
    //fontFamily: 'Open Sans',
    fontSize: 14,
    backgroundColor: "transparent"
  },
  CreateAccount: {
    color: "rgb(0, 0, 0)",
    //fontFamily: 'Open Sans',
    fontSize: 14,
    backgroundColor: "transparent"
  },
  indicatorContainer: {
    flex: 1,
    backgroundColor: "rgba(0, 0, 0,0.5)",
    alignItems: "center",
    justifyContent: "center"
  },
  indicator: {
    width: 80,
    height: 80,
    borderRadius: 5,
    shadowColor: "black",
    alignItems: "center",
    justifyContent: "center",
    shadowOffset: { width: 2, height: 2 },
    shadowOpacity: 0.4,
    shadowRadius: 3,
    backgroundColor: "white"
  },
  inputIcon: {
    position: "absolute",
    left: 50,
    top: 15
  }
});
