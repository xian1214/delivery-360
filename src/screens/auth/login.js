"use strict";

import React, { PureComponent } from "react";
import {
  StyleSheet,
  Text,
  View,
  Image,
  TextInput,
  TouchableOpacity,
  Keyboard,
  TouchableWithoutFeedback,
  Alert,
  ActivityIndicator,
  Modal,
  ScrollView,
  ImageBackground
} from "react-native";

import { Actions } from "react-native-router-flux";

import * as commonStyles from "../../styles/styles";
import * as commonColors from "../../styles/colors";
import api from "../../service/api";
import {Ionicons} from '@expo/vector-icons'
import Cache from '../../utils/cache'
//import LaunchNavigator from 'react-native-launch-navigator'
import UtilService from '../../utils/utils'

export default class SignIn extends PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      email: this.props.email || "",
      password: this.props.password || "",
      clientId:Cache.clientID||'',
      rightCallback: null,
      isWaiting: false
    };
  }

  componentDidMount() {
  }

  componentWillUnmount() {}

  onForgotPassword() {
    Actions.Forgot();
  }

  onCreateAccount() {
    Actions.Register();
  }

  onToggleConfirmPassword() {}

  goBack() {
    Actions.Intro();
  }

  onLogin() {
    if ( this.state.email=='' && this.state.password ==''){
      Alert.alert('Please enter your password correctly!')
      return;
    }
    this.setState({ isWaiting: true });
    api.login(this.state.clientId, this.state.email, this.state.password, (err, res) => {
      if(err){
        console.log('error on login',err)
        Alert.alert(err.toString())
        this.setState({ isWaiting: false });
        return
      }
      Cache.currentUser = res
      //Cache.currentUser = res;
      console.log('arrived user when login-',res)
      UtilService.saveLocalStringData("currentUser", JSON.stringify(res));
      UtilService.saveLocalStringData("client", Cache.clientID);
      UtilService.saveLocalStringData("pushToken", Cache.pushToken);
      Actions.Main()
      this.setState({ isWaiting: false });
    });

  }

  renderIndicator() {
    return (
      <Modal
        visible={this.state.isWaiting}
        transparent={true}
        onRequestClose={() => {}}
      >
        <View style={styles.indicatorContainer}>
          <View style={styles.indicator}>
            <ActivityIndicator size="large" />
          </View>
        </View>
      </Modal>
    );
  }

  faceLogin(){
    if ( !Cache.clientID && this.state.clientId == '' ){
      Alert.alert('Please input <Client ID>')
      return;
    }
    Cache.clientID = this.state.clientId
    Actions.ScanFace()
  }
  toNavigationService(){

    // let app = null;

    // if(LaunchNavigator.isAppAvailable(LaunchNavigator.APP.WAZE)){
    //     app = LaunchNavigator.APP.WAZE;
    // }else{
    //     console.warn("Waze not available - falling back to default navigation app");
    //     return
    // }
    //     LaunchNavigator.getAvailableApps()
    //       .then((apps) => {
    //           for(let app in apps){
    //               //console.log(LaunchNavigator.getAppDisplayName(app)+" is "+(apps[app] ? "available" : "unavailable"));
    //           }
    //       })
    //       .catch((error) => {
    //           console.error(error);
    //       });
      
  }
  render() {
    return (
        <ImageBackground source={commonStyles.background} style={{flex:1}}>
        <ScrollView style={styles.container}>
        {/* <View style={{width:'100%',height:50,backgroundColor:'white'}}></View> */}
        {/* <TouchableOpacity style={{marginHorizontal:20,padding:20,margin:20}} onPress={() =>{this.toNavigationService()}}>
            <Ionicons name={"md-sync"} size={24} color={"white"} />
        </TouchableOpacity> */}
        <View style={styles.logoWrapper}>
            <Image
              source={commonStyles.logo_black}
              style={styles.logoContent}
            />
          </View>
          <View style={styles.inputContainer}>
            {<View style={styles.inputWrapper}>
              <TextInput
                ref="clientId"
                autoCapitalize="none"
                autoCorrect={false}
                placeholder="Client ID"
                placeholderTextColor={commonColors.placeholderText}
                textAlign="left"
                style={styles.input}
                underlineColorAndroid="transparent"
                returnKeyType={"next"}
                value={this.state.clientId}
                onChangeText={text =>
                  this.setState({ clientId: text.replace(/\t/g, "") })
                }
                onSubmitEditing={() => this.refs.email.focus()}
              />
              <Ionicons name="md-call" size={20} color={commonColors.placeholderText} style={styles.inputIcon}/>
            </View>}
            <View style={styles.inputWrapper}>
              <TextInput
                ref="email"
                autoCapitalize="none"
                autoCorrect={false}
                placeholder="Mobile No."
                placeholderTextColor={commonColors.placeholderText}
                textAlign="left"
                style={styles.input}
                underlineColorAndroid="transparent"
                returnKeyType={"next"}
                keyboardType="phone-pad"
                value={this.state.email}
                onChangeText={text =>
                  this.setState({ email: text.replace(/\t/g, "") })
                }
                onSubmitEditing={() => this.refs.password.focus()}
              />
              <Ionicons name="md-call" size={20} color={commonColors.placeholderText} style={styles.inputIcon}/>
            </View>
            <View style={styles.inputWrapper}>
              <TextInput
                ref="password"
                autoCapitalize="none"
                autoCorrect={false}
                placeholder="Password"
                secureTextEntry={true}
                placeholderTextColor={commonColors.placeholderText}
                textAlign="left"
                style={styles.input}
                underlineColorAndroid="transparent"
                returnKeyType={"go"}
                value={this.state.password}
                onChangeText={text =>
                  this.setState({ password: text.replace(/\t/g, "") })
                }
                onSubmitEditing={() => this.onLogin()}
              />
              <Ionicons name="md-lock" size={20} color={commonColors.placeholderText} style={styles.inputIcon}/>
            </View>

            <TouchableOpacity
              activeOpacity={0.5}
              style={styles.loginButtonWrapper}
              onPress={() => this.onLogin()}
            >
              <View style={styles.buttonLogin}>
                <Text style={styles.textButton}>SIGN IN</Text>
              </View>
            </TouchableOpacity>
          </View>

          <View style={styles.bottomContainer}>
            <View style={styles.bottomContentWrap}>
                <Text style={{color:'#777', fontWeight:'bold'}}>or scan your face to sign in</Text>
                <TouchableOpacity onPress={()=>this.faceLogin()} style={{marginTop:15}}>
                  <Image source={commonStyles.face} style={{width:60, height:60, borderRadius:30, borderWidth:3, borderColor:'grey'}}/>
                </TouchableOpacity>
            </View>
          </View>
          {this.renderIndicator()}
        </ScrollView>
        </ImageBackground>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor:'transparent'
  },
  logoWrapper: {
    flex: 2,
    justifyContent: "center",
    alignItems: "center",
    marginTop: 60
  },
  logoContent: {
    width: 200,
    height: 120,
    resizeMode: "contain"
  },
  descriptionContainer: {
    marginTop: 20,
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  },
  inputContainer: {
    marginTop: 20,
    flex: 4,
    alignItems: "center",
  },
  bottomContainer: {
    marginTop:30,
    flex: 4,
    justifyContent: "flex-start"
  },
  bottomContentWrap: {
    // flex: 1,
    justifyContent: "flex-start",
    alignItems: "center",
    paddingTop: 10
  },
  textTitle: {
    top: 20,
    color: "rgb(0, 0, 0)",
    //fontFamily: 'Blanch',
    fontSize: 20,
    backgroundColor: "transparent"
  },
  textDescription: {
    color: "rgb(0, 0, 0)",
    //fontFamily: 'OpenSans-Semibold',
    fontSize: 12,
    paddingTop: 5,
    backgroundColor: "transparent"
  },
  textInvite: {
    color: "rgb(0, 0, 0)",
    //fontFamily: 'Open Sans',
    fontSize: 12,
    paddingVertical: 5,
    backgroundColor: "transparent"
  },
  imageEye: {
    width: 20,
    height: 13
  },
  eyeButtonWrapper: {
    justifyContent: "center",
    alignItems: "center",
    padding: 10,
    position: "absolute",
    right: 40
  },
  inputWrapper: {
    justifyContent: "center",
    alignItems: "center",
    alignSelf: "stretch"
  },
  input: {
    fontSize: 14,
    color: 'white',
    height: 50,
    alignSelf: "stretch",
    marginHorizontal: 20,
    // borderColor: '#aaa',
    // borderWidth: 1,
    borderRadius: 3,
    marginBottom: 8,
    paddingHorizontal: 40,
    backgroundColor: 'rgba(100, 100, 100, 0.5)'
  },
  loginButtonWrapper: {
    marginTop: 16,
    alignSelf: "stretch"
  },
  buttonWrapper: {
    marginTop: 16,
    alignItems: "center"
  },
  buttonLogin: {
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: 'rgba(14, 115, 240, 0.5)',
    borderRadius: 25,
    // borderWidth: 4,
    // borderColor: commonColors.theme,
    // borderStyle: 'solid',
    marginHorizontal: 20,
    height: 50
  },
  textButton: {
    color: "#fff",
    //fontFamily: 'Open Sans',
    fontWeight: 'bold',
    fontSize: 16,
    backgroundColor: "transparent"
  },
  textTitleButton: {
    color: "rgb(0, 0, 0)",
    //fontFamily: 'Open Sans',
    fontSize: 14,
    backgroundColor: "transparent"
  },
  CreateAccount: {
    color: "rgb(0, 0, 0)",
    //fontFamily: 'Open Sans',
    fontSize: 14,
    backgroundColor: "transparent"
  },
  indicatorContainer: {
    flex: 1,
    backgroundColor: "rgba(0, 0, 0,0.5)",
    alignItems: "center",
    justifyContent: "center"
  },
  indicator: {
    width: 80,
    height: 80,
    borderRadius: 5,
    shadowColor: "black",
    alignItems: "center",
    justifyContent: "center",
    shadowOffset: { width: 2, height: 2 },
    shadowOpacity: 0.4,
    shadowRadius: 3,
    backgroundColor: "white"
  },
  inputIcon:{
    position:'absolute',
    left:30,
    top: 15
  }
});
