"use strict";

import React, { PureComponent } from "react";

import {
  StyleSheet,
  View,
  Platform,
  Modal,
  TouchableOpacity,
  Text,
  StatusBar,
  Image,
  ScrollView,
  TextInput,
  ActivityIndicator,
  FlatList,
  RefreshControl,
  Slider,
  Alert,
} from "react-native";

import * as commonColors from "../styles/colors";
import * as commonStyles from "../styles/styles";


import { Ionicons, Feather } from "@expo/vector-icons";
import { Actions } from "react-native-router-flux";
import { Constants, Audio, Permissions, ImagePicker,ImageManipulator } from "expo";
import UtilService from '../utils/utils'
import Header from '../components/header'
import api from '../service/api'
import moment from 'moment'
import * as actions from '../store/chat/actions'
import { getChatItems } from '../store/chat/selectors'
import { connect } from 'react-redux'
import { bindActionCreators } from "redux";
import firebaseService from '../service/firebaseService'
import * as actionsForCommon from '../store/common/actions'
import common from "../store/common";
import * as config from '../config'
import Cache from '../utils/cache'
import { isMoment } from "moment";
import * as CONST from '../components/constant'
import MaterialCommunityIcons from "@expo/vector-icons/MaterialCommunityIcons";
var gHandler = null
function getMMSSFromMillis(millis) {
  const totalSeconds = millis / 1000;
  const seconds = Math.floor(totalSeconds % 60);
  const minutes = Math.floor(totalSeconds / 60);

  const padWithZero = number => {
    const string = number.toString();
    if (number < 10) {
      return "0" + string;
    }
    return string;
  };
  return padWithZero(minutes) + ":" + padWithZero(seconds);
}

const MyInputLine = ({ value, onChange }) => (
  <TextInput
    placeholder="Message"
    multiline={true}
    placeholderTextColor={'grey'}
    underlineColorAndroid="transparent"
    value={value}
    onChangeText={(text) => onChange(text)}
    style={{
      paddingHorizontal: 10, paddingTop: Platform.OS === 'android' ? 0 : 12,
      borderColor: commonColors.borderColor, borderWidth: 1,
      height: 44, borderRadius: 3, flex: 1,
      fontSize: 13, backgroundColor: 'white'
    }}
    returnKeyType={'send'}
  />
)

const YourText = ({ avatar, color, message }) => (
  <View style={{ flexDirection: 'row', marginTop: 10, marginLeft: 15 }}>
    
    <View style={{ width: 30 }}>
        {
          <Image source={commonStyles.noUser} style={{position:'absolute',left:0,top:0, width:30, height:30, borderRadius:15}}/>
        }
        {
          (message.user.avatar!=null) &&
          <Image source={{uri:config.SERVICE_FILE_URL +message.user.avatar}} style={{width:30, height:30, borderRadius:15}}/>
        }      
    </View>
    <View style={{ maxWidth: '75%' }}>
      <View style={{ width: 10, height: 10, transform: [{ rotate: '45deg' }], backgroundColor:color, position: 'absolute', left:10, top: 13 }} />
      {(!!message.text) && <View style={{ flexDirection: 'row' }}>
        <View style={{ backgroundColor: color, padding: 10, borderRadius: 4, marginLeft: 15, marginTop: 5 }}>
          <Text style={{ color: 'black', fontSize: 12 }}>{message.text}</Text>
        </View>
      </View>}
      {/* {(!!message.image) && <Image source={{ uri: message.image }} onPress={()=>Actions.ShowImage({uri:message.image})} style={{ width: 120, height: 120, borderWidth: 3, borderRadius: 8, borderColor: commonColors.theme, marginLeft: 15, marginTop: 5 }} />} */}
      
      {(!!message.image) &&<TouchableOpacity onPress={()=>Actions.ShowImage({uri:message.image})} style={{width:120, height:120}}>
            <Image source={{uri:message.image}} style={{ flex:1, borderRadius: 8, borderWidth: 3, borderColor: commonColors.theme }}/>
      </TouchableOpacity>}
      
      <View style={{ flexDirection: 'row', marginLeft: 10 }}>
        <Text style={{ fontSize: 12, color: '#aaa', marginLeft: 5 }}>{moment(message.createdAt).format("HH:mm")+' by '+ message.user.name}</Text>        
      </View>
    </View>

  </View>
)

const MyText = ({avatar, message }) => (
  <View style={{ alignItems: 'flex-end', marginTop: 10, marginRight: 10 }}>
    
    <View style={{ flexDirection: 'row', maxWidth: '80%' }}>
      {message.text && <View style={{ width: 10, height: 10, transform: [{ rotate: '45deg' }], backgroundColor: 'rgb(235, 235, 235)', position: 'absolute', right: 25, top: 10 }} />}
      <View style={{ alignItems: 'flex-end' }}>
        {message.text && <View style={{ backgroundColor: 'rgb(235, 235, 235)', padding: 10, borderRadius: 4, marginRight: 30 }}>
          <Text style={{ color: 'black', fontSize: 12 }}>{message.text}</Text>
        </View>}
        <View style={{height:10}}></View>
        {message.image &&<TouchableOpacity onPress={()=>Actions.ShowImage({uri:message.image})} style={{width:120, height:120}}>
            <Image source={{uri:message.image}} style={{ flex:1, borderRadius: 8, borderWidth: 3, borderColor: commonColors.theme }}/>
        </TouchableOpacity>}
        {/* {
            (message.user.avatar==null)&&
            <Image source={commonStyles.noUser} style={{width:30, height:30, borderRadius:15}}/>
          }
          {
            (message.user.avatar!=null) &&
            <Image source={{uri:config.SERVICE_FILE_URL +message.user.avatar}} style={{width:30, height:30, borderRadius:15}}/>
          }      */}
        {message.createdAt && <View style={{ flexDirection: 'row',  marginRight: 15 }}>
          {/* <Ionicons name="md-checkmark" size={12} color={'#aaa'} /> */}
          <Text style={{ fontSize: 12, color: '#aaa', marginLeft: 5 }}>{moment(message.createdAt).format("HH:mm")+' by '+message.user.name}
          </Text>
        </View>}
        {message.createdAt == null && <ActivityIndicator style={{ marginRight: 15 }} />}
      </View>
    </View>
  </View>
)

const MyRecord = ({isPaused, message, value, onPlay, isPlaying, onChange, onComplete, disabled, current, total }) => (
  <View style={{ alignItems: 'flex-end', marginTop: 10, marginRight: 10 }}>
    {/* <Text style={{fontSize:10}}>{message.user.name + ' (' +message.user.role+')'}</Text> */}
    <View style={{ flexDirection: 'row', maxWidth: '80%' }}>
      <View style={{ alignItems: 'flex-end' }}>

        <View style={{ backgroundColor: 'rgb(235,235,235)', borderRadius: 4, paddingVertical: 5, paddingHorizontal: 10, flexDirection: 'row', alignItems: 'center', width: 200, marginRight: 15, marginTop: 5 }}>
          <TouchableOpacity onPress={() => {onPlay();console.log('my record')}} style={{ backgroundColor: 'red', width: 80, height: 80, borderRadius: 20, alignItems: 'center', justifyContent: 'center' }}>
            <Ionicons name={(isPlaying && !isPaused)? 'md-pause' : 'md-play'} color={'white'} size={24} style={{ marginTop: Platform.OS === 'ios' ? 3 : 0, marginLeft: Platform.OS === 'ios' ? 3 : 0 }} />
          </TouchableOpacity>
          <Text></Text>
          <View style={{ marginLeft: 5, flex: 1 }}>
            <Slider
              thumbStyle={{ width: 15, height: 15, backgroundColor: commonColors.theme }}
              minimumTrackTintColor={commonColors.theme}
              value={value ? value : 0}
              onValueChange={() => onChange()}
              onSlidingComplete={() => onComplete()}
              disabled={disabled}
            />
            <View style={{ flexDirection: 'row', width: '100%' }}>
              <Text style={{ fontSize: 12, color: '#333', flex: 1 }}>{current ? current : '00:00'}</Text>
              <Text style={{ fontSize: 12, color: '#333' }}>{total}</Text>
            </View>
          </View>
        </View>

        {message.createdAt && <View style={{ flexDirection: 'row', marginRight: 15 }}>
          {/* <Ionicons name="md-checkmark" size={12} color={'#aaa'} /> */}
          <Text style={{ fontSize: 12, color: '#aaa', marginLeft: 5 }}>{moment(message.createdAt).format('HH:mm')+' by '+message.user.role}</Text>
        </View>}
        
      </View>

    </View>
  </View>
)

const YourRecord = ({ avatar, message, value, onPlay, isPlaying,isPaused, onChange, onComplete, disabled, current, total }) => (
  <View style={{ flexDirection: 'row', marginTop: 10, marginLeft: 15 }}>
    <View style={{ width: 30 }}>
      <Image source={avatar} style={{ width: 30, height: 30, borderRadius: 15 }} />
    </View>
    <View style={{ maxWidth: '75%' }}>
      <Text style={{ color: '#333', fontSize: 12, marginLeft: 15 }}>{message.user.email}</Text>
      <View style={{ backgroundColor: 'rgb(235,235,235)', borderRadius: 4, paddingVertical: 5, paddingHorizontal: 10, flexDirection: 'row', alignItems: 'center', width: 200, marginLeft: 15, marginTop: 5 }}>
        <TouchableOpacity onPress={() => onPlay()} style={{ backgroundColor: commonColors.theme, width: 40, height: 40, borderRadius: 20, alignItems: 'center', justifyContent: 'center' }}>
        <Ionicons name={(isPlaying && !isPaused)? 'md-pause' : 'md-play'} color={'white'} size={24} style={{ marginTop: Platform.OS === 'ios' ? 3 : 0, marginLeft: Platform.OS === 'ios' ? 3 : 0 }} />
        </TouchableOpacity>
        <View style={{ marginLeft: 5, flex: 1 }}>
          <Slider
            thumbStyle={{ width: 15, height: 15, backgroundColor: commonColors.theme }}
            minimumTrackTintColor={commonColors.theme}
            value={value ? value : 0}
            onValueChange={() => onChange()}
            onSlidingComplete={() => onComplete()}
            disabled={disabled}
          />
          <View style={{ flexDirection: 'row', width: '100%' }}>
            <Text style={{ fontSize: 12, color: '#333', flex: 1 }}>{current ? current : '00:00'}</Text>
            <Text style={{ fontSize: 12, color: '#333' }}>{total}</Text>
          </View>
        </View>
      </View>
      <View style={{ flexDirection: 'row', marginLeft: 10 }}>
        <Text style={{ fontSize: 12, color: '#aaa', marginLeft: 5 }}>{moment(message.createdAt).format('HH:mm')}</Text>
      </View>
    </View>

  </View>

)

const DayView = ({ value }) => (
  <View style={{ width: '100%', alignItems: 'center', marginVertical: 15 }}>
    <Text style={{ color: 'grey', fontWeight: 'bold', fontSize: 15 }}>{value}</Text>
  </View>
)

class Chat extends PureComponent {
  constructor(props) {
    super(props);
    
    this.state = {
      currentTeamMembers:[],
      message: '',
      messageList: [
        { message: 'Today', who: 3 },
        { message: 'When will you arrive?', date: '12:21', who: 1, name: 'Austin' },
        { message: 'I think half an hour', date: '12:24', who: 0, name: 'jon' },
        { message: 'There is a road block ahead', date: '12:24', who: 0, name: 'jon' },
        { message: 'Ok', date: '12:30', who: 1, name: 'Austin' },
        { message: 'Noted by HQ', date: '12:55', who: 2, name: 'HQ' },
        { message: 'On my way', date: '13:02', who: 0, name: 'jon' },
      ],
      clientAvatar: commonStyles.face1,
      HQAvatar: commonStyles.face2,
      recordingTime: '00:00',
      isRecording: false,
      isPlaying:false,
      current:[],
      progressIndicator:[],
      totalIndicator:[],
      pausedIndex:-2,
      playIndex: -1,
      isWaiting:false,
      isRefreshing:false,
      currentIndex:null,
      data:[],
      presentTime:null,
      teamActions:[],
      options:[]
    };
    this.state.current=[];

    this.recording = null;
    this.sound = null;
    this.length = 20
    gHandler = this
    this.mounted=false
  }
  _askForAudioPermission = async () => {
    const response = await Permissions.askAsync(Permissions.AUDIO_RECORDING);
    this.setState({
      haveRecordingPermissions: response.status === 'granted',
    });
    console.log("PERMISSION:", JSON.stringify(this.state.haveRecordingPermissions));
  };

  async componentDidMount() {
    this.mounted = true
    Cache.chatIsOpen = true
    this.props.actions.resetUnreadMsgCnt()
    this._askForAudioPermission();
    this.getTeamActions()
    this.setState({isRefreshing:true})

    this.props.actions.loadMessages(Cache.clientID + '-' + this.props.channel.DeliveryNoteNumber,this.length,(error,res)=>{
       this.setState({isRefreshing:false})
    })
    let status = await Audio.setAudioModeAsync({
      playsInSilentModeIOS: true,
      allowsRecordingIOS: true,
      playThroughEarpieceAndroid: true,
      interruptionModeIOS: Audio.INTERRUPTION_MODE_IOS_DO_NOT_MIX,
      interruptionModeAndroid: Audio.INTERRUPTION_MODE_ANDROID_DO_NOT_MIX,
      shouldDuckAndroid: true
    })
    api.getTeamMembers(this.props.channel.DeliveryNoteID,(error,res)=>{
     if(error){
          return
        }
        this.setState({currentTeamMembers:res.Results})
    })
    this.getActivities()
  }
  getActivities(){
    api.getActivities((error,res)=>{
      if(!!error){
        return
      }
      this.setState({options:res})
    })
  }
  getTeamActions(){
      api.getTeamActions(this.props.channel.DeliveryNoteID,(error,res)=>{
        console.log('res on gta--.',error,res)
        if(error){
          return
        }
        console.log('res on gta--.',res.Results)
        this.setState({teamActions:res.Results})
      })
  }
  selectTeamAct(id){  
    var res = null
    for(var i=0;i<this.state.teamActions.length;i++){
        if(this.state.teamActions[i].TeamActID==id){
          res =  this.state.teamActions[i]
        }
    }
    return res
  }
  getTeamActName(id){
    var res = 'Not defined'
    for(var i=0;i<this.state.teamActions.length;i++){
           
        if(this.state.teamActions[i].TeamActID==id){
          if(
            this.state.teamActions[i].Activity &&
            this.state.teamActions[i].Activity.Name
          ){
            res = this.state.teamActions[i].Activity.Name
            return res
          }    
        }
    }
  }
  getActionPosterName(id){
    var res = 'Not defined'
    for(var i=0;i<this.state.teamActions.length;i++){
           
        if(this.state.teamActions[i].TeamActID==id){
          if(
            this.state.teamActions[i].Activity &&
            this.state.teamActions[i].Activity.Name
          ){
            res = this.state.teamActions[i].User.FirstName+' '+this.state.teamActions[i].User.LastName
            return res
          }    
        }
    }
  }

  getRemarks(id){
    var res = 'Not defined'
    for(var i=0;i<this.state.teamActions.length;i++){
        if(this.state.teamActions[i].TeamActID==id){
              res = this.state.teamActions[i].Remarks
        }
    }
    return res
  }
  onEndReached(){
    return
    this.length += 20
    this.setState({isRefreshing:true})
    this.props.actions.loadMessages(Cache.clientID + '-' + this.props.channel.DeliveryNoteNumber,this.length,(error,res)=>{
      this.setState({isRefreshing:false})
    })
   }
   onRefresh(){
     return
    this.length = 20
    this.setState({isRefreshing:true})
    this.props.actions.loadMessages(Cache.clientID + '-' + this.props.channel.DeliveryNoteNumber,this.length,(error,res)=>{
      this.setState({isRefreshing:false})
    })
    
   }
 
  sendMessage() {
    let { message } = this.state
    let {channel} = this.props
     
    var chattingRoom = Cache.clientID + '-' + this.props.channel.DeliveryNoteNumber
    //console.log('chatting Rom on sendmessage.....',chattingRoom)
    this.setState({isRefreshing:true})
    this.props.actions.sendMessage(chattingRoom,this.props.channel.DeliveryNoteID,this.state.message, null, null,null,(error,res)=>{
      this.setState({isRefreshing:false})
      if(error){
          Alert.alert('Send message failed!')
      }
    })
    if (!!channel) {
      //console.log('Cache on sendMessage ...',Cache)
      var data = {
        Text: message,
        Time: moment(),
        Name: Cache.currentUser.user.UserName
      }
      //console.log('currentDelivery_____',channel)
      // api.lastMessageUpdate(channel.DeliveryNoteID, data, (error, res) => {
      //   if (error) {
      //     return
      //   }
      //     console.log("arrived chat info res is ",res)
      // })
    }
    setTimeout(() => this.setState({ message: '' }), 100);
  }

  async onMic() {
    const response = await Permissions.askAsync(Permissions.AUDIO_RECORDING);
    if (response.status === "granted") {

      if (this.recording !== null) {
        this.recording.setOnRecordingStatusUpdate(null);
        this.recording = null;
      }

      const recording = new Audio.Recording();
      await recording.prepareToRecordAsync(Expo.Audio.RECORDING_OPTIONS_PRESET_HIGH_QUALITY);
      recording.setOnRecordingStatusUpdate(status => {
        if (status.canRecord) {
          this.setState({
            isRecording: status.isRecording,
            recordingTime: getMMSSFromMillis(status.durationMillis)
          });
        } else if (status.isDoneRecording) {
          this.setState({
            isRecording: false,
            recordingDuration: getMMSSFromMillis(status.durationMillis)
          });
        }
      });

      this.recording = recording;
      await this.recording.startAsync(); // Will call callback to update the screen.
    }
  }

  async onShare() {
    const { status } = await Permissions.askAsync(Permissions.CAMERA_ROLL);
    if (status === "granted") {
      let result = await ImagePicker.launchImageLibraryAsync({ mediaTypes: 'All' })
      if (!result.cancelled) {
        //let currentUser = firebaseService.auth().currentUser
        this.setState({
          tmpMessage: {
            text: null,
            createdAt: null,
            user: {
              //id: currentUser.uid,
              //email: currentUser.email
            },
            image: result.uri,
          }
        })
        result.type = "image/jpeg"
        result.name = "file.jpeg"
        api.uploadFile(result, (err, res) => {
          // console.log(err, res)
          if (err == null) {
            var chattingRoom = Cache.clientID + '-' + this.props.channel.DeliveryNoteNumber
            this.setState({isWaiting:true})
            this.props.actions.sendMessage(chattingRoom,this.props.channel.DeliveryNoteID, null, config.SERVICE_FILE_URL + res.filePath, null,(error,res)=>{
              this.setState({isWaiting:false})
              if(error){
                  return
              }

            })
          }
          this.setState({ tmpMessage: null })
        })

      }
    }
  }

  
  async cancelRecording() {
    await this.recording.stopAndUnloadAsync();
  }

  async doneRecording() {
    await this.recording.stopAndUnloadAsync();
    //const currentUser = firebaseService.auth().currentUser
    this.setState({
      tmpMessage: {
        text: null,
        createdAt: null,
        user: {
          name:Cache.currentUser.user.UserName,
          avatar:Cache.currentUser.user.Avarta
        },
        image: null,
        voice:  this.recording.getURI()
      }
    })

    let result = {
      type: 'audio/mp3',
      name: 'file.mp3',
      uri: this.recording.getURI()
    }
    api.uploadFile(result, (err, res) => {
      // console.log(err, res)
      if (err == null) {
        var chattingRoom = Cache.clientID + '-' + this.props.channel.DeliveryNoteNumber
        this.props.actions.sendMessage(chattingRoom,this.props.channel.DeliveryNoteID, null, null, config.SERVICE_FILE_URL+res.filePath,(error,res)=>{
          if(error){

          }
        })
      }
      this.setState({ tmpMessage: null })
    })
  }

  _onPlayPausePressed = () => {
    if (this.sound != null) {
      if (this.state.isPlaying) {
        this.sound.pauseAsync();
        console.log("PAUSED");
      } else {
        this.sound.playAsync();
        console.log("PAUSE..PLAYED");
      }
    }
  };
  _updateScreenForSoundStatus = (status) => {
    console.log("UPDATING SCREEN FOR SOUND STATUS", status);
    if (status.isLoaded) {
      this.setState({
        soundDuration: status.durationMillis,
        soundPosition: status.positionMillis,
        shouldPlay: status.shouldPlay,
        isPlaying: status.isPlaying,
        rate: status.rate,
        muted: status.isMuted,
        volume: status.volume,
        shouldCorrectPitch: status.shouldCorrectPitch,
        isPlaybackAllowed: true,
      });
    } else {
      this.setState({
        soundDuration: null,
        soundPosition: null,
        isPlaybackAllowed: false,
      });
      if (status.error) {
        Alert(`FATAL PLAYER ERROR: ${status.error}`);
      }
    }
  };
  async playItem(item, index) {

    console.log('item is being played...playItem')
    if (this.state.playIndex != index) {

      this.setState({pausedIndex:-2})
      this.state.playIndex = index
      this.setState({playIndex:index})
      console.log('item is being played...2')
      this.setState({ playIndex: index })

      if (this.sound !== null) {
        await this.sound.unloadAsync();
        this.sound.setOnPlaybackStatusUpdate(null)
      }
      
      this.sound = new Expo.Audio.Sound();
      
      
      this.sound.setOnPlaybackStatusUpdate(({ durationMillis, positionMillis, isPlaying, didJustFinish }) => {
        this.setState({isPlaying})
        if (isPlaying) {
          var tempVal =  getMMSSFromMillis(positionMillis)
          
          this.state.progressIndicator[index] = getMMSSFromMillis(positionMillis)
          this.state.totalIndicator[index] = getMMSSFromMillis(durationMillis)
          this.setState({progressIndicator:[...this.state.progressIndicator]})
          this.setState({totalIndicator:[...this.state.totalIndicator]})
          this.setState({value: positionMillis / durationMillis })
        }
        if (didJustFinish) {
          this.setState({ playIndex: -1 })
          this.setState({ pausedIndex: -2 })
          this.setState({ current: getMMSSFromMillis(durationMillis) })
          this.setState({ value: 0 })
          this.state.progressIndicator[index]=0
          this.setState({progressIndicator:[...this.state.progressIndicator]})
          //this.sound.setPositionAsync(0);
        }
      })
      await this.sound.loadAsync({uri:item.voice})
      await this.sound.playAsync();
      
      
    } else {

      if(this.state.isPlaying){
         this.sound.pauseAsync()
         this.setState({pausedIndex:index})
      }
      else{
        this.setState({pausedIndex:-2})
        this.sound.playAsync()
      }
    }
  }
  renderRecordingModal() {
    return (
      <Modal
        visible={this.state.isRecording}
        transparent={true}
        onRequestClose={() => { }}
      >
        <View style={styles.modalContainer}>
          <Feather name="mic" size={120} color={'white'} />
          <Text style={{ fontSize: 24, fontWeight: 'bold', color: 'white', marginTop: 20 }}>{this.state.recordingTime}</Text>
          <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 20 }}>
            <TouchableOpacity onPress={() => this.cancelRecording()} style={{ backgroundColor: 'red', width: 40, height: 40, borderRadius: 20, alignItems: 'center', justifyContent: 'center' }}>
              <Ionicons name='ios-close' size={24} color={'white'} />
            </TouchableOpacity>
            <View style={{ width: 50 }} />
            <TouchableOpacity onPress={() => this.doneRecording()} style={{ backgroundColor: commonColors.theme, width: 70, height: 70, borderRadius: 35, alignItems: 'center', justifyContent: 'center' }}>
              <Ionicons name='ios-checkmark' size={50} color={'white'} />
            </TouchableOpacity>
          </View>
        </View>
      </Modal>
    );
  }

  componentDidUpdate() {
    if (this.props.messages && this.props.messages.length) {
      this.flatList.scrollToIndex({ animated: true, index: 0 });
    }
  }
  renderAction(id,message){
    return(
      <View  style={{borderRadius:10,width:'80%',marginLeft:'10%',marginVertical:10, backgroundColor:'black'}}>
          <View style={{padding:10,marginBottom:20}}>
            <View style={{flexDirection:'row',alignItems:'center',justifyContent:'center'}}>
                <View>
                  <Text style={{fontSize:18,paddingBottom:5, fontWeight:'bold',color:'white'}}>
                    {this.getTeamActName(id)}
                  </Text>
                  <Text style={{fontSize:12,color:'white'}}>{' (posted by '+this.getActionPosterName(id)+')'}</Text>
                </View>
               <View style={{flex:1}}></View>
               <Text style={{fontSize:12,paddingBottom:5, fontWeight:'bold',color:'white'}}>{moment(message.createdAt).format('hh:mm a')}</Text>
            </View>
            <Text style={{color:'white',marginTop:8}}>Team Members</Text>
          </View>
          <View style={{flexDirection:'row',flexWrap:'wrap'}}>
          {
              this.state.currentTeamMembers.map((item,index)=>{
                  return(
                    <View style={{flex:1,flexDirection:'row',alignItems:'center',marginBottom:10 ,justifyContent:"center"}} key={index+'+teammember'}>
                      <View style={{width:90,paddingHorizontal:20}}>
                          <Image source={commonStyles.noUser}  style={{width:50, height:50, borderRadius:25}}/>
                          <View style={{width:20,height:20,left:60, bottom:0,borderRadius:10, backgroundColor:'#5d92f4',position:'absolute',alignItems:'center',justifyContent:'center'}}>
                            <Ionicons name="md-checkmark" size={18} color={'white'}/>
                          </View>
                      </View>
                      <Text style={{color:"white",flex:1}}>{item.User.Username}</Text>
                    </View>
                  )  
              })
              
          }
          </View>
          <View style={{height:15}}></View>
          <View style={{paddingLeft:20,marginBottom:5}}>
          </View>
          <View style={{padding:20,marginBottom:5,marginTop:10,borderRadius:7}}>
            <Text style={{color:'white'}}>Remarks</Text>
            <View style={{backgroundColor:'#2f3444',borderRadius:7,marginTop:10,padding:15}}>
                <Text style={{color:'white'}}>
                  {this.getRemarks(id)}
                </Text>
            </View>
          </View>
      </View>
    )
    
  }
  renderItem({ item, index }) {
    //const isCurrentUser = firebaseService.auth().currentUser.email == item.user.email;
    const isCurrentUser = item.user.name == Cache.currentUser.user.UserName
    const actionID = item.actionID
    if (isCurrentUser && !!!actionID) {
      return item.voice == null ? <MyText message={item} /> :
        <MyRecord
          message={item}
          value={index == this.state.playIndex ? this.state.value : 0}
          onChange={() => { }}
          onPlay={() => {this.playItem(item, index);console.log('keyboard pressed........playItem.-------')}}
          onComplete={() => { }}
          current={this.state.progressIndicator[index]}
          total={this.state.totalIndicator[index]}
          disabled={true}
          index = {index}
          isWaiting = {this.state.isWaiting}
          isPaused={index == this.state.pausedIndex}
          isPlaying={index == this.state.playIndex} />
    }
    if (!isCurrentUser && !!!actionID) {
      return item.voice == null ? <YourText  color={'rgb(192, 233,248)'} message={item} /> :
        <YourRecord
          avatar={this.state.HQAvatar}
          message={item}
          value={index == this.state.playIndex ? this.state.value : 0}
          onChange={() => { }}
          // onPlay={() => this.playItem(item, index)}
          onPlay={() => {this.playItem(item, index)}}
          onComplete={() => { }}
          current={this.state.progressIndicator[index]}
          total={this.state.totalIndicator[index]}
          disabled={true}
          isPaused={index == this.state.pausedIndex}
          isPlaying={index == this.state.playIndex} />
    }
    if(!!actionID && this.hasEntity(actionID)){
      // return(<View style={{width:50,height:50,backgroundColor:'red'}}></View>)
      return(this.renderAction(actionID,item))
    }
    if ( item.who == 2 ) return <DayView key={index} value={item.message}/>
  }
  hasEntity(actionID){
    //console.log('this.state.teamActions==',this.state.teamActions[0])
    var res = false
    let {teamActions} = this.state
    for(var i=0;i<teamActions.length;i++){
      if(teamActions[i].TeamActID == actionID){
        res = true
        break;
      }
    }
    return res
  }
  componentWillUnmount(){
    Cache.chatIsOpen = false
    console.log ('updatereadstatus---')
    api.updateReadStatus(this.props.channel.DeliveryNoteID,CONST.HQ_USER,(error,res)=>{
      console.log ('updatereadstatus---2')
      if(error){
        console.log ('updatereadstatus---3')
        return
      }
      console.log ('updatereadstatus---4')
      this.props.actions.refreshDashboard()
    })
  }
  componentWillReceiveProps(nextProps){
    var currentMessages = getChatItems(nextProps.messages)
    var pastMessages = getChatItems(this.props.messages)
    if(currentMessages.length>1 && pastMessages.length>1){
      var  currentLast = currentMessages[currentMessages.length-1] 
      var  pastLast = pastMessages[pastMessages.length-1] 
      if(currentLast!=pastLast && (!!pastLast.actionID)){
          this.getTeamActions()
      }
    }
    
  }
  async takePicture(){
      console.log('take picture error 000')
      let {customerImage}=this.state
      console.log('take picture error 111')
      let res = await Permissions.askAsync(Permissions.CAMERA)
      console.log('take picture error 112')
      if ( res.status ==='granted'){
        console.log('take picture error 113')
          let {status} = await Permissions.askAsync(Permissions.CAMERA_ROLL)
          if ( status === 'granted' ){
            console.log('take picture error 114')
              let image = await ImagePicker.launchCameraAsync({
                  quality:0.4
              })
              console.log('take picture error 115')
              if ( !image.cancelled ){
                  this.setState({isRefreshing:true})
                  console.log('take picture error 116')
                  const manipResult = await ImageManipulator.manipulate(
                      image.uri,
                      [{resize:{width:768}}],
                      { format: 'jpeg', compress:0.6 }
                  );
                  api.uploadImage(manipResult.uri, (err, res)=>{
                    if (err == null) {
                      var chattingRoom = Cache.clientID + '-' + this.props.channel.DeliveryNoteNumber
                      this.setState({isRefreshing:true})
                      this.props.actions.sendMessage(chattingRoom,this.props.channel.DeliveryNoteID, null, config.SERVICE_FILE_URL + res.filePath, null,(error,res)=>{
                        if(error){
                          // this.setState({isWaiting:false})
                          this.setState({isRefreshing:false})
                          return
                        }
                        this.setState({isRefreshing:false})
                      })
                    }
                    this.setState({ tmpMessage: null })  
                  })
              }
              
          }
      }
  }
  _updateDate({viewableItems,changed}){
      if(!!viewableItems && viewableItems.length>0){
        var date =  moment(viewableItems[0].item.createdAt).format('dddd ') +
        moment(viewableItems[0].item.createdAt).format('YYYY/MM/DD') 
        gHandler.setState({presentTime:date}) 
      }      
  }
  render() {
    const messages = getChatItems(this.props.messages)
    if (this.state.tmpMessage) messages.push(this.state.tmpMessage)
    const data = messages.reverse()
    this.state.data = data
    return (
      <View style={styles.container}>
        <Header title={'Chat - '+ this.props.channel.DeliveryNoteNumber} />
        <View style={{flex:1}}>
          <View style={{flex:1}}></View>
          <FlatList
            refreshControl={
              <RefreshControl
                  refreshing={gHandler.state.isRefreshing}
                  onRefresh={() => gHandler.onRefresh()}
                  tintColor={'grey'}
                  progressViewOffset={250}
              />
            }
            ref={(c) => { this.flatList = c }}           
            onEndReachedThreshold={0.1}
            onEndReached={gHandler.onEndReached.bind(gHandler)}
            onViewableItemsChanged = {this._updateDate}
            contentContainerStyle={styles.flatlistContainer}
            data={data}
            keyExtractor={item => '' + item.createdAt}
            renderItem={this.renderItem.bind(this)}
            inverted
          />          
          {/* {this.renderAction()} */}
        </View>
        {
          !!this.state.presentTime &&
          <View style={{position:'absolute',justifyContent:'center',alignItems:'center',height:30,width:'100%',top:100}}>
                <View style={{backgroundColor:'rgba(100,100,100,0.5)',width:200,height:30,borderRadius:7,alignItems:'center',justifyContent:'center'}}>
                  <Text style={{color:"green",fontWeight:'bold'}}>{this.state.presentTime}</Text>
                </View>            
          </View>
        }
        {this.state.isWaiting && <ActivityIndicator style={{ marginRight: 15 }} />}
        <View style={{ height: 1, width: '100%',marginBottom:10, backgroundColor: '#ccc' }} />
        <View style={{height:60,paddingHorizontal:7}}>
          <ScrollView horizontal={true} showsHorizontalScrollIndicator={false} contentContainerStyle={{height:44}}>
                
                {
                  this.state.options.map((item,index)=>{
                    return(
                        <TouchableOpacity key={'horizontal_option'+index} onPress={()=>{Actions.GroupCheckIn({dn:this.props.channel,mode:item,updateActionList:()=>{this.getTeamActions()}})}} style={[styles.buttonContainer,{paddingLeft:5}]}>
                              <Text style={styles.buttonText}>{item.Name}</Text>
                        </TouchableOpacity>  
                    )  
                  })
                }    
          </ScrollView>
        </View>
        <View style={{ flexDirection: 'row', margin: 10, alignItems: 'center', backgroundColor: 'white' }}>
          <TouchableOpacity style={{ backgroundColor: 'transparent',
              width:40,height:40,alignItems:'center',justifyContent:'center', marginRight: 10 }} onPress={() => this.onShare()}>
            <MaterialCommunityIcons name="paperclip" size={42} color={commonColors.blue} />
          </TouchableOpacity>
          <MyInputLine
            value={this.state.message}
            onChange={(message) => this.setState({ message })}
          />
         {
            this.state.message.length == 0 &&
            <View style={{flexDirection:'row'}}> 
              <TouchableOpacity style={{ backgroundColor: commonColors.blue, height: 48, width: 48, borderRadius: 24, alignItems: 'center', justifyContent: 'center', marginLeft: 10, elevation:5}} onPress={() => this.onMic()}>
                <Feather name="mic" size={18} color={'white'} />
              </TouchableOpacity>
              <TouchableOpacity style={{ backgroundColor: commonColors.blue, height: 48, width: 48, borderRadius: 24, alignItems: 'center', justifyContent: 'center', marginLeft: 10, elevation:5}} onPress={() => this.takePicture()}>
                <Feather name="camera" size={18} color={'white'} />
              </TouchableOpacity>
            </View>
          }
          {this.state.message.length > 0 && <TouchableOpacity style={{ backgroundColor: commonColors.blue, height: 32, width: 32, borderRadius: 16, alignItems: 'center', justifyContent: 'center', marginLeft: 10 }} onPress={() => this.sendMessage()}>
            <Ionicons name="md-send" size={18} color={'white'} style={{ marginLeft: Platform.OS == 'ios' ? 2 : 0, marginTop: Platform.OS == 'ios' ? 2 : 0 }} />
          </TouchableOpacity>}
        </View>
        {this.renderRecordingModal()}
      </View>
    );
  }
}

export default connect(
  state => ({
    messages: state.chat.messages,
    internetConnection: state.common.internetConnection,
    currentDelivery: state.common.currentDelivery
  }),
  dispatch => ({
    actions: bindActionCreators({...actions,...actionsForCommon}, dispatch)
  })
)(Chat);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'rgb(250,250,250)',
    paddingTop: Constants.statusBarHeight,
  },
  buttonContainer: {
    width:80,
    paddingVertical:10,
    paddingHorizontal:7,
    flex:1,
    alignItems:'center',
    justifyContent:'center',
    marginHorizontal:5,
    borderRadius:15,
    backgroundColor:commonColors.blue
  },
  buttonText:{
    color:'white',
    fontSize:12
  },
  modalContainer: {
    flex: 1,
    backgroundColor: "rgba(0, 0, 0,0.5)",
    alignItems: "center",
    justifyContent: "center"
  },
  playbackSlider: {
    alignSelf: "stretch"
  },
  flatlistContainer: {
    // flex:1,
    marginTop:40,
    //flexGrow: 1,
    justifyContent: 'center'
  },
  bubble: {
    backgroundColor: 'rgba(100,100,100,0.5)',
    paddingHorizontal: 18,
    paddingVertical: 12,
    borderRadius: 20,
  },
  button: {
    width: 80,
    paddingHorizontal: 12,
    alignItems: 'center',
    marginHorizontal: 10,
  },
});
