"use strict";

import React, { PureComponent } from "react";

import {
  StyleSheet,
  View,
  Platform,
  Modal,
  TouchableOpacity,
  Text,
  StatusBar,
  TextInput,
  Image
} from "react-native";

import { Ionicons, FontAwesome, MaterialIcons } from "@expo/vector-icons";
import HeaderBar from "../components/header";
import * as commonStyles from "../styles/styles";
import * as commonColors from "../styles/colors";
import Cache from '../utils/cache'
import googleService from '../service/googleService'
import api from '../service/api'
import { Actions } from "react-native-router-flux";
import {MapView} from 'expo'

const Item = ({ icon, name, content }) => (
    <View style={{ flexDirection: "row", alignItems: "center", marginTop: 5 }}>
      <Ionicons name={icon} size={16} color={"grey"} style={{ width: 20 }} />
      <Text style={{ fontSize: 13, color: "grey", marginLeft: 8, width: 50 }}>
        {name}
      </Text>
      <Text style={{ fontSize: 14, color: "#333", }} >
        {content}
      </Text>
    </View>
  );

export default class Navigation2 extends PureComponent {
  constructor(props) {
    super(props);
    this.state={
        target: {latitude: props.data.DestinationLatitude,longitude:props.data.DestinationLongitude},
        start: { latitude: props.data.Warehouse.Latitude, longitude: props.data.Warehouse.Longitude },
        coordinates: [],
        latitudeDelta: 0.0922,
        longitudeDelta: 0.0421,
        modalNumber:0,
        reason:'create',
        start_address:'',
        end_address:'',
        index:0,
        Routes:[],
        step: 0,
    }
  }

  renderHeader() {
    return (
      <View style={styles.headerContainer}>
        <View style={styles.headerTitle}>
          <TouchableOpacity onPress={()=>Actions.pop()}>
            <Ionicons name="ios-arrow-back" color={"white"} size={30} />
          </TouchableOpacity>
          <Text style={{ marginLeft: 30, color: "white", fontSize: 18 }}>
            {this.props.data.DeliveryNoteNumber}
          </Text>
        </View>
      </View>
    );
  }

  onPause(){
    this.setState({modalNumber:1})
  }

  onChat(){
    Actions.Chat({channel:this.props.data.DeliveryNoteNumber})
  }

  onList(){
    this.setState({modalNumber:3})
  }

  onReroute(){
    this.setState({modalNumber:2})
  }

  onDetail(){
    Actions.CompleteDelivery({data:this.props.data, arrivalTime: '123123'})
  }


  renderModal(){
    return(
      <Modal
        visible={this.state.modalNumber>0?true:false}
        transparent={true}
        onRequestClose={() => {}}
      >
        <View style={styles.modalContainer}>
          <View style={styles.content}>
            {this.state.modalNumber==1&&this.renderPause()}
            {this.state.modalNumber==2&&this.renderReroute()}
            {this.state.modalNumber==3&&this.renderList()}
          </View>
        </View>
      </Modal>
    )
  }

  renderPause(){
    return(
      <View style={{paddingVertical:15}}>
        <Text stye={{color:'#aaa', fontSize:12}}>2:30 PM</Text>
        <Text stye={{color:'#333', fontSize:12}}>jalan cempaka, 1.2km</Text>

        <Text style={{color:'grey', fontSize:15, marginTop:15}}>What was your reason for pausing?</Text>
        <View style={{flexDirection:'row', alignItems:'center', justifyContent:'center', marginTop:15}}>
          <View style={{alignItems:'center'}}>
            <TouchableOpacity onPress={()=>this.setState({modalNumber:0})} style={{backgroundColor:commonColors.blue, height:40, width:40, borderRadius:20, alignItems:'center', justifyContent:'center'}}>
              <MaterialIcons name="wc" color={'white'} size={20}/>
            </TouchableOpacity>
            <Text style={{fontSize:12, color:'#aaa', marginTop:5}}>Toilet</Text>
          </View>
          <View style={{width:30}}/>
          <View style={{alignItems:'center'}}>
            <TouchableOpacity onPress={()=>this.setState({modalNumber:0})} style={{backgroundColor:commonColors.green, height:40, width:40, borderRadius:20, alignItems:'center', justifyContent:'center'}}>
              <MaterialIcons name="restaurant" color={'white'} size={20}/>
            </TouchableOpacity>
            <Text style={{fontSize:12, color:'#aaa', marginTop:5}}>Meal</Text>
          </View>
        </View>
        <Text style={{color:'#aaa', fontSize:12, marginTop:15}}>Others</Text>
        <View style={{flexDirection:'row', width:'100%', marginTop:10}}>
              <TextInput
                ref="reason"
                autoCapitalize="none"
                autoCorrect={false}
                placeholder="Your Reason"
                placeholderTextColor={commonColors.placeholderText}
                textAlign="left"
                style={[styles.input, {flex:1}]}
                underlineColorAndroid="transparent"
                value={this.state.reason}
                onChangeText={text =>
                  this.setState({ reason: text.replace(/\t/g, "") })
                }
              />
              <TouchableOpacity onPress={()=>this.setState({modalNumber:0})} style={{backgroundColor:commonColors.blue, height:40, width:40, borderRadius:20, alignItems:'center', justifyContent:'center', marginLeft:15}}>
                <MaterialIcons name="check" color={'white'} size={20}/>
              </TouchableOpacity>
        </View>
      </View>
    )
  }

  getRoutes(){
    let {DestinationLatitude, DestinationLongitude, Warehouse} = this.props.data
    // //console.log('getRoutes', this.props.data)
    googleService.getTripPath(
      Warehouse.Latitude+','+Warehouse.Longitude,
      DestinationLatitude+','+DestinationLongitude,
      (err, res)=>{
        if ( err == null ){
          // calcDist(res)
          this.setState({Routes:res, start_address:res[0].start_address, end_address:res[0].end_address})
          if ( this.state.coordinates.length == 0 ){
            this.setState({coordinates:res[0].path, index:0})
            this.mapView.fitToCoordinates(res[0].path, {
              edgePadding: {
                right: width / 20,
                bottom: height / 20,
                left: width / 20,
                top: height / 20
              }
            });
          }
        }
      }
    )
  }

  componentDidMount(){
    this.getRoutes()
  }

  reroute(index){
    let reasons = ['', 'Miss Turn', 'Traffic', 'Toilet', 'Meal']
    this.setState({modalNumber:0})
    if (this.props.reset ) {
      if ( index < 4 ) this.props.reset(reasons[index])
      else this.props.reset(this.state.rerouteReason)
    }
    Actions.SelectRoute({data:this.props.data, routes:this.state.Routes, index:this.state.index, update:(index)=>{
        this.setState({coordinates:this.state.Routes[index].path, index})
      }})
  }

  renderReroute(){
    return(
      <View style={{paddingVertical:15}}>
        <Text stye={{color:'#aaa', fontSize:12}}>2:30 PM</Text>
        <Text stye={{color:'#333', fontSize:12}}>jalan cempaka, 1.2km</Text>

        <Text style={{color:'grey', fontSize:15, marginTop:15}}>What was your reason for rerouting?</Text>
        <View style={{flexDirection:'row', alignItems:'center', justifyContent:'center', marginTop:15}}>
          <View style={{alignItems:'center'}}>
            <TouchableOpacity onPress={()=>this.reroute(1)} style={{backgroundColor:commonColors.brown, height:40, width:40, borderRadius:20, alignItems:'center', justifyContent:'center'}}>
              <MaterialIcons name="warning" color={'white'} size={20}/>
            </TouchableOpacity>
            <Text style={{fontSize:12, color:'#aaa', marginTop:5}}>Miss turn</Text>
          </View>
          <View style={{width:30}}/>
          <View style={{alignItems:'center'}}>
            <TouchableOpacity onPress={()=>this.reroute(2)} style={{backgroundColor:commonColors.pink, height:40, width:40, borderRadius:20, alignItems:'center', justifyContent:'center'}}>
              <MaterialIcons name="traffic" color={'white'} size={20}/>
            </TouchableOpacity>
            <Text style={{fontSize:12, color:'#aaa', marginTop:5}}>Traffic</Text>
          </View>
          <View style={{width:30}}/>
          <View style={{alignItems:'center'}}>
            <TouchableOpacity onPress={()=>this.reroute(3)} style={{backgroundColor:commonColors.blue, height:40, width:40, borderRadius:20, alignItems:'center', justifyContent:'center'}}>
              <MaterialIcons name="wc" color={'white'} size={20}/>
            </TouchableOpacity>
            <Text style={{fontSize:12, color:'#aaa', marginTop:5}}>Toilet</Text>
          </View>
          <View style={{width:30}}/>
          <View style={{alignItems:'center'}}>
            <TouchableOpacity onPress={()=>this.reroute(4)} style={{backgroundColor:commonColors.green, height:40, width:40, borderRadius:20, alignItems:'center', justifyContent:'center'}}>
              <MaterialIcons name="restaurant" color={'white'} size={20}/>
            </TouchableOpacity>
            <Text style={{fontSize:12, color:'#aaa', marginTop:5}}>Meal</Text>
          </View>
        </View>
        <Text style={{color:'#aaa', fontSize:12, marginTop:15}}>Others</Text>
        <View style={{flexDirection:'row', width:'100%', marginTop:10}}>
              <TextInput
                ref="reason"
                autoCapitalize="none"
                autoCorrect={false}
                placeholder="Your Reason"
                placeholderTextColor={commonColors.placeholderText}
                textAlign="left"
                style={[styles.input, {flex:1}]}
                underlineColorAndroid="transparent"
                value={this.state.rerouteReason}
                onChangeText={text =>
                  this.setState({ rerouteReason: text.replace(/\t/g, "") })
                }
              />
              <TouchableOpacity onPress={()=>this.reroute(5)} style={{backgroundColor:commonColors.blue, height:40, width:40, borderRadius:20, alignItems:'center', justifyContent:'center', marginLeft:15}}>
                <MaterialIcons name="check" color={'white'} size={20}/>
              </TouchableOpacity>
        </View>
      </View>
    )
  }

  renderList(){
    return(
      <View style={{paddingVertical:15}}>
        {this.state.delayList.map((item, index)=>{
          return(
            <View key={index} style={{flexDirection:'row', alignItems:'center', marginTop:10}}>
              <View style={{backgroundColor:commonColors.brown, height:40, width:40, borderRadius:20, alignItems:'center', justifyContent:'center', marginLeft:15}}>
                <MaterialCommunityIcons name="truck" size={20} color={"white"} />
              </View>
              <View style={{marginLeft:15, justifyContent:'center'}}> 
                <Text style={{fontSize:12, color:'#aaa'}}>{item.reason}</Text>
                <Text style={{fontSize:13, color:'#333', fontWeight:'bold'}}>new EDA: {item.ETA}</Text>
              </View>
            </View>
          )
        })}
        <TouchableOpacity onPress={()=>this.setState({modalNumber:0})} style={{position:'absolute', top:0, right:15}}>
          <Ionicons name="ios-close-circle" color={'red'} size={30}/>
        </TouchableOpacity>
      </View>
    )
  }

  renderBody() {
      return(
          <View style={{paddingVertical:40}}>
              <View style={{flexDirection:'row', paddingHorizontal:15}}>
                  <TouchableOpacity onPress={()=>this.onChat()} style={[styles.circleButtonContainer,{backgroundColor:commonColors.green}]}>
                      <Ionicons name={'ios-chatboxes'} color={'white'} size={30}/>
                  </TouchableOpacity>
                  <View style={{flex:1}}/>
                  <TouchableOpacity onPress={()=>this.onReroute()} style={[styles.circleButtonContainer,{backgroundColor:commonColors.yellow}]}>
                      <Ionicons name={'md-warning'} color={'white'} size={28}/>
                  </TouchableOpacity>
                  <View style={{flex:1}}/>
                  <TouchableOpacity onPress={()=>this.onDetail()} style={styles.circleButtonContainer}>
                      <Ionicons name={'ios-paper'} color={'white'} size={30}/>
                  </TouchableOpacity>
              </View>
              <View style={[styles.center, {marginTop:30}]}>
                  <View style={[styles.flexLeftShadow,{width:180, height:40}]}>
                      <Text style={{fontWeight:'bold', color:'grey', fontSize:15}}>ETA</Text>
                      <Text style={{fontWeight:'bold', color:'#222', fontSize:28, marginLeft:15, marginRight:10}}>2:40 PM</Text>
                  </View>
                  <View style={{flexDirection:'row', marginTop:2}}>
                    <View style={[styles.flexLeftShadow,{width:78, height:34}]}>
                        <Text style={{fontWeight:'bold', color:'#222', fontSize:24}}>42</Text>
                        <Text style={{fontWeight:'bold', color:'grey', fontSize:15, marginLeft:5}}>KM</Text>
                    </View>
                    <View style={[styles.flexLeftShadow,{width:100, height:34, marginLeft:2}]}>
                        <Text style={{fontWeight:'bold', color:'#222', fontSize:24, marginLeft:5}}>2</Text>
                        <Text style={{fontWeight:'bold', color:'grey', fontSize:15}}>H</Text>
                        <Text style={{fontWeight:'bold', color:'#222', fontSize:24, marginLeft:5}}>40</Text>
                        <Text style={{fontWeight:'bold', color:'grey', fontSize:15}}>M</Text>
                    </View>
                  </View>
              </View>

          </View>
      )
  }

  renderBottom() {
      return(
        <View style={{flex:1, flexDirection:'row'}}>
            <MapView
                ref={c => this.mapView = c}
                // minZoomLevel={9}
                initialRegion={{
                    ...this.state.start,
                    latitudeDelta: this.state.latitudeDelta,
                    longitudeDelta: this.state.longitudeDelta,
                }}
                style={{ flex: 1 }}
                >
                    <MapView.Marker
                    coordinate={this.state.start}
                    />
                    <MapView.Marker
                    coordinate={this.state.target}
                    />
                {this.state.coordinates.length>1&&<MapView.Polyline
                    coordinates={this.state.coordinates}
                    strokeColor={commonColors.theme}
                    fillColor={commonColors.theme}
                    strokeWidth={5}
                />}
            </MapView>
            <View style={styles.infoBoard}>
                <Item icon={"md-radio-button-off"} name={"From"} content={Cache.company.CompanyName} isGrey />
                <Item icon={"md-pin"} name={"To"} content={this.props.data.Contact.ContactName} isGrey />
                <View style={{width:'100%', height:1, backgroundColor:'#ccc', marginTop:15}}/>
                <View style={{alignItems:'center', justifyContent:'center', flex:1}}>
                    <Text style={{fontSize:14, color:'black', textAlign:'center'}}>
                        {"To begin delivery,\ntap 'start'."}
                    </Text>
                </View>
            </View>
        </View>
      )
  }

  nextStep(){
      this.setState({step:this.state.step+1})
  }

  renderButton(){
      let color=[commonColors.blue, commonColors.yellow, commonColors.yellow, commonColors.yellow]
      return(
          <View style={{position:'absolute', top:65, left:0, width:'100%', alignItems:'center', justifyContent:'center'}}>
            <TouchableOpacity onPress={()=>this.nextStep()} activeOpacity={0.7} style={[styles.topButton, {backgroundColor:color[this.state.step]}]}>
                <Text style={{color:'white', fontWeight:'bold', fontSize:16}}>Start</Text>
            </TouchableOpacity>
          </View>
      )
  }

  render() {
    return (
      <View style={styles.container}>
        {this.renderHeader()}
        {this.renderBody()}
        {this.renderBottom()}
        {this.renderModal()}
        {this.renderButton()}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "rgb(250,250,250)"
  },
  headerContainer: {
    backgroundColor: commonColors.theme,
    height: 100,
    width: "100%",
    elevation: 4,
    shadowColor: "black",
    shadowOffset: { width: 4, height: 4 },
    shadowOpacity: 0.3,
    shadowRadius: 4
  },
  headerTitle: {
    flexDirection: "row",
    alignItems: "center",
    padding: 15
  },
  circleButtonContainer:{
      width: 60,
      height: 60,
      alignItems: 'center',
      justifyContent: 'center',
      elevation: 4,
      borderRadius: 30,
      shadowColor: 'black',
      shadowOffset: {width:3, height:3},
      shadowOpacity: 0.3,
      shadowRadius: 3,
      backgroundColor: commonColors.theme
  },
  flexLeftShadow:{
      elevation:1,
      shadowColor: 'black',
      shadowOffset: {width:1, height:1},
      shadowOpacity: 0.3,
      shadowRadius: 2,
      flexDirection: 'row',
      padding: 5,
      backgroundColor:'white',
      alignItems:'flex-end',
      justifyContent:'flex-end'
  },
  center:{
      alignItems: 'center',
      justifyContent: 'center'
  },
  infoBoard:{
      flex:1,
      backgroundColor:'white',
      elevation: 4,
      shadowColor: 'black',
      shadowOffset: {width:1, height:1},
      shadowOpacity: 0.3,
      shadowRadius: 3,
      padding:15,
  },
  modalContainer: {
    flex: 1,
    backgroundColor: "rgba(0, 0, 0,0.5)",
  },
  content:{
    position:'absolute',
    bottom:0,
    left:0,
    width:'100%',
    backgroundColor:'white',
    borderTopLeftRadius: 5,
    borderTopRightRadius: 5,
    paddingHorizontal: 15,
    paddingVertical: 15,
  },
  input: {
    fontSize: 14,
    color: commonColors.title,
    height: 40,
    alignSelf: "stretch",
    // marginHorizontal: 40,
    // borderColor: '#aaa',
    // borderWidth: 1,
    borderRadius: 3,
    marginBottom: 3,
    paddingHorizontal: 10,
    backgroundColor: commonColors.inputColor
  },
  topButton:{
      borderRadius:10,
      height:54,
      width:240,
      alignItems: 'center',
      justifyContent: 'center',
      backgroundColor:commonColors.blue,
      elevation: 4,
      shadowColor: 'black',
      shadowOffset: {width:4, height:4},
      shadowOpacity: 0.3,
      shadowRadius: 4,
  }
});
