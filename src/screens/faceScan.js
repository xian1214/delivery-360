"use strict";

import React, { PureComponent } from "react";

import {
  StyleSheet,
  View,
  Platform,
  Modal,
  TouchableOpacity,
  Text,
  StatusBar,
  Image,
  ScrollView,
  TextInput,
  ActivityIndicator,
  Alert
} from "react-native";


import * as commonColors from "../styles/colors";
import * as commonStyles from "../styles/styles";
import { screenWidth, screenHeight } from "../styles/styles";

import api from "../service/api";
import Cache from "../utils/cache";
import { Ionicons, Feather } from "@expo/vector-icons";
import { Actions } from "react-native-router-flux";
import { SlideButton, SlideDirection } from '../components/sliderButton';
import { BarCodeScanner, Permissions, Camera } from 'expo';
import HeaderBar from '../components/header'
import azureService from '../service/azureService'

export default class FaceScan extends PureComponent {
  constructor(props) {
    super(props);

    this.state = {
        isWaiting:false,
        type:Camera.Constants.Type.back,
        hasCameraPermission:null,
    };
  }

  async componentWillMount(){
    let {status} = await Permissions.askAsync(Permissions.CAMERA)
    if ( status == 'granted' ){
      this.setState({hasCameraPermission:true})
    }
  }

  renderIndicator() {
    return (
      <Modal
        visible={this.state.isWaiting}
        transparent={true}
        onRequestClose={() => {}}
      >
        <View style={styles.indicatorContainer}>
          <View style={styles.indicator}>
            <ActivityIndicator size="large" />
          </View>
        </View>
      </Modal>
    );
  }

  async capture(){
      this.setState({isWaiting:true})
    let photo = await this.camera.takePictureAsync({quality:0.8})
    // //console.log(photo)
    azureService.faceLogin(photo.uri, (err, res) => {
        if ( err == null ){
            api.faceLogin(res.personID, (err,res)=>{
                this.setState({isWaiting:false})
                if ( err == null ){
                    Actions.Main()
                }else{
                    setTimeout(()=>Alert.alert('Failed to login. Try again later'),500)
                }
            })
        }else{
            this.setState({isWaiting:false})
            setTimeout(()=>Alert.alert('You are a stranger!'),500)
        }
    })
  }

  render() {
    return (
      <View style={styles.container}>
        {this.state.hasCameraPermission === null
        ? <Text>Requesting for camera permission</Text>
        : this.state.hasCameraPermission === false
            ? <Text style={{ color: '#fff' }}>
                Camera permission is not granted
                </Text>
            :<Camera
                ref={ref=>this.camera=ref}
                type={this.state.type}
                style={{
                    flex:1, justifyContent:'center', alignItems:'center'
                }}
                ratio="16:9"
                >    
                <View style={{width:screenHeight+300, height:screenHeight+300, borderRadius:screenHeight/2+150, borderWidth:screenHeight/2, backgroundColor:'transparent', borderColor:'rgba(0,0,0,0.5)'}}/>
            </Camera>}
            <View style={{position:'absolute', left:0, bottom:0, height:100, width:'100%', paddingTop:30, justifyContent:'center', alignItems:'center', backgroundColor:'transparent'}}>                
                <TouchableOpacity onPress={()=>this.capture()}>
                    <Ionicons name="ios-radio-button-on" size={60} color={'white'}/>
                </TouchableOpacity>
                <TouchableOpacity style={{position:'absolute', right:15, bottom:20}}
                onPress={() => {
                    this.setState({
                      type: this.state.type === Camera.Constants.Type.back
                        ? Camera.Constants.Type.front
                        : Camera.Constants.Type.back,
                    });
                  }}>
                  <Ionicons name="md-sync" color={'white'} size={30}/>
                </TouchableOpacity>
            </View>
        <View style={{position:'absolute', top:0, left:0, backgroundColor:'transparent', width:'100%'}}>
            <View style={{width:'100%', height:24, backgroundColor:commonColors.theme, elevation:5}}/>
            <HeaderBar 
                title={'Face Scanning'} 
            />
            <Text style={{height:80, width:'100%', textAlign:'center', fontSize:16, color:'grey', fontWeight:'bold', marginTop:15}}>
                Align your face within frame to scan.
            </Text>
        </View>
        {this.renderIndicator()}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'rgb(249,249,249)'
  },
  indicatorContainer: {
    flex: 1,
    backgroundColor: "rgba(0, 0, 0,0.5)",
    alignItems: "center",
    justifyContent: "center"
  },
  indicator: {
    width: 80,
    height: 80,
    borderRadius: 5,
    shadowColor: "black",
    alignItems: "center",
    justifyContent: "center",
    shadowOffset: { width: 2, height: 2 },
    shadowOpacity: 0.4,
    shadowRadius: 3,
    backgroundColor: "white"
  },
});
