"use strict";

import React, { PureComponent } from "react";

import {
  StyleSheet,
  View,
  Platform,
  Modal,
  TouchableOpacity,
  Text,
  StatusBar,
  Image,
  Linking
} from "react-native";


import * as commonColors from "../styles/colors";
import * as commonStyles from "../styles/styles";
import { screenWidth, screenHeight } from "../styles/styles";

import * as actionTypes from "../store/common/actionTypes";

import { bindActionCreators } from "redux";
import { connect } from "react-redux";
// import * as actions from "../redux/actions";
import * as actions from '../store/common/actions'
import * as session from '../store/session/actions'

import api from "../service/api";
import Cache from "../utils/cache";
import { Ionicons, MaterialCommunityIcons } from "@expo/vector-icons";
import { Actions } from "react-native-router-flux";
import HeaderBar from '../components/header'
import {MapView} from 'expo'
import UtilService from '../utils/utils'

const Item = ({ icon, name, content, isGrey }) => (
    <View style={{ flexDirection: "row", alignItems: "center", marginTop: 5 }}>
      <Ionicons name={icon} size={16} color={"grey"} style={{ width: 20 }} />
      <Text style={{ fontSize: 13, color: "grey", marginLeft: 8, width: 50 }}>
        {name}
      </Text>
      <Text
        style={{
          fontSize: 15,
          color: isGrey ? "grey" : "#333",
          fontWeight: "bold",
          flex:1
        }}
      >
        {content}
      </Text>
    </View>
  );

class DeliveryDetail extends PureComponent {
  constructor(props) {
    super(props);

    this.state = {
        projectName:'Project Name',
        DONumber:'RT02342',
        clientName: 'Austin',
        clientAddress: 'No.1, Jalan Bukit Bintang, Kuala Lumpur',
        items:[
            {title:'Cement', quantity:'100kg'},
            {title:'Steel', quantity:'800kg'}
        ],
        latitudeDelta: 0.0922,
        longitudeDelta: 0.0421,
        curPos: {latitude:props.data.DestinationLatitude, longitude:props.data.DestinationLongitude}
    };
  }

  onChat(){
      Actions.Chat({channel:this.props.data})
  }

  render() {
      let {Project, ETD, route, DeliveryRoute, DestinationAddress, IssueTime, DeliveryNoteItems, Warehouse} = this.props.data
      let path = JSON.parse(DeliveryRoute.PathData)
    return (
      <View style={styles.container}>
        <HeaderBar title={'Details'}/>
        <View style={{width:'100%', backgroundColor:'white', padding:15, elevation:5, flexDirection:'row'}}>
            <View style={{flex:1}}>
                <Text style={{fontSize:16, color:'black', fontWeight:'bold'}}>{this.props.data.DeliveryNoteNumber}</Text>
                {this.props.data.Project&&<Text style={{fontSize:13, color:'#333', marginTop:5}}>{this.props.data.Project.Name}</Text>}
            </View>
            <View style={{width:1, height:'100%', backgroundColor:'#ccc', marginHorizontal:15}}/>
            <View style={{alignItems:'center'}}>
                <Text style={{color:'grey', fontSize:12}}>ETA</Text>
                <Text style={{fontWeight:'bold', color:'black', fontSize:18}}>{UtilService.getHourMinutes(Cache.originETA)}</Text>
            </View>
        </View>
        <View style={{width:'100%', backgroundColor:'white', padding:15 }}>
            <Item icon={"md-calendar"} name={"Date"} content={UtilService.getDateTime(ETD)} /> 
            <Item icon={"md-radio-button-off"} name={"From"} content={Warehouse.Address} isGrey />
            <Item icon={"md-pin"} name={"To"} content={DestinationAddress} isGrey />
            {<Text style={{ marginTop: 10, color: "grey" }}>
                Distance {(path.dist/1000).toFixed(0)}km        ETA {UtilService.getHourMinutes(Cache.originETA)}
            </Text>}
        </View>
        <View style={{borderTopColor:'grey', borderTopWidth:1, marginBottom:70}}>
            <View style={{flexDirection:'row', backgroundColor:commonColors.cardColor3, alignItems:'center', height:34, width:'100%', marginTop:8, paddingHorizontal:20}}>
                <Text style={{color:'grey', flex:1}}>Item Description</Text>
                <Text style={{color:'grey', width:80, textAlign:'right'}}>Quantity</Text>
            </View>
            {DeliveryNoteItems.map((item, index)=>{
                return(
                  <View key={index}>
                    <View style={{flexDirection:'row', alignItems:'center', height:34, width:'100%', marginTop:8, paddingHorizontal:20}}>
                        <Text style={{color:'grey', flex:1}}>{item.Item.Name}</Text>
                        <Text style={{color:'grey', width:80, textAlign:'right'}}>{item.PlannedQuantity}</Text>
                    </View>
                    {
                      item.Item.ShelfLifeTime>0 &&
                      <View style={{flexDirection:'row', alignItems:'center', width:'100%', paddingHorizontal:20}}>
                        <Text style={{color:'grey'}}>Shelf Life</Text>
                        <Text style={{color:'#333', fontWeight:'bold', marginHorizontal:10}}>{(item.Item.ShelfLifeTime/60).toFixed(0)} hr {item.Item.ShelfLifeTime%60} min</Text>
                        <View style={{flex:1, height:3, backgroundColor:'#eee'}}>
                          <View style={{width:UtilService.getPercentOfSelf(IssueTime,item.Item.ShelfLifeTime)+'%', backgroundColor:commonColors.blue, height:3}}></View>
                        </View>
                        <Text style={{color:'grey', marginLeft:10}}>{UtilService.getPastDateTime(IssueTime,item.Item.ShelfLifeTime)}</Text>
                      </View>
                    }
                    
                  </View>
                )
            })}
        </View>
        <TouchableOpacity onPress={()=>this.onChat()} style={{ position:'absolute', right:15, top:20}}>
            <Ionicons name={'md-chatboxes'} size={24} color={'white'}/>
        </TouchableOpacity>
      </View>
    );
  }
}

export default connect(
  state => ({
    location:state.common.location, 
  }),
  dispatch => ({
    actions: bindActionCreators({...actions,...session}, dispatch)
  })
)(DeliveryDetail);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop:24,
    backgroundColor: 'white'
  },

});
