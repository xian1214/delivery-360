"use strict";

import React, { PureComponent } from "react";
import {
  StyleSheet,
  View,
  Platform,
  Modal,
  TouchableOpacity,
  Text,
  Image,
  ActivityIndicator,
  Alert,
  ImageBackground,
  Switch,
  TextInput,
  RefreshControl,
  ScrollView
} from "react-native";


import * as commonColors from "../styles/colors";
import * as commonStyles from "../styles/styles";
import { screenWidth, screenHeight } from "../styles/styles";
import * as actionTypes from "../store/common/actionTypes";
import Localization from 'expo-localization'

import { bindActionCreators } from "redux";
import { connect } from "react-redux";
// import * as actions from "../redux/actions";
import * as actions from '../store/common/actions'
import * as session from '../store/session/actions'
import * as actions2 from '../redux/actions'
import * as actionsForChat from '../store/chat/actions'
import { Constants, Location, Permissions } from "expo";

import api from "../service/api";
import * as config from '../config'
import googleService from '../service/googleService'
import Cache from "../utils/cache";
// import { Ionicons, MaterialCommunityIcons } from "@expo/vector-icons";
import Ionicons from 'react-native-vector-icons/Ionicons'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import * as CONST from '../components/constant'
import { Actions } from "react-native-router-flux";
import UtilService from '../utils/utils'
import InformationBar from '../components/informationBar'
import moment from 'moment'
import Expo from 'expo'
//import BackgroundJob from "react-native-background-job";
const regularJobKey = "regularJobKey";
const LOCATION_MODE_MOBILE = 1
const LOCATION_MODE_CLOUD = 2
const LOCATION_MODE = LOCATION_MODE_MOBILE

import CountryPicker, {
  getAllCountries
} from 'react-native-country-picker-modal'
const NORTH_AMERICA = ['US','CN','MY','IN','AE']

function isSameDate(A, B) {
  if (
    A.getDate() == B.getDate() &&
    A.getMonth() == B.getMonth() &&
    A.getFullYear() == B.getFullYear()
  )
    return true;
  return false;
}

const Card = ({ color, iconColor, title, onPress }) => (
  <TouchableOpacity activeOpacity={0.8} onPress={()=>onPress()} style={[styles.cardContainer, { backgroundColor: color }]}>
    <View style={[styles.icon, {backgroundColor:iconColor}]}>
      <Ionicons name="md-checkmark" size={24} color={"white"} />
    </View>
    <View style={{ marginLeft: 10 }}>
      <Text style={{ fontSize: 14, color: "#fff", fontWeight: "bold" }}>
        {title}
      </Text>
    </View>
  </TouchableOpacity>
);
class Main extends PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      
      username: Cache.currentUser.user.FirstName+' '+Cache.currentUser.user.LastName,
      avatar: {uri:config.SERVICE_FILE_URL+Cache.currentUser.user.Avarta},
      avatarUrl:Cache.currentUser.user.Avatar,
      avatarInstance: {uri:config.SERVICE_FILE_URL+Cache.currentUser.user.Avarta},
      text:'---',
      upcoming:[],
      completed:[],
      showModal: false,
      isWaiting: false,
      upcomingCount:0,
      todayCount: 0,
      todayUpcomingCount:0,
      checkedGoogleService: true,
      checkedInTime:null,
      checkedOutTime:null,
      checkedIn: Cache.currentUser.user.CheckIn,
      queueIn: Cache.currentUser.user.QueueIn,
      queueNo: Cache.currentUser.user.QueueNo,
      checkinReason:'',
      isCheckin:false,
      isCheckout:false,
      showSecondCheckInModal:false,
      showCheckOutModal:false,
      shelfLifeAlerts:[],
      oldShelfLifeAlerts:[],
      cca2:'US',
      manualDrum:0,
      callingCode:null  
    };
    this.infoBar = null
    this.index = 0
    this.oldHour = 0
    this.shelfAlertFlags=[]
    
  }
  // refresh(){
  //   this.props.actions.refreshStats()
  //   //console.log('before updateCR')
  //   
  // }
  componentWillMount() {
    ////console.log('willmount1---')
    //this.props.actions.getTodayUpcomingDeliveryCount()
    //this.props.actions.getFutureUpcomingDeliveryCount()
    //this.props.actions.updateCurrentDelivery()
    ////console.log('willmount2---')
    Cache.codes = []
    api.getCodes((err,res)=>{
      if (err == null){
        res.map((item)=>{
          Cache.codes[item.CodeID]=item
        })
      }
    })
    api.getUser(Cache.currentUser.user.UserID, (err, res)=>{
      if(err){
        return
      }
      var today = new Date()
      if((new Date(res.CheckInOutTime)).toDateString()==today.toDateString())
      {
        this.setState({checkedIn: res.CheckIn, checkedInTime:res.CheckInOutTime,checkedOutTime:res.CheckOutTime})
      }
      else{
        api.checkOut('a',this.props.location.coords.latitude, this.props.location.coords.longitude, (err,res)=>{
          this.setState({checkedIn:false, checkedInTime:null,checkedOutTime:null})
        })
      }
      this.props.actions.changeQueueState(res.QueueIn,false)
    })
    
  }
  refreshCounts(){
    var driverID = Cache.currentUser.user.UserID
    this.props.actions.updateCurrentDelivery()
    this.setState({isWaiting:true})
    api.getStats(driverID,(err,res)=>{
      if(err){
        return
      }
      this.setState({isWaiting:false})
      this.setState({
        upcomingCount:res.UpcomingCnt,
        todayUpcomingCount:res.TodayCnt
      })
    })  
    //this.getTodayUpdates()
  }
  _onRefresh = () => {
    this.refreshCounts()
  }
  componentWillReceiveProps(nextProps){
    if ( nextProps.actionType == actionTypes.RECEIVED_NOTIFICATION ){
        if(nextProps.notification.data.type == CONST.NOTY_TYPE_DN_CREATED){
          Alert.alert(nextProps.notification.data.message);
          this.refreshCounts()
        }
        else if(
          (!!this.props.notification &&
          nextProps.notification.data.type == CONST.NOTY_TYPE_DRIVER_UNREAD_MESSAGE &&
          nextProps.notification.data.createdAt != this.props.notification.data.createdAt)
          || ((!!!this.props.notification) && (nextProps.notification.data.type == CONST.NOTY_TYPE_DRIVER_UNREAD_MESSAGE))
        ){
          if(!Cache.chatIsOpen){
            this.props.actions.plusUnreadMsgCnt()
            setTimeout(()=>{
              Alert.alert('UnreadCnt is. '+String(this.props.unreadCnt))
            },500)
          }

          
          
        }
        
    }

    if ( nextProps.actionType == actionTypes.REFRESH_STATS ){
        this.refreshCounts()
    }

    if ( nextProps.actionType == actionTypes.GET_UPCOMING_DELIVERIES_REFRESH )
    {
      if (nextProps.status==actionTypes.LOADING ){
        this.setState({isWaiting:true})
      }
      if (nextProps.status==actionTypes.FAILED){
        this.setState({isWaiting:false})
        Alert.alert('Failed to fetch deliveries')
      }
      if (nextProps.status==actionTypes.SUCCESS){
        this.setState({isWaiting:false})
        if ( this.props.currentDelivery ){
          setTimeout(()=>this.props.actions.updateCurrentDelivery(this.props.currentDelivery.DeliveryNoteID),20)
        }
      }
      if(nextProps.status == actionTypes.CHANGE_QUEUE_STATE){
       
      }
      
    }
  }

  uploadLocation(){
    if(this.props.locationMode!=CONST.TRACK_MODE_BYPHONE){
        return
    }
    let {currentDelivery, location} = this.props
    if ( currentDelivery == null || location == null ) return;

    if ( currentDelivery.OTWStatus || currentDelivery.Status==commonStyles.DELIVERY_STATUS_RETURNING){
      api.postLocation({
        DeliveryRouteID: currentDelivery.DeliveryRouteID,
        DeliveryNoteID: currentDelivery.DeliveryNoteID,
        UserID: currentDelivery.User.UserID,
        Lat: location.coords.latitude,
        Long: location.coords.longitude,
      },(err, res) => {
        if ( err == null && Cache.hasInternetConnection){
          this.props.actions.setVelocityMileage(currentVal)
          Cache.isWithinGeofence = res.DeliveryRoute.IsWithinGeofence
        }
       
      });
    }
  }
  // setShelfLifeAlert(element,i,needWarn){

  //   this.state.shelfLifeAlerts[i] = element
  //   this.setState({shelfLifeAlerts:[...this.state.shelfLifeAlerts]})
  //   this.state.oldShelfLifeAlerts = this.state.shelfLifeAlerts
  // }
  watchShelfTime(){
    let {currentDelivery} = this.props
    if ( currentDelivery == null) return;
    var dnItems = currentDelivery.DeliveryNoteItems
    dnItems.forEach((item,i) => {
      var shelfTime = item.Item.ShelfLifeTime * 60
      if(shelfTime==0)return
      var issued2CurrentTimeSpan = UtilService.getPassTimeBySecondFrom(currentDelivery.IssueTime) //s
      var overTimeSpan = issued2CurrentTimeSpan-shelfTime
      var dueTimeString = UtilService.convertToHourMinSec(Math.abs(overTimeSpan))
      if(overTimeSpan<0){
        this.shelfAlertFlags[i] = false
      }
      else{
        this.shelfAlertFlags[i] = true
      }
    });
    this.props.actions.setShelfLifeAlerts(this.shelfAlertFlags);
  }
  
  getLocation(){
    if (Platform.OS === "android" && !Constants.isDevice) {
      this.setState({
        errorMessage:
          "Oops, this will not work on Sketch in an Android emulator. Try it on your device!"
      });
    } else {
      this._getLocationAsync();
    }
  }

  onCancel(){
    this.setState({showModal: false})
  }

  onGo(){
    this.setState({showModal: false})
    api.logout()
    Actions.SignIn()
  }

  renderModal2() {
    return (
      <Modal
        visible={this.state.showSecondCheckInModal}
        transparent={true}
        onRequestClose={() => {}}
      >
        <View style={styles.modalContainer}>
          <View style={styles.modal}>
            <View style={{flex:1, padding:15, width:'100%', justifyContent:'center'}}>
            
              <Text style={{fontSize:13, fontWeight:'bold', color:'grey',margin:10, textAlign:'center'}}>You are checking in a 2nd time</Text>
              <TextInput
                ref="reason"
                autoCapitalize="none"
                autoCorrect={false}
                placeholder="reason"
                placeholderTextColor={commonColors.placeholderText}
                textAlign="left"
                style={styles.input}
                underlineColorAndroid="transparent"
                returnKeyType={"go"}
                keyboardType='numbers-and-punctuation'
                value={this.state.checkinReason}
                onChangeText={text =>
                  this.setState({ checkinReason: text.replace(/\t/g, "") })
                }
                onSubmitEditing={() => {
                  // if ( this.state.checkinReason == '' ) return;
                  // api.checkIn(this.state.checkinReason, 1, this.props.location.coords.latitude, 
                  //   this.props.location.coords.longitude, (err, res)=>{
                  //   if ( err == null ){
                  //     this.setState({checkinReason:''})
                  //     this.setState({checkedInTime:res.CheckInAt})
                  //     this.setState({checkedOutTime:res.CheckOutAt})
                  //     this.setState({checkedIn:true})
                  //   }
                  // })
                }}
              />
            </View>
            <View style={{height:36, flexDirection:'row', width:'100%', borderBottomRightRadius:5, borderBottomLeftRadius:5, overflow:'hidden', borderTopWidth:1, borderTopColor:'#ccc'}}>
              <TouchableOpacity onPress={()=>this.setState({showSecondCheckInModal:false})} style={{flex:1, alignItems:'center', justifyContent:'center', backgroundColor:'rgb(249,249,249)'}}>
                <Text style={{color:commonColors.theme, fontSize:13}}>Cancel</Text>
              </TouchableOpacity>
              <TouchableOpacity onPress={()=>{
                this.setState({showSecondCheckInModal:false})
                this.setState({isWaiting:true})
                api.checkIn(this.state.checkinReason, 1, this.props.location.coords.latitude, 
                  this.props.location.coords.longitude, (err, res)=>{
                  this.setState({isWaiting:false})
                  if ( err != null ){
                    //this.setState({checkedIn: false})
                    Alert.alert('Check in failure')
                  }else{
                    this.setState({checkinReason:''})
                    this.setState({checkedIn:true})
                    this.setState({checkedInTime:res.CheckInAt})
                    this.setState({checkedOutTime:res.CheckOutAt})
                  }
               
                })
               
              }} style={{flex:1, alignItems:'center', justifyContent:'center', backgroundColor:commonColors.button}}>
                <Text style={{color:'white', fontSize:13, fontWeight:'bold'}}>Check In</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </Modal>
    );
  }

  renderModal() {
    return (
      <Modal
        visible={this.state.showModal}
        transparent={true}
        onRequestClose={() => {}}
      >
        <View style={styles.modalContainer}>
          <View style={styles.modal}>
            <View style={{flex:1, justifyContent:'center', alignItems:'center'}}>
              <Text style={{fontSize:14, color:'grey',margin:10, textAlign:'center'}}>Do you want to sign out?</Text>
            </View>
            <View style={{height:40, flexDirection:'row', width:'100%', borderBottomRightRadius:5, borderBottomLeftRadius:5, overflow:'hidden', borderTopWidth:1, borderTopColor:'#ccc'}}>
              <TouchableOpacity onPress={()=>this.onCancel()} style={{flex:1, alignItems:'center', justifyContent:'center', backgroundColor:'rgb(249,249,249)'}}>
                <Text style={{color:commonColors.theme, fontSize:13}}>Cancel</Text>
              </TouchableOpacity>
              <TouchableOpacity onPress={()=>this.onGo()} style={{flex:1, alignItems:'center', justifyContent:'center', backgroundColor:commonColors.button}}>
                <Text style={{color:'white', fontSize:13, fontWeight:'bold'}}>Sign Out</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </Modal>
    );
  }

  renderModal3() {
    return (
      <Modal
        visible={this.state.showCheckOutModal}
        transparent={true}
        onRequestClose={() => {}}
      >
        <View style={styles.modalContainer}>
          <View style={styles.modal}>
            <View style={{flex:1, justifyContent:'center', alignItems:'center'}}>
              <Text style={{fontSize:14, color:'grey',margin:10, textAlign:'center'}}>Do you want to check out?</Text>
            </View>
            <View style={{height:40, flexDirection:'row', width:'100%', borderBottomRightRadius:5, borderBottomLeftRadius:5, overflow:'hidden', borderTopWidth:1, borderTopColor:'#ccc'}}>
              <TouchableOpacity onPress={()=>{this.setState({showCheckOutModal:false})}} style={{flex:1, alignItems:'center', justifyContent:'center', backgroundColor:'rgb(249,249,249)'}}>
                <Text style={{color:commonColors.theme, fontSize:13}}>Cancel</Text>
              </TouchableOpacity>
              <TouchableOpacity onPress={()=>{
                this.setState({showCheckOutModal:false})
                this.setState({isWaiting:true})
                api.checkOut('a',this.props.location.coords.latitude, this.props.location.coords.longitude, (err,res)=>{
                  this.setState({isWaiting:false})
                  if(err){
                   return 
                  }
                  this.setState({checkedIn:err!=null})
                  this.setState({checkedOutTime:res.CheckOutAt})
                  this.setState({checkedInTime:res.CheckInAt})
                })
              
              }} style={{flex:1, alignItems:'center', justifyContent:'center', backgroundColor:commonColors.button}}>
                <Text style={{color:'white', fontSize:13, fontWeight:'bold'}}>Check Out</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </Modal>
    );
  }

  async componentDidMount(){
    console.log('hello world')
    this.setState({avatarInstance:{...this.state.avatarInstance}})
    this.props.actions.setLocationMode(CONST.TRACK_MODE_BYPHONE);
    
    if(!!!Cache.pushToken){
      Alert.alert("Failed to get push token")
    }  
    let userLocaleCountryCode = Localization.country
    const userCountryData = getAllCountries()
    .filter(country => NORTH_AMERICA.includes(country.cca2))
    .filter(country => country.cca2 === userLocaleCountryCode)
    .pop()

    let {callingCode,cca2} = this.state

    callingCode = null
    cca2 = userLocaleCountryCode
    if (!cca2 || !userCountryData) {
      cca2 = 'US'
      callingCode = '1'
    } else {
      callingCode = userCountryData.callingCode
    }

    this.setState({cca2:this.state.cca2})
    this.setState({callingCode:this.state.callingCode})
    
    api.getCompany((err, res)=>{
      if ( err == null ){
        Cache.company = res;
      }
    })
    
    this.getLocation()
    this.props.actions.loginUser('abc@ex.com', '123123')
    this.refreshCounts()
  }
  
  async handleLocation(){
   
    
    let location = await Location.getCurrentPositionAsync({
      enableHighAccuracy:true,
      maximumAge:2000,
    })
    
    this.props.actions.setBackupLocation(location)
    if(this.props.locationMode==CONST.TRACK_MODE_BYPHONE){
      let location = await Location.getCurrentPositionAsync({
        enableHighAccuracy:true,
        maximumAge:2000,
      })
       
       this.props.actions.setLocation(location)
    
    } 
    else if(this.props.locationMode==CONST.TRACK_MODE_BYTRACKER)
    {
      console.log('arrived location on main timer2',Cache.currentImei)
      api.getLocationFromCloud(Cache.currentImei,(error,res)=>{
          if(error){
            return
          }
          if(!!!res || !!!res[0]){
            return
          }
          var location={
            coords:{
              latitude:Number(res[0].Latitude),
              longitude:Number(res[0].Longitude)             
            },
            speed:Number(res[0].Speed),
            direct:res[0].Direct
          }
          console.log('arrived location on main timer3',location)
          this.props.actions.setLocation(location)          
      })
    }
    else{
     
    }
  }
  _getLocationAsync = async () => {
    let { status } = await Permissions.askAsync(Permissions.LOCATION);
    if (status !== "granted") {
      this.setState({
        errorMessage: "Permission to access location was denied"
      });
      Actions.SignIn()
      return;
    }
    
    this.handleLocation()
 
    this.myInterval = setInterval(()=>{
      this.handleLocation()
      let d = new Date()
      if ( this.oldHour==23 && d.getHours()==0 && this.props.location ){
      
        api.checkOut('a',this.props.location.coords.latitude, this.props.location.coords.longitude, (err,res)=>{
          this.setState({checkedIn:err!=null})
          this.setState({
            checkedInTime:null,
            checkedOutTime:null
          })
          this.setState({isCheckout:false})
        })
       
      }
      this.oldHour = d.getHours(); 
    }, CONST.GET_LOCATION_PERIOD)
  };

  renderHeader() {
    let {avatarUrl,avatarInstance} = this.state
    return (
      <View style={styles.headerContainer}>
        <View style={{flexDirection:'row', alignItems:'center',justifyContent:'center'}}>
          <Text style={styles.headerText}>TransGuard</Text>
          <View style={{flex:1}}/>
          {<View style={{marginTop:-5}}>
              
              <CountryPicker
                countryList={NORTH_AMERICA}
                onChange={value => {
                  Cache.locale = value.cca2
                  //Alert.alert(value.cca2)
                  this.infoBar.forceUpdate()
                  this.setState({ cca2: value.cca2, callingCode: value.callingCode })
                }}
                cca2={this.state.cca2}
                translation="eng"
              />
          </View>}         
          <TouchableOpacity style={{marginHorizontal:20}} onPress={() => {Actions.AsyncScreen()}}>
            <Ionicons name={"md-sync"} size={24} color={"white"} />
          </TouchableOpacity>
           
          <TouchableOpacity onPress={() => {this.setState({showModal:true})}}>
            <Ionicons name={"md-power"} size={20} color={"white"} />
          </TouchableOpacity>
        </View>
        <View style={{flexDirection:'row', alignItems:'center', marginTop:20}}>
          <TouchableOpacity activeOpacity={0.8} onPress={()=>Actions.Profile()}>
            {
              <Image source={commonStyles.noUser} style={{position:'absolute',left:0,top:0, width:50, height:50, borderRadius:25}}/>
            }
            {
              !!avatarInstance.uri &&
              <Image source={avatarInstance}  style={{width:50, height:50, borderRadius:25}}/>
            }
          </TouchableOpacity>
          <Text style={styles.username}>{this.state.username}</Text>
          <TouchableOpacity onPress={() => Actions.CheckinList()}>
            <Ionicons name={"md-arrow-forward"} size={20} color={"white"} />
          </TouchableOpacity>
        </View>
      </View>
    );
  }

  renderUpComings(){
    return(
      <TouchableOpacity activeOpacity={0.8} onPress={()=>Actions.Upcoming()} style={styles.upcomingContainer}>
        <View style={{alignItems:'center', flex:1}}>
          <View style={[{width:60, height:60, borderRadius:30, alignItems:'center', justifyContent:'center', backgroundColor:commonColors.yellow}]}>
            <MaterialCommunityIcons name="truck" size={30} color={"white"} />
          </View>
          <Text style={{fontSize:14, color:'white', fontWeight:'bold', marginTop:8}}>{Cache.getLang('label.upcoming_deliveries')}</Text>
        </View>
        <View style={{alignItems:'center', flex:1}}>
          <Text style={{fontSize:36, color:'white', fontWeight:'bold'}}>
            {this.state.todayUpcomingCount}
          </Text>
          <Text style={{fontSize:14, color:'white', fontWeight:'bold', marginTop:8}}>
            {Cache.getLang('label.today')}
          </Text>
        </View>
        <View style={{height:'100%', width:1, backgroundColor:'#ccc'}}/>
        <View style={{alignItems:'center', flex:1}}>
          <Text style={{fontSize:36, color:'white', fontWeight:'bold'}}>
            {this.state.upcomingCount}
          </Text>
          <Text style={{fontSize:14, color:'white', fontWeight:'bold', marginTop:8}}>
            {Cache.getLang('label.upcoming')}
          </Text>
        </View>
      </TouchableOpacity>
    )
  }

  renderContent() {
    return (
      <View style={{flex:1, padding:10, justifyContent:'center',backgroundColor:'',paddingTop:10}}>
        <ScrollView
          refreshControl={
            <RefreshControl
              refreshing={this.state.isWaiting}
              onRefresh={this._onRefresh}
            />
          }
        >
        {this.renderChatList()}
        <View style={{height:12}}/>
        {this.renderUpComings()}
        <View style={{flexDirection:'row', marginTop:20}}>
          <Card
            color={commonColors.theme}
            iconColor={commonColors.lightGreen}
            title= {Cache.getLang('label.completed_deliveries')}
            onPress={()=>Actions.Completed()}
          />
          <View style={{width:20}}/>
          <Card
            color={commonColors.theme}
            iconColor={commonColors.lightBlue}
            title= {Cache.getLang('label.delivery_summary')}
            onPress={()=>{Actions.Summary({data:this.state.completed})}}
          />
        </View>
        </ScrollView>
      </View>
    );
  }

  renderIndicator() {
    if ( Actions.currentScene != 'Main') return null
    return (
      <Modal
        visible={this.state.isWaiting}
        transparent={true}
        onRequestClose={() => {}}
      >
        <View style={styles.indicatorContainer}>
          <View style={styles.indicator}>
            <ActivityIndicator size="large" />
          </View>
        </View>
      </Modal>
    );
  }

  queueIn(value){
    this.props.actions.changeQueueState(value)
    
  }

  checkedIn(value){
    
    if(!value){
      if(this.state.checkedIn){
        this.setState({showCheckOutModal:true})
        return
      } 
      else{
        return
      } 
    }
    let {checkedInTime} = this.state
    if( (new Date(checkedInTime)).toDateString()===(new Date()).toDateString()){
      this.setState({showSecondCheckInModal:true})
      return
    }
    this.setState({isWaiting:true})
    api.checkIn(this.state.checkinReason, 1, this.props.location.coords.latitude, 
      this.props.location.coords.longitude, (err, res)=>{
      if (!!err){
        this.setState({isWaiting:false})
        Alert.alert("Check in failed")
        this.setState({checkedIn: false})
        this.setState({checkedInTime:null})
        this.setState({checkedOutTime:null})
      }else{
        this.setState({isWaiting:false})
        this.setState({checkinReason:''})
        this.setState({checkedInTime:res.CheckInAt})
        this.setState({checkedOutTime:res.CheckOutAt})
        this.setState({checkedIn:true})
      }
    })
     
  }

  renderStatus(){
    let {checkedInTime,checkedOutTime} = this.state
    let todayDate = (new Date()).getDate()
    return(
      <View style={{width:'100%', height:50, flexDirection:'row', alignItems:'center', backgroundColor:'rgb(87,87,87)', paddingVertical:5}}>
        <View style={{flex:1, flexDirection:'row', paddingRight:5, alignItems:'center', paddingLeft:20}}>
          <Switch value={this.state.checkedIn} onValueChange={(value)=>this.checkedIn(value)}/>
          <View style={{flex:1, alignItems:'center'}}>
            <Text style={{color:'white', fontWeight:'bold', fontSize:15,textAlign:'center'}}>{Cache.getLang('label.checkin')}</Text>
            <Text style={{color:'white', fontSize:12, textAlign:'center'}}>
              {(!!checkedInTime && this.compareDate((new Date()),(new Date(checkedInTime))))?moment(new Date(checkedInTime)).format('HH:mm A')+'-':''}
              {(!!checkedOutTime && this.compareDate((new Date()),(new Date(checkedOutTime))))?moment(new Date(checkedOutTime)).format('HH:mm A'):''}
            </Text> 
          </View>
        </View>
        <View style={{width:1, height:'100%', backgroundColor:'grey'}}/>
        <View style={{flex:1, flexDirection:'row', alignItems:'center', paddingRight:20, paddingLeft:5}}>
          <Switch value={this.props.queueIn} onValueChange={(value)=>this.queueIn(value)}/>
          <View style={{flex:1, textAlign:'center'}}>
            <Text style={{color:'white', fontWeight:'bold', fontSize:15, textAlign:'center'}}>{Cache.getLang('label.queuein')}</Text>
            {this.props.queueIn&&<Text style={{color:'white', fontSize:12, textAlign:'center'}}>No {Cache.currentUser.user.QueueNo}</Text>}
          </View>
        </View>

      </View>
    )
  }
  compareDate(src,dest){
    if(
      src.getDate()==dest.getDate() &&
      src.getFullYear()==dest.getFullYear() &&
      src.getMonth()==dest.getMonth() 
    ){
      return true
    }
    else{
      return false
    }
  }
  goChat(item){
    this.setState({goChat:true})
    api.getDelivery(item.DeliveryNoteID,(error,res)=>{
      if(error){
        return
      }
      this.setState({goChat:false})
      Actions.Chat({update:this.refresh.bind(this),channel:res})
    })
}

  renderChatList(){
    let {unreadChats} = this.state
    var testCnt = 29
    return(
      <View style={{padding:15, backgroundColor:'white',borderRadius:10}}>
        <View style={{flexDirection:'row', alignItems:'center'}}>
          <Text style={{fontSize:18, color:'#777', flex:1, fontWeight:'bold'}}>{'Chats'}</Text>
           <ScrollView horizontal={true} style={{flexDirection:'row-reverse'}}> 
            {!!unreadChats && unreadChats.map((item,i)=>{
              return(
                <TouchableOpacity onPress={()=>{this.goChat(item)}} key={'chat_'+i}style={{width:45,height:45,padding:10,position:'relative'}}>
                  <Image   onError={(e)=>{this.imageFailure(i)}} source={(!!(item.User.Avatar))?{uri:config.SERVICE_FILE_URL+item.User.Avatar}:commonStyles.noUser} style={{width:40, height:40, borderRadius:15,position:'absolute',marginTop:5}}/>
                  <View style={{right:0,top:0,width:(item.UnreadCount>=100)?30:20,height:20,backgroundColor:'red',borderRadius:10,position:'absolute',alignItems:'center',justifyContent:'center'}}>
                      <Text style={{fontWeight:'bold',color:'white',borderRadius:10,position:'absolute',textAlign:'center'}}>{(item.UnreadCount>=100)?'99+':item.UnreadCount}</Text>
                  </View>
                </TouchableOpacity>
              )
            })}
            </ScrollView>
          <TouchableOpacity onPress={()=>Actions.ChatList()}>
            <Text style={{fontSize:16, fontWeight:'bold', color:commonColors.blue, marginLeft:15}}>{"View all"}</Text>
          </TouchableOpacity>
        </View>
        
      </View>
    )
  }
  render() {
    //return (<View style={styles.container}><Text>Hello World</Text></View>)
    let {checkedInTime,checkedOutTime} = this.state
    let {currentDelivery, internetConnection} = this.props
    return (
      <View style={styles.container}>
        <InformationBar ref={(c)=>{this.infoBar = c}} iConnection={internetConnection} currentDelivery={currentDelivery}/>
        <ImageBackground source={commonStyles.background} style={{flex:1}}>
          {this.renderHeader()}
          {this.renderStatus()}
          {/* {this.renderChatList()}  */}
          {this.renderContent()}
          {this.renderModal()}
          {this.renderModal2()}
          {this.renderModal3()}
        </ImageBackground>
      </View>
    );
  }
}

export default connect(
  state => ({
    status: state.common.status,
    actionType: state.common.type,
    location:state.common.location, 
    heading:state.common.heading,
    notification: state.common.notification,
    upcomingDeliveries: state.common.upcomingDeliveries,
    completedDeliveries: state.common.completedDeliveries,
    todayUpcomingCount: state.common.todayUpcomingCount,
    otherUpcomingCount: state.common.otherUpcomingCount,
    queueIn: state.common.queueIn,
    currentDelivery: state.common.currentDelivery,
    internetConnection: state.common.internetConnection,
    locationMode:state.common.locationMode,
    unreadCnt:state.chat.unreadCnt
  }),
  dispatch => ({
    actions: bindActionCreators({...actionsForChat,...actions,...session,...actions2}, dispatch)
  })
)(Main);
const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop:24,
    backgroundColor: commonColors.theme
  },
  headerContainer: {
    padding: 20,
    width: "100%",
    elevation:5,
    shadowColor: 'black',
    shadowOpacity: 0.4,
    shadowOffset: {width:3, height:3},
    shadowRadius: 4,
    backgroundColor: commonColors.theme
  },
  headerText: {
    fontSize: 20,
    color: "white",
  },
  avatarContainer: {
    alignItems: "center",
    justifyContent: "center",
    padding: 40
  },
  avatar: {
    width: 60,
    height: 60,
    borderRadius: 30,
  },
  username: {
    fontSize: 18,
    fontWeight: "bold",
    color: "white",
    marginLeft:15,
    flex:1,
  },
  cardContainer: {
    backgroundColor: "white",
    flexDirection: "row",
    paddingHorizontal: 15,
    paddingVertical: 40,
    elevation: 4,
    alignItems: "center",
    borderRadius:4,
    flex:1,
    shadowColor: 'black',
    shadowOpacity: 0.4,
    shadowOffset: {width:3, height:3},
    shadowRadius: 4,
  },
  icon: {
    width: 40,
    height: 40,
    borderRadius: 20,
    alignItems: "center",
    justifyContent: "center"
  },
  modalContainer: {
    flex: 1,
    backgroundColor: "rgba(0, 0, 0,0.5)",
    alignItems: "center",
    justifyContent: "center"
  },
  modal: {
    width: 260,
    height: 160,
    borderRadius: 5,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "white",
    elevation:3,
  },
  indicatorContainer: {
    flex: 1,
    backgroundColor: "rgba(0, 0, 0,0.5)",
    alignItems: "center",
    justifyContent: "center"
  },
  indicator: {
    width: 80,
    height: 80,
    borderRadius: 5,
    shadowColor: "black",
    alignItems: "center",
    justifyContent: "center",
    shadowOffset: { width: 2, height: 2 },
    shadowOpacity: 0.4,
    shadowRadius: 3,
    backgroundColor: "white"
  },
  upcomingContainer:{
    width:'100%',
    backgroundColor:commonColors.theme,
    elevation:5,
    borderRadius:4,
    flexDirection:'row',
    alignItems: 'center',
    paddingHorizontal:20,
    paddingVertical:40,
    shadowColor: 'black',
    shadowOpacity: 0.4,
    shadowOffset: {width:3, height:3},
    shadowRadius: 4,
  },
  input: {
    fontSize: 13,
    color: commonColors.title,
    height: 44,
    alignSelf: "stretch",
    borderRadius: 3,
    marginBottom: 3,
    paddingHorizontal: 10,
    backgroundColor: commonColors.inputColor,
    width:'100%'
  },
});
