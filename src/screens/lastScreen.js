"use strict";

import React, { PureComponent } from "react";

import {
  StyleSheet,
  View,
  Platform,
  Modal,
  TouchableOpacity,
  Text,
  StatusBar,
  Image,
  Linking,
  Alert,
  NativeModules,
  ActivityIndicator
} from "react-native";


import * as commonColors from "../styles/colors";
import * as commonStyles from "../styles/styles";
import { screenWidth, screenHeight } from "../styles/styles";

import * as actionTypes from "../store/common/actionTypes";

import { bindActionCreators } from "redux";
import { connect } from "react-redux";
// import * as actions from "../redux/actions";
import * as actions from '../store/common/actions'
import * as session from '../store/session/actions'

import api from "../service/api";
import UtilService from '../utils/utils'
import Cache from "../utils/cache";
import * as config from '../config'
import { Ionicons, MaterialCommunityIcons } from "@expo/vector-icons";
import { Actions } from "react-native-router-flux";
import HeaderBar from '../components/header'
import QRCode from 'react-native-qrcode';
// import { QRCode } from 'react-native-custom-qr-codes';
import InformationBar from '../components/informationBar'
const PrintManager = NativeModules.PrintManager;

class LastScreen extends PureComponent {
    constructor(props) {
      super(props);
  
      this.state = {
          qrValue:[],
          content:'empty',
          finishingDelivery:false
      };
      this.props.data.DeliveryNoteItems.map((item, index)=>{
          this.state.qrValue.push({name:item.Item.Name, quantity:item.PlannedQuantity, delivered:item.DeliveredQuantity})
      })
  
      let {Username, Avarta, UserID, FirstName, LastName } = Cache.currentUser.user
      this.state.content = JSON.stringify({items:this.state.qrValue, signature:this.props.signature})
    }
  
    componentDidMount(){
      // //console.log(this.props.currentDelivery)
      //console.log('this.props.localSignature..on lastscreen************',this.props.localSignature)
    }
  
    next(){
        this.setState({finishingDelivery:true})
        if(this.props.currentDelivery==null){
          return
        }
        api.createAlert({
            Type: commonStyles.Finish,
            Level: 1,
            Message: '',
            ReferenceID: this.props.currentDelivery.DeliveryNoteID,
            UserID: this.props.currentDelivery.User.UserID
        }, (err1, res1)=>{
            this.setState({finishingDelivery:false})
            if ( Cache.hasInternetConnection ){
                this.props.actions.updateCurrentDelivery()
                this.props.actions.getTodayUpcomingDeliveryCount()
                this.props.actions.getFutureUpcomingDeliveryCount()
            }else{
                this.props.actions.manualUpdate(null)
            }
            this.props.actions.refreshStats()
            setTimeout(() => {
                Actions.popTo('Main')    
            }, 100);
            
        })
        
        
       
    }
  
    returning(){
        Actions.BranchList()
    }
  
    back(){
      Actions.popTo('Main')
      this.props.actions.refreshStats()
      this.props.actions.getTodayUpcomingDeliveryCount()
      this.props.actions.getFutureUpcomingDeliveryCount()
    }
    print(){
        var str = "";
        this.props.data.DeliveryNoteItems.map((item, index)=>{    
          str+="\n"
          str+="\n"
          str+="    "+ item.Item.Name + "      "
          str+=item.DeliveredQuantity+"\n"
        })
        var text = "John";
        var imgPath = config.SERVICE_FILE_URL+this.props.data.Signature
        
      
        var noInternetFlag = !Cache.hasInternetConnection
        //noInternetFlag = true;
        if(noInternetFlag){
            if(this.props.localSignature==null){
                Alert.alert("No report to print")
                return
            }
            var isLocalFile = true
            var strList= this.props.localSignature.split('/') 
            var filename = strList[strList.length-1]; 
            var localImgPath = "/sdcard/saved_signature/" + filename
             PrintManager.printText(isLocalFile,str.toString(),localImgPath.toString(),(greeting)=>{
                //console.log('arrved string>>>>>>',greeting)
            })   
        }
        else{
            var isLocalFile = false
            PrintManager.printText(isLocalFile,str.toString(),imgPath.toString(),(greeting)=>{
                //console.log('arrved string>>>>>>',greeting)
            })    
        }
        //console.log('localImgPah on lastscreen--',localImgPath)
        
      }
    render() {
      let {Project, DeliveryNoteNumber, Contact, DeliveryNoteItems} = this.props.data
      let {currentDelivery, internetConnection} = this.props 
      if (this.state.finishingDelivery){
        return(
           <View style={{flex:1,alignItems:'center',justifyContent:'center'}}>
               <Text style={{color:'blue',fontWeight:'bold',fontSize:20}}>{this.state.statusText}</Text>
               <ActivityIndicator size={150}/>
           </View>
       )
      }
      return (
        <View style={styles.container}>
          <InformationBar iConnection={internetConnection} currentDelivery={currentDelivery}/>   
          <HeaderBar title={DeliveryNoteNumber}
              back={()=>this.back()}
              rightElement={(
                  <View style={{flexDirection:'row', alignItems:'center'}}>
                      <TouchableOpacity onPress={()=>Actions.AlertList({data:this.props.data})} style={{padding:10}}>
                          <Ionicons name={'md-warning'} size={24} color={'white'}/>
                      </TouchableOpacity>
                      <TouchableOpacity style={{padding:10}} onPress={()=>Actions.Chat({channel:currentDelivery})}>
                          <Ionicons name={'md-chatboxes'} size={24} color={'white'}/>
                      </TouchableOpacity>
                  </View>
              )}
          />
          {/* <Text>{(!!currentDelivery)?(currentDelivery.DeliveryNoteID+','+currentDelivery.Responsible+','+currentDelivery.CustomerID):''}</Text> */}
          <View style={{padding:15, alignItems:'center', justifyContent:'center'}}>
              <View style={{padding:10, backgroundColor:'white'}}>
                <View style={{width:250, height:250, overflow:'hidden'}}>
                  <QRCode
                      //value={this.state.content}
                      value={(!!currentDelivery)?(currentDelivery.DeliveryNoteID+','+currentDelivery.Responsible+','+currentDelivery.CustomerID):'0,0,0'}
                      size={250}
                      stle={{overflow:'hidden'}}
                      bgColor='black'
                      fgColor='white'/>
                </View>
              </View>
          </View>
  
          <View style={{padding:15, flex:1}}>
              <TouchableOpacity style={{backgroundColor:commonColors.theme, borderRadius:10, alignItems:'center', justifyContent:'center', height:80, elevation:4, flexDirection:'row'}} onPress={()=>this.print()} >
                  <View style={[styles.icon, {backgroundColor:commonColors.blue}]}>
                      <MaterialCommunityIcons name="printer" size={24} color={"white"} />
                  </View>
                  <Text style={{fontSize:18, fontWeight:'bold', color:'white', marginLeft:10}}>{Cache.getLang('label.print_delivery_report')}</Text>
              </TouchableOpacity>
              <View style={{flexDirection:'row', flex:1, marginTop:15}}>
                  <TouchableOpacity onPress={()=>this.returning()} style={{backgroundColor:commonColors.theme, borderRadius:10, alignItems:'center', justifyContent:'center', flex:1, elevation:4}}>
                      <View style={[styles.icon, {backgroundColor:commonColors.green}]}>
                          <MaterialCommunityIcons name="truck" size={24} color={"white"} style={{transform:[{rotateY:'180deg'}]}} />
                      </View>
                      <Text style={{fontSize:18, fontWeight:'bold', color:'white', textAlign:'center', marginTop:8}}>{Cache.getLang('label.return_back')}</Text>
                  </TouchableOpacity>
                  <View style={{width:15}}/>
                  <TouchableOpacity onPress={()=>{
                        if(!this.state.finishingDelivery){
                            this.next();
                        }}
                        } style={{backgroundColor:commonColors.theme, borderRadius:10, alignItems:'center', justifyContent:'center', flex:1, elevation:4}}>
                      <View style={[styles.icon, {backgroundColor:commonColors.button}]}>
                          <MaterialCommunityIcons name="truck" size={24} color={"white"}/>
                      </View>
                      <Text style={{fontSize:18, fontWeight:'bold', color:'white', textAlign:'center', marginTop:8}}>{Cache.getLang('label.next_delivery')}</Text>
                  </TouchableOpacity>
              </View>
          </View>
        </View>
      );
    }
  }
  
  export default connect(
    state => ({
      currentDelivery: state.common.currentDelivery,
      internetConnection: state.common.internetConnection,
      localSignature: state.common.localSignature
    }),
    dispatch => ({
      actions: bindActionCreators(actions, dispatch)
    })
  )(LastScreen);
  
  const styles = StyleSheet.create({
    container: {
      flex: 1,
      paddingTop:24,
      backgroundColor: 'rgb(49,49,49)'
      //backgroundColor:'red'
    },
    icon: {
      width: 40,
      height: 40,
      borderRadius: 20,
      alignItems: "center",
      justifyContent: "center"
    },
  });