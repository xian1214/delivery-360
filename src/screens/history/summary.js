import React, { Component } from "react";
import { StyleSheet, View, TouchableOpacity, Text, ScrollView, Image } from "react-native";

import {bindActionCreators} from 'redux'
import {connect} from 'react-redux'
import * as actions from '../../redux/actions'

import NavTitleBar from '../../components/navTitle'
import * as commonColors from "../../styles/colors";
import * as commonStyles from "../../styles/styles";
import { screenWidth, screenHeight } from "../../styles/styles";
import Ionicons from "@expo/vector-icons/Ionicons";
import { Actions } from "react-native-router-flux";

import SummaryTab from './summaryTab'
import DeliveryItems from './itemsTab'
import DeliveryReport from './reportTab'
import SignTab from './signTab'

let cars=[require('../../../public/images/car_green.png'), require('../../../public/images/car_grey.png'), require('../../../public/images/car_blue.png'), require('../../../public/images/car_blue.png')]
class Summary extends Component {
  constructor(props) {
    super(props);
    this.state = {
      tabIndex:0,
    };
  }

  renderHeader(){
    return(
      <View style={{flexDirection:'row'}}>
        <TouchableOpacity onPress={()=>this.setState({tabIndex:0})} activeOpacity={0.8} style={{height:40, alignItems:'center', justifyContent:'center', backgroundColor:this.state.tabIndex==0?'white':'#eee', flex:1, borderColor:'#ccc', borderWidth:0.5}}>
          <Text style={{fontSize:13, color:this.state.tabIndex==0?'black':'grey'}}>Items</Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={()=>this.setState({tabIndex:1})} activeOpacity={0.8} style={{height:40, alignItems:'center', justifyContent:'center', backgroundColor:this.state.tabIndex==1?'white':'#eee', flex:1, borderColor:'#ccc', borderWidth:0.5}}>
          <Text style={{fontSize:13, color:this.state.tabIndex==1?'black':'grey'}}>Report</Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={()=>this.setState({tabIndex:2})} activeOpacity={0.8} style={{height:40, alignItems:'center', justifyContent:'center', backgroundColor:this.state.tabIndex==2?'white':'#eee', flex:1, borderColor:'#ccc', borderWidth:0.5}}>
          <Text style={{fontSize:13, color:this.state.tabIndex==2?'black':'grey'}}>Summary</Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={()=>this.setState({tabIndex:3})} activeOpacity={0.8} style={{height:40, alignItems:'center', justifyContent:'center', backgroundColor:this.state.tabIndex==3?'white':'#eee', flex:1, borderColor:'#ccc', borderWidth:0.5}}>
          <Text style={{fontSize:13, color:this.state.tabIndex==3?'black':'grey'}}>Sign</Text>
        </TouchableOpacity>
      </View>
    )
  }

  render() {
    return (
      <View style={styles.container}>
        <NavTitleBar
          buttons={commonStyles.NavBackButton}
          onBack={()=>Actions.pop()}
          title={"Summary"}
        />
        {this.renderHeader()}
        {this.state.tabIndex==0?<DeliveryItems/>:this.state.tabIndex==1?<DeliveryReport/>:this.state.tabIndex==2?<SummaryTab/>:<SignTab/>}
      </View>
    );
  }
}

export default connect(
  state => ({}),
  dispatch => ({
    actions: bindActionCreators(actions, dispatch)
  })
)(Summary);

const styles = StyleSheet.create({
  container: {
    flex: 1
  }
});
