import React from 'react';
import { Text, View, TouchableOpacity } from 'react-native';
import { Actions } from 'react-native-router-flux'
import { Camera, Permissions } from 'expo';
import {Ionicons} from '@expo/vector-icons'

export default class CameraView extends React.Component {
  state = {
    hasCameraPermission: null,
    type: Camera.Constants.Type.back,
    disableBack: false,
  };

  async componentWillMount() {
    
    const { status } = await Permissions.askAsync(Permissions.CAMERA);
    this.setState({ hasCameraPermission: status === 'granted' });
  }
  
  async takePicture(){
    // this.setState({disableBack:true})
    let photo = await this.camera.takePictureAsync();
    if ( this.props.callback ) {
      this.props.callback(photo.uri)
    }
    Actions.pop()
  }

  render() {
    // //console.log('----*************-----')
    const { hasCameraPermission } = this.state;
    if (hasCameraPermission === null) {
      return <View />;
    } else if (hasCameraPermission === false) {
      return <Text>No access to camera</Text>;
    } else {
      return (
        <View style={{ flex: 1, backgroundColor:'black' }}>
          <View style={{height:70, paddingTop:20, backgroundColor:'black', paddingHorizontal:15, justifyContent:'center'}}>
            <TouchableOpacity disabled={this.state.disableBack} onPress={()=>{
              this.setState({disableBack:true})
              Actions.pop()
            }}>
              <Ionicons name="ios-arrow-back" color={'white'} size={30}/>
            </TouchableOpacity>
          </View>
          {/* <Camera 
            ref={ref => { this.camera = ref; }} 
            style={{ width:200, height:200 }} 
            type={this.state.type}
          /> */}
          <View style={{height:100, width:'100%', paddingTop:30, justifyContent:'center', alignItems:'center'}}>
              <TouchableOpacity disabled={this.state.disableBack} onPress={()=>this.takePicture()}>
                <Ionicons name="ios-camera" size={50} color={'white'}/>
              </TouchableOpacity>
              <TouchableOpacity style={{position:'absolute', right:15, bottom:20}}
               onPress={() => {
                  this.setState({
                    type: this.state.type === Camera.Constants.Type.back
                      ? Camera.Constants.Type.front
                      : Camera.Constants.Type.back,
                  });
                }}>
                <Ionicons name="md-sync" color={'white'} size={30}/>
              </TouchableOpacity>
          </View>
        </View>
      );
    }
  }
}