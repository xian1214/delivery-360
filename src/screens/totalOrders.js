"use strict";

import React, { PureComponent } from "react";

import {
  StyleSheet,
  View,
  Platform,
  Modal,
  TouchableOpacity,
  Text,
  StatusBar,
  Image,
  ScrollView
} from "react-native";


import * as commonColors from "../styles/colors";
import * as commonStyles from "../styles/styles";
import { screenWidth, screenHeight } from "../styles/styles";

import * as actionTypes from "../store/common/actionTypes";

import { bindActionCreators } from "redux";
import { connect } from "react-redux";
// import * as actions from "../redux/actions";
import * as actions from '../store/common/actions'
import * as session from '../store/session/actions'

import api from "../service/api";
import Cache from "../utils/cache";
import { Ionicons, MaterialCommunityIcons } from "@expo/vector-icons";
import { Actions } from "react-native-router-flux";
import HeaderBar from '../components/header'
import UtilService from '../utils/utils'

const Item=({data})=>(
  <View style={{marginTop:10, elevation:4, borderRadius:8, borderWidth:3, borderColor:commonColors.green, overflow:'hidden'}}>
    <View style={{paddingHorizontal:20, height:30, alignItems:'center', backgroundColor:'white', flexDirection:'row'}}>
    <Text style={{color:'#333', fontSize:13, fontWeight:'bold', flex:1}}>#{data.DeliveryNoteNumber}</Text>
    <Text style={{color:'#333', fontSize:11}}>{UtilService.getDateTime(data.CompleteTime)}</Text>
    </View>
    <View style={{flexDirection:'row', backgroundColor:'rgb(228,228,233)', alignItems:'center', paddingHorizontal:20, height:30}}>
      <Text style={{color:'grey', fontSize:13, flex:1}}>Item</Text>
      <Text style={{color:'grey', fontSize:13, width:120}}>Deliveried</Text>
    </View>
    {data.DeliveryNoteItems.map((item, index)=>(
      <View key={index} style={{flexDirection:'row', alignItems:'center', paddingHorizontal:20, backgroundColor:(index%2==0)?'white':'rgb(251,251,251)', height:30}}>
        <Text style={{color:'#333', fontSize:14, flex:1, fontWeight:'bold'}}>{item.Item.Name}</Text>
        <Text style={{color:'#333', fontSize:14, width:120}}>{item.PlannedQuantity}</Text>
      </View>
    ))}
  </View>
)

class TotalOrders extends PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      selectedOption:{name:'This Month', index:2},
      showFiltering: false,
      options:[
        {name:'Today', index:0},
        {name:'This Week', index:1},
        {name:'This Month', index:2},
        {name:'This Year', index:3},
        {name:'Date Range', index:4},
      ],
      deliveries:[
        {items:[
          {name:'Cement', count:100, unit:'kg'},
          {name:'Steel', count:100, unit:'kg'},
          {name:'Sand', count:10000, unit:'kg'},
        ], DONumber:'Rj23523'},
        {items:[
          {name:'Cement', count:100, unit:'kg'},
          {name:'Steel', count:100, unit:'kg'},
          {name:'Sand', count:10000, unit:'kg'},
        ], DONumber:'KT12523'},
        {items:[
          {name:'Cement', count:100, unit:'kg'},
          {name:'Steel', count:100, unit:'kg'},
        ], DONumber:'SS64523'}
      ],
    };
  }

  render() {
    return (
      <View style={styles.container}>
        <HeaderBar title={'Delivery Summary'}/>
        <View style={{height:50, alignItems:'center', backgroundColor:'white', flexDirection:'row', elevation:5, paddingHorizontal:20}}>
          <Text style={{fontSize:13, color:'#aaa', flex:1}}>Total Delivery Orders</Text>
          <Text style={{fontSize:15, color:'#333', fontWeight:'bold'}}>{this.props.data.length}</Text>
        </View>
        <ScrollView style={{padding:10}}>
            {this.props.data.map((item, index)=>(
              <Item data={item} key={index}/>
            ))}
            <View style={{height:100}}/>
        </ScrollView>
      </View>
    );
  }
}

export default connect(
  state => ({
    completedDeliveries: state.common.completedDeliveries
  }),
  dispatch => ({
    actions: bindActionCreators({...actions,...session}, dispatch)
  })
)(TotalOrders);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop:24, 
    backgroundColor: 'rgb(250,250,250)'
  },

});
