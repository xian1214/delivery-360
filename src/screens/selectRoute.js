"use strict";

import React, { PureComponent } from "react";

import {
  StyleSheet,
  View,
  Platform,
  Modal,
  TouchableOpacity,
  Text,
  StatusBar,
  Image,
  Linking,
  Dimensions,
  Alert
} from "react-native";


import * as commonColors from "../styles/colors";
import * as commonStyles from "../styles/styles";
import { screenWidth, screenHeight } from "../styles/styles";

import * as actionTypes from "../store/common/actionTypes";

import { bindActionCreators } from "redux";
import { connect } from "react-redux";
// import * as actions from "../redux/actions";
import * as actions from '../store/common/actions'
import * as session from '../store/session/actions'

import api from "../service/api";
import Cache from "../utils/cache";
import { Ionicons, MaterialCommunityIcons } from "@expo/vector-icons";
import { Actions } from "react-native-router-flux";
import { SlideButton, SlideDirection } from '../components/sliderButton';

import HeaderBar from '../components/header'
import {MapView} from 'expo'
//const GOOGLE_MAPS_APIKEY = "AIzaSyB1ZUeJmQkzp66r38xEsqY67G0fikYJnC4";
import googleService from '../service/googleService'
import moment from 'moment'
const { width, height } = Dimensions.get("window");
const ASPECT_RATIO = width / height;
const LATITUDE = 37.771707;
const LONGITUDE = -122.4053769;
const LATITUDE_DELTA = 0.0922;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;

function getHourMinutes(duration, date){
    let d = new Date(date)
    let dd = new Date(d.getTime()+duration)
    
    return moment(dd).format('hh:mm A')
}

const color=[commonColors.theme, commonColors.blue, commonColors.green]

class SelectRoute extends PureComponent {
  constructor(props) {
    super(props);
    if ( this.props.routes && this.props.routes.length>0){
        let length = this.props.routes[0].path.length;
        this.state = {
            start_address: props.routes.length>0?props.routes[0].start_address:'---',
            end_address: props.routes.length>0?props.routes[0].end_address:'---',
            start: this.props.routes[0].path[0],
            target: this.props.routes[0].path[length-1]
        }
    }
    
  }

  componentDidMount(){
    if ( !this.props.routes || this.props.routes.length==0){
        Alert.alert('There is no path!')
        Actions.pop()
        return;
    }
      let totalPoints = []
      this.props.routes.map((item)=>{
        totalPoints = totalPoints.concat(item.path)
      })
      let myInterval = setInterval(()=>{
            if ( this.mapView ){
                this.mapView.fitToCoordinates(totalPoints, {
                    edgePadding: {
                    right: width / 20,
                    bottom: height / 20,
                    left: width / 20,
                    top: height / 20
                    }
                });    
                clearInterval(myInterval)
            }
        
      }, 100)
  }

  chooseRoute(index){
      if ( this.props.update ){
          this.props.update(index)
      }
      Actions.pop()
  }

  render() {
    if (!this.props.routes||this.props.routes.length==0) return null;
    return (
      <View style={styles.container}>
        <HeaderBar title={'Choose Route'}/>
        <View style={{width:'100%', backgroundColor:commonColors.theme, elevation:5}}>
            <View style={{flexDirection:'row', alignItems:'center', padding:15}}>
                <View style={{flex:1, overflow:'hidden', backgroundColor:commonColors.theme}}>
                    <View style={{flexDirection:'row', alignItems:'center'}}>
                        <Text style={{fontSize:14, color:'white', width:50}}>From</Text>
                        <Text style={{fontSize:14, color:'white', overflow:'hidden', flex:1}}>{this.state.start_address}</Text>
                    </View>
                    <View style={{flexDirection:'row', alignItems:'center', marginTop:8}}>
                        <Text style={{fontSize:14, color:'white', width:50}}>To</Text>
                        <Text style={{fontSize:14, color:'white', overflow:'hidden', flex:1}}>{this.state.end_address}</Text>
                    </View>
                </View>
            </View>
        </View>
        
        {this.props.routes.map((item, index)=>(
            <TouchableOpacity key={index} onPress={()=>this.chooseRoute(index)} style={{width:'100%', backgroundColor:'white', flexDirection:'row', alignItems:'center', padding:15, marginTop:10, borderLeftColor:color[index], borderLeftWidth:5}}>
                <View style={{flex:1}}>
                    {this.props.data.Project&&<Text style={{fontWeight:'bold', color:'grey', fontSize:15}}>{this.props.data.Project.Name}</Text>}
                    <Text style={{color:'grey', fontSize:13}}>{(item.distance/1000).toFixed(1)} km, {(item.duration/3600).toFixed(0)}hr {((item.duration%3600)/60).toFixed(0)}min</Text>
                </View>
                <View style={{alignItems:'center'}}>
                    <Text style={{color:'grey', fontSize:12}}>ETA</Text>
                    <Text style={{fontWeight:'bold', color:'#333', fontSize:18}}>{getHourMinutes(item.duration*1000, new Date())}</Text>
                </View>
            </TouchableOpacity>
        ))}

        <MapView
          ref={c => this.mapView = c}
          initialRegion={{
            ...this.state.start,
            latitudeDelta: this.state.latitudeDelta,
            longitudeDelta: this.state.longitudeDelta,
          }}
          style={{ flex: 1 }}
        >
            <MapView.Marker
              coordinate={this.state.start}
            >
              <View style={{width:20, height:20, borderRadius:10, backgroundColor:'rgb(66,192,147)', alignItems:'center', justifyContent:'center'}}>
                <Text style={{fontSize:12, color:'white', fontWeight:'bold'}}>1</Text>
              </View>
            </MapView.Marker>
            <MapView.Marker
              coordinate={this.state.target}
            >
              <View>
                <Ionicons name="ios-home" size={30} color={commonColors.theme}/>
              </View>
            </MapView.Marker>
            {this.props.routes.map((item, index)=>{
                return(
                    <MapView.Polyline
                        key={index}
                        coordinates={item.path}
                        strokeColor={color[index]}
                        fillColor={color[index]}
                        strokeWidth={5}
                    />
                )
            })}
        </MapView>
      </View>
    );
  }
}

export default connect(
  state => ({
    status: state.common.status,
    location:state.common.location, 
    heading:state.common.heading
  }),
  dispatch => ({
    actions: bindActionCreators({...actions,...session}, dispatch)
  })
)(SelectRoute);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop:24,
    backgroundColor: 'rgb(249,249,249)'
  },

});
