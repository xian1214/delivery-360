"use strict";

import React, { PureComponent } from "react";
import {
  StyleSheet,
  View,
  ImageBackground,
  TouchableOpacity,
  Image,
  Text,
  TextInput,
  ScrollView,
} from "react-native";

import { bindActionCreators } from "redux";
import { connect } from 'react-redux'
import * as commonColors from "../styles/colors";
import * as commonStyles from "../styles/styles";
import { Constants, Location, Permissions,ImagePicker,ImageManipulator} from "expo";
import { Ionicons, MaterialCommunityIcons } from "@expo/vector-icons";
import { Actions } from "react-native-router-flux";
import HeaderBar from '../components/header'
import api from "../service/api";
import * as CONST from '../components/constant' 
import Cache from '../utils/cache'
import * as actions from '../store/chat/actions'
import * as actionsForCommon from '../store/common/actions'

class GroupCheckIn extends PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      avatarInstance:{},
      image:null,
      currentTeamMembers:[],
      disabled:false,
      message:null,
      remarks:''
    };
  }
  componentWillMount() {
    
    
  }
  componentWillReceiveProps(nextProps){
  
  }

  
  componentDidMount(){
    if(!!!this.props.dn){
      return
    }
    api.getTeamMembers(this.props.dn.DeliveryNoteID,(error,res)=>{
        if(error){
          return
        }

        this.setState({currentTeamMembers:res.Results})
    })
    

  }
  done(){
      // var mode = CONST.TEAM_ACTION_CHECKIN
      // if(this.props.mode.Name=='checkin'){
      //   mode = CONST.TEAM_ACTION_CHECKIN
      // }
      // if(this.props.mode=='checkout'){
      //   mode = CONST.TEAM_ACTION_CHECKOUT
      // }
      // if(this.props.mode=='jobreport'){
      //   mode = CONST.TEAM_ACTION_JOBREPORT
      // }
      // if(this.props.mode=='pickup'){
      //   mode = CONST.TEAM_ACTION_PICKUP
      // }
      // if(this.props.mode=='arrive'){
      //   mode = CONST.TEAM_ACTION_ARRIVE
      // }
      // if(this.props.mode=='lunch'){
      //   mode = CONST.TEAM_ACTION_LUNCH
      // }
    

      var act = {
        DeliveryNoteID:this.props.dn.DeliveryNoteID,
        ActivityID:this.props.mode.ActivityID,
        Remarks:this.state.remarks,
        PosterID:Cache.currentUser.user.UserID
      }
      var chattingRoom = Cache.clientID + '-' + this.props.dn.DeliveryNoteNumber
      api.createTeamAction(act,(error,res)=>{
        if(!!error){
            return
        }
        this.props.actions.sendMessage(chattingRoom,this.props.dn.DeliveryNoteID,this.state.message, null, null,res.TeamActID,(error,res)=>{
          this.setState({isRefreshing:false})
          if(error){
              Alert.alert('Send message failed!')
          }
        })
        this.props.updateActionList()
        Actions.pop()
      })
  }
  getTitle(){
    var res = 'Group Check In'
    if(this.props.mode=='checkin'){
        res = 'Group Check In'
    }
    if(this.props.mode=='checkout'){
        res = 'Group Check Out'
    }
    if(this.props.mode=='jobreport'){
      res = 'Job Report'
    }
    if(this.props.mode=='pickup'){
      res = 'Pick Up'
    }
    if(this.props.mode=='arrive'){
      res = 'Arrive'
    }
    if(this.props.mode=='lunch'){
      res = 'Lunch'
    }
    return res
  }
  async takePicture(){
    console.log('take picture error 000')
    let {customerImage}=this.state
    console.log('take picture error 111')
    let res = await Permissions.askAsync(Permissions.CAMERA)
    console.log('take picture error 112')
    if ( res.status ==='granted'){
      console.log('take picture error 113')
        let {status} = await Permissions.askAsync(Permissions.CAMERA_ROLL)
        if ( status === 'granted' ){
          console.log('take picture error 114')
            let image = await ImagePicker.launchCameraAsync({
                quality:0.6
            })
            console.log('take picture error 115')   
            if ( !image.cancelled ){
                this.setState({isWaiting:true})
                console.log('take picture error 116')
                const manipResult = await ImageManipulator.manipulateAsync(
                    image.uri,
                    [{resize:{width:768}}],
                    { format: 'jpeg', compress:0.6 }
                );
                api.uploadImage(manipResult.uri, (err, res)=>{
                  console.log('err,res on up',err,res)
                  if ( err == null ){
                    this.customerLogoURL = res.filePath
                    
                    this.state.customerImage = {uri:config.SERVICE_FILE_URL+res.filePath}    
                    console.log('image.....',this.state.customerImage)
                    this.setState({customerImage:{...this.state.customerImage}})               
                 }     
                    this.setState({isWaiting:false})   
                })

                
            }
            
        }
    }
}
  render() {
    let {checkedInTime,checkedOutTime,avatarInstance,image} = this.state
    let {currentDelivery, internetConnection} = this.props
    return (
      <View style={styles.container}>
         <HeaderBar title={this.getTitle()} back={()=>Actions.pop()}
          rightIcon={"md-checkmark"}
          rightCallback={() => this.done()}
        />
        <ScrollView source={commonStyles.background} style={{flex:1,resizeMode:'cover'}}>
            <View style={{padding:10,marginBottom:20}}>
              <Text style={{color:'white'}}>Team Members</Text>
            </View>
            {
                this.state.currentTeamMembers.map((item,index)=>{
                    return(
                      <View style={{flexDirection:'row',alignItems:'center',marginBottom:10 ,justifyContent:"center"}} key={index+'+teammember'}>
                        <View style={{width:90,paddingHorizontal:20}}>
                            <Image source={commonStyles.noUser}  style={{width:50, height:50, borderRadius:25}}/>
                            <View style={{width:20,height:20,left:60, bottom:0,borderRadius:10, backgroundColor:'#5d92f4',position:'absolute',alignItems:'center',justifyContent:'center'}}>
                              <Ionicons name="md-checkmark" size={18} color={'white'}/>
                            </View>
                        </View>
                        <Text style={{color:"white",flex:1}}>{item.User.Username}</Text>
                      </View>
                    )  
                })
            }
            <View style={{height:15}}></View>
            <View style={{paddingLeft:20,marginBottom:5}}>
              <Text style={{color:'white'}}>Photos (Optional)</Text>
              <View style={{height:10}}></View>
              {this.state.customerImage==null&&<TouchableOpacity onPress={()=>this.takePicture()} style={{backgroundColor:'#2f3444', borderColor:'rgb(0,185,237)', borderWidth:2, alignItems:'center', justifyContent:'center', width:90, height:90}}>
                  <Ionicons name={'ios-camera'} color={'white'} size={30}/>
              </TouchableOpacity>}
              {this.state.customerImage!=null&&<TouchableOpacity onPress={()=>this.takePicture()} style={{backgroundColor:'#2f3444',width:90, height:90}}>
                  <Image source={this.state.customerImage} style={{flex:1, resizeMode:'cover'}}/>
              </TouchableOpacity>}
            </View>
            <View style={{padding:20,marginBottom:5,marginTop:10,borderRadius:7,flex:1}}>
              <Text style={{color:'white'}}>Remarks</Text>
              <View style={{flex:1,backgroundColor:'#2f3444',borderRadius:7,marginTop:10,padding:15}}>
                  <TextInput
                    ref="name"
                    autoCapitalize="none"
                    autoCorrect={false}
                    placeholder=""
                    multiline 
                    placeholderTextColor={commonColors.placeholderText}
                    textAlign="left"
                    style={[
                      styles.input
                    ]}
                    underlineColorAndroid="transparent"
                    value={this.state.remarks}
                    onChangeText={text =>
                      this.setState({ remarks: text.replace(/\t/g, "") })
                    }
                  />
              </View>
            </View>
        </ScrollView>
      </View>
    );
  }
}


export default connect(
  state => ({
    messages: state.chat.messages,
    internetConnection: state.common.internetConnection,
    currentDelivery: state.common.currentDelivery
  }),
  dispatch => ({
    actions: bindActionCreators({...actions,...actionsForCommon}, dispatch)
  })
)(GroupCheckIn);



const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: commonColors.theme,
    paddingTop: Constants.statusBarHeight,
  },
  headerContainer: {
    padding: 20,
    width: "100%",
    backgroundColor: commonColors.theme
  },
  headerText: {
    fontSize: 20,
    color: "white",
  },
  avatarContainer: {
    alignItems: "center",
    justifyContent: "center",
    padding: 40
  },
  avatar: {
    width: 60,
    height: 60,
    borderRadius: 30,
  },
  username: {
    fontSize: 18,
    fontWeight: "bold",
    color: "white",
    marginLeft:15,
    flex:1,
  },
  cardContainer: {
    backgroundColor: "white",
    flexDirection: "row",
    paddingHorizontal: 15,
    paddingVertical: 40,
    elevation: 4,
    alignItems: "center",
    borderRadius:4,
    flex:1,
    shadowColor: 'black',
    shadowOpacity: 0.4,
    shadowOffset: {width:3, height:3},
    shadowRadius: 4,
  },
  icon: {
    width: 40,
    height: 40,
    borderRadius: 20,
    alignItems: "center",
    justifyContent: "center"
  },
  modalContainer: {
    flex: 1,
    backgroundColor: "rgba(0, 0, 0,0.5)",
    alignItems: "center",
    justifyContent: "center"
  },
  modal: {
    width: 260,
    height: 160,
    borderRadius: 5,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "white",
    elevation:3,
  },
  indicatorContainer: {
    flex: 1,
    backgroundColor: "rgba(0, 0, 0,0.5)",
    alignItems: "center",
    justifyContent: "center"
  },
  indicator: {
    width: 80,
    height: 80,
    borderRadius: 5,
    shadowColor: "black",
    alignItems: "center",
    justifyContent: "center",
    shadowOffset: { width: 2, height: 2 },
    shadowOpacity: 0.4,
    shadowRadius: 3,
    backgroundColor: "white"
  },
  upcomingContainer:{
    width:'100%',
    backgroundColor:commonColors.theme,
    elevation:5,
    borderRadius:4,
    flexDirection:'row',
    alignItems: 'center',
    paddingHorizontal:20,
    paddingVertical:40,
    shadowColor: 'black',
    shadowOpacity: 0.4,
    shadowOffset: {width:3, height:3},
    shadowRadius: 4,
  },
  input: {
    fontSize:16,
    flex:1,
    height:150,
    color: 'white',
    alignSelf: "stretch",
    borderRadius: 3,
    marginBottom: 3,
    textAlignVertical:'top',
    paddingHorizontal: 10,
    backgroundColor: 'transparent',
    width:'100%'
  },
  buttonLogin: {
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: 'rgb(14, 115, 240)',
    borderRadius: 25,
    width:160,
    margin:10,
    // borderWidth: 4,
    // borderColor: commonColors.theme,
    // borderStyle: 'solid',
    marginHorizontal: 20,
    height: 50
  }
  
});
