"use strict";


import React, { PureComponent } from "react";

import {
  StyleSheet,
  View,
  Platform,
  Modal,
  TouchableOpacity,
  Text,
  StatusBar,
  Image,
  ScrollView,
  RefreshControl,
  Alert,
} from "react-native";

import moment from 'moment'
import * as commonColors from "../styles/colors";
import * as commonStyles from "../styles/styles";
import { screenWidth, screenHeight } from "../styles/styles";

import * as actionTypes from "../store/common/actionTypes";

import { bindActionCreators } from "redux";
import { connect } from "react-redux";
// import * as actions from "../redux/actions";
import * as actions from '../store/common/actions'
import * as session from '../store/session/actions'


import api from "../service/api";
import Cache from "../utils/cache";
import { Ionicons, MaterialCommunityIcons } from "@expo/vector-icons";
import { Actions } from "react-native-router-flux";
import HeaderBar from "../components/header";
import UtilService from '../utils/utils'
import InformationBar from '../components/informationBar'
import { gestureHandlerRootHOC } from "react-native-gesture-handler";

function isSameDate(A, B) {
  if (
    A.getDate() == B.getDate() &&
    A.getMonth() == B.getMonth() &&
    A.getFullYear() == B.getFullYear()
  )
    return true;
  return false;
}
var gThis = null
const Item = ({ icon, name, content, isGrey }) => (
  <View style={{ flexDirection: "row", alignItems: "center", marginTop: 5 }}>
    <Ionicons name={icon} size={16} color={"grey"} style={{ width: 20 }} />
    <Text style={{ fontSize: 13, color: "grey", marginLeft: 8, width: 50 }}>
      {name}
    </Text>
    <Text
      style={{
        fontSize: 13,
        color: isGrey ? "grey" : "#333",
        fontWeight: "bold",
        overflow:'hidden',
        flex:1,
      }}
    >
      {content}
    </Text>
  </View>
);

const Card = ({ card, isPending, onPress, onShowItems, isOpenList, type }) => (
  <TouchableOpacity
    onPress={() => onPress()}
    disabled = {gThis.state.disabled}
    style={[
      styles.cardContainer,
      { borderColor: isPending ? commonColors.yellow : commonColors.blue }
    ]}
  >
    {type>0&&<View style={{position:'absolute', top:0, left:0, backgroundColor:isPending ? commonColors.yellow : commonColors.blue, width:'100%', height:40, justifyContent:'center', paddingHorizontal:15}}>
      <Text style={{color:'white'}}>{type==1?'Arrival Confirming...':'Pending Receipt Signoff'}</Text>
    </View>}
    <View style={{padding:15, marginTop:type>0?40:0}}>
      {card.Project&&<Text style={{ fontWeight: "bold", color: "#333" }}>
        {card.Project.Name}
      </Text>}
      <Text style={{ color: "#777", marginTop: 5 }}># {card.DeliveryNoteNumber}</Text>
      <Item icon={"md-calendar"} name={"Date"} content={UtilService.getDateTime(card.ETD)} /> 
      <Item
        icon={"md-radio-button-off"}
        name={"From"}
        content={card.Warehouse.Address}
        isGrey
      />
      <Item icon={"md-pin"} name={"To"} content={card.DestinationAddress} isGrey />
      {card.route&&<Text style={{ marginTop: 10, color: "grey" }}>
        Approx. {card.dist}km, ETA {card.ETA}
      </Text>}
    </View>
    <View style={{width:'100%', height:1, backgroundColor:'#ccc'}}/>
    <TouchableOpacity onPress={()=>onShowItems()} style={{flexDirection:'row', alignItems:'center', padding:8}}>
      <Text style={{fontWeight:'bold', color:'grey', fontSize:14, flex:1}}>
        Delivery Items
      </Text>
      <View style={{padding:0}} >
        <Ionicons name={card.showList?'ios-arrow-up':'ios-arrow-down'} size={30} color={commonColors.theme}/>
      </View>
    </TouchableOpacity>
    {isOpenList&&<TouchableOpacity onPress={()=>onShowItems()} style={{paddingBottom:8}}>
        <View style={{flexDirection:'row', backgroundColor:commonColors.cardColor3, alignItems:'center', height:30, width:'100%', marginTop:8, paddingHorizontal:20}}>
            <Text style={{color:'grey', flex:1}}>Item</Text>
            <Text style={{color:'grey', width:80}}>Quantity</Text>
        </View>
        {card.DeliveryNoteItems.map((item, index)=>{
            return(
                <View key={index} style={{flexDirection:'row', alignItems:'center', height:30, width:'100%', marginTop:8, paddingHorizontal:20}}>
                    <Text style={{color:'grey', flex:1}}>{item.Item.Name}</Text>
                    <Text style={{color:'grey', width:80}}>{item.PlannedQuantity} {item.Item.Uom?item.Item.Uom.Name:''}</Text>
                </View>
            )
        })}
    </TouchableOpacity>}
  </TouchableOpacity>
);

const CardList = ({ cards, onPress, onShowItems, isRefreshing, onRefresh, selectedIndex, currentID, status }) => (
  <View style={{ flex: 1 }}>
    <ScrollView 
      refreshControl={
          <RefreshControl
              refreshing={isRefreshing}
              onRefresh={() => onRefresh()}
              tintColor={'grey'}
          />
      }
      style={{ flex: 18 }}>
      <View style={{ height: 20 }} />
      {cards.map((card, index) => {
        let isShowDate = true;
        if (index > 0) {
          let A = new Date(card.ETD),
            B = new Date(cards[index - 1].ETD);
          if (isSameDate(A, B)) isShowDate = false;
        }
        return (
          <View
            key={index}
            style={{ paddingHorizontal: 20, paddingVertical: 5 }}
          >
            {isShowDate && <Text style={styles.date}>{UtilService.getDateTime(card.ETD)}</Text>}
            <Card
              card={card}
              isPending={isSameDate(new Date(card.ETD), new Date())}
              onPress={() => onPress(card)}
              onShowItems={()=>onShowItems(index)}
              isOpenList={selectedIndex==index}
              type={currentID!=card.DeliveryNoteID?0:(status?1:2)}
            />
          </View>
        );  
      })}
      <View style={{ height: 20 }} />
      {
        (gThis.props.upcomingDeliveries.length>0 && gThis.state.showReadmore)&&
        <TouchableOpacity style={{margin:30,paddingHorizontal:70,height:40,backgroundColor:'#eee',borderRadius:10,justifyContent:'center',alignItems:'center'}} onPress={()=>gThis.refreshMore()}>
          <Text style={{color:'black',fontSize:12}}>Read more</Text>
        </TouchableOpacity>
      }
      
    </ScrollView>
  </View>
);
class Upcoming extends PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      isRefreshing: false,
      selected: -1,
      isReady: false,
      showReadmore:false,
      disabled:false
    };
    this.readCnt = 10
    gThis = this
    this.isConverting = false
  }
 
  componentDidMount(){
    console.log('cache.current',Cache.currentUser.user.UserID)
    this.props.actions.getUpcomingAllDeliveries(this.readCnt)
    setTimeout(()=>{
      
      this.setState({isReady:true})
    },100)
  }

  componentWillReceiveProps(nextProps){
    if ( nextProps.actionType == actionTypes.GET_UPCOMING_DELIVERIES_REFRESH ){
      // //console.log(nextProps.status)
      if (nextProps.status==actionTypes.LOADING){
        this.setState({isRefreshing:true})
      }
      if (nextProps.status==actionTypes.FAILED){
        this.setState({isRefreshing:false})
        Alert.alert('Failed to fetch deliveries')
      }
      if (nextProps.status==actionTypes.SUCCESS){
        this.setState({isRefreshing:false})
        if ( this.props.currentDelivery ){
          setTimeout(()=>this.props.actions.updateCurrentDelivery(),20)
        }

      }
    }
  }

  onRefresh(){
   
      this.readCnt = 10
      this.props.actions.getUpcomingAllDeliveries(this.readCnt)
      //this.getUpcomingAllDeliveries(this.readCnt)
   
  }
  refreshMore(){
    if ( this.props.status != actionTypes.LOADING){
      this.readCnt+=10
      this.getUpcomingAllDeliveries(this.readCnt);
      //this.props.actions.getUpcomingAllDeliveries(this.readCnt)
    }
  }
  getUpcomingAllDeliveries(readCnt){
    api.getDeliveries(
      Cache.currentUser.user.UserID, 
      false,
      1,
      readCnt,
      null,//moment().format('YYYY-MM-DD') + ' 00:00:00',
      null,
      (err, res)=>{
          //console.log('res.Results on upcoming.....',res.Results[0])
          this.setState({upcomingDeliveries:res.Results})
          if(readCnt<res.TotalNumberOfRecords){
            this.setState({showReadmore:false})
          }
          else
          {
            this.setState({showReadmore:true})
          }
          //console.log('res on getUpcomingAllDeliveries search...!!!',res)
      }
    )
  }
  render() {
    // //console.log(this.state.isRefreshing)
    let {currentDelivery, upcomingDeliveries, internetConnection} = this.props
    
    return (
      <View style={styles.container}>
        <InformationBar iConnection={internetConnection} currentDelivery={currentDelivery}/>
        <HeaderBar title={"Upcoming Deliveries"} />
        <View style={{width:'100%',paddingTop:10,paddingHorizontal:20}}>
          <Text style={{color:'white',fontSize:12,fontWeight:'bold'}}>{'Instruction'}</Text>
          <Text style={{color:'white',fontSize:12}}>{'1. Choose Delivery Order to start'}</Text>
          <Text style={{color:'white',fontSize:12}}>{'2. Sign off for delivery report'}</Text>
          <Text style={{color:'white',fontSize:12}}>{'3. Return'}</Text>
        </View>
        {this.state.isReady&&<CardList
          cards={upcomingDeliveries}
          onPress={item => {    
            ////console.log('on action:::::',Cache.originETA)
            this.setState({disabled:true})
            setTimeout(()=>{this.setState({disabled:false})},3000)
            if(this.isConverting==true){return;}
            //this.isConverting = true
            if ( currentDelivery && currentDelivery.DeliveryNoteID == item.DeliveryNoteID ){
              if ( currentDelivery.OTWStatus ){
                Actions.Navigation()
              }else{
                Actions.DeliveryReport({ data: item, editable:true })
              }
              //this.isConverting = false
              return;
            }
            Actions.SetRoute({data:item})
          }}
          selectedIndex={this.state.selected}
          isRefreshing={this.state.isRefreshing}
          onRefresh={()=>this.onRefresh()}
          onShowItems={index=>{
            if ( index == this.state.selected ) {
              this.setState({selected:-1})
            }else{
              this.setState({selected:index})
            }
          }}
          currentID={currentDelivery?currentDelivery.DeliveryNoteID:-1}
          status={currentDelivery?currentDelivery.OTWStatus:false}
        />}
      </View>
    );
  }
}

export default connect(
  state => ({
    actionType: state.common.type,
    status: state.common.status,
    upcomingDeliveries: state.common.upcomingDeliveries,
    currentDelivery:state.common.currentDelivery,
    internetConnection: state.common.internetConnection
  }),
  dispatch => ({
    actions: bindActionCreators({...actions,...session}, dispatch)
  })
)(Upcoming);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop:24, 
    backgroundColor: "rgb(43,43,43)"
  },
  cardContainer: {
    elevation: 3,
    borderRadius: 8,
    backgroundColor: "white",
    paddingBottom:0, 
    borderWidth: 3,
    borderColor: commonColors.blue,
    
  },
  date: {
    fontSize: 13,
    color: "white",
    marginVertical: 5,
    fontWeight: "bold"
  }
});
