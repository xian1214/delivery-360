import React, { Component } from "react";
import { StyleSheet,View, Text, TouchableOpacity } from "react-native";

import {bindActionCreators} from 'redux'
import {connect} from 'react-redux'
import * as actions from '../../redux/actions'

import NavTitleBar from '../../components/navTitle'
import * as commonColors from "../../styles/colors";
import * as commonStyles from "../../styles/styles";
import { screenWidth, screenHeight } from "../../styles/styles";
import Ionicons from "@expo/vector-icons/Ionicons";

const Content=({icon, title, content, onPress})=>(
  <TouchableOpacity onPress={()=>onPress()} activeOpacity={0.8} style={{flexDirection:'row', alignItems:'center', height:50, backgroundColor:'white', borderTopColor:'#ccc', borderTopWidth:1, paddingHorizontal:15}}>
    <Ionicons name={icon} size={16} color={'grey'} style={{width:20}}/>
    <Text numberOfLines={1} style={{fontSize:14, color:'#333', fontWeight:'bold', width:100}}>{title} </Text>
    <Text numberOfLines={1} style={{fontSize:14, color:'#333'}}>{content}</Text>
  </TouchableOpacity>
)

class Me extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name:'John Lee',
      phone:'+86 123 12312 123',
      mobile:'+86 123 12312 123',
    };
  }

  render() {
    return (
      <View style={styles.container}>
        <NavTitleBar
          buttons={commonStyles.NavNoneButton}
          onBack={this.goBack}
          title={"About Me"}
        />
        <View style={{}}>
          <Content icon={'md-person'} title={'Customer'} content={this.state.name} onPress={()=>{}}/>
          <Content icon={'md-call'} title={'Phone'} content={this.state.phone} onPress={()=>{}}/>
          <Content icon={'md-phone-portrait'} title={'Mobile'} content={this.state.mobile} onPress={()=>{}}/>
          <Content icon={'md-log-out'} title={'Log Out'} content={''} onPress={()=>{Actions.Login()}}/>
        </View>
      </View>
    );
  }
}

export default connect(
  state => ({}),
  dispatch => ({
    actions: bindActionCreators(actions, dispatch)
  })
)(Me);

const styles = StyleSheet.create({
  container: {
    flex: 1
  }
});
