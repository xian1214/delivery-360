"use strict";

import React, { PureComponent } from "react";

import {
  StyleSheet,
  View,
  Platform,
  Modal,
  TouchableOpacity,
  Text,
  StatusBar,
  Image,
  ScrollView,
  ActivityIndicator
} from "react-native";


import * as commonColors from "../styles/colors";
import * as commonStyles from "../styles/styles";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import * as actions from '../store/common/actions'
import * as session from '../store/session/actions'
import api from "../service/api";
import Cache from "../utils/cache";
import { Ionicons, MaterialCommunityIcons } from "@expo/vector-icons";
import { Actions } from "react-native-router-flux";
import HeaderBar from "../components/header";
import UtilService from '../utils/utils'
import InformationBar from '../components/informationBar'
import * as config from '../config'
//import LaunchNavigator from 'react-native-launch-navigator'

function isSameDate(A, B) {
  if (
    A.getDate() == B.getDate() &&
    A.getMonth() == B.getMonth() &&
    A.getFullYear() == B.getFullYear()
  )
    return true;
  return false;
}

class NavService extends PureComponent {
  constructor(props) {
    super(props);
    this.state={
      isRefreshing: false,
      showModal: false,
      username: Cache.currentUser.user.FirstName+' '+Cache.currentUser.user.LastName,
      avatar: {uri:config.SERVICE_FILE_URL+Cache.currentUser.user.Avarta},
      avatarUrl:Cache.currentUser.user.Avatar,
      avatarInstance: {uri:config.SERVICE_FILE_URL+Cache.currentUser.user.Avarta},
      navApps:[],
      kkk:[1,2,3,],
      isWaiting:true,
      
    }
    this.navigator = null
  }

  componentDidMount(){
    //console.log('current user on profile ----> >>>',Cache.currentUser)
    this.getAvailableApps()
    this.setState({isWaiting:true})
  }
  
  onCancel(){
    this.setState({showModal: false})
  }

  onGo(){
    this.setState({showModal: false})
    api.logout()
    Actions.SignIn()
  }
  componentWillReceiveProps(nextProps){
    
  }
  

  renderIndicator() {
    return (
      <Modal
        visible={this.state.isWaiting}
        transparent={true}
        onRequestClose={() => {}}
      >
        <View style={styles.indicatorContainer}>
          <View style={styles.indicator}>
            <ActivityIndicator size="large" />
          </View>
        </View>
      </Modal>
    );
  }

  render() {
    let {avatarUrl,avatarInstance} = this.state
    return (
      <View style={styles.container}>
        <HeaderBar title={"Navigation Service"} 
        />
           {this.renderIndicator()}
        <View style={{width:'100%',height:30}}></View>
        <ScrollView>
        {
            !!this.state.navApps && this.state.navApps.map((app,index)=>{
              if(app.status=="available"){
                return(
                  <TouchableOpacity style={{backgroundColor:'white'}} onPress={() => {}}>
                    <View style={{width:'100%',height:70,justifyContent:'center',alignItems:'flex-start'}}>
                        <Text style={styles.listTextStyle}>
                          {app.displayName}
                        </Text>
                    </View>    
                    <View style={{height:1,width:'100%',backgroundColor:'rgb(30,30,30)'}}></View>
                  </TouchableOpacity>
                )
              }              
            })
        }
        </ScrollView>
      </View>
    );
  }
}

export default connect(
  state => ({
    actionType: state.common.type,
    status: state.common.status,
    currentDelivery: state.common.currentDelivery,
    completedDeliveries:state.common.completedDeliveries,
    internetConnection: state.common.internetConnection,
    location:state.common.location
  }),
  dispatch => ({
    actions: bindActionCreators({...actions,...session}, dispatch)
  })
)(NavService);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop:24, 
    backgroundColor: "rgb(48,48,48)"
  },
  cardContainer: {
    elevation: 3,
    borderRadius: 8,
    backgroundColor: "white",
    // padding: 10,
    borderWidth: 3,
    borderColor: commonColors.blue
  },
  date: {
    fontSize: 13,
    color: "white",
    marginVertical: 5,
    fontWeight: "bold"
  },
  listTextStyle:{
    color:'#777',
    fontSize:20, 
    fontWeight:'bold', 
    marginLeft:20
  },
  username: {
    fontSize: 18,
    fontWeight: "bold",
    color: "white",
    marginLeft:15,
    flex:1,
  },
  modalContainer: {
    flex: 1,
    backgroundColor: "rgba(0, 0, 0,0.5)",
    alignItems: "center",
    justifyContent: "center"
  },
  modal: {
    width: 260,
    height: 160,
    borderRadius: 5,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "white",
    elevation:3,
  },
  indicatorContainer: {
    flex: 1,
    backgroundColor: "rgba(0, 0, 0,0.5)",
    alignItems: "center",
    justifyContent: "center"
  },
  indicator: {
    width: 80,
    height: 80,
    borderRadius: 5,
    shadowColor: "black",
    alignItems: "center",
    justifyContent: "center",
    shadowOffset: { width: 2, height: 2 },
    shadowOpacity: 0.4,
    shadowRadius: 3,
    backgroundColor: "white"
  },
});
