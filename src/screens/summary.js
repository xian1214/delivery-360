"use strict";

import React, { PureComponent } from "react";

import {
  StyleSheet,
  View,
  Platform,
  Modal,
  TouchableOpacity,
  Text,
  StatusBar,
  TextInput,
  Image
} from "react-native";


import * as commonColors from "../styles/colors";
import * as commonStyles from "../styles/styles";
import { screenWidth, screenHeight } from "../styles/styles";

import * as actionTypes from "../store/common/actionTypes";

import { bindActionCreators } from "redux";
import { connect } from "react-redux";
// import * as actions from "../redux/actions";
import * as actions from '../store/common/actions'
import * as session from '../store/session/actions'

import api from "../service/api";
import Cache from "../utils/cache";
import { Ionicons, MaterialCommunityIcons } from "@expo/vector-icons";
import { Actions } from "react-native-router-flux";
import HeaderBar from '../components/header'
import moment from "moment";

const Item=({data})=>(
  <View style={{marginTop:5, elevation:4}}>
    <View style={{flexDirection:'row', backgroundColor:'rgb(228,228,233)', alignItems:'center', paddingHorizontal:20, height:30}}>
      <Text style={{color:'grey', fontSize:13, flex:1}}>Item</Text>
      <Text style={{color:'grey', fontSize:13, width:120, textAlign:'right'}}>Delivered</Text>
    </View>
    {data.map((item, index)=>(
      <View key={index} style={{flexDirection:'row', alignItems:'center', paddingHorizontal:20, backgroundColor:(index%2==0)?'white':'rgb(251,251,251)', height:30}}>
        <Text style={{color:'#333', fontSize:14, flex:1}}>{item.Item.Name}</Text>
        <Text style={{color:'#333', fontSize:14, width:120, textAlign:'right'}}>{item.DeliveryCount}<Text style={{paddingLeft:10,marginLeft:5}}>{' '+item.Item.Uom.Name}</Text></Text>
      </View>
    ))}
  </View>
)

class Summary extends PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      selectedOption:{name:'Today', index:0},
      showFiltering: false,
      options:[
        {name:'Today', index:0},
        {name:'This Week', index:1},
        {name:'This Month', index:2},
        {name:'This Year', index:3},
        {name:'Date Range', index:4},
      ],
      deliveries:[],
      total:[],
      isSetDating: false
    };
  }

  fetchData(FromDate, ToDate){
    
    this.setState({isWaiting: true})
    api.getSummary({
      FromDate,
      ToDate,
      UserID:Cache.currentUser.user.UserID
    }, (err, res)=>{
      this.setState({isWaiting: false})
      //console.log('summary getting on didmount', res, FromDate,ToDate)
      if ( err == null ){
        //console.log('arrived deliveries on summary', res)
        this.setState({totalDeliveryCount:res.CompletedDnCnt})
        this.setState({total:res.DeliveredItems})
      }
      
    })
    //this.setState({isWaiting: true})
    // api.getDeliveries(Cache.currentUser.user.UserID, true, 1, 1, 
    //   FromDate, 
    //   ToDate, (err, res)=>{
    //     if ( err == null ){
    //       //console.log('arrived res ',)
    //       this.setState({totalDeliveryCount:res.TotalNumberOfRecords})

    //     }
    //     this.setState({isWaiting: false})
    //   })
  }
  getMonday( date ) {
    var day = date.getDay() || 7;  
    if( day !== 1 ) 
        date.setHours(-24 * (day - 1)); 
    return date;
}
  onPressItem(item){
    if ( item.index==4){
      this.setState({showFiltering:false, isSetDating: true})
    }else{
      this.setState({selectedOption:item, showFiltering:false})
      //console.log('arrived item on onPressItem',item)
      var startDate = null
      var endDate = null
      if(item.name=='Today'){
          startDate = new Date()
          endDate = new Date()
      }
      if(item.name=='This Week'){
        startDate = this.getMonday(new Date())
        endDate = new Date()
      }
      if(item.name=='This Month'){
        var date = new Date();
        startDate = new Date(date.getFullYear(), date.getMonth(), 1);
        endDate = new Date()
      }
      if(item.name=='Date Range'){
        startDate = new Date()
        endDate = new Date()
      }
      this.startDate = startDate
      this.endDate = endDate
      var customerID = 0
      var status = ''
      var isSignOff = false
      this.setState({isWaiting:true})
      var query = ''

      //console.log('filtering is started.......')
      // api.getDeliveriesWithFilter(query,customerID,status,isSignOff,startDate,endDate,(error,res)=>{
      //   if(error){
      //     return
      //   }
      //   //console.log('res for upcoming>>>>>>>>>>',res)
      //   this.setState({upcoming:res.Results})
      //   this.setState({isWaiting:false})
      // })
      this.fetchData(
          moment(startDate).format("YYYY-MM-DD" + ' 00:00:00'),
          moment(endDate).format("YYYY-MM-DD" + ' 23:59:59')
      )
    }
  }
  // onPressItem(item){
  //   //console.log('item is being....refreshed on summary',item);
  //   if ( item.index==4){
  //     this.setState({showFiltering:false, isSetDating: true})

  //   }else{
  //     this.setState({selectedOption:item, showFiltering:false})
  //     let diffs=[1, 7, 30, 365]
  //     this.fetchData(
  //       moment().subtract(diffs[item.index], 'd').format('YYYY-MM-DD hh:mm:ss A'),
  //       moment().format('YYYY-MM-DD hh:mm:ss A')
  //     )
  //   }
  // }

  componentDidMount(){
    if ( !this.props.internetConnection ){
      alert('you can not see summary in offline mode!')
      Actions.pop()
      return;
    }
    this.onPressItem(this.state.selectedOption)
    
  }

  renderFiltering(){
    return(
      <TouchableOpacity activeOpacity={1} onPress={()=>this.setState({showFiltering:false})} style={{elevation:5, flex:1, backgroundColor:'rgb(131, 131, 137)'}}>
        <View style={{width:'100%', backgroundColor:'white', padding:15}}>
          {this.state.options.map((item, index)=>{
            let isSelected=(item.index==this.state.selectedOption.index)
            let color=isSelected?'grey':'#aaa'
            let weight=isSelected?'bold':'normal'
            return(
              <TouchableOpacity onPress={()=>this.onPressItem(item)} key={index} style={{flexDirection:'row', alignItems:'center', height:40}}>
                <Text style={{flex:1, fontSize:15, color:color, fontWeight:weight}}>{item.name}</Text>
                {isSelected&&
                  <Ionicons name="md-checkmark" color={commonColors.theme} size={24}/>}
              </TouchableOpacity>
            )
          })}
        </View>
      </TouchableOpacity>
    )
  }

  renderContent(){
    return(
      <View>
        <TouchableOpacity onPress={()=>Actions.CompletedSummary({startDate:this.startDate,endDate:this.endDate})} 
          style={{flexDirection:'row', alignItems:'center', backgroundColor:'white', elevation:4,paddingHorizontal:15, height:50}}>
          <Text style={{fontSize:16, color:'grey', fontWeight:'bold', flex:1}}>Total Delivery Orders</Text>
          <Text style={{fontSize:18, color:'#333', fontWeight:'bold'}}>{this.state.totalDeliveryCount}</Text>
          <View style={{marginLeft:10}}>
            <Ionicons name='ios-arrow-forward' size={24} color={commonColors.theme}/>
          </View>
        </TouchableOpacity>

          <Item data={this.state.total}/>
      </View>
    )
  }

  onGo(){
    this.setState({isSetDating: false, selectedOption:this.state.options[4]})
    this.fetchData(
      moment(this.state.from).format('YYYY-MM-DD 00:00:00'),
      moment(this.state.to).format('YYYY-MM-DD 23:59:59')
    )
  }
  onCancel(){
    this.setState({isSetDating: false})
  }

  renderModal() {
    return (
      <Modal
        visible={this.state.isSetDating}
        transparent={true}
        onRequestClose={() => {}}
      >
        <View style={styles.modalContainer}>
          <View style={styles.modal}>
            <View style={{flex:1, padding:5, width:'100%'}}>
            <Text style={{fontSize:12, color:'grey',margin:10, textAlign:'center'}}>Show between</Text>
            <TextInput
                ref="from"
                autoCapitalize="none"
                autoCorrect={false}
                placeholder="YYYY-MM-DD"
                placeholderTextColor={commonColors.placeholderText}
                textAlign="left"
                style={styles.input}
                underlineColorAndroid="transparent"
                returnKeyType={"go"}
                keyboardType='numbers-and-punctuation'
                value={this.state.from}
                onChangeText={text =>
                  this.setState({ from: text.replace(/\t/g, "") })
                }
                onSubmitEditing={() => this.refs.to.focus()}
              />
              <Text style={{fontSize:12, color:'grey',margin:10, textAlign:'center'}}>and</Text>
              <TextInput
                ref="to"
                autoCapitalize="none"
                autoCorrect={false}
                placeholder="YYYY-MM-DD"
                placeholderTextColor={commonColors.placeholderText}
                textAlign="left"
                style={styles.input}
                underlineColorAndroid="transparent"
                returnKeyType={"go"}
                keyboardType='numbers-and-punctuation'
                value={this.state.to}
                onChangeText={text =>
                  this.setState({ to: text.replace(/\t/g, "") })
                }
                onSubmitEditing={() => this.onGo()}
              />
            </View>
            <View style={{height:36, flexDirection:'row', width:'100%', borderBottomRightRadius:5, borderBottomLeftRadius:5, overflow:'hidden', borderTopWidth:1, borderTopColor:'#ccc'}}>
              <TouchableOpacity onPress={()=>this.onCancel()} style={{flex:1, alignItems:'center', justifyContent:'center', backgroundColor:'rgb(249,249,249)'}}>
                <Text style={{color:commonColors.theme, fontSize:13}}>Cancel</Text>
              </TouchableOpacity>
              <TouchableOpacity onPress={()=>this.onGo()} style={{flex:1, alignItems:'center', justifyContent:'center', backgroundColor:'rgb(240,250,253)'}}>
                <Text style={{color:commonColors.theme, fontSize:13, fontWeight:'bold'}}>Go</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </Modal>
    );
  }
  
  render() {
    return (
      <View style={styles.container}>
        <HeaderBar title={'Delivery Summary'}/>
        <View style={{height:50, alignItems:'center', backgroundColor:'white', flexDirection:'row', elevation:5, paddingHorizontal:20}}>
          <Text style={{color:'#aaa', fontSize:13}}>Range:</Text>
          <TouchableOpacity onPress={()=>this.setState({showFiltering:true})} style={{flexDirection:'row', alignItems:'center'}}>
            <Text style={{color:'#333', fontWeight:'bold', fontSize:14, marginHorizontal:10}}>{this.state.selectedOption.name}</Text>
            <Ionicons name={'md-arrow-dropdown'} color={commonColors.theme} size={14}/>
          </TouchableOpacity>
        </View>
        {this.state.showFiltering&&this.renderFiltering()}
        {!this.state.showFiltering&&this.renderContent()}
        {this.renderModal()}
      </View>
    );
  }
}

export default connect(
  state => ({
    internetConnection: state.common.internetConnection,
    completedDeliveries: state.common.completedDeliveries
  }),
  dispatch => ({
    actions: bindActionCreators({...actions,...session}, dispatch)
  })
)(Summary);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop:24, 
    backgroundColor: 'rgb(250,250,250)'
  },
  modalContainer: {
    flex: 1,
    backgroundColor: "rgba(0, 0, 0,0.5)",
    alignItems: "center",
    justifyContent: "center"
  },
  modal: {
    width: 200,
    height: 200,
    borderRadius: 5,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "white",
    elevation:3,
  },
  input: {
    fontSize: 12,
    color: commonColors.title,
    height: 36,
    alignSelf: "stretch",
    borderRadius: 3,
    marginBottom: 3,
    paddingHorizontal: 10,
    backgroundColor: commonColors.inputColor,
    width:'100%'
  },
});
