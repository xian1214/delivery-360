"use strict";

import React, { PureComponent } from "react";

import {
  StyleSheet,
  View,
  Platform,
  Modal,
  TouchableOpacity,
  Text,
  StatusBar,
  Image,
  TextInput,
  ScrollView,
  Alert,
} from "react-native";


import * as commonColors from "../styles/colors";
import * as commonStyles from "../styles/styles";
import { screenWidth, screenHeight } from "../styles/styles";

import * as actionTypes from "../store/common/actionTypes";

import { bindActionCreators } from "redux";
import { connect } from "react-redux";
// import * as actions from "../redux/actions";
import * as actions from '../store/common/actions'
import * as session from '../store/session/actions'

import api from "../service/api";
import Cache from "../utils/cache";
import * as config from '../config'
import { Ionicons, MaterialCommunityIcons } from "@expo/vector-icons";
import { Actions } from "react-native-router-flux";
import HeaderBar from '../components/header'
import async from 'async'
import {ImagePicker, Permissions, ImageManipulator} from 'expo'
import {WaitingModal} from '../components/waiting'
import UtilService from "../utils/utils";

class CheckList extends PureComponent {
  constructor(props) {
    super(props);

    this.state = {
        items:[],
        quantity:''+this.props.item.DeliveredQuantity,
        isWaiting: false,
        ReturnedQuantity:this.props.item.ReturnedQuantity
    };
    this.convert = []
    this.checked = false
  }

  componentDidMount(){
    let {CheckResultIDs, Item, CheckResults} = this.props.item
    ////console.log('arrived noteitem ------- on checkList.js',this.props.item)
    //console.log('currentDelivery on checklist---->>>>>',this.props.currentDelivery)
    
    
    let items = []
    CheckResults.map((result)=>{
        if(result==null || result.CheckListItem==null || this.props.item==null){
          return
        }
        items.push({
          Name:result.CheckListItem.Name, 
          CheckListItemID:result.CheckListItemID, 
          CheckResultID:result.CheckResultID, 
          Value:result.Value,
          DeliveryNoteItemID: this.props.item.DeliveryNoteItemID,
          ImageArray: result.Images==''||result.Images==null?[]:result.Images.split(',')
        })
    })
    this.setState({items})
    items.map((item, index)=>{
      item.ImageArray.map(async(image)=>{
        if ( image.length < 4 ){
          this.convert[image] = await UtilService.getLocalStringData(image)
          image = Number(image)
        }
      })
    })
    // if ( CheckResultIDs == null ){
    //   if ( Item.CheckItemIDs == null ) return;
    //   let list = Item.CheckItemIDs.split(',')
    //   list.splice(-1, 1)
    //   async.mapSeries(list, (id, cb)=>{
    //     api.getCheckList(id, cb)
    //   }, (err, results)=>{
    //     if ( err == null ){
    //       let items = []
    //       results.map((item)=>item&&items.push({
    //         Name:item.Name, 
    //         CheckListItemID:item.CheckListItemID,
    //         DeliveryNoteItemID: this.props.item.DeliveryNoteItemID
    //       }))
    //       this.setState({items})
    //     }
    //   })  
    // }else{
    //   let items = []
    //   CheckResults.map((result)=>{
    //       items.push({
    //         Name:result.CheckListItem.Name, 
    //         CheckListItemID:result.CheckListItemID, 
    //         CheckResultID:result.CheckResultID, 
    //         Value:result.Value,
    //         DeliveryNoteItemID: this.props.item.DeliveryNoteItemID,
    //         ImageArray: result.Images==''||result.Images==null?[]:result.Images.split(',')
    //       })
    //   })
    //   this.setState({items})
    // }
  }


  async takePicture(index){
    let res = await Permissions.askAsync(Permissions.CAMERA)
    if ( res.status ==='granted'){
        let {status} = await Permissions.askAsync(Permissions.CAMERA_ROLL)
        if ( status === 'granted' ){
            let image = await ImagePicker.launchCameraAsync({
                quality:0.6
            })
            if ( !image.cancelled ){
              const manipResult = await ImageManipulator.manipulateAsync(
                image.uri,
                [{resize:{width:768}}],
                { format: 'jpeg', compress:0.6 }
              );
                this.setState({isWaiting:true})
                api.uploadImage(manipResult.uri, (err, res)=>{
                  if ( err == null ){
                    if ( this.state.items[index].ImageArray == undefined ){
                      this.state.items[index].ImageArray = []
                    }
                    if ( Cache.hasInternetConnection ){
                      this.state.items[index].ImageArray.push(config.SERVICE_FILE_URL+res.filePath)
                    }else{
                      this.state.items[index].ImageArray.push(res)
                      this.convert[res] = manipResult.uri
                    }
                    
                    // this.setState({items:[...this.state.items]})  
                  }     
                  this.setState({isWaiting:false})  
                })
            }
            
        }
    }
    
  }

  done(){
    this.props.item.DeliveredQuantity = this.state.quantity
      async.mapSeries(this.state.items, (item, cb)=>{
        item.Images = item.ImageArray?item.ImageArray.join(','):null
        api.updateCheckResult(item, cb)  
      }, (err, results)=>{
        //console.log(err, results)
        if ( err == null ){
          if ( Cache.hasInternetConnection ){
            this.props.actions.updateCurrentDelivery()
          }else{
            for ( let i = 0; i < this.state.items.length; i ++ ){
              this.props.item.CheckResults[i].Value = this.state.items[i].Value
              this.props.item.CheckResults[i].Images = this.state.items[i].Images
            }
          }
          this.checked = true
          Alert.alert('Checklist submitted!')
        }else{
          Alert.alert('Failed to create checklist')
        }
      })
    // }else{
      // async.mapSeries(this.state.items, (item, cb)=>{
      //   item.Images = item.ImageArray?item.ImageArray.join(','):''
      //   api.createCheckResult(item, cb)  
      // }, (err, results)=>{
      //   //console.log(err, results)
      //   if ( err == null ){
      //     if ( this.props.update ) this.props.update(results, this.state.items)
      //     let resultIds = '';
      //     results.map((item)=>resultIds+= item.CheckResultID+',')
      //     this.props.currentDelivery.DeliveryNoteItems[this.props.index].CheckResultIDs = resultIds
      //     this.setState({isWaiting:true})
        
      //     api.setCheckResult(this.props.currentDelivery.DeliveryNoteID, this.props.currentDelivery, (err, res)=>{
      //       if ( err == null ){
      //         this.setState({items:results})
      //         Alert.alert('created checklist successfully!')
      //       }else{
      //         // //console.log(err)
      //         Alert.alert('Failed to save checklist!')
      //       }
      //       this.setState({isWaiting:false})
      //     })
      //   }else{
      //     Alert.alert('Failed to create checklist')
      //   }
      // })
    //}

    // Actions.pop()
  }

  showPicture(uri){
    Actions.ShowImage({uri})
  }

  remove(index, idx){
    this.state.items[index].ImageArray.splice(idx, 1)
    this.setState({items:[...this.state.items]})
  }
  backWarn(){
    if(!this.checked){
      Alert.alert(
        'Warning',
        'Do you need to exit without submition?',
        [
          {text: 'Ask me later', onPress: () =>{}},
          {text: 'Cancel', onPress: () => {}},
          {text: 'OK', onPress: () =>{
            console.log('on CHECK_LIST_BACK',(!!this.props.currentDelivery)?'PASS':'NOT PASS')
            
            if ( this.props.update ) this.props.update()
            setTimeout(()=>{Actions.pop()},500)
            
          }},
        ],
        { cancelable: false }
      )
    }
    else{
      if ( this.props.update ) this.props.update()
      setTimeout(()=>{Actions.pop()},500)
    }
    
  }
  render() {
    let {Item, PlannedQuantity} = this.props.item
    let {ReturnedQuantity} = this.state
    return (
      <View style={styles.container}>
        {this.props.currentDelivery&&<View style={{width:'100%', paddingVertical:5, backgroundColor:commonColors.yellow}}>
          <Text style={{color:'white', fontWeight:'bold', fontSize:12, marginLeft:15}}>In Progress...</Text>
        </View>}
        <HeaderBar title={'Checklist'} 
          back={()=>{
            this.backWarn()
            // //console.log('on CHECK_LIST_BACK',(!!this.props.currentDelivery)?'PASS':'NOT PASS')
            
            // if ( this.props.update ) this.props.update()
            // setTimeout(()=>{Actions.pop()},500)
            
          }}
          rightElement={(
            <TouchableOpacity onPress={()=>this.done()}>
              <Ionicons name="md-checkmark" size={24} color={'white'}/>
            </TouchableOpacity>
          )}
        />
        <ScrollView style={{padding:15}}>
            <View  style={{display:'flex',flexDirection:'row',marginBottom:15}}>
              <Text style={{flex:1,fontSize:24, color:'grey', fontWeight:'bold'}}>{Item.Name}</Text>
              <TouchableOpacity style={{flex:1}} 
                  onPress={
                    ()=>{Actions.ItemReturn({item:this.props.item,update:(returnQty)=>{this.setState({ReturnedQuantity:returnQty})}})}
                    }>
                <View style={{flex:1,fontSize:24,fontWeight:'bold',borderRadius:18,justifyContent:'center',alignItems:'center',borderColor:'blue',borderWidth:2}}>
                    <Text style={{color:'blue'}}>Item Return</Text>
                </View>
              </TouchableOpacity>
            </View>
            <View style={{flexDirection:'row', alignItems:'center'}}>
                <Text style={{fontSize:13, color:'grey'}}>Planned Quantity</Text>
                <Text style={{fontSize:15, color:'#333', marginLeft:10}}>{PlannedQuantity} {Item.Uom?Item.Uom.Name:''}</Text>
                <View style={{flex:1}}/>
            </View>
            <View style={{flexDirection:'row', alignItems:'center'}}>
                <Text style={{fontSize:13, color:'grey'}}>Returned Quantity</Text>
                <Text style={{fontSize:15, color:'#333', marginLeft:10}}>{ReturnedQuantity} {Item.Uom?Item.Uom.Name:''}</Text>
                <View style={{flex:1}}/>
            </View>
            {this.state.items.map((item, index)=>{
                return (
                    <View key={index} style={{paddingVertical:15, borderBottomColor:'#ccc', borderBottomWidth:1}}>
                        <Text style={{fontSize:13, color:'grey', marginBottom:8}}>{item.Name}</Text>
                        <TextInput
                            ref="remarks"
                            autoCapitalize="none"
                            autoCorrect={false}
                            placeholder="Remarks"
                            placeholderTextColor={commonColors.placeholderText}
                            textAlign="left"
                            style={styles.input}
                            underlineColorAndroid="transparent"
                            returnKeyType={"next"}
                            value={item.Value}
                            onChangeText={text =>{
                                item.Value = text.replace(/\t/g, "")
                                this.setState({ items: [...this.state.items]  })
                            }}
                        />
                        {/* {(!item.ImageArray||item.ImageArray.length==0)&&<TouchableOpacity onPress={()=>this.takePicture(index)} style={{borderColor:commonColors.theme, borderWidth:2, alignItems:'center', justifyContent:'center', width:90, height:90, marginTop:5}}>
                            <Ionicons name={'ios-camera'} color={commonColors.theme} size={30}/>
                        </TouchableOpacity>}
                        {item.ImageArray&&item.ImageArray.length>0&&<TouchableOpacity onPress={()=>this.takePicture(index)} style={{width:90, height:90, marginTop:5}}>
                            <Image source={{uri:item.ImageArray[0]}} style={{flex:1, resizeMode:'cover'}}/>
                        </TouchableOpacity>} */}
                        <ScrollView horizontal={true}>
                            <View style={{flexDirection:'row'}}>
                              {item.ImageArray&&item.ImageArray.map((img, idx)=>{
                                let uri = img
                                if ( typeof img === 'number'){
                                  uri = this.convert[img]
                                }
                                return (
                                  <View key={idx} >
                                    <TouchableOpacity onPress={()=>this.showPicture(uri)} style={{width:90, height:90, marginTop:5, marginRight:5}}>
                                      <Image source={{uri}} style={{flex:1, resizeMode:'cover', backgroundColor:'grey'}}/>
                                    </TouchableOpacity>
                                    <TouchableOpacity onPress={()=>this.remove(index, idx)} style={{width:90, paddingVertical:5, marginTop:5, backgroundColor:commonColors.theme, borderRadius:15, alignItems:'center'}}>
                                      <Ionicons name="ios-trash" color="white" size={20}/>
                                    </TouchableOpacity>
                                  </View>
                                  )
                              })}
                              <TouchableOpacity onPress={()=>this.takePicture(index)} style={{borderColor:commonColors.theme, borderWidth:2, alignItems:'center', justifyContent:'center', width:90, height:90, marginTop:5, marginRight:5}}>
                                  <Ionicons name={'ios-camera'} color={commonColors.theme} size={30}/>
                              </TouchableOpacity>
                            </View>
                          </ScrollView>
                    </View>
                )
            })}
            <View style={{height:50}}/>
        </ScrollView>
        <WaitingModal isWaiting={this.state.isWaiting}/>
      </View>
    );
  }
}

export default connect(
  state => ({
    currentDelivery:state.common.currentDelivery
  }),
  dispatch => ({
    actions: bindActionCreators({...actions,...session}, dispatch)
  })
)(CheckList);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop:24,
    backgroundColor: 'white'
  },
  input: {
    fontSize: 14,
    color: commonColors.title,
    height: 36,
    borderRadius: 3,
    marginBottom: 3,
    paddingHorizontal: 10,
    backgroundColor: commonColors.inputColor
  },
  modalContainer: {
    flex: 1,
    backgroundColor: "rgba(0, 0, 0,0.5)",
    alignItems: "center",
    justifyContent: "center"
  },
  modal: {
    width:360,
    borderRadius: 5,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "white",
    elevation:3,
  },
});
