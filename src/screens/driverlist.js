"use strict";

import React, { PureComponent } from "react";

import {
  StyleSheet,
  View,
  Platform,
  Modal,
  TouchableOpacity,
  Text,
  StatusBar,
  Image,
  TextInput,
  ScrollView,
  ActivityIndicator
} from "react-native";

import * as commonColors from "../styles/colors";
import * as commonStyles from "../styles/styles";
import { screenWidth, screenHeight } from "../styles/styles";

import * as actionTypes from "../store/common/actionTypes";

import { bindActionCreators } from "redux";
import { connect } from "react-redux";
// import * as actions from "../redux/actions";
import * as actions from '../store/common/actions'
import * as session from '../store/session/actions'

import api from "../service/api";
import Cache from "../utils/cache";
import { Ionicons, MaterialCommunityIcons, MaterialIcons } from "@expo/vector-icons";
import { Actions } from "react-native-router-flux";
import HeaderBar from '../components/header'
import async from 'async'
import UtilService from '../utils/utils'
import * as config from '../config'

class DriverList extends PureComponent {
  constructor(props) {
    super(props);

    this.state = {
        drivers:[],
        isLoading:false
    };
  }

  componentDidMount(){
    this.setState({isLoading:true})
      api.getUsers(1, 50, (err,res)=>{
          // //console.log(err, res)
          this.setState({isLoading:false})
          if ( err == null ){
            this.setState({drivers:res.Results})
          }
      })
  }

  selectDriver(driver){
      this.props.selectDriver(driver)
      Actions.pop()
  }

  render() {
    if ( this.props.currentDelivery == null  ) return null;
    return (
      <View style={styles.container}>
        <HeaderBar title={this.props.currentDelivery.DeliveryNoteNumber} />
        <ScrollView>
            {!this.state.isLoading&&this.state.drivers.map((item, index)=>{
                return (
                    <TouchableOpacity key={index} onPress={()=>this.selectDriver(item)} style={{flexDirection:'row', alignItems:'center'}}>
                        {item.Avatar!=null&&<Image source={{uri:config.SERVICE_FILE_URL+ item.Avatar}} defaultSource={commonStyles.face} style={{width:50, height:50, borderRadius:25, marginHorizontal:20, marginVertical:10}}/>}
                        {item.Avatar==null&&<Image source={commonStyles.face}  style={{width:50, height:50, borderRadius:25, marginHorizontal:20, marginVertical:10}}/>}
                        <View style={{paddingVertical:10, borderBottomWidth:1, borderBottomColor:'#ccc', flex:1, justifyContent:'center'}}>
                            <Text style={{fontWeight:'bold', color:'grey', fontSize:14}}>{item.FirstName+' '+item.LastName}</Text>
                        </View>
                    </TouchableOpacity>
                )
            })}
            {this.state.isLoading&&<ActivityIndicator size="large" style={{flex:1}}/>}
        </ScrollView>
      </View>
    );
  }
}

export default connect(
  state => ({
    currentDelivery:state.common.currentDelivery
  }),
  dispatch => ({
    actions: bindActionCreators({...actions,...session}, dispatch)
  })
)(DriverList);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop:24,
    backgroundColor: 'white'
  },
});
