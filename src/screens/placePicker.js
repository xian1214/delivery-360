import React, { Component } from 'react';

import {
  View,
  StyleSheet,
  TouchableOpacity,
  Text,
  Modal,
  TextInput,
  Linking, 
  Platform,
  Image,
  ScrollView,
  Dimensions,
  Alert,
} from 'react-native';



import { Ionicons, MaterialIcons, MaterialCommunityIcons, Octicons } from "@expo/vector-icons";
import { Actions } from "react-native-router-flux";

import * as actionTypes from "../redux/actionTypes";
import * as commonColors from "../styles/colors";
import * as commonStyles from "../styles/styles";
import { screenWidth, screenHeight } from "../styles/styles";

import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import * as actions from "../redux/actions";

import {MapView} from 'expo'
const { width, height } = Dimensions.get("window");
import HeaderBar from '../components/header'


function getHourMinutes(duration, date){
  let d = new Date(date)
  let dd = new Date(d.getTime()+duration)
  let h = dd.getHours(), m = dd.getMinutes()
  let AP = ' AM'
  if (h >12){
      h = h - 12;
      AP = ' PM'
  }
  
  return h+':'+m+AP
}

class PlacePicker extends Component {

  constructor(props) {
    super(props);

    this.state = {
      prevPos: null,
      curPos: {latitude:this.props.CurrentLat||1.36, longitude:this.props.CurrentLat||103.7},
      //coordinates:{latitude:this.props.CurrentLat||1.36, longitude:this.props.CurrentLat||103.7},
      isPause: false,
      showListModal: false,
      showReasonModal: false,
      modalNumber:0,
      rerouteReason:'',
      reason: '',
      coordinates:[
        { latitude: 37.8025259, longitude: -122.4351431 },
        { latitude: 37.7896386, longitude: -122.421646 },
        { latitude: 37.7665248, longitude: -122.4161628 },
        { latitude: 37.7734153, longitude: -122.4577787 },
        { latitude: 37.7948605, longitude: -122.4596065 },
        { latitude: 37.8025259, longitude: -122.4351431 }
      ],
      distance: 42000,
      duration: 8000,
      showingConfirm: false,
      foundError: false,
      overETA10: false,
      isWaiting: false,
      address:'Liaoning Shenyang in China',
      end_address:'Liaoning Shenyang in China',
      x:null,
      alertList:[]
    };
    this.mapView = null
  }

  componentWillReceiveProps(nextProps){
    
  }

  componentDidMount(){
    
    //console.log('this.props.destination on place picker',this.props.destination)
    if(!!!(this.props.destination)){
      this.setState({targetPos:this.state.curPos})
    }else
    {
      this.setState({targetPos:this.props.destination})
    }
  }

  onBack(){
    Actions.pop()
  }
  done(){
    if(this.props.update) this.props.update(this.state.curPos)
    Actions.pop()
  }
  render() {
    let {distance, duration} = this.state
    let {delivery} = this.state
    //if ( delivery == null ) return null
    return (
      <View style={styles.flex}>
        <HeaderBar 
          title={'Pick Place'}
          rightIcon = {"md-checkmark"}
          rightCallback={()=>this.done()}
        />
        <MapView
          
          ref={(el) => (this.mapView = el)}
          style={styles.flex}
          initialRegion={{
            ...this.state.targetPos,
            latitudeDelta: 0.1,
            longitudeDelta: 0.5,
          }}
        >
          {/* {this.state.coordinates&&<MapView.Polyline
              coordinates={this.state.coordinates}
              strokeColor={commonColors.theme}
              fillColor={commonColors.theme}
              strokeWidth={5}
          />} */}
          {this.state.coordinates&&<MapView.Polygon
              coordinates={this.state.coordinates}
              strokeColor={commonColors.theme}
              fillColor={commonColors.theme}
              strokeWidth={5}
          />}
          <MapView.Marker draggable
            coordinate={this.state.targetPos}
            onDragEnd={(e) => this.setState({ curPos: e.nativeEvent.coordinate })}
          />
        </MapView>
      </View>
    );
  }
}

export default connect(
  state => ({
    alerts:state.common.alerts
  }),
  dispatch => ({
    actions: bindActionCreators(actions, dispatch)
  })
)(PlacePicker);

const styles = StyleSheet.create({
  flex: {
    flex: 1,
    paddingTop: Platform.OS === 'android' ? 24 : 0
  },
  buttonContainerUpDown: {
    ...StyleSheet.absoluteFillObject,
    flexDirection: 'row',
    justifyContent: 'center',
  },
  buttonContainerLeftRight: {
    ...StyleSheet.absoluteFillObject,
    flexDirection: 'column',
    justifyContent: 'center',
  },
  button: {
    backgroundColor: 'rgba(100,100,100,0.2)',
    position: 'absolute',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 20,
    height: 50,
    width: 50,
  },
  modalContainer: {
    flex: 1,
    backgroundColor: "rgba(0, 0, 0,0.5)",
    alignItems: 'center',
    justifyContent:'center'
  },
  content:{
    position:'absolute',
    bottom:0,
    left:0,
    width:'100%',
    backgroundColor:'white',
    borderTopLeftRadius: 5,
    borderTopRightRadius: 5,
    paddingHorizontal: 15,
    paddingVertical: 15,
  },
  input: {
    fontSize: 14,
    color: commonColors.title,
    height: 40,
    alignSelf: "stretch",
    // marginHorizontal: 40,
    // borderColor: '#aaa',
    // borderWidth: 1,
    borderRadius: 3,
    marginBottom: 3,
    paddingHorizontal: 10,
    backgroundColor: commonColors.inputColor
  },
  confirmModal:{
    backgroundColor:'white',
    borderRadius:8,
    elevation:5,
    width:300,
  },
  alertContainer:{
    height:50, 
    width:'100%', 
    borderRadius:25, 
    alignItems:'center', 
    flexDirection:'row',
    shadowColor: 'black',
    shadowOffset: {width:3, height:3},
    shadowOpacity: 0.3,
    shadowRadius: 5,
    paddingHorizontal:20,
    backgroundColor:'white'
  },
  markerInfo:{
    flex:1,
    borderRadius:4, 
    backgroundColor:'white',
    shadowColor: 'black',
    shadowOffset: {width:3, height:3},
    shadowOpacity: 0.3,
    elevation:4,
    borderWidth: 2,
    padding:10,
    borderColor:commonColors.theme,
  },





  panel: {
    flex: 1,
    backgroundColor: 'white',
    position: 'relative'
  },
  panelHeader: {
    height: 40,
    backgroundColor: '#b197fc',
    alignItems: 'center',
    justifyContent: 'center'
  },
});