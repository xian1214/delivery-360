"use strict";

import React, { PureComponent } from "react";

import {
  StyleSheet,
  View,
  Platform,
  Modal,
  TouchableOpacity,
  Text,
  StatusBar,
  Image,
  TextInput,
  ScrollView,
  Alert,
  ActivityIndicator
} from "react-native";


import * as commonColors from "../styles/colors";
import * as commonStyles from "../styles/styles";
import { screenWidth, screenHeight } from "../styles/styles";

import * as actionTypes from "../store/common/actionTypes";

import { bindActionCreators } from "redux";
import { connect } from "react-redux";
// import * as actions from "../redux/actions";
import * as actions from '../store/common/actions'
import * as session from '../store/session/actions'

import api from "../service/api";
import Cache from "../utils/cache";
import { Ionicons, MaterialCommunityIcons, MaterialIcons } from "@expo/vector-icons";
import { Actions } from "react-native-router-flux";
import HeaderBar from '../components/header'
import async from 'async'
import UtilService from '../utils/utils'
import * as CONST from '../components/constant'
import moment from 'moment'

class AlertList extends PureComponent {
  constructor(props) {
    super(props);

    this.state = {
        alertList:[],
        isWaiting:false,
    };
  }

  componentDidMount(){
    if ( !this.props.internetConnection ) {
      Alert.alert('You can not see alert list on offline mode!')
      Actions.pop()
      return;
    }
    //console.log('code list on alertList.......',Cache.codes)
    this.setState({isWaiting:true})
    api.getAlerts(this.props.data.DeliveryNoteID, (err, res)=>{
         //console.log('first alert  on alertlist', err, res.Results[1])
         this.setState({isWaiting:false})
         if ( err == null ){
          let alertList = []
          res.Results.map((item)=>{
            if ( item.ReferenceID == this.props.data.DeliveryNoteID )
              alertList.push(item)
          })
          console.log('second res==',res.Results[1])
          this.setState({alertList})
        }
      })
  }

  renderIndicator() {
    return (
      <Modal
        visible={this.state.isWaiting}
        transparent={true}
        onRequestClose={() => {}}
      >
        <View style={styles.indicatorContainer}>
          <View style={styles.indicator}>
            <ActivityIndicator size="large" />
          </View>
        </View>
      </Modal>
    );
  }
  render() {
    if (  !!!this.props.data ) return null
    return (
      <View style={styles.container}>
        {this.props.currentDelivery!=null&&<View style={{width:'100%', paddingVertical:5, backgroundColor:commonColors.yellow}}>
          <Text style={{color:'white', fontWeight:'bold', fontSize:12, marginLeft:15}}>In Progress...</Text>
        </View>}
        <HeaderBar title={'Alerts'} 
            rightElement={(
                <TouchableOpacity onPress={()=>Actions.Chat({channel:this.props.currentDelivery.DeliveryNoteNumber})}>
                    <Ionicons name="md-chatboxes" size={24} color={'white'}/>
                </TouchableOpacity>
            )}
        />
        <View style={{width:'100%', backgroundColor:'white', padding:15, elevation:5, flexDirection:'row'}}>
            <View style={{flex:1}}>
                <Text style={{fontSize:16, color:'black', fontWeight:'bold'}}>{this.props.data.DeliveryNoteNumber}</Text>
                {this.props.data.Project&&<Text style={{fontSize:13, color:'#333', marginTop:5}}>{this.props.data.Project.Name}</Text>}
            </View>
            <View style={{width:1, height:'100%', backgroundColor:'#ccc', marginHorizontal:15}}/>
            <View style={{alignItems:'center'}}>
                <Text style={{color:'grey', fontSize:12}}>ETA</Text>
                <Text style={{fontWeight:'bold', color:'black', fontSize:18}}>{moment(Cache.originETA).format('hh:mm:ss a')}</Text>
            </View>
        </View>
        <ScrollView style={{padding:0}}>
            {this.state.alertList.map((item, index)=>{
                return(
                <View key={index} style={{flexDirection:'row', alignItems:'center', marginTop:10}}>
                    {item.Message!='Miss Turn'&&
                    item.Message!='Traffic'&&
                    item.Message!='Toilet'&& 
                    item.Message!='Meal'&&<View style={{backgroundColor:(item.Level==CONST.ALERT_LEVEL)?'red':'green', height:40, width:40, borderRadius:20, alignItems:'center', justifyContent:'center', marginLeft:15}}>
                      <MaterialCommunityIcons name="truck" size={20} color={"white"} />
                    </View>}
            {item.Message=='Miss Turn'&&<TouchableOpacity onPress={()=>this.reroute(1)} style={{backgroundColor:commonColors.brown, height:40, width:40, borderRadius:20, alignItems:'center', justifyContent:'center', marginLeft:15}}>
              <MaterialIcons name="warning" color={'white'} size={20}/>
            </TouchableOpacity>}
            {item.Message=='Traffic'&&<TouchableOpacity onPress={()=>this.reroute(2)} style={{backgroundColor:commonColors.pink, height:40, width:40, borderRadius:20, alignItems:'center', justifyContent:'center', marginLeft:15}}>
              <MaterialIcons name="traffic" color={'white'} size={20}/>
            </TouchableOpacity>}
            {item.Message=='Toilet'&&<TouchableOpacity onPress={()=>this.reroute(3)} style={{backgroundColor:commonColors.blue, height:40, width:40, borderRadius:20, alignItems:'center', justifyContent:'center', marginLeft:15}}>
              <MaterialIcons name="wc" color={'white'} size={20}/>
            </TouchableOpacity>}
            {item.Message=='Meal'&&<TouchableOpacity onPress={()=>this.reroute(4)} style={{backgroundColor:commonColors.green, height:40, width:40, borderRadius:20, alignItems:'center', justifyContent:'center', marginLeft:15}}>
              <MaterialIcons name="restaurant" color={'white'} size={20}/>
            </TouchableOpacity>}
                    <View style={{marginLeft:15, justifyContent:'center'}}> 
                    <Text style={{fontSize:16, color:'grey'}}>{UtilService.getAlarmText(item)}</Text>
                    <Text style={{fontSize:13, color:'#333', fontWeight:'bold', marginTop:5}}>{moment(new Date(item.CreatedAt)).format('YYYY-MM-DD hh:mm:ss a')}</Text>
                    </View>
                </View>
                )
            })}
            <View style={{height:50}}/>
        </ScrollView>
        {this.renderIndicator()}
      </View>
    );
  }
}

export default connect(
  state => ({
    //currentDelivery:state.common.currentDelivery,
    internetConnection: state.common.internetConnection
  }),
  dispatch => ({
    actions: bindActionCreators({...actions,...session}, dispatch)
  })
)(AlertList);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop:24,
    backgroundColor: 'white'
  },
  input: {
    fontSize: 14,
    color: commonColors.title,
    height: 36,
    borderRadius: 3,
    marginBottom: 3,
    paddingHorizontal: 10,
    backgroundColor: commonColors.inputColor
  },
  modalContainer: {
    flex: 1,
    backgroundColor: "rgba(0, 0, 0,0.5)",
    alignItems: "center",
    justifyContent: "center"
  },
  indicatorContainer: {
    flex: 1,
    backgroundColor: "rgba(0, 0, 0,0.5)",
    alignItems: "center",
    justifyContent: "center"
  },
  indicator: {
    width: 80,
    height: 80,
    borderRadius: 5,
    shadowColor: "black",
    alignItems: "center",
    justifyContent: "center",
    shadowOffset: { width: 2, height: 2 },
    shadowOpacity: 0.4,
    shadowRadius: 3,
    backgroundColor: "white"
  },
  modal: {
    width:360,
    borderRadius: 5,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "white",
    elevation:3,
  },
});
