"use strict";

import React, { PureComponent } from "react";

import {
  StyleSheet,
  View,
  Platform,
  Modal,
  TouchableOpacity,
  Text,
  TextInput,
  Image,
  ActivityIndicator,
  Alert,
  ScrollView,
  RefreshControl
} from "react-native";


import * as commonColors from "../styles/colors";
import * as commonStyles from "../styles/styles";
import { screenWidth, screenHeight } from "../styles/styles";

import * as actionTypes from "../store/common/actionTypes";
import moment from 'moment'
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
// import * as actions from "../redux/actions";
import * as actions from '../store/common/actions'
import * as session from '../store/session/actions'
import * as config from '../config'
import api from "../service/api";
import Cache from "../utils/cache";
import { Ionicons, MaterialCommunityIcons } from "@expo/vector-icons";
import { Actions } from "react-native-router-flux";
import HeaderBar from '../components/header'
import UtilService from '../utils/utils'
import Octicons from "@expo/vector-icons/Octicons";
var gHandler = null
var timeout = null
class ChatList extends PureComponent {
  constructor(props) {
    super(props);

    this.state = {
        chats:[
            {count:3},
            {count:3},
            {count:3},
            {count:0},
            {count:0},
            {count:0},
            {count:0},
            
        ],
        selectedOption:{name:Cache.getLang('label.today'), index:0},
        showFiltering: false,
        upcoming:[],
        duration:Cache.getLang('label.today'),
        showSearchModal:false,
        isWaiting:false,
        showSearchBox:false,
        options:[
          {name:Cache.getLang('label.today'), index:0},
          {name:Cache.getLang('label.this_week'), index:1},
          {name:Cache.getLang('label.this_month'), index:2},
          {name:Cache.getLang('label.this_year'), index:3},
          {name:Cache.getLang('label.date_range'), index:4},
        ],
    };
    gHandler = this
    this.startDate = new Date()
    this.endDate = new Date()
    this.isSignOff = false
    this.order = false
  }

  getMonday( date ) {
      var day = date.getDay() || 7;  
      if( day !== 1 ) 
          date.setHours(-24 * (day - 1)); 
      return date;
  }
  componentDidMount(){
    //this.props.actions.getUpcomingAllDeliveries()
    //this.refresh()
    this.onPressItem(this.state.options[0])
  }
  renderFiltering(){
    console.log('is renderfilter')
    return(
      <TouchableOpacity activeOpacity={1} onPress={()=>this.setState({showFiltering:false})} style={{elevation:5, backgroundColor:'rgb(131, 131, 137)'}}>
        <View style={{width:'100%', backgroundColor:'white', padding:15}}>
          {this.state.options.map((item, index)=>{
            let isSelected=(item.index==this.state.selectedOption.index)
            let color=isSelected?'grey':'#aaa'
            let weight=isSelected?'bold':'normal'
            return(
              <TouchableOpacity onPress={()=>this.onPressItem(item)} key={index} style={{flexDirection:'row', alignItems:'center', height:40}}>
                <Text style={{flex:1, fontSize:15, color:color, fontWeight:weight}}>{item.name}</Text>
                {isSelected&&
                  <Ionicons name="md-checkmark" color={commonColors.theme} size={24}/>}
              </TouchableOpacity>
            )
          })}
        </View>
      </TouchableOpacity>
    )
  }
  onPressItem(item){
    if ( item.index==4){
      this.setState({showFiltering:false, isSetDating: true})
    }else{
      this.setState({selectedOption:item, showFiltering:false})
      console.log('arrived item on onPressItem',item)
      var startDate = null
      var endDate = null
      if(item.name==Cache.getLang('label.today')){
          startDate = new Date()
          endDate = new Date()
      }
      if(item.name==Cache.getLang('label.this_week')){
        startDate = this.getMonday(new Date())
        endDate = new Date()
      }
      if(item.name==Cache.getLang('label.this_month')){
        var date = new Date();
        startDate = new Date(date.getFullYear(), date.getMonth()-1, 1);
        endDate = new Date()
      }
      if(item.name==Cache.getLang('label.date_range')){
        startDate = new Date()
        endDate = new Date()
      }
      this.startDate = startDate
      this.endDate = endDate
      var customerID = 0
      var status = ''
      var isSignOff = false
      this.setState({isWaiting:true})
      var query = ''

      console.log('filtering is started.......',this.startDate,this.endDate)
      api.getDeliveriesWithFilter(query,customerID,status,null,startDate,endDate,(error,res)=>{
        if(error){
          return
        }
        this.setState({upcoming:res.Results})
        this.setState({isWaiting:false})
      })
    }
  }
  refresh(){
    this.setState({isWaiting:true})
    this.order = true
    api.getDeliveriesByOrder(this.order,this.state.query,null, this.isSignOff, 1, this.pageSize,0, moment(this.startDate).format("YYYY-MM-DD")+" 00:00:00",
    (moment(this.endDate).format("YYYY-MM-DD")+' 23:59:59'),null, (err, res)=>{
      if ( err == null ){ 
        this.setState({upcoming:res.Results})
        this.setState({showSearchModal:false})
        this.setState({showSearchBox:false})
        setTimeout(()=>{this.setState({isWaiting:false})},500)
      }
    })

   
  }
  
  renderHeader(){
    return(
        <View style={{width:'100%', height:50, flexDirection:'row', alignItems:'center', paddingHorizontal:15, elevation:3, shadowColor:'black', shadowRadius:1, shadowOpacity:0.3, shadowOffset:{width:1, height:1}, backgroundColor:commonColors.theme, zIndex:1000}}>
          <Text style={{color:'white', fontWeight:'bold', fontSize:15, flex:1}}>{Cache.getLang('label.transguard')}</Text>
          <TouchableOpacity onPress={() => {this.setState({showModal:true})}} style={{padding:8}}>
            <Ionicons name={"md-power"} size={18} color={"white"} />
          </TouchableOpacity>
        </View>
    )
  }
  imageFailure(index){
    var temp = this.state.upcoming[index]
    temp.User.Avatar = null
    this.setState({upcoming:[...this.state.upcoming]})
    
}
  
  renderChatList(){
    //let items = this.props.upcomingDeliveries.concat(this.props.completedDeliveries)
    let {upcoming} = this.state
    return(
      <View>
        {upcoming.map((item, index)=>{
          return(
            <TouchableOpacity onPress={()=>Actions.Chat({channel:item})} key={index} style={{flexDirection:'row', alignItems:'center', paddingHorizontal:15, paddingVertical:10, backgroundColor:index%2==0?'#f4f4f5':'#f9f9f9'}}>
              {/* <Image source={commonStyles.face1} style={{width:40, height:40, borderRadius:20}}/> */}
              {/* <Image  source={commonStyles.noUser} style={{height:44, width:44, borderRadius:22, marginLeft:20}}/> */}
              <Image source={commonStyles.noUser} style={{position:'absolute',left:35,top:10, width:44, height:44, borderRadius:22}}/>
              {
                !!item.User &&
                <Image  source={{uri:config.SERVICE_FILE_URL+item.User.Avatar}} style={{height:44, width:44, borderRadius:22, marginLeft:20}}/>
              }
              
              <View style={{marginLeft:15, flex:1}}>
                <Text style={{color:'#777', fontSize:14, fontWeight:'bold'}}>#{item.DeliveryNoteNumber}</Text>
                <Text style={{color:'#333', fontSize:13, marginTop:5}}>{item.User.FirstName+' '+item.User.LastName}</Text>
              </View>
              <View style={{alignItems:'flex-end'}}>
                <Text style={{fontSize:13, color:'#aaa'}}>{UtilService.getDateTime(item.CreatedAt)}</Text>
                {item.count>0&&<View style={{width:18, height:18, borderRadius:9, alignItems:'center', justifyContent:'center', backgroundColor:'red', marginTop:5}}>
                  <Text style={{color:'white', fontSize:12}}>{item.UnreadCntByHQ}</Text>
                </View>}
                {item.count==0&&<View style={{height:18}}/>}
              </View>
            </TouchableOpacity>
          )
        })}
      </View>
    )
  }
  onRefresh(){
    console.log('onRefresh is started.....')
    this.pageSize = 20
    this.refresh()
  }
  renderContent(){
    return(
      <ScrollView style={{flex:1}}
        refreshControl={
          <RefreshControl
              refreshing={this.state.isWaiting}
              onRefresh={() => gHandler.onRefresh()}
              tintColor={'grey'}
          />
        }
      >
        {this.renderChatList()}
      </ScrollView>
    )
  }
  renderFilterBar(){
    console.log('renderfilterbar render______')  
    return(
          
          <View style={{elevation:5, backgroundColor:'white'}}>
            <View style={{width:'100%', height:1, backgroundColor:'grey'}}/>
            <View  style={{paddingHorizontal:15, flexDirection:'row', alignItems:'center', paddingVertical:5}}>
                <Text style={{fontSize:12, color:'#aaa'}}>{Cache.getLang('label.range')}:</Text>
                <TouchableOpacity style={{flexDirection:'row', alignItems:'center', marginLeft:10}}
                    onPress={()=>{this.setState({showFiltering:true});console.log('showFiltering is set')}}>
                    <Text style={{color:'black', fontWeight:'bold', fontSize:12, marginRight:20}}>{this.state.selectedOption.name}</Text>
                    <Octicons name="triangle-down" size={20} color={commonColors.blue}/>
                </TouchableOpacity>
                <View style={{flex:1}}/>
                <TouchableOpacity style={{padding:10}} onPress={()=>{console.log('showSearchModal is showing');this.state.showSearchBox?this.setState({showSearchBox:false}):this.setState({showSearchBox:true})}}>
                    <MaterialCommunityIcons name="magnify" size={24} color={commonColors.blue}/>
                </TouchableOpacity>
            </View>
          </View>
      )
  }
 
  onSearch()
  {
    this.setState({showSearchModal: false})
    var startDate = this.startDate
    var endDate = this.endDate
    var customerID = 0
    var status = ''
    var isSignOff = true
    var query = this.state.query
    this.setState({isWaiting:true})
      api.getDeliveries(query,null, false, 1, 20, null, null,null, (err, res)=>{
        if ( err == null ){
          this.setState({upcoming:res.Results})
          this.setState({isWaiting:false})
        }
      })
  }
  onSearchCancel()
  {
    this.setState({showSearchModal: false})
  }
  doDelayedSearch(text){

    if (timeout) {  
      clearTimeout(timeout);
    }
    timeout = setTimeout(function() {
       gHandler.refresh()
    }, 600);
 
}

  renderSearchBox(){
    return(
      <View style={{paddingHorizontal:15, flexDirection:'row', backgroundColor:'white', width:'100%', paddingVertical:8,elevation:3}}>
        <TextInput
          ref="search"
          autoCapitalize="none"
          autoCorrect={false}
          placeholder={Cache.getLang('label.search')}
          placeholderTextColor={commonColors.placeholderText}
          textAlign="left"
          style={[styles.input, {flex:1}]}
          underlineColorAndroid="transparent"
          value={this.state.query}
          onChangeText={text =>{
            this.setState({ query: text.replace(/\t/g, "") })
            //this.state.query = text.replace(/\t/g, "")
            this.doDelayedSearch()
            }
          }
        />
        <Ionicons name={'md-search'} color={commonColors.blue} size={20} style={{position:'absolute', left:30, top:16}}/>
      </View>
    )
  }
  render() {
    return (
      <View style={styles.container}>
        <HeaderBar title={Cache.getLang('label.chats')}/>
        {this.renderFilterBar()}
        {this.state.showFiltering&&this.renderFiltering()}
        {this.state.showSearchBox&&this.renderSearchBox()}
        {this.renderContent()}
        {/* {this.renderIndicator()} */}
      </View>
    );
  }
}

export default connect(
  state => ({
    upcomingDeliveries:state.common.upcomingDeliveries,
    completedDeliveries:state.common.completedDeliveries
  }),
  dispatch => ({
    actions: bindActionCreators(actions, dispatch)
  })
)(ChatList);

const styles = StyleSheet.create({
  
  container: {
    //paddingTop:(Platform.OS==="android")?24:0,
    flex: 1,
    paddingTop:24,
    backgroundColor: 'rgb(249,249,249)'
  },
  searchModalContainer:{
    flex: 1,
    backgroundColor: "rgba(0, 0, 0,0.5)",
    alignItems: "center",
    justifyContent: "center"
  },
  searchModal: {
    width: 200,
    height: 100,
    borderRadius: 5,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "white",
    elevation:3,
  },
  indicatorContainer: {
    flex: 1,
    backgroundColor: "rgba(0, 0, 0,0.5)",
    alignItems: "center",
    justifyContent: "center"
  },
  input: {
    fontSize: 14,
    color: commonColors.title,
    height: 40,
    alignSelf: "stretch",
    // marginHorizontal: 40,
    // borderColor: '#aaa',
    // borderWidth: 1,
    borderRadius: 3,
    marginBottom: 3,
    paddingHorizontal: 10,
    paddingLeft:50,
    backgroundColor: commonColors.cardColor6
  }
});
