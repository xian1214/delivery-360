import React, { Component } from 'react';

import {
  View,
  StyleSheet,
  TouchableOpacity,
  Text,
  Modal,
  TextInput,
  Linking, 
  Platform,
  Image,
  ScrollView,
  Dimensions,
  Alert,
  ActivityIndicator
} from 'react-native';

import MapView from 'react-native-maps';
import carImage from '../../public/images/car.png';
import { SlideButton, SlideDirection } from '../components/sliderButton';
import { Ionicons, MaterialIcons, MaterialCommunityIcons, Octicons } from "@expo/vector-icons";
import { Actions } from "react-native-router-flux";

import * as commonColors from "../styles/colors";
import * as commonStyles from "../styles/styles";
import { screenWidth, screenHeight } from "../styles/styles";

import * as actionTypes from "../store/common/actionTypes";

import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import * as actions from '../store/common/actions'
import * as session from '../store/session/actions'
import * as actions2 from '../redux/actions'
import itemsTab from './home/itemsTab';
import api from '../service/api'
import googleService from '../service/googleService'
const { width, height } = Dimensions.get("window");
import Cache from '../utils/cache'
import UtilService from '../utils/utils'
import { WaitingModal } from '../components/waiting';
import InformationBar from '../components/informationBar'
import moment from 'moment'
import * as CONST from '../components/constant'
import {ActionPicker} from 'react-native-action-picker'


const blue_east =require('../../public/images/car/Green/East.png')
const blue_north =require('../../public/images/car/Green/North.png')
const blue_northeast =require('../../public/images/car/Green/Northeast.png')
const blue_northwest =require('../../public/images/car/Green/Northwest.png')
const blue_south =require('../../public/images/car/Green/South.png')
const blue_southeast =require('../../public/images/car/Green/Southeast.png')
const blue_southwest =require('../../public/images/car/Green/Southwest.png')
const blue_west =require('../../public/images/car/Green/West.png')

const red_east =require('../../public/images/car/Red/East.png')
const red_north =require('../../public/images/car/Red/North.png')
const red_northeast =require('../../public/images/car/Red/Northeast.png')
const red_northwest =require('../../public/images/car/Red/Northwest.png')
const red_south =require('../../public/images/car/Red/South.png')
const red_southeast =require('../../public/images/car/Red/Southeast.png')
const red_southwest =require('../../public/images/car/Red/Southwest.png')
const red_west =require('../../public/images/car/Red/West.png')


function getHourMinutes(duration, date){
  let d = new Date(date)
  let dd = new Date(d.getTime()+duration)
  
  return moment(dd).format('hh:mm A')
}

class Navigation extends Component {

  constructor(props) {
    super(props);


    if ( props.currentDelivery ) Cache.originETA = (new Date(props.currentDelivery.ETA)).getTime()
    this.state = {
      prevPos: null,
      curPos: props.location?props.location.coords:{latitude:1.36, longitude:103.7},
      isPause: false,
      showListModal: false,
      showReasonModal: false,
      modalNumber:0,
      rerouteReason:'',
      reason: '',
      coordinates: null,
      distance: 12,
      duration: 12,
      showingConfirm: false,
      latitudeDelta: 0.0922,
      longitudeDelta: 0.0421,
      foundError: false,
      overETA10: false,
      onIdle:false,
      isWaiting: false,
      address:'',//props.location.address.formatted_address,
      end_address:'',//this.props.currentDelivery.DestinationAddress,
      velocity:0,
      displayMileage:0,
      shelfLifeAlerts:[],
      currentETA:Cache.originETA,
      isModalVisible:false,
      dnForCheck:null,
      markerIcon:blue_north,
      statusText:'Ready',
      isArrivalConfirming:false
    };
    this.mapView = null
    this.targetSpeed=0
    this.mustDistance=1
    this.mileage=0
    this.idleCounter = 0;
    this.oldDistance = 0
    this.oldShelfLifeAlerts=[]
  }
  checkShortageDistance(){

    let {currentDelivery} = this.props
    if(!!!currentDelivery){return}
    if(currentDelivery.OTWDatetime==null){
      return
    }
    
    // 10% shortage alert
    var startTime = (new Date(currentDelivery.OTWDatetime)).getTime();
    var currentTime = UtilService.getCurrentLocalDateTime().getTime();
    var duration = (currentTime - startTime)/1000;
    this.mustDistance = duration * this.targetSpeed
    if(this.mustDistance==0) return
    var percent = this.mileage/this.mustDistance
    if(percent<0.9){
      this.setState({overETA10:true})
    }
    
    //idle alert

  }
  getMarkerIcon(direction){
    console.log('direction ----1')
    if(direction=='North'){
      console.log('direction ----2')
        this.setState({markerIcon:blue_north})
        this.setState({alarmMarkerIcon:red_north})
    }
    else if(direction=='Northeast'){
      console.log('direction ----3')
      this.setState({markerIcon:blue_northeast})
      this.setState({alarmMarkerIcon:red_northeast})
    }
    else if(direction=='Northwest'){
      console.log('direction ----4')
      this.setState({markerIcon:blue_northwest})
      this.setState({alarmMarkerIcon:red_northwest})
    }
    else if(direction=='South'){
      console.log('direction ----5')
      this.setState({markerIcon:blue_south})
      this.setState({alarmMarkerIcon:red_south})
    }
    else if(direction=='Southeast'){
      console.log('direction ----6')
      this.setState({markerIcon:blue_southeast})
      this.setState({alarmMarkerIcon:red_southeast})
    }
    else if(direction=='Southwest'){
      console.log('direction ----7')
      this.setState({markerIcon:blue_southwest})
      this.setState({alarmMarkerIcon:red_southwest})
    }
    else if(direction=='West'){
      console.log('direction ----8')
      this.setState({markerIcon:blue_west})
      this.setState({alarmMarkerIcon:red_west})
    }
    else if(direction=='East'){
      console.log('direction----9')
      this.setState({markerIcon:blue_east})
      this.setState({alarmMarkerIcon:red_east})
    }
    else{
      this.setState({markerIcon:blue_north})
      this.setState({alarmMarkerIcon:red_north})
    }
  }
  checkTrackMode(){

    //console.log('checkTrackMode on navigation this.props.currentDelivery.DeliveryNoteID',this.props.currentDelivery.DeliveryNoteID)
    var id
    if(this.props.currentDelivery){
      id = this.props.currentDelivery.DeliveryNoteID
    }
    else{
      id = this.props.data.DeliveryNoteID
    }
    console.log('checkT 3')
    api.getDelivery(id, (err, res)=>{
      if(err){
        return
      }
      console.log('checkT 4')
      this.setState({dnForCheck:res})
      if(res.Vehicle.TrackMode==CONST.TRACK_MODE_BYTRACKER){
        console.log('checkT 5')
        this.props.actions.setLocationMode(CONST.TRACK_MODE_BYTRACKER);
        var imei = null
        if((!!res.Vehicle) && (!!res.Vehicle.Trackers) &&(!!res.Vehicle.Trackers[0]) &&(!!res.Vehicle.Trackers[0].Tracker)){
          imei = res.Vehicle.Trackers[0].Tracker.ImeiNo
        }
        Cache.currentImei = imei
        api.getLocationFromCloud(imei,(error,res)=>{
            if(error){
              console.log('checkT 6')
              //Alert.alert("Tracker on vehicle is now available now! \nConverting to mobile device mode")    
              this.props.actions.setLocationMode(CONST.TRACK_MODE_BYPHONE);
              return
          } 
          console.log('checkT 7')
            var location={
              coords:{
                latitude:Number(res[0].Latitude),
                longitude:Number(res[0].Longitude),
              },
              speed:Number(res[0].Speed)
            }
            this.props.actions.setLocation(location)
            this.setState({gotLocation:true})
            console.log('this.props.location---on navigation cloud',this.props.location)
            //Alert.alert('Tracker is available and vehicle is tracked by gps tracker!')    
        })
      }
      else if(res.Vehicle.TrackMode==CONST.TRACK_MODE_BYPHONE){
         Alert.alert("Vehicle is tracked by phone")
         this.props.actions.setLocationMode(CONST.TRACK_MODE_BYPHONE);
         this.setState({gotLocation:true})
      }
      else{

      }
  
    })

      
  }
  uploadLocation(){
    api.postLocation({
      DeliveryRouteID: this.props.currentDelivery.DeliveryRouteID,
      DeliveryNoteID: this.props.currentDelivery.DeliveryNoteID,
      UserID: this.props.currentDelivery.User.UserID,
      Lat: this.props.location.coords.latitude,
      Long: this.props.location.coords.longitude
    },(err, res) => {
        
    });  
  }
  checkRoute(){
    let {currentDelivery} = this.props
    if(currentDelivery==null){return}
    var limit = 5
    if ( !this.state.coordinates ) return;    
    if(currentDelivery.DriveOffDeviation!=0 && currentDelivery.DriveOffDeviation!=null){
          limit = currentDelivery.DriveOffDeviation
    }
    let dis = UtilService.getDistanceFromRoute(this.state.coordinates, this.state.curPos)
    this.setState({foundError: dis > limit?true:false})
  }
  componentWillReceiveProps(nextProps){
    
    let {curPos,mileage} = this.state
    let {currentDelivery} = this.props
    if ( nextProps.type == actionTypes.SET_LOCATION && nextProps.status==actionTypes.SUCCESS ){
      console.log('nextProps.location==',nextProps.location)
      if(!!nextProps.location && !!nextProps.location.direct && nextProps.location!=this.props.location){
        this.getMarkerIcon(nextProps.location.direct)
      }      
      // if(nextProps.location==null) {return;} 
      // var nextPos = nextProps.location.coords
      // var distance = UtilService.getDistanceFromLatLonInMeter(curPos.latitude,curPos.longitude,nextPos.latitude,nextPos.longitude);
      // this.mileage+=distance/1000
      // this.setState({displayMileage:this.mileage})
      // ////console.log('this.state.mileage>>>distance>>',this.state.mileage,' ',distance)
      // var limitTimeSpan = (!!!currentDelivery || (!!currentDelivery && currentDelivery.IdleDuration==0))?5:currentDelivery.IdleDuration
      // ////console.log('gps location is being got........',limitTimeSpan*60*1000/CONST.GET_LOCATION_PERIOD)      
      // // idle check
      // var tempVelocity = (distance/CONST.GET_LOCATION_PERIOD*1000)
      // this.setState({velocity:tempVelocity})
      // if(distance<50 && this.oldDistance<50){
      //   this.idleCounter++;
      //   ////console.log('idleCounter/distance/olddistance/limitTimeSpan',this.idleCounter,'-',distance,'-',this.oldDistance,'-',limitTimeSpan)
      //   if(this.idleCounter==limitTimeSpan*60*1000/CONST.GET_LOCATION_PERIOD){ // idle during 5 mins
      //     this.setState({onIdle:true})
      //     this.idleCounter = 0;
      //   }
      // }
      // else{
      //   this.idleCounter=0
      // }
      // // driver off check
      // this.oldDistance = distance
      // this.setState({curPos: nextProps.location.coords},()=>{        
      //   this.checkRoute()
      // })
      
      
    }  
    if ( nextProps.type == actionTypes.SET_ALERT_SHELFLIFE){
        ////console.log('shelfLifeAlerts signal is detected.....uuuuuuuu------oldShelfAlerts',this.oldShelfAlerts)
        // if(this.oldShelfLifeAlerts!=nextProps.shelfLifeAlerts){
        //   this.setState({shelfLifeAlerts:nextProps.shelfLifeAlerts})
        //   this.sendShelfLifeAlert()
        // }
        // this.oldShelfLifeAlerts = nextProps.shelfLifeAlerts
    }

  }
  sendShelfLifeAlert(){
    api.createAlert({
      Type:CONST.ALARM_TYPE_EXPIRED,
      Level:CONST.ALERT_LEVEL ,
      Message: 'Item Expired',
      ReferenceID: this.props.currentDelivery.DeliveryNoteID,
      UserID: this.props.currentDelivery.User.UserID
    }, (err, res)=>{
       if(err){
         return
       }

    })
  }
  

  componentWillUnmount(){
    this.hasMounted = false
  }
  //this.getMarkerIcon(xItem.Direct)
  componentDidMount(){
    
    this.hasMounted=true
    this.setState({statusText:'Get route'})
    let myRouteTimer = setInterval(()=>{
      if ( this.props.currentDelivery && this.props.currentDelivery.DeliveryRoute && this.state.gotLocation){
        setTimeout(()=>{
          this.getRoute()
          clearInterval(myRouteTimer)
        },200)
        
      }
    }, 500)

    setTimeout(()=>{this.setState({isModalVisible:true})},500)
    console.log('checkT 1')
    this.setState({gotLocation:false})
    if(!!!this.props.isFromSetRoute){
      this.setState({statusText:'Checking track mode'})
      this.checkTrackMode()
    }
    else{
      this.setState({gotLocation:true})
    }
  }

  getRoute(){
    // //console.log('current', this.props.currentDelivery)
    let {StartLat, StartLong, EndLat, EndLong, PathData} = this.props.currentDelivery.DeliveryRoute
    if (this.props.currentDelivery.IsSignOff){
      StartLat = this.props.currentDelivery.ReturnRoute.StartLat
      StartLong = this.props.currentDelivery.ReturnRoute.StartLong
      EndLat = this.props.currentDelivery.ReturnRoute.EndLat
      EndLong = this.props.currentDelivery.ReturnRoute.EndLong
      PathData = this.props.currentDelivery.ReturnRoute.PathData
    }
    let {index} = JSON.parse(PathData)
    googleService.getTripPath(
      StartLat+','+StartLong,
      EndLat+','+EndLong,
      (err, res)=>{
        if ( err == null ){
          // //console.log('length', res[index].path.length)
          this.setState({coordinates:res[index].path, index, duration:res[index].duration, distance:res[index].distance},()=>{
            //this.checkRoute()
          })
          this.mapView.fitToCoordinates(res[index].path, {
            edgePadding: {
              right: width / 20,
              bottom: height / 7,
              left: width / 20,
              top: height / 7
            }
          });
        }
      })
  }

  onPause(){
    if ( this.state.isPause ){
      this.resumeDrive()
      return;
    }
    this.setState({modalNumber:1})
  }

  onChat(){
    Actions.Chat({type:'replace',channel:this.props.currentDelivery})
  }

  onList(){
    api.getAlerts((err, res)=>{
      // //console.log('alert list', res)
      if ( err == null ){
        let alertList = []
        res.Results.map((item)=>{
          if ( item.ReferenceID == this.props.currentDelivery.DeliveryNoteID )
            alertList.push(item)
        })
        this.setState({modalNumber:3, alertList})
      }
    })
    
  }

  onReroute(){
    if ( !this.props.internetConnection ){
      Alert.alert('You can not reroute on offline mode!')
      return;
    }
    this.setState({modalNumber:2})
  }

  renderConfirmModal(){
    return(
    <Modal
        visible={this.state.showingConfirm}
        transparent={true}
        onRequestClose={() => {}}
      >
        <View style={styles.modalContainer}>
          <View style={styles.confirmModal}>
              <Text style={{textAlign:'center', margin:20, fontSize:14}}>
                {Cache.getLang('label.geofence_confirm_warning')}
              </Text>
              <TouchableOpacity activeOpacity={0.7} onPress={()=>this.arrived()} style={{width:'100%', height:50, alignItems:'center', justifyContent:'center', backgroundColor:'rgb(60, 118, 177)'}}>
                <Text style={{fontSize:15, color:'white', fontWeight:'bold'}}>
                {Cache.getLang('label.geofence_confirm_done')}
                </Text>
              </TouchableOpacity>
              <Text onPress={()=>this.setState({showingConfirm:false})} style={{fontSize:15, color:'rgb(85, 151,221)', fontWeight:'bold', textAlign:'center', margin:15}}>
                {Cache.getLang('label.geofence_confirm_goback')}
              </Text>
          </View>
        </View>
      </Modal>
    )
  }

  renderModal(){
    return(
      <Modal
        visible={this.state.modalNumber>0?true:false}
        transparent={true}
        onRequestClose={() => {}}
      >
        <View style={styles.modalContainer}>
          <View style={styles.content}>
            {this.state.modalNumber==1&&this.renderPause()}
            {this.state.modalNumber==2&&this.renderReroute()}
            {this.state.modalNumber==3&&this.renderList()}
          </View>
        </View>
      </Modal>
    )
  }

  pauseDrive(index){
    let reasonList = ['Toilet', 'Meal'], reason=''
    if ( index < 2 ) reason = reasonList[index]
    else reason = this.state.reasonPause

    api.createAlert({
      Type: commonStyles.PAUSE,
      Level: CONST.ALERT_LEVEL,
      Message: reason,
      ReferenceID: this.props.currentDelivery.DeliveryNoteID,
      UserID: this.props.currentDelivery.User.UserID
    }, (err, res)=>{
      this.setState({isPause:!this.state.isPause, modalNumber:0})
    })
  }

  resumeDrive(){
    api.createAlert({
      Type: commonStyles.RESUME,
      Level: CONST.EVENT_LEVEL,
      Message: 'Resume',
      ReferenceID: this.props.currentDelivery.DeliveryNoteID,
      UserID: this.props.currentDelivery.User.UserID
    }, (err, res)=>{
      this.setState({isPause:!this.state.isPause, modalNumber:0})
    })
  }

  renderPause(){
    return(
      <View style={{paddingVertical:15}}>
        <Text stye={{color:'#aaa', fontSize:12}}>{getHourMinutes(0, (new Date()))}</Text>
        <Text stye={{color:'#333', fontSize:12}}>{this.state.address}</Text>

        <Text style={{color:'grey', fontSize:15, marginTop:15}}>What was your reason for pausing?</Text>
        <View style={{flexDirection:'row', alignItems:'center', justifyContent:'center', marginTop:15}}>
          <View style={{alignItems:'center'}}>
            <TouchableOpacity onPress={()=>this.pauseDrive(0)} style={{backgroundColor:commonColors.blue, height:40, width:40, borderRadius:20, alignItems:'center', justifyContent:'center'}}>
              <MaterialIcons name="wc" color={'white'} size={20}/>
            </TouchableOpacity>
            <Text style={{fontSize:12, color:'#aaa', marginTop:5}}>Toilet</Text>
          </View>
          <View style={{width:30}}/>
          <View style={{alignItems:'center'}}>
            <TouchableOpacity onPress={()=>this.pauseDrive(1)} style={{backgroundColor:commonColors.green, height:40, width:40, borderRadius:20, alignItems:'center', justifyContent:'center'}}>
              <MaterialIcons name="restaurant" color={'white'} size={20}/>
            </TouchableOpacity>
            <Text style={{fontSize:12, color:'#aaa', marginTop:5}}>Meal</Text>
          </View>
        </View>
        <Text style={{color:'#aaa', fontSize:12, marginTop:15}}>Others</Text>
        <View style={{flexDirection:'row', width:'100%', marginTop:10}}>
              <TextInput
                ref="reason"
                autoCapitalize="none"
                autoCorrect={false}
                placeholder="Your Reason"
                placeholderTextColor={commonColors.placeholderText}
                textAlign="left"
                style={[styles.input, {flex:1}]}
                underlineColorAndroid="transparent"
                value={this.state.reasonPause}
                onChangeText={text =>
                  this.setState({ reasonPause: text.replace(/\t/g, "") })
                }
              />
              <TouchableOpacity onPress={()=>this.pauseDrive(3)} style={{backgroundColor:commonColors.blue, height:40, width:40, borderRadius:20, alignItems:'center', justifyContent:'center', marginLeft:15}}>
                <MaterialIcons name="check" color={'white'} size={20}/>
              </TouchableOpacity>
        </View>
        <TouchableOpacity onPress={()=>this.setState({modalNumber:0})} style={{position:'absolute', right:0, top:0, padding:5}}>
            <Ionicons name="md-close" size={28} color={'#333'}/>
        </TouchableOpacity>
      </View>
    )
  }

  reroute(index){
    let reasons = ['', 'Miss Turn', 'Traffic', 'Toilet', 'Meal']
    this.setState({modalNumber:0})
    if ( index <= 4 ) this.state.reason = reasons[index]
    else this.state.reason = this.state.rerouteReason
    this.selectRoute()
  }

  renderReroute(){
    return(
      <View style={{paddingVertical:15}}>
        <Text stye={{color:'#aaa', fontSize:12}}>{getHourMinutes(0, (new Date()))}</Text>
        <Text stye={{color:'#333', fontSize:12}}>{this.state.address}</Text>

        <Text style={{color:'grey', fontSize:15, marginTop:15}}>What was your reason for rerouting?</Text>
        <View style={{flexDirection:'row', alignItems:'center', justifyContent:'center', marginTop:15}}>
          <View style={{alignItems:'center'}}>
            <TouchableOpacity onPress={()=>this.reroute(1)} style={{backgroundColor:commonColors.brown, height:40, width:40, borderRadius:20, alignItems:'center', justifyContent:'center'}}>
              <MaterialIcons name="warning" color={'white'} size={20}/>
            </TouchableOpacity>
            <Text style={{fontSize:12, color:'#aaa', marginTop:5}}>Miss turn</Text>
          </View>
          <View style={{width:30}}/>
          <View style={{alignItems:'center'}}>
            <TouchableOpacity onPress={()=>this.reroute(2)} style={{backgroundColor:commonColors.pink, height:40, width:40, borderRadius:20, alignItems:'center', justifyContent:'center'}}>
              <MaterialIcons name="traffic" color={'white'} size={20}/>
            </TouchableOpacity>
            <Text style={{fontSize:12, color:'#aaa', marginTop:5}}>Traffic</Text>
          </View>
          <View style={{width:30}}/>
          <View style={{alignItems:'center'}}>
            <TouchableOpacity onPress={()=>this.reroute(3)} style={{backgroundColor:commonColors.blue, height:40, width:40, borderRadius:20, alignItems:'center', justifyContent:'center'}}>
              <MaterialIcons name="wc" color={'white'} size={20}/>
            </TouchableOpacity>
            <Text style={{fontSize:12, color:'#aaa', marginTop:5}}>Toilet</Text>
          </View>
          <View style={{width:30}}/>
          <View style={{alignItems:'center'}}>
            <TouchableOpacity onPress={()=>this.reroute(4)} style={{backgroundColor:commonColors.green, height:40, width:40, borderRadius:20, alignItems:'center', justifyContent:'center'}}>
              <MaterialIcons name="restaurant" color={'white'} size={20}/>
            </TouchableOpacity>
            <Text style={{fontSize:12, color:'#aaa', marginTop:5}}>Meal</Text>
          </View>
        </View>
        <Text style={{color:'#aaa', fontSize:12, marginTop:15}}>Others</Text>
        <View style={{flexDirection:'row', width:'100%', marginTop:10}}>
              <TextInput
                ref="reason"
                autoCapitalize="none"
                autoCorrect={false}
                placeholder="Your Reason"
                placeholderTextColor={commonColors.placeholderText}
                textAlign="left"
                style={[styles.input, {flex:1}]}
                underlineColorAndroid="transparent"
                value={this.state.rerouteReason}
                onChangeText={text =>
                  this.setState({ rerouteReason: text.replace(/\t/g, "") })
                }
              />
              <TouchableOpacity onPress={()=>this.reroute(5)} style={{backgroundColor:commonColors.blue, height:40, width:40, borderRadius:20, alignItems:'center', justifyContent:'center', marginLeft:15}}>
                <MaterialIcons name="check" color={'white'} size={20}/>
              </TouchableOpacity>
        </View>
        <TouchableOpacity onPress={()=>this.setState({modalNumber:0})} style={{position:'absolute', right:0, top:0, padding:5}}>
            <Ionicons name="md-close" size={28} color={'#333'}/>
        </TouchableOpacity>
      </View>
    )
  }

  

  renderList(){
    return(
      <View style={{paddingVertical:15, height:300}}>
        <ScrollView>
          {this.state.alertList.map((item, index)=>{
            return(
              <View key={index} style={{flexDirection:'row', alignItems:'center', marginTop:10}}>
                <View style={{backgroundColor:commonColors.brown, height:40, width:40, borderRadius:20, alignItems:'center', justifyContent:'center', marginLeft:15}}>
                  <MaterialCommunityIcons name="truck" size={20} color={"white"} />
                </View>
                <View style={{marginLeft:15, justifyContent:'center'}}> 
                  <Text style={{fontSize:12, color:'#aaa'}}>{item.Message}</Text>
                  <Text style={{fontSize:13, color:'#333', fontWeight:'bold'}}>new EDA: {}</Text>
                </View>
              </View>
            )
          })}
        </ScrollView>
        <TouchableOpacity onPress={()=>this.setState({modalNumber:0})} style={{position:'absolute', top:0, right:15}}>
          <Ionicons name="ios-close-circle" color={'red'} size={30}/>
        </TouchableOpacity>
      </View>
    )
  }
omen
  linkGoogleMap(){
    let {latitude, longitude} = this.state.curPos;
    let scheme = Platform.OS === 'ios' ? 'maps:' : 'geo:'
        let url = scheme + latitude + ',' + longitude
        Linking.canOpenURL(url).then(supported => {
            if (supported) {
                Linking.openURL(url);
            } else {
                // //console.log('don`t know url')
            }
        })
  }

  arrived(){
    this.setState({showingConfirm: false})
    
    this.setState({isArrivalConfirming:true})
    this.setState({statusText:'Confirming arrival'})  
    console.log('this.props.currentDelivery.DeliveryNoteID on arrive',this.props.currentDelivery.DeliveryNoteID)
    api.createAlert({
      Type: this.props.currentDelivery.IsSignOff?commonStyles.Finish:CONST.ALARM_TYPE_ARRIVAL,
      Level:CONST.EVENT_LEVEL,
      Message: this.props.currentDelivery.IsSignOff?'finished':'Arrived',
      ReferenceID: this.props.currentDelivery.DeliveryNoteID,
      UserID: this.props.currentDelivery.User.UserID
    }, (err, res)=>{
        console.log('check arrive 11',err,res)
        this.setState({isArrivalConfirming:false})
        if(err){
          console.log('check arrive 12')
          Alert.alert('Confirm arriving failed!')
          return
        }
        //this.props.currentDelivery.OTWStatus = false   
        if(Cache.hasInternetConnection){
          console.log("check arrive 1")
          //this.props.actions.updateCurrentDelivery(this.props.currentDelivery.DeliveryNoteID)
          this.props.actions.updateCurrentDelivery()
        }else{
          if ( this.props.currentDelivery.IsSignOff ){
            this.props.actions.manualUpdate(null)
          }else{
            this.props.currentDelivery.OTWStatus = false;
          }
        }
       // if(!!this.props.currentDelivery)
       // {
        console.log("check arrive 2")
          if ( !this.props.currentDelivery.IsSignOff ){
            console.log("check arrive 3")
            console.log("this.props.currentDelivery before arrive",(!!this.props.currentDelivery)?'DN is not null':'DN is null',this.props.currentDelivery.DeliveryNoteNumber)
            Actions.DeliveryReport({type:'replace', currentDelivery:this.props.currentDelivery, editable:true})
          }else{
            Actions.popTo('Main')
            this.props.actions.getTodayUpcomingDeliveryCount()
            this.props.actions.getFutureUpcomingDeliveryCount()
          }
       // }
        
    })
  
  }

  onBack(){
    Actions.popTo('Main')
    if ( this.props.currentDelivery.IsSignOff ){
      this.props.actions.getTodayUpcomingDeliveryCount()
      this.props.actions.getFutureUpcomingDeliveryCount()
    }
  }

  renderIndicator() {
    return (
      <Modal
        visible={true}
        transparent={true}
        onRequestClose={() => {}}
      >
        <View style={styles.indicatorContainer}>
          <View style={styles.indicator}>
            <ActivityIndicator size="large" />
          </View>
        </View>
      </Modal>
    );
  }

  selectRoute(){

    let {DestinationLatitude, DestinationLongitude, Warehouse, DeliveryNoteID, User, Status} = this.props.currentDelivery
    if ( this.props.currentDelivery.IsSignOff ){
      DestinationLatitude = this.props.currentDelivery.ReturnWarehouse.Latitude
      DestinationLongitude = this.props.currentDelivery.ReturnWarehouse.Longitude
    }
    if(!!!this.props.location){
      Alert.alert("Can't get current location")
      return
    }
    googleService.getTripPath(
      this.props.location.coords.latitude+','+this.props.location.coords.longitude,
      DestinationLatitude+','+DestinationLongitude,
      (err, result)=>{
        if ( err == null ){
          // calcDist(res)
          this.setState({Routes:result})
          Actions.SelectRoute({data:this.props.currentDelivery, routes:result, index:0, update:(index)=>{
            this.setState({coordinates:this.state.Routes[index].path, index, duration:this.state.Routes[index].duration, distance:this.state.Routes[index].distance})
            this.mapView.fitToCoordinates(result[0].path, {
              edgePadding: {
                right: width / 20,
                bottom: height / 20,
                left: width / 20,
                top: height / 20
              }
            });

            api.postRoute({
              DeliveryNoteID: DeliveryNoteID,
              UserID:User.UserID,
              StartLat:this.props.location.coords.latitude,
              StartLong:this.props.location.coords.longitude,
              EndLat:DestinationLatitude,
              EndLong:DestinationLongitude,
              PathData:JSON.stringify({index,dist:result[index].distance, duration:result[index].duration}),
              TotalLength:100,
              LastUpdateReason: this.state.reason,
              isReturnRoute:Status==commonStyles.DELIVERY_STATUS_RETURNING
            },
            (err, res)=>{
              // //console.log('------', err)
              //this.targetSpeed = Routes[index].distance/Routes[index].distance*1000 // m/s
              this.setState({isWaiting:false})
              Cache.originETA = (new Date()).getTime()+ this.state.Routes[index].duration*1000;
              this.setState({currentETA:Cache.originETA})
              if ( err == null ){
                this.props.currentDelivery.DeliveryRoute = res
                this.props.currentDelivery.DeliveryRouteID = res.DeliveryRouteID
                //this.uploadLocation()
                api.createAlert({
                  Type: commonStyles.REROUTE,
                  Level: 2,
                  Message: this.state.reason,
                  ReferenceID: DeliveryNoteID,
                  UserID: User.UserID
                }, (err1, res1)=>{
                  // //console.log('----create alert----', err1, res1)
                })
              }else{
                Alert.alert("you can't select route now")
              }
            })

          }})
        }
      }
    )

  }

  renderReroutAlert(){
    return(
      <View style={{position:'absolute', left:0, top:170, backgroundColor:'transparent', padding:15, width:'100%', elevation:5}}>
        <TouchableOpacity onPress={()=>this.onReroute()} activeOpacity={0.7} style={[styles.alertContainer,{elevation:4, borderWidth:1, borderColor:'grey'}]}>
          <Ionicons name={'md-warning'} size={18} color={commonColors.brown}/>
          <Text style={{color:'grey', fontWeight:'bold', marginLeft:10, flex:1}}>Rerouting detected!</Text>
          <Text style={{color:'rgb(60, 117, 177)', fontWeight:'bold'}}>Respond</Text>
        </TouchableOpacity>
      </View>

    )
  }

  renderOver10Alert(){
    return(
      <View style={{position:'absolute', left:0, top:170, backgroundColor:'transparent', padding:15, width:'100%'}}>
        <TouchableOpacity onPress={()=>this.setState({overETA10:false})} activeOpacity={0.7} style={[styles.alertContainer,{elevation:4, borderWidth:1, borderColor:'grey'}]}>
          <Ionicons name={'md-warning'} size={18} color={commonColors.brown}/>
          <Text style={{color:'grey', fontWeight:'bold', marginLeft:10, flex:1}}>Need to speed up</Text>
        </TouchableOpacity>
      </View>
    )
  }

  renderIdleAlert(){
    return(
      <View style={{position:'absolute', left:0, top:170, backgroundColor:'transparent', padding:15, width:'100%',elevation:5}}>
        <TouchableOpacity onPress={()=>this.setState({onIdle:false})} activeOpacity={0.7} style={[styles.alertContainer,{elevation:4, borderWidth:1, borderColor:'grey'}]}>
          <Ionicons name={'md-warning'} size={18} color={commonColors.brown}/>
          <Text style={{color:'grey', fontWeight:'bold', marginLeft:10, flex:1}}>Truck is on idle</Text>
        </TouchableOpacity>
      </View>
    )
  }

  done(){
    if ( Cache.isWithinGeofence ){
      this.arrived()
    }else{
      this.setState({showingConfirm:true})
    }
    
  }
  sendAlert = (actionName) => {
    Alert.alert('Info', `${actionName} pressed!`);
  };

  startDelivery(id){
    //-----------Create Alert------
    api.createAlert({
      Type: isReturnRoute?commonStyles.Returning:commonStyles.START,
      Level: CONST.EVENT_LEVEL,
      Message: this.state.reason,
      ReferenceID: id,
      UserID: User.UserID
    }, (err1, res1)=>{
      if(err1){
        Alert.alert('Delivery start error')
        return
      }
      
    })
  }
  render() {
    if ( !!!this.props.currentDelivery || !this.state.gotLocation || this.state.isArrivalConfirming){       
        return(
          <View style={{flex:1,alignItems:'center',justifyContent:'center'}}>
              <Text style={{color:'blue',fontWeight:'bold',fontSize:20}}>{this.state.statusText}</Text>
              <ActivityIndicator size={150}/>
          </View>
        )

    }
    if ( this.props.currentDelivery.IsSignOff && this.props.currentDelivery.ReturnWarehouse==null) return null
    let {distance, duration} = this.state
    let {currentDelivery, internetConnection} = this.props
    let dnItems = currentDelivery.DeliveryNoteItems
    return (
      <View style={styles.flex}>
      <InformationBar iConnection={internetConnection} currentDelivery={currentDelivery}/>  
      {/* <Text onPress={()=>{this.map.animateToCoordinate({latitude: 37.420814, longitude: -122.081949})}}>{this.state.text}</Text> */}
        <View style={{backgroundColor:commonColors.theme, flexDirection:'row', alignItems:'center', height:60, paddingHorizontal:15, elevation:5}}>
          <TouchableOpacity onPress={()=>this.onBack()} style={{padding:10}}>
            <Ionicons name="ios-arrow-round-back" color={'white'} size={40}/>
          </TouchableOpacity>
          <View style={{flex:1, marginLeft:15, overflow:'hidden'}}>
            <Text style={{fontSize:18, color:'white'}}>{currentDelivery.DeliveryNoteNumber}</Text>
            <Text numberOfLines={2} style={{fontSize:12, color:'white'}}>{currentDelivery.DestinationAddress}</Text>
          </View>
          <TouchableOpacity onPress={()=>{Actions.NavService()}} style={{marginRight:15, padding:10}}>
            <Ionicons name={'ios-compass'} size={24} color={'white'}/>
          </TouchableOpacity>
          <TouchableOpacity onPress={()=>this.onChat()} style={{padding:10}}>
            <Ionicons name={'md-chatboxes'} size={24} color={'white'}/>
            {   
                currentDelivery.UnreadCntByDriver>0 &&
                <View style={{right:-7,top:-5,width:18,height:18,backgroundColor:'red',borderRadius:10,position:'absolute'}}>
                  <Text style={{fontWeight:'bold',right:-7,top:0,width:20,height:20,color:'white',borderRadius:10,position:'absolute',textAlign:'center'}}>{currentDelivery.UnreadCntByDriver}</Text>
                </View>                
            }
            
          </TouchableOpacity>
        </View>
        <View style={{width:'100%', backgroundColor:'white', flexDirection:'row', alignItems:'center',paddingVertical:8, paddingHorizontal:15, elevation:5}}>
            <View style={{flex:1}}>
              <Text style={{color:'black', fontSize:16, fontWeight:'bold'}}>
                  {(distance/1000).toFixed(1)}
                  <Text style={{color:'grey', fontSize:16}}> km,</Text>
              </Text>            
              <View style={{flex:1, flexDirection:'row'}}>
                  <Text style={{color:'black', fontSize:16, fontWeight:'bold'}}> {(duration/3600).toFixed(0)}</Text>
                  <Text style={{color:'grey', fontSize:16}}> hr</Text>
                  <Text style={{color:'black', fontSize:16, fontWeight:'bold'}}> {((duration%3600)/60).toFixed(0)}</Text>
                  <Text style={{color:'grey', fontSize:16}}> min</Text>
              </View>
            </View>
            <TouchableOpacity onPress={()=>this.onReroute()}>
                <Image source={commonStyles.path_theme} style={{width:40, height:40, resizeMode:'contain'}}/>
            </TouchableOpacity>
            <View style={{width:1, height:'100%', backgroundColor:'#ccc', marginHorizontal:15}}/>
            <View style={{alignItems:'center'}}>
                <Text style={{color:'grey', fontSize:12}}>ETA</Text>
                <Text style={{fontWeight:'bold', color:'#333', fontSize:18}}>{moment(this.state.currentETA).format('hh:mm A')}</Text>
            </View>
        </View>
        <View style={{elevation:1, position:'absolute',left:0,top:170,width:'100%', flexDirection:'row', alignItems:'center',paddingVertical:8, paddingHorizontal:15, elevation:5}}>
            <View style={{flex:1, flexDirection:'row'}}>
            </View>
            <View style={{alignItems:'center',margin:5,backgroundColor:"rgba(182, 179, 179,0.3)",borderRadius:5}}>
                <Text style={{color:'black',fontWeight:'bold',color:'blue', fontSize:14}}>{' '+((!!this.state.dnForCheck && !!this.state.dnForCheck.Vehicle)?this.state.dnForCheck.Vehicle.LicensePlate:'')} ({(this.props.locationMode==CONST.TRACK_MODE_BYPHONE)?'TRACKING BY PHONE':'TRACKING BY TRACKER'} ) </Text>
            </View>
        </View>
        {/* <View style={{elevation:1, position:'absolute',left:0,top:170,width:'100%',backgroundColor:'transparent', flexDirection:'row', alignItems:'center',paddingVertical:8, paddingHorizontal:15, elevation:5}}>
            <View style={{flex:1, flexDirection:'row'}}>
            </View>
            <View style={{alignItems:'center'}}>
                <Text style={{color:'black',fontWeight:'bold',color:'blue', fontSize:24}}>{this.state.velocity.toFixed(2)} m/s</Text>
            </View>
        </View>
        <View style={{elevation:1,position:'absolute',left:0,top:200,width:'100%',backgroundColor:'transparent', flexDirection:'row', alignItems:'center',paddingVertical:8, paddingHorizontal:15, elevation:5}}>
            <View style={{flex:1, flexDirection:'row'}}>
            </View>
            <View style={{alignItems:'center'}}>
                <Text style={{zIndex:1000,color:'black',fontWeight:'bold',color:'blue', fontSize:24}}>{this.state.displayMileage.toFixed(3)} km</Text>
            </View>
        </View> */}
        {/* <View style={{position:'absolute', left:0, top:170, backgroundColor:'transparent', padding:15, width:'100%'}}>
          <TouchableOpacity onPress={()=>this.setState({onIdle:false})} activeOpacity={0.7} style={[styles.alertContainer,{elevation:4, borderWidth:1, borderColor:'grey'}]}>
            <Ionicons name={'md-warning'} size={18} color={commonColors.brown}/>
            <Text style={{color:'grey', fontWeight:'bold', marginLeft:10, flex:1}}>Truck is on idle</Text>
          </TouchableOpacity>
        </View> */}
        <MapView
          ref={(el) => (this.mapView = el)}
          style={styles.flex}
          // minZoomLevel={12}
          // followsUserLocation={true}
          // showsCompass={true}
          // showsUserLocation={true}
          initialRegion={{
            ...this.state.curPos,
            latitudeDelta: this.state.latitudeDelta,
            longitudeDelta: this.state.longitudeDelta,
          }}
        >
          {this.state.coordinates&&<MapView.Polyline
              coordinates={this.state.coordinates}
              strokeColor={commonColors.theme}
              fillColor={commonColors.theme}
              strokeWidth={5}
          />}
          {!!this.props.location && !!this.props.location.coords &&
          <MapView.Marker
              //onPress = {()=>{Actions.VehicleDetail()}}
              coordinate={this.props.location.coords}
            >
              <Image source={this.state.markerIcon} style={{width:60,height:60}}/>
              <MapView.Callout style={{width:130, height:80, padding:5}}>
                  <View style={styles.markerInfo}>
                    <Text style={{color:'#333', fontSize:10}}>Lat:{this.props.location.coords.latitude}</Text>
                    <Text style={{color:'#333', fontSize:10}}>Lon:{this.props.location.coords.longitude}</Text>
                  </View>
              </MapView.Callout>
          </MapView.Marker>
          }
          <MapView.Marker
            coordinate={{
              longitude:this.props.currentDelivery.IsSignOff?this.props.currentDelivery.ReturnWarehouse.Longitude:this.props.currentDelivery.DestinationLongitude,
              latitude:this.props.currentDelivery.IsSignOff?this.props.currentDelivery.ReturnWarehouse.Latitude:this.props.currentDelivery.DestinationLatitude
            }}
          >
            <View style={{}}>
              <Ionicons name="ios-home" size={40} color={'rgb(245,106,85)'}/>
            </View>
          </MapView.Marker>
        </MapView>
        {this.state.foundError&&this.renderReroutAlert()}
        {this.state.overETA10&&this.renderOver10Alert()}
        {this.state.onIdle && this.renderIdleAlert()}
        <View style={{flexDirection:'row', alignItems:'flex-end', paddingBottom:10, position:'absolute', left:0, bottom:0, width:'100%', height:80, backgroundColor:'transparent'}}>
          <View style={{backgroundColor:'white', alignItems:'center', justifyContent:'center', position:'absolute', bottom:0, left:0, width:'100%', height:60}}/>
            <TouchableOpacity onPress={()=>Actions.AlertList({data:this.props.currentDelivery})} style={{borderRadius:20, width:40, height:40, backgroundColor:commonColors.brown, alignItems:'center', justifyContent:'center', elevation:5, marginLeft:15}}>
                <Ionicons name={'md-warning'} size={24} color={'white'}/>
            </TouchableOpacity>
            <TouchableOpacity onPress={()=>Actions.DeliveryDetail({data:this.props.currentDelivery})} style={{marginLeft:15, width:40, height:40, borderRadius:20, borderColor:commonColors.green, alignItems:'center', justifyContent:'center', backgroundColor:'rgb(0, 183, 237)', marginLeft:30}}>
                <Ionicons name={'md-list'} size={24} color={'white'}/>
            </TouchableOpacity>
            <TouchableOpacity onPress={()=>this.onPause()} style={{borderRadius:30, width:60, height:60, backgroundColor:'rgb(60, 117, 177)', alignItems:'center', justifyContent:'center', marginLeft:30, elevation:5}}>
                <Ionicons name={this.state.isPause?'ios-play':'ios-pause'} size={24} color={'white'}/>
            </TouchableOpacity>
            <TouchableOpacity onPress={()=>this.done()} style={{marginHorizontal:20, flex:1, height:40, borderRadius:20, alignItems:'center', justifyContent:'center', backgroundColor:commonColors.green, elevation:5}}>
              <Text style={{fontSize:14, color:'white', fontWeight:'bold'}}>{Cache.getLang('label.arrived')}</Text>
            </TouchableOpacity>
          </View>
        {this.renderModal()}
        {this.renderConfirmModal()}
        {
          (!!this.state.shelfLifeAlerts) && (!!dnItems) &&
          this.state.shelfLifeAlerts.map((alert,i)=>{
            if(alert) {
              return(
                <View style={{position:'absolute', left:0, top:170, backgroundColor:'transparent', padding:15, width:'100%',elevation:5}}>
                  <TouchableOpacity onPress={()=>{this.state.shelfLifeAlerts[i]=false, this.setState({shelfLifeAlerts:[...this.state.shelfLifeAlerts]})}} activeOpacity={0.7} style={[styles.alertContainer,{elevation:4, borderWidth:1, borderColor:'grey'}]}>
                    <Ionicons name={'md-warning'} size={18} color={commonColors.brown}/>
                    <Text style={{color:'grey', fontWeight:'bold', marginLeft:10, flex:1}}>{dnItems[i].Item.Name} {Cache.getLang('label.expired_waring')}</Text>
                  </TouchableOpacity>
                </View>
              )
            }             
          })
        }
        {/* <ActionPicker
          options={this.createOptions()}
          isVisible={this.state.isModalVisible}
          onCancelRequest={this.toggleModal} /> */}
        <WaitingModal isWaiting={this.state.isWaiting}/>
      </View>
    );
  }
}

export default connect(
  state => ({
    type: state.common.type,
    status: state.common.status,
    location:state.common.location, 
    heading:state.common.heading,
    upcomingDeliveries: state.common.upcomingDeliveries,
    currentDelivery: state.common.currentDelivery,
    internetConnection: state.common.internetConnection,
    targetSpeed:state.common.targetSpeed,
    shelfLifeAlerts:state.common.shelfLifeAlerts,
    locationMode:state.common.locationMode
  }),
  dispatch => ({
    actions: bindActionCreators({...actions,...session,...actions2}, dispatch)
  })
)(Navigation);

const styles = StyleSheet.create({
  flex: {
    flex: 1,
    backgroundColor:commonColors.theme,
    paddingTop:24,
  },
  buttonContainerUpDown: {
    ...StyleSheet.absoluteFillObject,
    flexDirection: 'row',
    justifyContent: 'center',
  },
  buttonContainerLeftRight: {
    ...StyleSheet.absoluteFillObject,
    flexDirection: 'column',
    justifyContent: 'center',
  },
  button: {
    backgroundColor: 'rgba(100,100,100,0.2)',
    position: 'absolute',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 20,
    height: 50,
    width: 50,
  },
  modalContainer: {
    flex: 1,
    backgroundColor: "rgba(0, 0, 0,0.5)",
    alignItems: 'center',
    justifyContent:'center'
  },
  content:{
    position:'absolute',
    bottom:0,
    left:0,
    width:'100%',
    backgroundColor:'white',
    borderTopLeftRadius: 5,
    borderTopRightRadius: 5,
    paddingHorizontal: 15,
    paddingVertical: 15,
  },
  input: {
    fontSize: 14,
    color: commonColors.title,
    height: 40,
    alignSelf: "stretch",
    // marginHorizontal: 40,
    // borderColor: '#aaa',
    // borderWidth: 1,
    borderRadius: 3,
    marginBottom: 3,
    paddingHorizontal: 10,
    backgroundColor: commonColors.inputColor
  },
  confirmModal:{
    backgroundColor:'white',
    borderRadius:8,
    elevation:5,
    width:300,
  },
  alertContainer:{
    height:50, 
    width:'100%', 
    borderRadius:25, 
    alignItems:'center', 
    flexDirection:'row',
    shadowColor: 'black',
    shadowOffset: {width:3, height:3},
    shadowOpacity: 0.3,
    shadowRadius: 5,
    paddingHorizontal:20,
    backgroundColor:'white'
  },
  markerInfo:{
    flex:1,
    borderRadius:4, 
    backgroundColor:'white',
    shadowColor: 'black',
    shadowOffset: {width:3, height:3},
    shadowOpacity: 0.3,
    elevation:4,
    borderWidth: 2,
    padding:10,
    borderColor:commonColors.theme,
  },
  indicatorContainer: {
    flex: 1,
    backgroundColor: "rgba(0, 0, 0,0.5)",
    alignItems: "center",
    justifyContent: "center"
  },
  indicator: {
    width: 80,
    height: 80,
    borderRadius: 5,
    shadowColor: "black",
    alignItems: "center",
    justifyContent: "center",
    shadowOffset: { width: 2, height: 2 },
    shadowOpacity: 0.4,
    shadowRadius: 3,
    backgroundColor: "white"
  },
});