"use strict";

import React, { PureComponent } from "react";

import {
  StyleSheet,
  View,
  Platform,
  Modal,
  TouchableOpacity,
  Text,
  StatusBar,
  Image
} from "react-native";


import * as commonColors from "../styles/colors";
import * as commonStyles from "../styles/styles";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import * as actions from '../store/common/actions'
import * as session from '../store/session/actions'
import api from "../service/api";
import Cache from "../utils/cache";
import { Ionicons, MaterialCommunityIcons } from "@expo/vector-icons";
import { Actions } from "react-native-router-flux";
import HeaderBar from "../components/header";
import UtilService from '../utils/utils'
import InformationBar from '../components/informationBar'
import * as config from '../config'

function isSameDate(A, B) {
  if (
    A.getDate() == B.getDate() &&
    A.getMonth() == B.getMonth() &&
    A.getFullYear() == B.getFullYear()
  )
    return true;
  return false;
}

class Profile extends PureComponent {
  constructor(props) {
    super(props);
    this.state={
      isRefreshing: false,
      showModal: false,
      username: Cache.currentUser.user.FirstName+' '+Cache.currentUser.user.LastName,
      avatar: {uri:config.SERVICE_FILE_URL+Cache.currentUser.user.Avarta},
      avatarUrl:Cache.currentUser.user.Avatar,
      avatarInstance: {uri:config.SERVICE_FILE_URL+Cache.currentUser.user.Avarta},
    }
  }

  componentDidMount(){
    //console.log('current user on profile ----> >>>',Cache.currentUser)
  }

  onCancel(){
    this.setState({showModal: false})
  }

  onGo(){
    this.setState({showModal: false})
    api.logout()
    Actions.SignIn()
  }
  componentWillReceiveProps(nextProps){
    
  }
  
  renderModal() {
    return (
      <Modal
        visible={this.state.showModal}
        transparent={true}
        onRequestClose={() => {}}
      >
        <View style={styles.modalContainer}>
          <View style={styles.modal}>
            <View style={{flex:1, justifyContent:'center', alignItems:'center'}}>
              <Text style={{fontSize:14, color:'grey',margin:10, textAlign:'center'}}>Do you want to sign out?</Text>
            </View>
            <View style={{height:40, flexDirection:'row', width:'100%', borderBottomRightRadius:5, borderBottomLeftRadius:5, overflow:'hidden', borderTopWidth:1, borderTopColor:'#ccc'}}>
              <TouchableOpacity onPress={()=>this.onCancel()} style={{flex:1, alignItems:'center', justifyContent:'center', backgroundColor:'rgb(249,249,249)'}}>
                <Text style={{color:commonColors.theme, fontSize:13}}>Cancel</Text>
              </TouchableOpacity>
              <TouchableOpacity onPress={()=>this.onGo()} style={{flex:1, alignItems:'center', justifyContent:'center', backgroundColor:commonColors.button}}>
                <Text style={{color:'white', fontSize:13, fontWeight:'bold'}}>Sign Out</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </Modal>
    );
  }
  render() {
    let {avatarUrl,avatarInstance} = this.state
    return (
      <View style={styles.container}>
        <HeaderBar title={"Profile"} 
        />
        <View style={{flexDirection:'row', alignItems:'center', marginTop:20,marginBottom:20}}>
          <TouchableOpacity activeOpacity={0.8} style={{paddingLeft:20}}>
            {
              (avatarUrl==null || avatarUrl=="")&&
              <Image source={commonStyles.noUser} style={{width:50, height:50, borderRadius:25}}/>
            }
            {
              !(avatarUrl==null || avatarUrl=="")&&
              <Image source={avatarInstance} style={{width:50, height:50, borderRadius:25}}/>
            }
          </TouchableOpacity>
          <Text style={styles.username}>{this.state.username}</Text>
        </View>
        <TouchableOpacity style={{backgroundColor:'white'}} onPress={() => Actions.CheckinList()}>
          <View style={{width:'100%',height:70,justifyContent:'center',alignItems:'flex-start'}}>
              <Text style={styles.listTextStyle}>
                Check In/Out Time
              </Text>
          </View>    
          <View style={{height:1,width:'100%',backgroundColor:'rgb(30,30,30)'}}></View>
        </TouchableOpacity>
        <TouchableOpacity style={{backgroundColor:'white'}} onPress={()=>{Actions.ChangePassword()}}>
          <View style={{width:'100%',height:70,justifyContent:'center',alignItems:'flex-start'}}>
          <Text style={styles.listTextStyle}>
                Change Password
              </Text>
          </View>    
          <View style={{height:1,width:'100%',backgroundColor:'rgb(30,30,30)'}}></View>
        </TouchableOpacity>
        <TouchableOpacity style={{backgroundColor:'white'}} onPress={() => {this.setState({showModal:true})}}>
          <View style={{width:'100%',height:70,justifyContent:'center',alignItems:'flex-start'}}>
          <Text style={styles.listTextStyle}>
                Sign Out
              </Text>
          </View>    
          <View style={{height:1,width:'100%',backgroundColor:'rgb(30,30,30)'}}></View>
        </TouchableOpacity>
        <View style={{flex:1,backgroundColor:'white'}}></View>
        {this.renderModal()}
      </View>
    );
  }
}

export default connect(
  state => ({
    actionType: state.common.type,
    status: state.common.status,
    currentDelivery: state.common.currentDelivery,
    completedDeliveries:state.common.completedDeliveries,
    internetConnection: state.common.internetConnection
  }),
  dispatch => ({
    actions: bindActionCreators({...actions,...session}, dispatch)
  })
)(Profile);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop:24, 
    backgroundColor: "rgb(48,48,48)"
  },
  cardContainer: {
    elevation: 3,
    borderRadius: 8,
    backgroundColor: "white",
    // padding: 10,
    borderWidth: 3,
    borderColor: commonColors.blue
  },
  date: {
    fontSize: 13,
    color: "white",
    marginVertical: 5,
    fontWeight: "bold"
  },
  listTextStyle:{
    color:'#777',
    fontSize:20, 
    fontWeight:'bold', 
    marginLeft:20
  },
  username: {
    fontSize: 18,
    fontWeight: "bold",
    color: "white",
    marginLeft:15,
    flex:1,
  },
  modalContainer: {
    flex: 1,
    backgroundColor: "rgba(0, 0, 0,0.5)",
    alignItems: "center",
    justifyContent: "center"
  },
  modal: {
    width: 260,
    height: 160,
    borderRadius: 5,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "white",
    elevation:3,
  }
});
