"use strict";

import React, { PureComponent } from "react";

import {
  StyleSheet,
  View,
  Platform,
  Modal,
  TouchableOpacity,
  Text,
  StatusBar,
  Image,
  Linking,
  Dimensions,
  ActivityIndicator,
  Alert
} from "react-native";


import * as commonColors from "../styles/colors";
import * as commonStyles from "../styles/styles";
import { screenWidth, screenHeight } from "../styles/styles";

import * as actionTypes from "../store/common/actionTypes";

import { bindActionCreators } from "redux";
import { connect } from "react-redux";
// import * as actions from "../redux/actions";
import * as actions from '../store/common/actions'
import * as actions2 from '../redux/actions'
import * as session from '../store/session/actions'

import api from "../service/api";
import Cache from "../utils/cache";
import { Ionicons, MaterialCommunityIcons } from "@expo/vector-icons";
import { Actions } from "react-native-router-flux";
import { SlideButton, SlideDirection } from '../components/sliderButton';
import {WaitingModal} from '../components/waiting'

import HeaderBar from '../components/header'
import {MapView} from 'expo'
const GOOGLE_MAPS_APIKEY = "AIzaSyB1ZUeJmQkzp66r38xEsqY67G0fikYJnC4";
import googleService from '../service/googleService'
import UtilService from "../utils/utils";
import InformationBar from '../components/informationBar'
import moment from 'moment'
import * as CONST from '../components/constant'

const { width, height } = Dimensions.get("window");
const ASPECT_RATIO = width / height;
const LATITUDE = 37.771707;
const LONGITUDE = -122.4053769;
const LATITUDE_DELTA = 0.0922;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;
const blue_north =require('../../public/images/car/Green/North.png')

function getHourMinutes(duration, date){
  let d = new Date(date)
  let dd = new Date(d.getTime()+duration)
  
  return moment(dd).format('hh:mm A')
}

class SetRoute extends PureComponent {
  constructor(props) {
    super(props);

    this.state = {
        items:[
            {title:'Cement', quantity:'100kg'},
            {title:'Steel', quantity:'800kg'}
        ],
        target: {latitude: props.data.DestinationLatitude,longitude:props.data.DestinationLongitude},
        start: this.props.location?this.props.location.coords:{ latitude: 0, longitude: 0},
        coordinates: [],
        latitudeDelta: 0.0922,
        longitudeDelta: 0.0421,
        reason:'create',
        start_address:'',
        end_address:'',
        index:0,
        Routes:[],
        isWaiting: false,
        dnForCheck:null,
        gotRoute:false,
        statusText:'Ready',
        isStarting:false,
        markerIcon:blue_north
    };
    this.checkFinished = false
    // //console.log(props)
  }
  distanceAlertCheck(){
    
  }
  async done() {
    if ( !this.props.internetConnection ) {
      Alert.alert('You can not start to delivery on offline mode')
      return;
    }
    let {DeliveryNoteID, User, DeliveryRouteID}=this.props.data
    let {index, Routes} = this.state
    if ( !Routes || Routes.length<=index) {
      Alert.alert('not found path!')
      return;
    }

    this.setState({isWaiting:true})
    let isReturnRoute = this.props.currentDelivery&&this.props.data.DeliveryNoteID==this.props.currentDelivery.DeliveryNoteID
    //console.log('------setRoute--------- 1')
    // if ( isReturnRoute ) //console.log('*********************')
    this.setState({statusText:"Uploading route data"})
    api.postRoute({
      DeliveryNoteID: DeliveryNoteID,
      UserID:User.UserID,
      StartLat:this.props.location.coords.latitude,
      StartLong:this.props.location.coords.longitude,
      EndLat:isReturnRoute?this.props.data.ReturnWarehouse.Latitude:this.state.target.latitude,
      EndLong:isReturnRoute?this.props.data.ReturnWarehouse.Longitude:this.state.target.longitude,
      PathData:JSON.stringify({index,dist:Routes[index].distance,coordinates:this.state.coordinates, duration:Routes[index].duration}),
      TotalLength:100,
      LastUpdateReason: this.state.reason,
      isReturnRoute: isReturnRoute
    },
    (err, res)=>{
      
      this.setState({statusText:"ETA calculation"})
      Cache.originETA = (new Date()).getTime()+Routes[index].duration*1000;
      if ( err == null ){
        if ( isReturnRoute ) {
          this.props.data.ReturnRoute = res
          this.props.data.ReturnRouteID = res.DeliveryRouteID
          this.props.currentDelivery.ReturnRoute = res
        }else{
          this.props.data.DeliveryRoute = res
          this.props.data.DeliveryRouteID = res.DeliveryRouteID
          this.props.data.OTWStatus = true
        }
       
        //-----------Create Alert------
        this.setState({statusText:"Delivery starting...."})
        api.createAlert({
          Type: isReturnRoute?commonStyles.Returning:commonStyles.START,
          Level: CONST.EVENT_LEVEL,
          Message: this.state.reason,
          ReferenceID: DeliveryNoteID,
          UserID: User.UserID
        }, (err1, res1)=>{
          if(err1){
            Alert.alert('Delivery start error')
            return
          }
          this.updateLocationAndDn(res,DeliveryNoteID,User)
          this.setState({isWaiting:false})
          Actions.Navigation({type:'replace',noTrackModeCheck:true,isFromSetRoute:true})
        })
      }else{
        // //console.log(err)
      }
    })
  }
  async doneForSim() {
    if ( !this.props.internetConnection ) {
      Alert.alert('You can not start to delivery on offline mode')
      return;
    }
    let {DeliveryNoteID, User, DeliveryRouteID}=this.props.data
    let {index, Routes} = this.state
    if ( !Routes || Routes.length<=index) {
      Alert.alert('not found path!')
      return;
    }

    this.setState({isWaiting:true})
    let isReturnRoute = this.props.currentDelivery&&this.props.data.DeliveryNoteID==this.props.currentDelivery.DeliveryNoteID

    // if ( isReturnRoute ) //console.log('*********************')
    api.postRoute({
      DeliveryNoteID: DeliveryNoteID,
      UserID:User.UserID,
      StartLat:this.props.location.coords.latitude,
      StartLong:this.props.location.coords.longitude,
      EndLat:isReturnRoute?this.props.data.ReturnWarehouse.Latitude:this.state.target.latitude,
      EndLong:isReturnRoute?this.props.data.ReturnWarehouse.Longitude:this.state.target.longitude,
      PathData:JSON.stringify({index,dist:Routes[index].distance,coordinates:this.state.coordinates, duration:Routes[index].duration}),
      TotalLength:100,
      LastUpdateReason: this.state.reason,
      isReturnRoute: isReturnRoute
    },
    (err, res)=>{
      
      this.targetSpeed = Routes[index].distance/Routes[index].distance*1000 // m/s
      this.setState({isWaiting:false})
      Cache.originETA = (new Date()).getTime()+Routes[index].duration*1000;
      if ( err == null ){
        //=this.props.actions.updateCurrentDelivery()
        if ( isReturnRoute ) {
          this.props.data.ReturnRoute = res
          this.props.data.ReturnRouteID = res.DeliveryRouteID
          this.props.currentDelivery.ReturnRoute = res
        }else{
          this.props.data.DeliveryRoute = res
          this.props.data.DeliveryRouteID = res.DeliveryRouteID
          this.props.data.OTWStatus = true
        }
        Actions.PolygonCreator({type:'replace',data:this.props.data})
      }else{
        // //console.log(err)
      }
    })
  }
  // updateLocationAndDn(route,DeliveryNoteID,User){
  //     //---------Post Location---------
  //     api.postLocation(
  //     {
  //       DeliveryRouteID: route.DeliveryRouteID,
  //       DeliveryNoteID: DeliveryNoteID,
  //       UserID: User.UserID,
  //       Lat: this.props.location?this.props.location.coords.latitude:'1.03',
  //       Long: this.props.location?this.props.location.coords.longitude:'106'
  //     },
  //     (err, res) => {
  //       // //console.log('---postLocation---', err, res);
  //       if ( Cache.hasInternetConnection ){
  //         this.props.actions.updateCurrentDelivery(this.props.data.DeliveryNoteID)
  //       }else{
  //         this.props.currentDelivery.OTWStatus = true
  //         this.props.actions.manualUpdate(this.props.currentDelivery)
  //       }
  //        //--------Set ETA-------------
  //        api.setETA(DeliveryNoteID, new Date(Cache.originETA), (err, res)=>{
  //         // //console.log('----Set ETA---', err, res)
  //       })
  //     }
  //   );
  // }
  updateLocationAndDn(route,DeliveryNoteID,User){
     
        if ( Cache.hasInternetConnection ){
          this.props.actions.updateCurrentDelivery(this.props.data.DeliveryNoteID)
        }else{
          this.props.currentDelivery.OTWStatus = true
          this.props.actions.manualUpdate(this.props.currentDelivery)
        }
        api.setETA(DeliveryNoteID, new Date(Cache.originETA), (err, res)=>{
        })
      
  }
  getRoutes(){
    let {DestinationLatitude, DestinationLongitude, Warehouse} = this.props.data
    if (this.props.currentDelivery){
      DestinationLatitude = this.props.data.ReturnWarehouse.Latitude
      DestinationLongitude = this.props.data.ReturnWarehouse.Longitude
      this.setState({target:{latitude:DestinationLatitude, longitude:DestinationLongitude}})
    }
    // //console.log('---getRoute', this.props.location.coords.latitude+','+this.props.location.coords.longitude,DestinationLatitude+','+DestinationLongitude)
    //console.log('this.props.location.coords----on getRoutes',this.props.location.coords)
    googleService.getTripPath(
      this.props.location.coords.latitude+','+this.props.location.coords.longitude,
      // Warehouse.Latitude+','+ Warehouse.Longitude,
      DestinationLatitude+','+DestinationLongitude,
      (err, res)=>{
        if ( err == null ){
          // calcDist(res)
          this.setState({Routes:res, start_address:res[0].start_address, end_address:res[0].end_address})
          if ( this.state.coordinates.length == 0 ){
            this.setState({coordinates:res[0].path, index:0})
            let myInterval = setInterval(()=>{
                if (this.mapView==null) return;
                clearInterval(myInterval)
                this.mapView.fitToCoordinates(res[0].path, {
                edgePadding: {
                  right: width / 20,
                  bottom: height / 20,
                  left: width / 20,
                  top: height / 20
                }
              })
            }, 100)
          }
        }else{
          Alert.alert(Cache.getLang("message.no_path"))
        }
        this.setState({gotRoute:true})
      }
    )
  }
  setLocationFromCloud(){
    //console.log('setlocation from mobile----')
    api.getLocationFromCloud(Cache.currentImei,(error,res)=>{
      //console.log('setlocation from mobile----',error,res)
        if(error){
          return
        }
       
        var location={
          coords:{
            latitude:Number(res[0].Latitude),
            longitude:Number(res[0].Longitude),
          },
          speed:Number(res[0].Speed)
        }
        this.props.actions.setLocation(location) 
        this.checkFinished = true
        //console.log('this.props.location...>setlocation from mobile----',this.props.location)         
    })
  }
  setLocationFromMobile(){
      //console.log('this.props.backupLocation on by getting mobile ',this.props.backupLocation)
      this.props.actions.setLocation(this.props.backupLocation)
      this.checkFinished = true
  }
  checkTrackMode(){
    var id
    if(this.props.currentDelivery){
      id = this.props.currentDelivery.DeliveryNoteID
    }
    else{
      id = this.props.data.DeliveryNoteID
    }
    this.setState({statusText:'Check tracking mode'})
    api.getDelivery(id, (err, res)=>{
      if(err){
        return
      }
      console.log('res on setRoute..... xxx',res.Vehicle)
      if(!!!res.Vehicle){
          Alert.alert('Vehicle is not allocated!')
          Actions.pop()
          return
      }
      this.setState({dnForCheck:res})
      if(res.Vehicle.TrackMode==CONST.TRACK_MODE_BYTRACKER){
        var imei = null
        if((!!res.Vehicle) && (!!res.Vehicle.Trackers) &&(!!res.Vehicle.Trackers[0]) &&(!!res.Vehicle.Trackers[0].Tracker)){
          imei = res.Vehicle.Trackers[0].Tracker.ImeiNo
        }
        Cache.currentImei = imei
        this.setState({statusText:'Getting location from cloud'})
        api.getLocationFromCloud(imei,(error,res)=>{
            if(error){
              this.props.actions.setLocationMode(CONST.TRACK_MODE_BYPHONE);
              this.setLocationFromMobile()
              Alert.alert("Can't get location from gps platform")
              //Actions.Main()
              return
            }
            console.log('arrived res on serRoute---',res)
            this.setState({statusText:'Setting location'})
            this.props.actions.setLocationMode(CONST.TRACK_MODE_BYTRACKER);
            //this.setLocationFromCloud()
            var location={
              coords:{
                latitude:Number(res[0].Latitude),
                longitude:Number(res[0].Longitude),
              },
              speed:Number(res[0].Speed)
            }
            this.props.actions.setLocation(location) 
            this.checkFinished = true
            
        })
      }
      else if(res.Vehicle.TrackMode==CONST.TRACK_MODE_BYPHONE){
         Alert.alert("Vehicle is tracked by phone")
         this.props.actions.setLocationMode(CONST.TRACK_MODE_BYPHONE);
         this.setLocationFromMobile()
         
      }
      else{

      }
  
    })

      
  }
  componentDidMount(){
    this.checkFinished = false
    if ( this.props.currentDelivery && this.props.data.DeliveryNoteID!=this.props.currentDelivery.DeliveryNoteID ) {
      Alert.alert(`You still have ongoing delivery order #${this.props.currentDelivery.DeliveryNoteNumber} to be completed, kindly complete the delivery order before starting a new delivery.`)
      Actions.pop()
      return;
    }   
    this.checkTrackMode()
    var timerForGetRoute= setInterval(()=>{
        if(this.checkFinished ){
          //console.log('this.props.location.....when interval',this.props.location)
          this.getRoutes()
          this.setState({start: this.props.location?this.props.location.coords:{ latitude: 0, longitude: 0}})
          clearInterval(timerForGetRoute)
        }        
    },300)
    
  }
  componentWillReceiveProps(nextProps){

    if ( nextProps.actionType == actionTypes.UPDATE_CURRENT_DELIVERY && nextProps.status== actionTypes.SUCCESS){
        //console.log('current delivery is updated on setRoute')
    }

   
  }
  render() {
    if (!this.state.gotRoute || this.state.isWaiting){
       return(
          <View style={{flex:1,alignItems:'center',justifyContent:'center'}}>
              <Text style={{color:'blue',fontWeight:'bold',fontSize:20}}>{this.state.statusText}</Text>
              <ActivityIndicator size={150}/>
          </View>
      )
    }

    let distance = 0, duration = 0;
    if ( this.state.Routes.length > 0){
      distance = this.state.Routes[this.state.index].distance
      duration = this.state.Routes[this.state.index].duration
    }
    let {currentDelivery, internetConnection} = this.props
    return (
      <View style={styles.container}>
        <InformationBar iConnection={internetConnection} currentDelivery={currentDelivery}/>   
        <View style={{elevation:1, position:'absolute',left:0,top:170,width:'100%', flexDirection:'row', alignItems:'center',paddingVertical:8, paddingHorizontal:15, elevation:5}}>
            <View style={{flex:1, flexDirection:'row'}}>
            </View>
            <View style={{alignItems:'center',margin:5,backgroundColor:"rgba(182, 179, 179,0.9)",borderRadius:5}}>
                <Text style={{color:'black',fontWeight:'bold',color:'blue', fontSize:14}}>{' '+((!!this.state.dnForCheck && !!this.state.dnForCheck.Vehicle)?this.state.dnForCheck.Vehicle.LicensePlate:'')} ({(this.props.locationMode==CONST.TRACK_MODE_BYPHONE)?'TRACKING BY PHONE':'TRACKING BY TRACKER'} ) </Text>
            </View>
        </View>
        <View style={{backgroundColor:commonColors.theme, flexDirection:'row', alignItems:'center', height:80, paddingHorizontal:15, elevation:5}}>
          <TouchableOpacity onPress={()=>Actions.pop()}>
            <Ionicons name="ios-arrow-round-back" color={'white'} size={40}/>
          </TouchableOpacity>
          <View style={{flex:1, marginLeft:15, overflow:'hidden'}}>
            <Text style={{fontSize:18,fontWeight:'bold', color:'white'}}>{this.props.data.DeliveryNoteNumber}</Text>
            <Text style={{fontSize:12, color:'white'}}>{this.props.data.DestinationAddress}</Text>
          </View>
          {/* <TouchableOpacity style={{marginRight:30}} onPress={()=>Actions.PolygonCreator({data:this.props.data})}>
              <Ionicons name={'md-pin'} size={24} color={'white'}/>
          </TouchableOpacity> */}
          <TouchableOpacity onPress={()=>Actions.Chat({channel:this.props.data })}>
              <Ionicons name={'md-chatboxes'} size={36} color={'white'}/>
              {
                  (this.props.unreadCnt >0) &&
                  <View style={{position:'absolute',right:0,top:0}}>
                    <View style={{right:-7,top:-5,width:18,height:18,backgroundColor:'red',borderRadius:10,position:'absolute'}}></View>
                    <Text style={{fontWeight:'bold',fontSize:12,right:-7,top:-5,width:20,height:20,color:'white',borderRadius:10,position:'absolute',textAlign:'center'}}>{this.props.unreadCnt}</Text>
                  </View>                  
              }
              
          </TouchableOpacity>
        </View>
        <View style={{width:'100%', backgroundColor:commonColors.theme, elevation:5}}>

            <View style={{width:'100%', backgroundColor:'white', flexDirection:'row', alignItems:'center', padding:15}}>
                <View style={{flex:1, flexDirection:'row'}}>
                    <Text style={{color:'black', fontSize:18, fontWeight:'bold'}}>{(distance/1000).toFixed(1)}</Text>
                    <Text style={{color:'grey', fontSize:18}}> km,</Text>
                    <Text style={{color:'black', fontSize:18, fontWeight:'bold'}}> {(duration/3600).toFixed(0)}</Text>
                    <Text style={{color:'grey', fontSize:18}}> hr</Text>
                    <Text style={{color:'black', fontSize:18, fontWeight:'bold'}}> {((duration%3600)/60).toFixed(0)}</Text>
                    <Text style={{color:'grey', fontSize:18}}> min</Text>

                </View>
                <TouchableOpacity onPress={()=>Actions.SelectRoute({data:this.props.data, routes:this.state.Routes, index:this.state.index, update:(index)=>{
                  this.setState({coordinates:this.state.Routes[index].path, index})
                }})}>
                    <Image source={commonStyles.path_theme} style={{width:40, height:40, resizeMode:'contain'}}/>
                </TouchableOpacity>
                <View style={{width:1, height:'100%', backgroundColor:'#ccc', marginHorizontal:15}}/>
                <View style={{alignItems:'center'}}>
                    <Text style={{color:'grey', fontSize:12}}>ETA</Text>
                    <Text style={{fontWeight:'bold', color:'black', fontSize:18}}>{getHourMinutes(duration*1000, new Date())}</Text>
                </View>
            </View>
        </View>
        <MapView
          ref={c => this.mapView = c}
          // minZoomLevel={9}
          // showsCompass={true}
          // showsUserLocation={true}
          initialRegion={{
            ...this.state.start,
            latitudeDelta: this.state.latitudeDelta,
            longitudeDelta: this.state.longitudeDelta,
          }}
          style={{ flex: 1 }}
        >
            <MapView.Marker
              coordinate={this.state.start}
            >
              <View style={{width:20, height:20, borderRadius:10, backgroundColor:'rgb(66,192,147)', alignItems:'center', justifyContent:'center'}}>
                <Text style={{fontSize:12, color:'white', fontWeight:'bold'}}>1</Text>
              </View>
            </MapView.Marker>
            {!!this.props.data.DestinationLatitude && !!this.props.data.DestinationLongitude&&<MapView.Marker
              coordinate={this.state.target}
            >
              <View style={{}}>
                <Ionicons name="ios-home" size={40} color={'rgb(245,106,85)'}/>
                {/* <Text style={{position:'absolute', left:7, top:8, backgroundColor:'rgb(245,106,85)',fontSize:12, color:'white', fontWeight:'bold'}}>2</Text> */}
              </View>
            </MapView.Marker>}
            {
              (!!this.props.location)&&(!!this.props.location.coords)&&
              <MapView.Marker
                coordinate={this.props.location.coords}
              >
                <Image source={this.state.markerIcon} style={{width:60,height:60}}/>
                {/* <MaterialCommunityIcons name="truck" size={24} color={commonColors.brown} /> */}
              </MapView.Marker>
            }
          {this.state.coordinates.length>1&&<MapView.Polyline
                        coordinates={this.state.coordinates}
                        strokeColor={commonColors.theme}
                        fillColor={commonColors.theme}
                        strokeWidth={5}
                    />}
        </MapView>
        <View style={{position:'absolute', left:0, bottom:0, width:'100%', height:90, padding:15}}>
            <TouchableOpacity onPress={()=>this.done()} activeOpacity={0.8} style={{backgroundColor:commonColors.button, height:60, width:'100%', borderRadius:30, alignItems:'center', justifyContent:'center', elevation:3}}>
              <Text style={{fontSize:16, color:'white', fontWeight:'bold'}}>{this.props.currentDelivery!=null&&this.props.currentDelivery.IsSignOff?Cache.getLang('label.return_back'):Cache.getLang('label.start_delivery')}</Text>
            </TouchableOpacity>
        </View>
        {/* <View style={{position:'absolute', left:0, bottom:100, width:'100%', height:90, padding:15}}>
            <TouchableOpacity onPress={()=>this.doneForSim()} activeOpacity={0.8} style={{backgroundColor:commonColors.button, height:60, width:'100%', borderRadius:30, alignItems:'center', justifyContent:'center', elevation:3}}>
              <Text style={{fontSize:16, color:'white', fontWeight:'bold'}}>Simulation</Text>
            </TouchableOpacity>
        </View> */}
        {/* <WaitingModal isWaiting={this.state.isWaiting}/> */}
      </View>
    );
  }
}

export default connect(
  state => ({
    status: state.common.status,
    location:state.common.location, 
    heading:state.common.heading,
    currentDelivery:state.common.currentDelivery,
    locationMode:state.common.locationMode,
    internetConnection:state.common.internetConnection,
    backupLocation:state.common.backupLocation,
    unreadCnt:state.chat.unreadCnt
  }),
  dispatch => ({
    actions: bindActionCreators({...actions,...session,...actions2}, dispatch)
  })
)(SetRoute);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop:24,
    backgroundColor: commonColors.theme
  },

});
