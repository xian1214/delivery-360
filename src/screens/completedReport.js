"use strict";

import React, { PureComponent } from "react";

import {
  StyleSheet,
  View,
  Platform,
  Modal,
  TouchableOpacity,
  Text,
  StatusBar,
  Image,
  TextInput,
  ScrollView
} from "react-native";


import * as commonColors from "../styles/colors";
import * as commonStyles from "../styles/styles";
import { screenWidth, screenHeight } from "../styles/styles";

import * as actionTypes from "../store/common/actionTypes";

import { bindActionCreators } from "redux";
import { connect } from "react-redux";
// import * as actions from "../redux/actions";
import * as actions from '../store/common/actions'
import * as session from '../store/session/actions'

import api from "../service/api";
import Cache from "../utils/cache";
import { Ionicons, MaterialCommunityIcons } from "@expo/vector-icons";
import { Actions } from "react-native-router-flux";
import HeaderBar from '../components/header'
import async from 'async'
import UtilService from '../utils/utils'
import { WaitingModal } from "../components/waiting";
import * as config from '../config'
import * as CONST from '../components/constant'

class DeliveryReport extends PureComponent {
  constructor(props) {
    super(props);

    this.state = {
        items:[
            {title:'Cement', quantity:'100kg', delivered:'100kg'},
            {title:'Steel', quantity:'800kg', delivered:'800kg'}
        ],
        pictures:[
            {image:null, area:'', comment:''},
        ],
        isConfirmed: false,
        showSignModal: false,
        signatureUri:null,
        showConfirmModal: false,
        qrScanData: null,
        subRequests:this.props.data.DeliveryNoteItems,
        startUnloadingTime: this.props.data.UnloadingTime?UtilService.getDateTime(this.props.data.UnloadingTime):null,
        doneUnloadingTime: this.props.data.UnloadedTime?UtilService.getDateTime(this.props.data.UnloadedTime):null,
        isWaiting: false,
    };
  }

  componentDidMount(){
      if ( !this.props.internetConnection ){
          alert('You can not see report in offline mode!')
          Actions.pop()
          return;
      }
      this.setState({isWaiting: true})
    
    api.getDelivery(this.props.data.DeliveryNoteID, (err,res)=>{
      if ( err == null ){
        // //console.log('photos', res)
        if(this.state.subRequests.length==0 || this.state.subRequests==null){
            this.setState({isWaiting:false})
            return;
        }
        this.state.subRequests.map((request, index)=>{
            api.getCheckResultsByDnItem(request.DeliveryNoteItemID, (error,res)=>{
                this.setState({isWaiting:false})
                if(error==null){
                    request.checkResults = res
                    //console.log('arrived chkRes:',res)
                }
            })
        })
        this.setState({pictures:res.DeliveryReportPhotos, signatureUri:this.props.data.Signature})
      }else{
          this.setState({isWaiting:false})
      }
    })
 } 
  
  showAll(){
      if ( this.state.showIndex == -2 ){
          this.setState({showIndex:-1})
      }else{
          this.setState({showIndex:-2})
      }
  }

  showIndex(index){
    if ( this.state.showIndex == index ){
        this.setState({showIndex:-1})
    }else{
        this.setState({showIndex:index})
    }
  }

  renderDetail(index){
    let {checkResults} = this.state.subRequests[index]
    if ( !checkResults ) return null
      return(
          <View>
              {checkResults.map((result, index)=>{
                  return(
                      <View key={index} style={{paddingHorizontal:20, marginTop:10}}>
                        <Text style={{color:'#ccc', fontSize:13}}>{result.CheckListItem.Name}</Text>
                        <Text style={{color:'grey', fontSize:14, marginTop:5}}>{result.Value}</Text>
                        <ScrollView horizontal={true}>
                            {result.Images!=null&&result.Images!=''&&result.Images.split(',').map((item, idx)=>{
                                return(
                                    <TouchableOpacity key={idx} onPress={()=>Actions.ShowImage({uri:item})}>
                                        <Image source={{uri:item}} style={{width:60, height:60, marginTop:10, marginRight:10}}/>
                                    </TouchableOpacity>
                                )
                            })}
                            
                        </ScrollView>
                        <View style={{width:'100%', height:1, backgroundColor:'#ccc', marginTop:10}}/>
                      </View>
                  )
              })}
          </View>
      )
  }

  render() {
      let {DeliveryNoteNumber, IssueTime} = this.props.data
    return (
      <View style={styles.container}>
        <HeaderBar title={DeliveryNoteNumber}/>
        <ScrollView >
        <View>
            
            <View style={{flexDirection:'row', alignItems:'center', height:40, width:'100%', paddingHorizontal:20, backgroundColor:'rgb(228,228,233)', elevation:4}}>
                <Text style={{color:'grey', flex:1}}>Item</Text>
                <Text style={{color:'grey', width:100, textAlign:'right'}}>Delivered Qty</Text>
                <TouchableOpacity onPress={()=>this.showAll()} style={{marginLeft:80}}>
                    <Ionicons name={"ios-arrow-down"} size={24} color={'rgb(80,118,177)'} style={{width:20}}/>
                </TouchableOpacity>
            </View> 
            { this.props.data.DeliveryNoteType!= CONST.DELIVERY_TYPE_QAC && this.state.subRequests.map((item, index)=>{
                return(
                    <View key={index} style={{width:'100%'}}>
                        <View style={{flexDirection:'row', alignItems:'center', width:'100%', paddingHorizontal:20, paddingVertical:8, backgroundColor:index%2==0?'white':'#eee', elevation:4}}>
                            <View style={{flex:1}}>
                                <View style={{flexDirection:'row', alignItems:'center'}}>
                                    <Text style={{color:'black', flex:1, fontWeight:'bold'}}>{this.props.data.DeliveryNoteType==2?item.Note.split(',')[0]:item.Item.Name}</Text>
                                    <Text style={{color:'grey', width:100}}>{this.props.data.DeliveryNoteType==2?1:item.DeliveredQuantity} {item.Item.Uom.Name}</Text>
                                </View>
                                
                            </View>
                            <TouchableOpacity onPress={()=>this.showIndex(index)}>
                                <Ionicons name={(this.state.showIndex == index || this.state.showIndex==-2)?"ios-arrow-up":"ios-arrow-down"} size={24} color={'rgb(80,118,177)'} style={{width:20}}/>
                            </TouchableOpacity>
                        </View>
                        {(this.state.showIndex == index || this.state.showIndex==-2)&&this.renderDetail(index)}
                    </View>
                )
            })}
             {this.props.data.DeliveryNoteType == CONST.DELIVERY_TYPE_QAC && this.state.subRequests.map((item, index)=>{
                return(
                    <View key={index} style={{width:'100%'}}>
                        <View style={{flexDirection:'row', alignItems:'center', width:'100%', paddingHorizontal:20, paddingVertical:8, backgroundColor:index%2==0?'white':'#eee', elevation:4}}>
                            <View style={{flex:1}}>
                                <View style={{flexDirection:'row', alignItems:'center'}}>
                                    <Text style={{color:'black', flex:1, fontWeight:'bold'}}>{this.props.data.DeliveryNoteType==2?item.Note.split(',')[0]:item.Item.Name}</Text>
                                    <Text style={{color:'grey', width:100}}>{this.props.data.DeliveryNoteType==2?1:item.PlannedQuantity}</Text>
                                </View>
                                
                            </View>
                            <TouchableOpacity onPress={()=>this.showIndex(index)}>
                                <Ionicons name={(this.state.showIndex == index || this.state.showIndex==-2)?"ios-arrow-up":"ios-arrow-down"} size={24} color={'rgb(80,118,177)'} style={{width:20}}/>
                            </TouchableOpacity>
                        </View>
                        {(this.state.showIndex == index || this.state.showIndex==-2)&&this.renderDetail(index)}
                    </View>
                )
            })}
            </View>
            <View style={{width:'100%', backgroundColor:'rgb(228,228,233)', paddingVertical:8, paddingHorizontal:15, elevation:4, marginTop:10}}>
                <Text style={{color:'grey', fontSize:13}}>Remarks</Text>
            </View>
            <View style={{backgroundColor:'white', elevation:4}}>
                {this.state.pictures.map((item, index)=>{
                    return(
                        <View key={index} style={{flexDirection:'row', paddingHorizontal:20, paddingVertical:10}}>
                            {item.AfterPhoto==null&&<View style={{borderColor:'rgb(0,185,237)', borderWidth:2, alignItems:'center', justifyContent:'center', width:60, height:60}}>
                                {/* <Ionicons name={'ios-camera'} color={commonColors.theme} size={30}/> */}
                                <Text style={{fontSize:8,color:'#333'}}>No Photo</Text>
                            </View>}
                            {item.AfterPhoto!=null&&<TouchableOpacity onPress={()=>Actions.ShowImage({uri:config.SERVICE_FILE_URL+item.AfterPhoto})} style={{width:60, height:60}}>
                                <Image source={{uri:config.SERVICE_FILE_URL+item.AfterPhoto}} style={{flex:1, resizeMode:'cover'}}/>
                            </TouchableOpacity>}
                            {/* <Text style={{flex:1}}>{'afterphoto-url:'+config.SERVICE_FILE_URL+'/'+item.AfterPhoto}</Text> */}
                            <View style={{marginLeft:15, flex:1}}>
                                
                                <View style={{marginTop:10, paddingRight:20}}>
                                    <View style={{flexDirection:'row', alignItems:'flex-end'}}>
                                        <Text style={{fontSize:13, color:'grey'}}>Area</Text>
                                        <Text style={{fontSize:15, color:'black', marginLeft:10}}>{item.Area}</Text>
                                        <View style={{flex:1}}></View>
                                        {
                                            !!item.DestinationLatitude && !!item.DestinationLongitude &&
                                            <TouchableOpacity onPress={()=> Actions.PlacePicker({destination:{latitude:item.DestinationLatitude||0, longitude:item.DestinationLongitude||0}})} 
                                                style={{width:40,justifyContent:'center',alignItems:'flex-end'}}>
                                                <Ionicons name={'md-pin'} size={30} color={'rgb(0,185,237)'}/>
                                            </TouchableOpacity>
                                        }
                                        
                                    </View>
                                    <Text style={{fontSize:15, color:'black', marginTop:10}}>{item.Remark}</Text>
                                </View>
                            </View>
                        </View>
                    )
                })}
            </View>
                        
            <Text style={{color:'grey',fontWeight:'bold', fontSize:15, marginLeft:15, marginTop:20}}>
                I here by agree with the contents of the delivery report above
            </Text>
            {this.state.signatureUri&&<Image source={{uri:config.SERVICE_FILE_URL+this.state.signatureUri}} style={{width:'100%', height:250, resizeMode:'contain'}}/>}
            {this.state.qrScanData&&<View style={{flexDirection:'row', justifyContent:'center'}}>
                <Text style={{color:'#ccc', fontSize:13}}>Acceptor:</Text>
                <Image source={{uri:this.state.acceptorUri}} style={{width:30, height:30, borderRadius:15, margin:8}}/>
                <Text style={{color:'#333', fontWeight:'bold', fontSize:15}}>{this.state.acceptorName}</Text>
            </View>}
            <View style={{height:20}}/>
        </ScrollView>
        <WaitingModal isWaiting={this.state.isWaiting}/>
      </View>
    );
  }
}

export default connect(
  state => ({
    internetConnection:state.common.internetConnection
  }),
  dispatch => ({
    actions: bindActionCreators({...actions,...session}, dispatch)
  })
)(DeliveryReport);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop:24,
    backgroundColor: 'white'
  },
  input: {
    fontSize: 14,
    color: commonColors.title,
    height: 36,
    borderRadius: 3,
    marginBottom: 3,
    paddingHorizontal: 10,
    backgroundColor: commonColors.inputColor
  },
  modalContainer: {
    flex: 1,
    backgroundColor: "rgba(0, 0, 0,0.5)",
    alignItems: "center",
    justifyContent: "center"
  },
  modal: {
    width:360,
    borderRadius: 5,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "white",
    elevation:3,
  },
});
