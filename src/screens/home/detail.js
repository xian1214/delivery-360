import React, { Component } from "react";
import { StyleSheet, View, TouchableOpacity, Text, ScrollView, Image, Switch } from "react-native";

import {bindActionCreators} from 'redux'
import {connect} from 'react-redux'
import * as actions from '../../redux/actions'

import NavTitleBar from '../../components/navTitle'
import * as commonColors from "../../styles/colors";
import * as commonStyles from "../../styles/styles";
import { screenWidth, screenHeight } from "../../styles/styles";
import Ionicons from "@expo/vector-icons/Ionicons";
import { Actions } from "react-native-router-flux";
import {MapView} from 'expo'

const Content=({icon, title, content})=>(
  <View style={{flexDirection:'row', alignItems:'center', marginTop:6}}>
    <Ionicons name={icon} size={16} color={'grey'} style={{width:20}}/>
    <Text numberOfLines={1} style={{fontSize:14, color:'#333', fontWeight:'bold'}}>{title} : </Text>
    <Text numberOfLines={1} style={{fontSize:14, color:'#333'}}>{content}</Text>
  </View>
)

let cars=[require('../../../public/images/car_green.png'), require('../../../public/images/car_grey.png'), require('../../../public/images/car_blue.png'), require('../../../public/images/car_blue.png')]
class DeliveryDetail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      delivery:props.delivery,
    };
    // //console.log(this.state.delivery.DeliveryRoute)
  }

  changeArrived(){
    this.state.delivery.isArrived = !this.state.delivery.isArrived
    this.forceUpdate()
  }

  changeOTW(){
    this.state.delivery.isOTW = !this.state.delivery.isOTW
    this.forceUpdate()
  }

  render() {
    let item = this.state.delivery
    let STATUS=['Fully Available', 'Issued', 'Completed', 'Cancelled']
    return (
      <View style={styles.container}>
        <NavTitleBar
          buttons={commonStyles.NavBackButton}
          onBack={()=>Actions.pop()}
          title={"Delivery in Detail"}
        />
        <View activeOpacity={0.8} onPress={()=>{Actions.Detail({delivery:item})}} style={{backgroundColor:'white', width:'100%', height:230, padding:15, paddingBottom:0, borderBottomColor:'#ccc', borderBottomWidth:1}}>
        <View style={{flexDirection:'row'}}>
                  <View style={{flex:1}}>
                    <Text style={{fontSize:13, color:'grey'}}>#{item.DeliveryNoteNumber}</Text>
                    <Image source={cars[0]} style={{width:'80%', flex:1, resizeMode:'contain'}}/>
                    <Text style={{fontSize:13, color:'#333'}}>{STATUS[item.status]}</Text>
                  </View>
                  <View style={{height:'100%', width:1, backgroundColor:'#aaa'}}/>
                  <View style={{flex:2, marginLeft:10}}>
                    <Content icon={'md-person'} title={'Customer'} content={item.Customer?item.Customer.Name:''}/>
                    <Content icon={'md-call'} title={'Phone'} content={item.Phone}/>
                    <Content icon={'md-phone-portrait'} title={'Mobile'} content={item.Mobile}/>
                    <Content icon={'md-bicycle'} title={'Delivery'} content={item.User!=null?item.User.FirstName+' '+item.User.LastName:'---'}/>
                  </View>
                </View>
                <View style={{height:5}}/>
                <Content icon={'md-pin'} title={'Address'} content={item.Customer?item.Customer.City:''}/>
                <Content icon={'md-clock'} title={'Request'} content={item.RequestTime}/>
                <View style={{marginTop:20, flexDirection:'row'}}>
                  <View style={{flexDirection:'row', alignItems:'center', flex:1}}>
                    <Text style={{fontSize:13,color:'#333', fontWeight:'bold'}}>On the way</Text>
                    <Switch value={item.OTWStatus} onValueChange={()=>this.changeOTW()}
                      style={{marginLeft:10}}/>
                  </View>
                  <View style={{flexDirection:'row', alignItems:'center', flex:1}}>
                    <Text style={{fontSize:13,color:'#333', fontWeight:'bold'}}>Arrived</Text>
                    <Switch value={item.ArrivalStatus} style={{marginLeft:10}} onValueChange={()=>this.changeArrived()}/>
                  </View>
                </View>
              </View>

          <ScrollView style={{marginTop:10, backgroundColor:'white'}}>
            {item.DeliveryNoteItems.map((subItem, index)=>{
              return(
                <View key={index} style={{flexDirection:'row', height:50, width:'100%', alignItems:'center', backgroundColor:'white', paddingHorizontal:15, borderBottomWidth:1, borderBottomColor:'#ccc'}}>
                  <Text style={{flex:1, color:'#333', fontSize:14}}>{subItem.Item?subItem.Item.Name:''}</Text>
                  <Text style={{color:'grey', fontSize:14}}>{subItem.PlannedQuantity}</Text>
                </View>
              )
            })}
            <TouchableOpacity onPress={()=>Actions.EditRouter({
              DeliveryNoteID:this.state.delivery.DeliveryNoteID,
              UserID:this.state.delivery.User?this.state.delivery.User.UserID:'',
              DeliveryRoute: this.state.delivery.DeliveryRoute,
              update:(route)=>this.state.delivery.DeliveryRoute=route
              })} style={{flexDirection:'row', alignItems:'center', justifyContent:'center', backgroundColor:commonColors.theme, width:80, height:34, margin:10}}>
              <Ionicons name="md-create" size={15} color={'white'}/>
              <Text style={{fontSize:11, color:'white', fontWeight:'bold', marginLeft:8}}>EDIT</Text>
            </TouchableOpacity>
            <MapView initialRegion={{
              latitude: 37,
              longitude: -122,
              latitudeDelta: 0.1,
              longitudeDelta: 0.04
              }}
              style={{width:'100%', height:200}}
            />
            
          </ScrollView>
          <TouchableOpacity activeOpacity={0.8} onPress={()=>Actions.CheckOut()} style={{height:50, width:'100%', backgroundColor:commonColors.theme, alignItems:'center', justifyContent:'center'}}>
            <Text style={{color:'white', fontSize:13, fontWeight:'bold'}}>
              RECEIPT(SIGN)
            </Text>
          </TouchableOpacity>
      </View>
    );
  }
}

export default connect(
  state => ({}),
  dispatch => ({
    actions: bindActionCreators(actions, dispatch)
  })
)(DeliveryDetail);

const styles = StyleSheet.create({
  container: {
    flex: 1
  }
});
