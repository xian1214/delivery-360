import React, { Component } from "react";
import { StyleSheet, View, TouchableOpacity, Text, ScrollView, Image } from "react-native";

import {bindActionCreators} from 'redux'
import {connect} from 'react-redux'
import * as actions from '../../redux/actions'

import NavTitleBar from '../../components/navTitle'
import * as commonColors from "../../styles/colors";
import * as commonStyles from "../../styles/styles";
import { screenWidth, screenHeight } from "../../styles/styles";
import Ionicons from "@expo/vector-icons/Ionicons";

// import SignatureCapture from 'react-native-signature-capture'

class SignTab extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    return (
      <View style={styles.container}>
      {/* <SignatureCapture
        style={styles.signature}
        ref="sign"
        onSaveEvent={this._onSaveEvent}
        onDragEvent={this._onDragEvent}
        saveImageFileInExtStorage={false}
        showNativeButtons={false}
        showTitleLabel={false}
        viewMode={"portrait"} /> */}
        <View style={{height:300, width:'100%', padding:10}}>
            <View style={{width:'100%', height:'100%', borderColor:'black', borderWidth:1}}/>
        </View>
        <View style={{width:'100%', padding:10}}>
        <TouchableOpacity activeOpacity={0.8} onPress={()=>{}} style={{height:50, width:'100%', backgroundColor:commonColors.theme, alignItems:'center', justifyContent:'center'}}>
            <Text style={{color:'white', fontSize:13, fontWeight:'bold'}}>
              CONFIRM
            </Text>
          </TouchableOpacity>
          </View>
      </View>
    );
  }
}

export default connect(
  state => ({}),
  dispatch => ({
    actions: bindActionCreators(actions, dispatch)
  })
)(SignTab);

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  signature: {
    flex:1,
    borderColor: '#000033',
    borderWidth: 1,
    margin:20,
    },
});
