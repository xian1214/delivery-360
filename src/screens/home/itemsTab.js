import React, { Component } from "react";
import { StyleSheet, View, TouchableOpacity, Text, ScrollView, Image } from "react-native";

import {bindActionCreators} from 'redux'
import {connect} from 'react-redux'
import * as actions from '../../redux/actions'

import NavTitleBar from '../../components/navTitle'
import * as commonColors from "../../styles/colors";
import * as commonStyles from "../../styles/styles";
import { screenWidth, screenHeight } from "../../styles/styles";
import Ionicons from "@expo/vector-icons/Ionicons";

let cars=[require('../../../public/images/car_green.png'), require('../../../public/images/car_grey.png'), require('../../../public/images/car_blue.png'), require('../../../public/images/car_blue.png')]
class ItemTab extends Component {
  constructor(props) {
    super(props);
    this.state = {
        items:[
            {name:'Keyboard', count:4},
            {name:'Mouse', count:3},
            {name:'Monitor', count:44},
            {name:'Laptop', count:1},
    
          ]
    };
  }

  render() {
    return (
      <View style={styles.container}>
        <ScrollView style={{marginTop:10, backgroundColor:'white'}}>
            {this.state.items.map((item, index)=>{
              return(
                <View key={index} style={{flexDirection:'row', height:50, width:'100%', alignItems:'center', backgroundColor:'white', paddingHorizontal:15, borderBottomWidth:1, borderBottomColor:'#ccc'}}>
                  <Text style={{flex:1, color:'#333', fontSize:14}}>{item.name}</Text>
                  <Text style={{color:'grey', fontSize:14}}>{item.count}</Text>
                </View>
              )
            })}
          </ScrollView>
      </View>
    );
  }
}

export default connect(
  state => ({}),
  dispatch => ({
    actions: bindActionCreators(actions, dispatch)
  })
)(ItemTab);

const styles = StyleSheet.create({
  container: {
    flex: 1
  }
});
