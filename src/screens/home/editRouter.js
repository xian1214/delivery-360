import React, { Component } from "react";
import {
  StyleSheet,
  View,
  TouchableOpacity,
  Text,
  ScrollView,
  Image,
  Dimensions,
  Alert,
} from "react-native";

import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import * as actions from "../../redux/actions";
import api from '../..//service/api'

import NavTitleBar from "../../components/navTitle";
import * as commonColors from "../../styles/colors";
import * as commonStyles from "../../styles/styles";
import { screenWidth, screenHeight } from "../../styles/styles";
import Ionicons from "@expo/vector-icons/Ionicons";
import { Actions } from "react-native-router-flux";
import { MapView } from "expo";
// import MapViewDirections from "react-native-maps-directions";
const GOOGLE_MAPS_APIKEY = "AIzaSyB1ZUeJmQkzp66r38xEsqY67G0fikYJnC4";

const { width, height } = Dimensions.get("window");
const ASPECT_RATIO = width / height;
const LATITUDE = 37.771707;
const LONGITUDE = -122.4053769;
const LATITUDE_DELTA = 0.0922;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;

class ItemTab extends Component {
  constructor(props) {
    super(props);
    this.state = {
      coordinates: props.DeliveryRoute?JSON.parse(props.DeliveryRoute.PathData):[]
    };
  }

  goBack() {
    Actions.pop();
  }

  reset() {
    this.setState({coordinates:[]})
  }

  done() {
    if ( this.state.coordinates.length < 2 ){
      Alert.alert('Select at least 2 locations!')
      return;
    }
    let {DeliveryNoteID, UserID}=this.props
    api.postRoute({
      DeliveryNoteID: DeliveryNoteID,
      UserID:UserID,
      StartLat:this.state.coordinates[0].latitude,
      StartLong:this.state.coordinates[0].longitude,
      EndLat:this.state.coordinates[1].latitude,
      EndLong:this.state.coordinates[1].longitude,
      PathData:JSON.stringify(this.state.coordinates),
      TotalLength:100,
    },
    (err, res)=>{
      // //console.log(err, res)
      if ( err == null ){
        this.props.update(res)
        Actions.pop();
      }
    })
  }

  render() {
    return (
      <View style={styles.container}>
        <NavTitleBar
          buttons={commonStyles.NavBackButton}
          onBack={this.goBack}
          title={"Edit Router"}
          rightText={"RESET"}
          rightCallback={() => this.reset()}
        />

        <MapView
          ref={c => this.mapView = c}
          initialRegion={{
            latitude: LATITUDE,
            longitude: LONGITUDE,
            latitudeDelta: LATITUDE_DELTA,
            longitudeDelta: LONGITUDE_DELTA
          }}
          onPress={e => {
            this.setState({
              coordinates: [...this.state.coordinates, e.nativeEvent.coordinate]
            });
          }}
          tappable={true}
          style={{ flex: 1 }}
        >
          {this.state.coordinates.map((coordinate, index) => (
            <MapView.Marker
              key={`coordinate_${index}`}
              coordinate={coordinate}
            />
          ))}
          {this.state.coordinates.length >= 2 && (
            // <MapViewDirections
            //   origin={this.state.coordinates[0]}
            //   waypoints={
            //     this.state.coordinates.length > 2
            //       ? this.state.coordinates.slice(1, -1)
            //       : null
            //   }
            //   destination={
            //     this.state.coordinates[this.state.coordinates.length - 1]
            //   }
            //   apikey={GOOGLE_MAPS_APIKEY}
            //   strokeWidth={3}
            //   strokeColor="hotpink"
            //   onStart={params => {}}
            //   onReady={result => {
            //     this.mapView.fitToCoordinates(result.coordinates, {
            //       edgePadding: {
            //         right: width / 20,
            //         bottom: height / 20,
            //         left: width / 20,
            //         top: height / 20
            //       }
            //     });
            //   }}
            //   onError={errorMessage => {
            //     // //console.log('GOT AN ERROR');
            //   }}
            // />
          )}
        </MapView>
        <TouchableOpacity
          activeOpacity={0.8}
          onPress={() => this.done()}
          style={styles.doneButton}
        >
          <Text style={{ color: "white", fontSize: 13, fontWeight: "bold" }}>
            DONE
          </Text>
        </TouchableOpacity>
      </View>
    );
  }
}

export default connect(
  state => ({}),
  dispatch => ({
    actions: bindActionCreators(actions, dispatch)
  })
)(ItemTab);

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  doneButton: {
    height: 50,
    width: "100%",
    backgroundColor: commonColors.theme,
    alignItems: "center",
    justifyContent: "center"
  }
});
