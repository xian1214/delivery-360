import React, { Component } from "react";
import { StyleSheet, View, TouchableOpacity, Text, ScrollView, Image } from "react-native";

import {bindActionCreators} from 'redux'
import {connect} from 'react-redux'
import * as actions from '../../redux/actions'

import NavTitleBar from '../../components/navTitle'
import * as commonColors from "../../styles/colors";
import * as commonStyles from "../../styles/styles";
import { screenWidth, screenHeight } from "../../styles/styles";
import Ionicons from "@expo/vector-icons/Ionicons";
import {MapView} from 'expo'

let cars=[require('../../../public/images/car_green.png'), require('../../../public/images/car_grey.png'), require('../../../public/images/car_blue.png'), require('../../../public/images/car_blue.png')]
class SummaryTab extends Component {
  constructor(props) {
    super(props);
    this.state = {
        items:[
            {name:'Keyboard', count:4},
            {name:'Mouse', count:3},
            {name:'Monitor', count:44},
            {name:'Laptop', count:1},
    
          ]
    };
  }

  render() {
    return (
      <View style={styles.container}>
        <ScrollView style={{marginTop:10, backgroundColor:'white'}}>
            {this.state.items.map((item, index)=>{
              return(
                <View key={index} style={{flexDirection:'row', height:50, width:'100%', alignItems:'center', backgroundColor:'white', paddingHorizontal:15, borderBottomWidth:1, borderBottomColor:'#ccc'}}>
                  <Text style={{flex:1, color:'#333', fontSize:14}}>{item.name}</Text>
                  <Text style={{color:'grey', fontSize:14}}>{item.count}</Text>
                </View>
              )
            })}
            {this.state.items.map((item, index)=>(
                <View key={index} style={{flexDirection:'row', padding:15, marginTop:0, backgroundColor:'white', borderWidth:1, borderColor:'#ccc', height:200}}>
                    <Image source={require('../../../public/images/bg5.jpg')} style={{flex:1, height:'100%'}}/>
                    <View style={{width:15}}/>
                    <Image source={require('../../../public/images/bg7.jpg')} style={{flex:1, height:'100%'}}/>
                </View>
            ))}
            <MapView initialRegion={{
            latitude: 37,
            longitude: -122,
            latitudeDelta: 0.1,
            longitudeDelta: 0.04
        }}
        style={{width:'100%', height:300, marginTop:10}}
        />
          </ScrollView>
        
      </View>
    );
  }
}

export default connect(
  state => ({}),
  dispatch => ({
    actions: bindActionCreators(actions, dispatch)
  })
)(SummaryTab);

const styles = StyleSheet.create({
  container: {
    flex: 1,
  }
});
