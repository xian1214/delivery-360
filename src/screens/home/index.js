import React, { Component } from "react";
import {
  StyleSheet,
  View,
  TouchableOpacity,
  Text,
  ScrollView,
  Image
} from "react-native";

import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import * as actions from "../../redux/actions";

import NavTitleBar from "../../components/navTitle";
import * as commonColors from "../../styles/colors";
import * as commonStyles from "../../styles/styles";
import { screenWidth, screenHeight } from "../../styles/styles";
import Ionicons from "@expo/vector-icons/Ionicons";
import { Actions } from "react-native-router-flux";

import api from "../../service/api";
import Cache from "../../utils/cache";

const Content = ({ icon, title, content }) => (
  <View style={{ flexDirection: "row", alignItems: "center", marginTop: 6 }}>
    <Ionicons name={icon} size={16} color={"grey"} style={{ width: 20 }} />
    <Text
      numberOfLines={1}
      style={{ fontSize: 14, color: "#333", fontWeight: "bold" }}
    >
      {title} :{" "}
    </Text>
    <Text numberOfLines={1} style={{ fontSize: 14, color: "#333" }}>
      {content}
    </Text>
  </View>
);

let cars = [
  require("../../../public/images/car_green.png"),
  require("../../../public/images/car_grey.png"),
  require("../../../public/images/car_blue.png"),
  require("../../../public/images/car_blue.png")
];
class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      deliveries: []
    };
  }

  componentDidMount() {
    // this.props.actions.get
  }

  render() {
    return (
      <View style={styles.container}>
        <NavTitleBar
          buttons={commonStyles.NavNoneButton}
          onBack={this.goBack}
          title={"My Tasks"}
        />
        <ScrollView style={{ padding: 15 }}>
          {this.state.deliveries.map((item, index) => {
            let STATUS = [
              "Fully Available",
              "Issued",
              "Completed",
              "Cancelled"
            ];

            return (
              <TouchableOpacity
                activeOpacity={0.8}
                key={index}
                onPress={() => {
                  Actions.Detail({ delivery: item });
                }}
                style={styles.cardContainer}
              >
                <View style={{ flexDirection: "row" }}>
                  <View style={{ flex: 1 }}>
                    <Text style={{ fontSize: 13, color: "grey" }}>
                      #{item.DeliveryNoteNumber}
                    </Text>
                    <Image
                      source={cars[0]}
                      style={{ width: "80%", flex: 1, resizeMode: "contain" }}
                    />
                    <Text style={{ fontSize: 13, color: "#333" }}>
                      {STATUS[item.status]}
                    </Text>
                  </View>
                  <View
                    style={{
                      height: "100%",
                      width: 1,
                      backgroundColor: "#aaa"
                    }}
                  />
                  <View style={{ flex: 2, marginLeft: 10 }}>
                    <Content
                      icon={"md-person"}
                      title={"Customer"}
                      content={item.Customer ? item.Customer.Name : ""}
                    />
                    <Content
                      icon={"md-call"}
                      title={"Phone"}
                      content={item.Phone}
                    />
                    <Content
                      icon={"md-phone-portrait"}
                      title={"Mobile"}
                      content={item.Mobile}
                    />
                    <Content
                      icon={"md-bicycle"}
                      title={"Delivery"}
                      content={
                        item.User != null
                          ? item.User.FirstName + " " + item.User.LastName
                          : "---"
                      }
                    />
                  </View>
                </View>
                <View style={{ height: 5 }} />
                <Content
                  icon={"md-pin"}
                  title={"Address"}
                  content={item.Customer ? item.Customer.City : ""}
                />
                <Content
                  icon={"md-clock"}
                  title={"Request"}
                  content={item.RequestTime}
                />
                <View style={{ marginTop: 10 }}>
                  <View style={{ flexDirection: "row" }}>
                    <Text style={{ flex: 1, textAlign: "left" }}>Start</Text>
                    <Text style={{ flex: 1, textAlign: "center" }}>
                      On the way
                    </Text>
                    <Text style={{ flex: 1, textAlign: "right" }}>Arrived</Text>
                  </View>
                  <View
                    style={{
                      width: "100%",
                      alignItems: "center",
                      marginVertical: 4
                    }}
                  >
                    <View
                      style={{
                        position: "absolute",
                        left: 0,
                        top: 4,
                        width: "50%",
                        height: 4,
                        backgroundColor:
                          item.ArrivalStatus || item.OTWStatus
                            ? commonColors.greenColor
                            : "#aaa"
                      }}
                    />
                    <View
                      style={{
                        position: "absolute",
                        right: 0,
                        top: 4,
                        width: "50%",
                        height: 4,
                        backgroundColor: item.ArrivalStatus
                          ? commonColors.greenColor
                          : "#aaa"
                      }}
                    />
                    <View
                      style={{
                        position: "absolute",
                        left: 0,
                        top: 0,
                        width: 12,
                        height: 12,
                        borderRadius: 6,
                        backgroundColor:
                          item.ArrivalStatus || item.OTWStatus
                            ? commonColors.greenColor
                            : "grey"
                      }}
                    />
                    <View
                      style={{
                        width: 12,
                        height: 12,
                        borderRadius: 6,
                        backgroundColor:
                          item.ArrivalStatus || item.OTWStatus
                            ? commonColors.greenColor
                            : "grey"
                      }}
                    />
                    <View
                      style={{
                        position: "absolute",
                        right: 0,
                        top: 0,
                        width: 12,
                        height: 12,
                        borderRadius: 6,
                        backgroundColor: item.ArrivalStatus
                          ? commonColors.greenColor
                          : "grey"
                      }}
                    />
                  </View>
                  <View style={{ flexDirection: "row" }}>
                    <Text style={{ flex: 1, textAlign: "left", fontSize: 12 }}>
                      {item.OTWDatetime}
                    </Text>
                    <Text style={{ flex: 1, textAlign: "right", fontSize: 12 }}>
                      {item.ArrivalDatetime}
                    </Text>
                  </View>
                </View>
              </TouchableOpacity>
            );
          })}
          <View style={{ height: 30, width: 1 }} />
        </ScrollView>
      </View>
    );
  }
}

export default connect(
  state => ({}),
  dispatch => ({
    actions: bindActionCreators(actions, dispatch)
  })
)(Home);

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  cardContainer:{
    backgroundColor: "white",
    borderRadius: 3,
    shadowColor: "black",
    shadowOffset: { width: 2, height: 2 },
    shadowOpacity: 0.5,
    shadowRadius: 3,
    width: "100%",
    height: 230,
    padding: 15,
    paddingBottom: 0,
    marginVertical: 10
  }
});
