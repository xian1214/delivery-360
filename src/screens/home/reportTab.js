import React, { Component } from "react";
import { StyleSheet, View, TouchableOpacity, Text, ScrollView, Image } from "react-native";

import {bindActionCreators} from 'redux'
import {connect} from 'react-redux'
import * as actions from '../../redux/actions'

import NavTitleBar from '../../components/navTitle'
import * as commonColors from "../../styles/colors";
import * as commonStyles from "../../styles/styles";
import { screenWidth, screenHeight } from "../../styles/styles";
import Ionicons from "@expo/vector-icons/Ionicons";

let cars=[require('../../../public/images/car_green.png'), require('../../../public/images/car_grey.png'), require('../../../public/images/car_blue.png'), require('../../../public/images/car_blue.png')]
class ReportTab extends Component {
  constructor(props) {
    super(props);
    this.state = {
        items:[
            {name:'Keyboard', count:4},
            {name:'Mouse', count:3},
            {name:'Monitor', count:44},
            {name:'Laptop', count:1},
    
          ]
    };
  }

  render() {
    return (
      <View style={styles.container}>
        <ScrollView>
            {this.state.items.map((item, index)=>(
                <View key={index} style={{flexDirection:'row', padding:15, marginTop:0, backgroundColor:'white', borderWidth:1, borderColor:'#ccc', height:200}}>
                    <Image source={require('../../../public/images/bg5.jpg')} style={{flex:1, height:'100%'}}/>
                    <View style={{width:15}}/>
                    <Image source={require('../../../public/images/bg7.jpg')} style={{flex:1, height:'100%'}}/>
                </View>
            ))}
        </ScrollView>
      </View>
    );
  }
}

export default connect(
  state => ({}),
  dispatch => ({
    actions: bindActionCreators(actions, dispatch)
  })
)(ReportTab);

const styles = StyleSheet.create({
  container: {
    flex: 1
  }
});
