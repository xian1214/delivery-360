"use strict";

import React, { PureComponent } from "react";

import {
  StyleSheet,
  View,
  Platform,
  Modal,
  TouchableOpacity,
  Text,
  StatusBar,
  Image,
  TextInput,
  ScrollView
} from "react-native";

import * as commonColors from "../styles/colors";
import * as commonStyles from "../styles/styles";
import { screenWidth, screenHeight } from "../styles/styles";

import * as actionTypes from "../store/common/actionTypes";

import { bindActionCreators } from "redux";
import { connect } from "react-redux";
// import * as actions from "../redux/actions";
import * as actions from '../store/common/actions'
import * as session from '../store/session/actions'

import api from "../service/api";
import Cache from "../utils/cache";
import { Ionicons, MaterialCommunityIcons, MaterialIcons } from "@expo/vector-icons";
import { Actions } from "react-native-router-flux";
import HeaderBar from '../components/header'
import async from 'async'
import UtilService from '../utils/utils'

class BranchList extends PureComponent {
  constructor(props) {
    super(props);

    this.state = {
        wareHouses:[]
    };
  }

  componentDidMount(){
    if ( !this.props.internetConnection ){
      alert('Nothing to do here in offline mode!')
      Actions.pop()
      return;
    }
    api.getWareHouses((err,res)=>{
        // //console.log(err, res)
        if ( err == null ){
          this.setState({wareHouses:res.Results})
        }
    })
  }

  setRoute(item){
    api.selectWareHouse(this.props.currentDelivery.DeliveryNoteID, item.WarehouseID, (err, res)=>{
      // //console.log('select', err, res)
      if (err == null){
        this.props.actions.updateCurrentDelivery()
        Actions.SetRoute({data:res})
      }
    })
  }

  render() {
    if ( this.props.currentDelivery == null  ) return null;
    return (
      <View style={styles.container}>
        <HeaderBar title={this.props.currentDelivery.DeliveryNoteNumber} />
        <ScrollView>
            {this.state.wareHouses.map((item, index)=>{
                return (
                    <TouchableOpacity key={index} onPress={()=>this.setRoute(item)} style={{flexDirection:'row', alignItems:'center'}}>
                        <Image source={require('../../public/images/car_blue.png')} style={{width:50, height:50, borderRadius:25, marginHorizontal:20, marginVertical:10}}/>
                        <View style={{paddingVertical:10, borderBottomWidth:1, borderBottomColor:'#ccc', flex:1, justifyContent:'center'}}>
                            <Text style={{fontWeight:'bold', color:'grey', fontSize:14}}>{item.Name}</Text>
                        </View>
                    </TouchableOpacity>
                )
            })}
        </ScrollView>
      </View>
    );
  }
}

export default connect(
  state => ({
    currentDelivery:state.common.currentDelivery,
    internetConnection:state.common.internetConnection
  }),
  dispatch => ({
    actions: bindActionCreators({...actions,...session}, dispatch)
  })
)(BranchList);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop:24,
    backgroundColor: 'white'
  },
});
