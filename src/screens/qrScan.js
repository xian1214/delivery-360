"use strict";

import React, { PureComponent } from "react";

import {
  StyleSheet,
  View,
  Platform,
  Modal,
  TouchableOpacity,
  Text,
  StatusBar,
  Image,
  ScrollView,
  TextInput,
  ActivityIndicator,
  Animated,
  Dimensions
} from "react-native";


import * as commonColors from "../styles/colors";
import * as commonStyles from "../styles/styles";
import { screenWidth, screenHeight } from "../styles/styles";
import {BarCodeScanner, Permissions, } from 'expo'

import api from "../service/api";
import Cache from "../utils/cache";
import { Ionicons, Feather } from "@expo/vector-icons";
import { Actions } from "react-native-router-flux";
import { SlideButton, SlideDirection } from '../components/sliderButton';

import HeaderBar from '../components/header'
const MARK_SIZE = 200
const MARK_BORDER = 3

export default class QRCodeScan extends PureComponent {
  constructor(props) {
    super(props);

    this.state = {
        scanBarTop: new Animated.Value(0),
    };
  }

  _requestCameraPermission = async () => {
    const { status } = await Permissions.askAsync(Permissions.CAMERA);
    this.setState({
      hasCameraPermission: status === 'granted',
    });
  };

  _handleBarCodeRead = result => {
    if ( this.props.getQRCode ) this.props.getQRCode(result)
    Actions.pop()
    //Actions.ConfirmClient({type:'replace',qrdata:result.data})
  };
  
  componentDidMount() {
    this._requestCameraPermission();
    this.mounted = true
    let toValue = MARK_SIZE
    let myInterval = setInterval(() => {
        if (this.mounted) {
            Animated.timing(                  // Animate over time
                this.state.scanBarTop,            // The animated value to drive
                {
                    toValue: toValue,                   // Animate to opacity: 1 (opaque)
                    duration: 1000,              // Make it take a while
                }
            ).start();
            if ( toValue == MARK_SIZE ) toValue = 0
            else if ( toValue == 0 ) toValue = MARK_SIZE
        }
    }, 1000)
  }

  render() {
    return (
      <View style={styles.container}>
        <HeaderBar title={'QR Scan'}/>
        <View style={{height:80, alignItems:'center', justifyContent:'center'}}>
            <Text style={{fontSize:16, color:'grey', fontWeight:'bold'}}>
                Align QR code within frame to scan.
            </Text>
        </View>



        <View style={{flex:1}}>
            {this.state.hasCameraPermission === null
            ? <Text>Requesting for camera permission</Text>
            : this.state.hasCameraPermission === false
                ? <Text style={{ color: '#fff' }}>
                    Camera permission is not granted
                    </Text>
                : <BarCodeScanner
                    onBarCodeRead={this._handleBarCodeRead}
                    style={{
                        flex:1, justifyContent:'center', alignItems:'center'
                    }}
                    >
                    <View style={{ height: MARK_SIZE, width: MARK_SIZE }}>
                        <View style={{ position: 'absolute', left: -MARK_BORDER, top: -MARK_BORDER, height: 40, width: 40, borderTopColor: commonColors.theme, borderTopWidth: MARK_BORDER, borderLeftColor: commonColors.theme, borderLeftWidth: MARK_BORDER }} />
                        <View style={{ position: 'absolute', right: -MARK_BORDER, bottom: -MARK_BORDER, height: 40, width: 40, borderBottomColor: commonColors.theme, borderBottomWidth: MARK_BORDER, borderRightColor: commonColors.theme, borderRightWidth: MARK_BORDER }} />
                        <View style={{ position: 'absolute', right: -MARK_BORDER, top: -MARK_BORDER, height: 40, width: 40, borderTopColor: commonColors.theme, borderTopWidth: MARK_BORDER, borderRightColor: commonColors.theme, borderRightWidth: MARK_BORDER }} />
                        <View style={{ position: 'absolute', left: -MARK_BORDER, bottom: -MARK_BORDER, height: 40, width: 40, borderBottomColor: commonColors.theme, borderBottomWidth: MARK_BORDER, borderLeftColor: commonColors.theme, borderLeftWidth: MARK_BORDER }} />
                        <Animated.View style={{ height: 2, width: '100%', backgroundColor: 'red', top: this.state.scanBarTop, position: 'absolute' }} />
                    </View>
                    </BarCodeScanner>}
            {/* <View style={{position:'absolute', top:80, left:0, width:screenWidth, height:screenHeight-80, justifyContent:'center', alignItems:'center'}}>
                <View style={{ height: MARK_SIZE, width: MARK_SIZE }}>
                    <View style={{ position: 'absolute', left: -MARK_BORDER, top: -MARK_BORDER, height: 40, width: 40, borderTopColor: commonColors.theme, borderTopWidth: MARK_BORDER, borderLeftColor: commonColors.theme, borderLeftWidth: MARK_BORDER }} />
                    <View style={{ position: 'absolute', right: -MARK_BORDER, bottom: -MARK_BORDER, height: 40, width: 40, borderBottomColor: commonColors.theme, borderBottomWidth: MARK_BORDER, borderRightColor: commonColors.theme, borderRightWidth: MARK_BORDER }} />
                    <View style={{ position: 'absolute', right: -MARK_BORDER, top: -MARK_BORDER, height: 40, width: 40, borderTopColor: commonColors.theme, borderTopWidth: MARK_BORDER, borderRightColor: commonColors.theme, borderRightWidth: MARK_BORDER }} />
                    <View style={{ position: 'absolute', left: -MARK_BORDER, bottom: -MARK_BORDER, height: 40, width: 40, borderBottomColor: commonColors.theme, borderBottomWidth: MARK_BORDER, borderLeftColor: commonColors.theme, borderLeftWidth: MARK_BORDER }} />
                    <Animated.View style={{ height: 2, width: '100%', backgroundColor: 'red', top: this.state.scanBarTop, position: 'absolute' }} />
                </View>
            </View> */}
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop:24,
    backgroundColor: 'rgb(249,249,249)'
  },

});
