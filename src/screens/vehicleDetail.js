"use strict";

import React, { PureComponent } from "react";

import {
  StyleSheet,
  View,
  Platform,
  Modal,
  TouchableOpacity,
  Text,
  StatusBar,
  Image,
  ScrollView,
  TextInput,
  ActivityIndicator
} from "react-native";

// import TabNavigator from "react-native-tab-navigator";

import * as commonColors from "../styles/colors";
import * as commonStyles from "../styles/styles";
import { screenWidth, screenHeight } from "../styles/styles";
import * as actions from '../store/common/actions'
import api from "../service/api";
import Cache from "../utils/cache";
import { Ionicons, Feather } from "@expo/vector-icons";
import { Actions } from "react-native-router-flux";
import { SlideButton, SlideDirection } from '../components/sliderButton';
import {Constant,Svg} from 'expo'
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import HeaderBar from '../components/header'

const TextRow = ({title,content,unit})=>(
    <View style={{flexDirection:'row',padding:10}}>
        <Text style={{flex:1,color:'grey',fontSize:18,fontWeight:'bold'}}>{title}</Text>
        <Text style={{flex:1,color:'black',fontSize:18,fontWeight:'bold',textDecorationLine:'underline'}}>
          {content}<Text style={{paddingLeft:7,color:'green',textAlign:'left', fontSize:18,fontWeight:'bold',textDecorationLine:'underline'}}>{' '+unit}</Text>
        </Text>
        
    </View>
);
class VehicleDetail extends PureComponent {
  constructor(props) {
    super(props);

    this.state = {
        projectName:'Project Name',
        DONumber:'RT02342',
        clientName: 'Austin',
        clientAddress: 'No.1, Jalan Bukit Bintang, Kuala Lumpur',
        items:[],
        signatureArrays:[],
        example:'',
        dnID:'',
        responsible:'',
        customerID:''
    };
  }

  confirm(){
      Actions.pop();
  }
  componentDidMount(){
   
  }
  render() {
    if(!!!this.props.vehicleDetail){
      <View>
        <Text>{'Dn vehicleDetail is not defined...'}</Text>
      </View>
    }
    return (
      // <ScrollView>
      <View style={styles.container}>
        <HeaderBar title={'Vehicle Detail'}/>
        <View style={{justifyContent:'center',padding:20,marginBottom:20}}>
          <TextRow title={'Address'} content={this.props.vehicleDetail.Address} unit={''}/>
          <TextRow title={'Latitude'} content={this.props.vehicleDetail.Latitude} unit={''}/>
          <TextRow title={'Longitude'} content={this.props.vehicleDetail.Longitude} unit={''}/>
          <TextRow title={'ImeiNo'} content={this.props.vehicleDetail.ImeiNo} unit={''}/>
          <TextRow title={'Mileage'} content={this.props.vehicleDetail.Mileage} unit={'km'}/>
          <TextRow title={'Speed'} content={this.props.vehicleDetail.Speed} unit={'m/s'}/>
          <TextRow title={'Weight'} content={this.props.vehicleDetail.Weight} unit={'kg'}/>
          <TextRow title={'Temperature'} content={this.props.vehicleDetail.Temperature}  unit={'degrees'}/>
          <TextRow title={'Drum'} content={this.props.vehicleDetail.Drum} unit={''}/>
          <TextRow title={'Fuel'} content={this.props.vehicleDetail.Fuel} unit={'litters'}/>
        </View>
      </View>
    );
  }
}
export default connect(
  state => ({
    vehicleDetail: state.common.vehicleDetail,
    internetConnection: state.common.internetConnection,
    vehicleDetail:state.common.vehicleDetail
  }),
  dispatch => ({
    actions: bindActionCreators({...actions}, dispatch)
  })
)(VehicleDetail);
const styles = StyleSheet.create({
  container: {
    paddingTop:(Platform.OS==="android")?24:0,
    flex: 1,
    backgroundColor: 'rgb(249,249,249)'
  },
  signContainer: {
    alignItems: "center",
    justifyContent: "center",
    padding: 20
  },
});
