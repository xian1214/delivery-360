import React, { Component } from "react";
import { StyleSheet, View, Text, TouchableOpacity, Alert } from "react-native";
import * as commonColors from "../styles/colors";
import * as commonStyles from "../styles/styles";
import {Actions} from 'react-native-router-flux'

import * as actionTypes from "../store/common/actionTypes";

import { bindActionCreators } from "redux";
import { connect } from "react-redux";
// import * as actions from "../redux/actions";
import Cache from "../utils/cache";
import * as actions from '../store/common/actions'
import * as session from '../store/session/actions'

class InformationBar extends Component {
  constructor(props) {
    super(props);
  }

  componentDidMount(){
  }

  goOngoing(){
    let {currentDelivery} = this.props
    if ( currentDelivery == null ) return ;
    if ( currentDelivery.IsSignOff ){
      if ( currentDelivery.Status == commonStyles.DELIVERY_STATUS_RETURNING && currentDelivery.ReturnWarehouse!=null ) {
        Actions.Navigation()
      }else{
        Actions.LastScreen({data:currentDelivery})
      }
    }else{
      if ( currentDelivery.OTWStatus ){
        Actions.Navigation()
      }else{
        Actions.DeliverySecondReport({editable:true})
      }
    }
    
  }

  componentWillReceiveProps(next){
    //   Alert.alert(next.iConnection+(next.currentDelivery!=null?next.currentDelivery.DeliveryNoteNumber:'----'))
  }
  
  render() {
      let {iConnection, currentDelivery, batteryInfo} = this.props
      if ( iConnection && currentDelivery == null && (batteryInfo.level==undefined || batteryInfo.level>=0.15) ) return null
      
    return (
        <TouchableOpacity onPress={()=>this.goOngoing()} style={styles.container}>
            {currentDelivery!=null&&<View style={{flex:2, backgroundColor:commonColors.yellow, justifyContent:'center'}}>
                <Text style={{color:'white', fontWeight:'bold', fontSize:12, marginLeft:15}}>{Cache.getLang('label.in_progress')} (#{currentDelivery.DeliveryNoteNumber})</Text>
            </View>}
            {!iConnection&&<View style={{flex:1, backgroundColor:commonColors.pink, justifyContent:'center'}}>
            <Text style={{color:'white', fontWeight:'bold', fontSize:12, marginLeft:15}}>Offline</Text>
            </View>}
            {batteryInfo.level<0.15&&<View style={{flex:1, backgroundColor:'rgb(251,70,57)', justifyContent:'center'}}>
              <Text style={{color:'white', fontWeight:'bold', fontSize:12, marginLeft:15}}>Low Battery</Text>
            </View>}
        </TouchableOpacity>
    );
  }
}

export default connect(
  state => ({
    batteryInfo: state.common.batteryInfo,
    currentDelivery: state.common.currentDelivery,
    internetConnection: state.common.internetConnection
  }),
  dispatch => ({
    actions: bindActionCreators({...actions,...session}, dispatch)
  })
)(InformationBar);

const styles = StyleSheet.create({
  container: {
    height: 24,
    width:'100%',
    flexDirection:'row',
    elevation:5,
  },
});













