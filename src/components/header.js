import React, { Component } from "react";
import { StyleSheet, View, Text, TouchableOpacity } from "react-native";
import { Ionicons } from "@expo/vector-icons";
import * as commonColors from "../styles/colors";
import {Actions} from 'react-native-router-flux'

export default class extends Component {
  constructor(props) {
    super(props);
    this.backing = false
    this.state={
        disabled:true     
    }
    this.clicked = false
  }
  pressButton() {
    if (this.clicked) return
    this.clicked = true
    this.props.back?this.props.back():Actions.pop()
    console.log('----header----', this.props.title)
    // enable after 5 second
  }
  render() {
    return (
      <View style={styles.container}>
        { 
          !this.props.NoBackButton&&
          <TouchableOpacity 
              onPress={()=>{
                  this.pressButton()
                }
              }
              style={{padding:5,width:30}}
          >
          <Ionicons name="ios-arrow-round-back" size={36} color={"white"} />
        </TouchableOpacity>}
        <Text style={styles.text}>{this.props.title}</Text>
        {this.props.rightElement}
        {this.props.rightIcon&&<TouchableOpacity onPress={()=>this.props.rightCallback()} style={styles.rightIcon}>
          <Ionicons name={this.props.rightIcon} size={24} color={"white"} />
        </TouchableOpacity>}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    height: 60,
    width:'100%',
    paddingHorizontal: 20,
    alignItems: "center",
    flexDirection: "row",
    backgroundColor: commonColors.theme,
    elevation:5,
  },
  text: {
    marginLeft: 30,
    color: "white",
    fontSize: 18,
    fontWeight:'bold',
    flex:1
  }
});
