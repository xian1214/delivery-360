import Cache from "../utils/cache";
import * as config from "../config";
import UtilService from "../utils/utils";
import moment from 'moment'
import { deliver } from "../styles/styles";

module.exports = {

  async checkToken(cb){
    let request = {
      method: 'GET',
      headers: {
        AppCode:"A005",
        Company:1,
        UDID:Cache.pushToken,
        Accept: "application/json",
        "Content-Type": "application/json",
        Authorization: Cache.currentUser
          ? "bearer " + Cache.currentUser["token"]
          : null,
        client: Cache.clientID ? Cache.clientID : null
      }
    };
    try {
       //console.log('arrived 1')
      let tStamp = moment(new Date())
      //console.log('arrived 2',Cache.pushToken)
      let response = await fetch(config.SERVICE_API_URL+'/api/pingAuth', request);
      //console.log('arrived 3',response)
      if (response.status == 200) {
        //console.log('arrived 4')
        cb(null);
      } else {
        //console.log('arrived okay when checktoken2',response)
        cb('err');
      }
    } catch (error) {
      //console.log('arrived okay when checktoken3')
      cb(error);
    }
  },
  async checkVersion(cb){
    let request = {
      method: 'GET',
      headers: {
        AppCode:"A005",
        Company:1,
        UDID:Cache.pushToken,
        Accept: "application/json",
        "Content-Type": "application/json",
        Authorization: Cache.currentUser
          ? "bearer " + Cache.currentUser["token"]
          : null,
        client: Cache.clientID ? Cache.clientID : null
      }
    };
    try {
       console.log('arrived 1')
      let tStamp = moment(new Date())
      console.log('arrived 2',Cache.pushToken)
      let response = await fetch(config.SERVICE_API_URL+'/api/AppMgmt/VerUpToDateCheck?appCode=A005&platformCode=ANDROID&clientAppVer=1.0', request);
      let responseJson = await response.json();
      console.log('arrived 3',response)
      if (response.status == 200) {
        console.log('arrived 4')
        cb(null,responseJson);
      } else {
        //console.log('arrived okay when checktoken2',response)
        cb(responseJson);
      }
    } catch (error) {
      //console.log('arrived okay when checktoken3')
      cb(error);
    }
  },
  async fetchData(url, request, cb) {
    // //console.log(url, request)
    try {
      let response = await fetch(url, request);
      // //console.log(response)
      let responseJson = await response.json();
      // //console.log(responseJson)
      if (response.status == 200) {
        cb(null, responseJson);
      } else {
        cb(responseJson);
      }
    } catch (error){
        if(cb==false){
          console.log('url when cb false->>',url)
        }
        cb(error,null)
    }
  },
  async fetchGPSData(id, cb) {
    let request = {
      method:"GET",
      timeout:15000,
    };
    try {
      
      var url = "http://www.meridians2.com:7800/Api/LatestLocation.aspx?trackerid="+id
      let response = await fetch(url, request);
      let responseJson = await response.json();
      if (response.status == 200) {
        cb(null, responseJson);
      } else {
        cb(responseJson);
      }
    } catch (error) {
      cb(error);
    }
  },
 
  baseApi(sub_url, method, json_data, cb, isText) {
 
    let request = {
      method,
      timeout:10000,
      headers: {
        "Cache-Control":'no-cache,no-store,must-revalidate',
        'Pragma':'no-cache',
        'Expires':0,
        AppCode:"A005",
        UDID:Cache.pushToken,
        Company:1,
        Accept: isText?undefined:"application/json",
        "Content-Type": "application/json",
        Authorization: Cache.currentUser
          ? "bearer " + Cache.currentUser["token"]
          : null,
        client: Cache.clientID ? Cache.clientID : null
      }
    };
    if (method == "POST" || method == "PUT") {
      request["body"] = JSON.stringify(json_data);
    }else{
      // sub_url += '&t='+(new Date()).getTime()
    }
    ////console.log('sub_url=-----',sub_url)
    this.fetchData(config.SERVICE_API_URL + sub_url, request, cb);
  },
  gpsApi(sub_url, method, json_data, cb, isText) {
 
    let request = {
      method,
      timeout:10000,
      headers: {
        Accept: isText?undefined:"application/json",
        "Content-Type": "application/json",
        Authorization: Cache.currentUser
          ? "bearer " + Cache.currentUser["token"]
          : null,
        client: Cache.clientID ? Cache.clientID : null
      }
    };
    if (method == "POST" || method == "PUT") {
      request["body"] = JSON.stringify(json_data);
    }else{
      // sub_url += '&t='+(new Date()).getTime()
    }

    this.fetchData(config.GPS_URL + sub_url, request, cb);
  },

  async init(cb) {
    let user = await UtilService.getLocalStringData("currentUser");
    let pushToken = await UtilService.getLocalStringData("pushToken");
    Cache.clientID = await UtilService.getLocalStringData("client");
    if (user == null || user == undefined ) {
      cb("err for user");
    } else {
      Cache.currentUser = JSON.parse(user);
      Cache.pushToken = pushToken;
      this.checkToken(async (err)=>{
        //console.log('err on init',err)
        if ( err == null ){
          //console.log('err on init 2')
          let locationHeader = await UtilService.getLocalStringData('locationHeader')
          let locations = await UtilService.getLocalStringData('locations')
    
          Cache.locationHeader = JSON.parse(locationHeader)
          Cache.locations = JSON.parse(locations)
          //console.log('err on init 3')
          cb(null);
        }else{
          //console.log('err on init 4')
          this.logout()
          cb(err)
        }
      })
    }
  },

  login(ClientID, PhoneNumber, Password, cb) {
    Cache.PhoneNumber = PhoneNumber;
    Cache.Password = Password;
    Cache.clientID = ClientID;
    let tStamp = moment(new Date())
    //console.log('999999999999999999999999999',Cache)
    this.baseApi("/api/appLogin?tx="+tStamp, "POST", { PhoneNumber, Password }, cb);
  },

  confirmOTP(PhoneNumber, OtpNumber, PushToken, cb) {
    this.baseApi(
      "/api/otpLogin",
      "POST",
      { PhoneNumber, OtpNumber, PushToken },
      (err, res) => {
        // //console.log('user', err, res)
        if (err == null) {
          Cache.currentUser = res;
          UtilService.saveLocalStringData("currentUser", JSON.stringify(res));
          UtilService.saveLocalStringData("client", Cache.clientID);
          cb(err, res);
          return;
        }
        cb(err, res);
      }
    );
  },

  logout() {
    UtilService.removeLocalObjectData("currentUser");
    UtilService.removeLocalObjectData("client");
    UtilService.removeLocalObjectData("locationHeader");
    UtilService.removeLocalObjectData("locations");
  },

  async uploadImage(file, cb) {
    // //console.log('uploadImage',file)
    if ( !Cache.hasInternetConnection ){
      let index = await this.pushApi('uploadImage', null, file)
      UtilService.saveLocalStringData(''+index,file)
      cb(null, index)
      return;
    }
    try {
      let image = {
        uri: file,
        type: "image/jpeg",
        name: "file.jpeg"
      };

      let formData = new FormData();
      formData.append("file", image);
      // //console.log('uploadImage',file)
      let response = await fetch(
        config.SERVICE_API_URL + "/api/common/files/upload",
        {
          method: "POST",
          headers: {
            Accept: "application/json",
            "Content-Type": "multipart/form-data",
            Authorization: "bearer " + Cache.currentUser["token"],
            client: Cache.clientID
          },
          body: formData
        }
      );
      let status = response.status;

      let responseJson = await response.json();
      if (status == 200 || status == 201) {
        cb(null, responseJson);
      } else {
        cb(responseJson.message);
      }
    } catch (error) {
      cb(error);
    }
  },
  async uploadFile(file, cb) {
    // //console.log('uploadImage',file)
    if ( !Cache.hasInternetConnection ){
      let index = await this.pushApi('uploadFile', null, file)
      cb(null, index)
      return;
    }
    try {

      let formData = new FormData();
      formData.append("file", file);
      // //console.log('uploadImage',file)
      let response = await fetch(
        config.SERVICE_API_URL + "/api/common/files/upload",
        {
          method: "POST",
          headers: {
            Accept: "application/json",
            "Content-Type": "multipart/form-data",
            Authorization: "bearer " + Cache.currentUser["token"],
            client: Cache.clientID
          },
          body: formData
        }
      );
      let status = response.status;

      let responseJson = await response.json();
      if (status == 200 || status == 201) {
        cb(null, responseJson);
      } else {
        cb(responseJson.message);
      }
    } catch (error) {
      cb(error);
    }
  },
  async baseUploadApi(sub_url, file){
    // //console.log('baseUploadApi', file)
    if ( !Cache.hasInternetConnection ){
      return null;
    }
      let image = {
        uri: file,
        type: "image/jpeg",
        name: "file.jpeg"
      };
      try{
      let formData = new FormData();
      formData.append("file", image);
      let response = await fetch(
        config.SERVICE_API_URL + sub_url,
        {
          method: "POST",
          headers: {
            Accept: "application/json",
            "Content-Type": "multipart/form-data",
            "client":"c001"
          },
          body: formData
        }
      );
      let status = response.status;
      let responseJson = await response.json();
      // //console.log('status',status, 'responseJson', responseJson)
      return {
        status,
        data:JSON.parse(responseJson.Result)
      }
      }catch(error){
        // //console.log(error)
        return error
      }
  },

  // async uploadAllData(cb){
  //   if ( Cache.locationHeader ){
  //     this.uploadLocations(async(err, res)=>{
  //       if ( err == null) {
  //         Cache.locationHeader = null
  //         Cache.locations = []
  //         await UtilService.removeLocalObjectData('locationHeader')
  //         await UtilService.removeLocalObjectData('locations')
  //       }
  //     })
  //   }
  //   if ( Cache.requestStack.length > 0 ){
  //     async.mapSeries(Cache.requestStack, async ({url, request}, cb)=>{
  //       try {
  //         let response = await fetch(url, request);
  //         let responseJson = await response.json();
  //         if (response.status == 200) {
  //           cb(null, responseJson);
  //         } else {
  //           cb(responseJson);
  //         }
  //       } catch (error) {
  //         cb(error);
  //       }
  //     }, async (error, results)=>{
  //       Cache.requestStack.splice(0, results.length)
  //       await UtilService.removeLocalObjectData('requests')
  //       if ( error != null ){
  //         await UtilService.saveLocalStringData('requests', JSON.stringify(Cache.requestStack))
  //       }
  //       cb(err, results)
  //     })
  //   }
  // },
  updateReadStatus(deliveryNoteID,userType, cb){
    //console.log('updatereadstatus---2')
    this.baseApi('/api/inventory/deliveryNotes/updateReadStatus/'+deliveryNoteID+'?userType='+userType, 'PUT', {}, cb)
  },
  getDeliveries(Responsible, isSignOff, page, pageSize, from, to, cb) {

    let tStamp = moment(new Date())
    //console.log('tstamp on get deliveries=',tStamp)
    this.baseApi( `/api/inventory/deliveryNotes?Responsible=${Responsible}&IsSignOff=`+isSignOff
    +(page==null||pageSize==null?'':`&page=${page}&pageSize=${pageSize}`)
    +(from==null?'':`&FromDate=${from}`)
    +(to==null?'':`&ToDate=${to}`)+'&SortField=CreatedAt&SortDirection=-1'
    + '&tx='+ tStamp, 
    "GET", {}, cb );
  },

  incrementUnreadCnt(deliveryNoteID,userType,message, cb){
    this.baseApi('/api/inventory/deliveryNotes/incrementUnreadCnt/'+deliveryNoteID+'?userType='+userType + '&message='+message, 'PUT', {}, cb)
  },
  getCompletedDeliveriesByTime(from, to,id, cb) {
  
    this.baseApi( `/api/inventory/deliveryNotes/completedDNs?FromDate=${from}&ToDate=${to}&DriverID=${id}`,'GET', {}, cb );
  },
  getDelivery(id, cb){
    this.baseApi(`/api/inventory/deliveryNotes/${id}`+'?a=a', 'GET', {}, cb)
  },
  lastMessageUpdate(id, data,cb){
    //console.log('update is being ..',data)
    this.baseApi(`/api/inventory/deliveryNotes/${id}/lastMessageUpdate`, 'PUT', data, cb)
  },
  getCheckResultsByDnItem(id, cb){
    this.baseApi(`/api/inventory/checkResults/byDnItem/${id}`, 'GET', {}, cb)
  },
  postRoute(data, cb) {
    //console.log('post route is being **************************')
    this.baseApi("/api/inventory/deliveryRoutes"+(data.isReturnRoute?'/ReturnRoute':''), "POST", data, cb);
  },
  async postLocation(data, cb) {
    if (Cache.hasInternetConnection ){
      this.baseApi("/api/inventory/deliveryLocations", "POST", data, cb);
    }else{
      let index = await this.pushApi('/api/inventory/deliveryLocations', 'POST', data)
      cb(null, index)
    }
  },
  getCompany(cb) {
    this.baseApi("/api/setting/companies"+'?a=a', "GET", {}, (err, res) => {
      if(!!res && res.length>0)
      {
        cb(err, res[0]);
      }
      else{
        cb(err,null);
      }
    });
  },
  arrivedDelivery(id, status, cb) {
    this.baseApi( `/api/inventory/deliveryNotes/${id}/setArrivedStatus?status=${status}`, "PUT", {}, cb );
  },
  updateDeliveryNoteItem(id,data,cb){
    this.baseApi( `/api/inventory/deliveryNotes/dnItems/${id}`, "PUT", data, cb );
  },
  getLocationFromCloud(id,cb){
    this.fetchGPSData(id,cb);
  },
  updatePassword(data,cb){
    this.baseApi( `/api/setting/users/updatePassword`, "PUT", data, cb );
  },
  async createPhoto(id, data, cb) {
    
    if ( Cache.hasInternetConnection ){
      // if(!!data && !!data.AfterPhoto){
      //   //console.log('arrived createPhoto>>>> !!data&&!!data.AfterPhoto')
      //   data.AfterPhoto = data.AfterPhoto.uri
      // }
      // else{
      //   //console.log('else -----arrived createPhoto>>>> !!data&&!!data.AfterPhoto')
      //   data.AfterPhoto = null
      // }
      //console.log('create photo with geotag')      
      this.baseApi( `/api/inventory/deliveryNotes/${id}/createPhoto`, "POST", data, cb );
    }else{
      let index = await this.pushApi(`createPhoto`, "POST", data)
      cb(null, index)
    }
  },
  confirmRequest(id, data, cb) {
    this.baseApi( `/api/inventory/deliveryNotes/${id}/confirmSignedRequest`, "PUT", data, cb );
  },
  async sendEmail(id, cb) {
    if ( Cache.hasInternetConnection ){
      this.baseApi( `/api/inventory/deliveryNotes/${id}/sendEmail`, "PUT", {}, cb );
    }else{
      let index = await this.pushApi(`/api/inventory/deliveryNotes/${id}/sendEmail`, "PUT", {})
      cb(null, index)
    }
    
  },
  async createAlert(data, cb) {
    if ( Cache.hasInternetConnection ){
      console.log('delivery start api')
      this.baseApi(`/api/setting/events`, "POST", data, cb);
    }else{
      let index = await this.pushApi(`/api/setting/events`, "POST", data)
      cb(null, index)
    }  
  },
  getAlerts(ReferenceID, cb) {
    this.baseApi("/api/setting/events"+'?ReferenceID='+ReferenceID, "GET", {}, cb);
  },
  getSummary(data, cb) {
    this.baseApi( "/api/inventory/transferRequests/getDeliverySummary", "POST", data, cb );
  },
  getCheckList(id, cb){
    this.baseApi('/api/inventory/checkItemList/'+id+'?a=a', 'GET', {}, cb)
  },
  createCheckResult(data, cb){
    this.baseApi('/api/inventory/checkResults', 'POST', data, cb)
  },
  async updateCheckResult(data, cb){
    if ( Cache.hasInternetConnection ){
      this.baseApi('/api/inventory/checkResults/'+data.CheckResultID, 'PUT', data, cb)
    }else{
      let index = await this.pushApi('updateCheckResult', null, data)
      cb(null, index)
    }
    
  },
  getCheckResult(id, cb){
    this.baseApi('/api/inventory/checkResults/'+id+'?a=a', 'GET', {}, cb)
  },
  detectFace(file){
    return this.baseUploadApi('/api/faceDetect', file)
  },
  createTeamAction(act,cb) {
    console.log('act on api page...')
    this.baseApi("/api/inventory/deliveryTeamActs", "POST", act, cb);
  },
  getDeliveriesWithFilter(query, customerID,status,isSignOff,startTime,endTime,cb) {
    console.log('startTime on fileter',moment(new Date(startTime)).format("YYYY-MM-DD" + ' 00:00:00'))
    this.baseApi( '/api/inventory/deliveryNotes?' 
    + 'Query=' + query
    + '&CustomerID='+customerID  
    + '&IsSignOff=' + isSignOff
    + '&FromDate=' + moment(new Date(startTime)).format("YYYY-MM-DD" + ' 00:00:00')
    + '&ToDate=' + moment(new Date(endTime)).format("YYYY-MM-DD" + ' 23:59:59')
    + '&Status='+ status
    + '&hasTodayUpdate='+ false, "GET", {}, cb );
  },
  getActivities(cb) {
    this.baseApi(`/api/setting/activities`, "GET", {}, cb);
  },
  getTeamMembers(dnID, cb){
    this.baseApi('/api/inventory/deliveryteammembers?DeliveryNoteID='+dnID, 'GET', {}, cb)
  },
  getTeamActions(deliveryNoteID,cb) {
    console.log('act on api page...')
    this.baseApi("/api/inventory/deliveryTeamActs?DeliveryNoteID=" + deliveryNoteID, "GET", {}, cb);
  },
  createPersonFace(personGroupId, personId, file){
    return this.baseUploadApi(`/api/createPersonFace?PersonGroupID=${personGroupId}&PersonID=${personId}`, file)
  },
  getCustomer(id,cb){
    this.baseApi(`/api/crm/customers/${id}`, 'GET', {}, cb)
  },

  setUserFaceID(FaceID, cb){
    this.baseApi(`/api/setting/users/${Cache.currentUser.user.UserID}/setUserFaceID`, 'PUT', {FaceID}, cb)
  },
  setETA(id, ETA, cb){
    this.baseApi(`/api/inventory/deliveryNotes/${id}/SetETA`, 'PUT', {ETA}, cb)
  },
  getCodes(cb){
    this.baseApi(`/api/common/codes`+'?a=a', 'GET', {}, cb)
  },
  faceLogin(FaceID, cb){
    this.baseApi(`/api/appFaceLogin`, 'POST', {FaceID}, (err, res)=>{
      if (err == null) {
        Cache.currentUser = res;
        UtilService.saveLocalStringData("currentUser", JSON.stringify(res));
        UtilService.saveLocalStringData("client", Cache.clientID);
        cb(err, res);
        return;
      }
      cb(err, res);
    })
  },
  queueIn(cb){
    this.baseApi(`/api/TransGuardBranch/DriverQueueIn`, 'GET', {}, cb, true)
  },
  queueDrop(cb){
    this.baseApi(`/api/TransGuardBranch/DriverQueueDrop`, 'GET', {}, cb)
  },
  completeDelivery(id, cb){
    this.baseApi(`/api/inventory/deliveryNotes/${id}/complete`, 'PUT', {}, cb)
  },
  getWareHouses(cb){
    this.baseApi('/api/inventory/warehouses'+'?a=a', 'GET', {}, cb)
  },
  selectWareHouse(deliveryID, wareHouseID, cb){
    this.baseApi(`/api/inventory/deliveryNotes/${deliveryID}/SetReturnWarehouse?warehouseId=${wareHouseID}`, 'PUT', {}, cb)
  },
  getUser(id, cb){
    this.baseApi(`/api/setting/users/${id}`+'?a=a', 'GET', {}, cb)
  },
  getInprocessDelivery(cb){
    //console.log('get dn inprocess on api--------')
    this.baseApi(`/api/inventory/deliveryNotes/inprocess/`+Cache.currentUser.user.UserID+'?a=a', 'GET', {}, cb)
  },
  getVehicleStatus(id,cb) {
    this.baseApi(`/api/TransGuardHq/VehTrackVehGpsLog?vehicleIDs[]=${id}`,'GET',{},cb)
  },
  getStats(id,cb){
    //console.log('getStats is being........',id)
    var todayStart = moment().format('YYYY-MM-DD') + ' 00:00:00'
    var todayEnd = moment().format('YYYY-MM-DD') + ' 23:59:59'
    this.baseApi(`/api/inventory/deliveryNotes/statsForDriver?driverID=${id}&todayStart=${todayStart}&todayEnd=${todayEnd}`, 'GET', {}, cb)
  },
  getCheckinList(from, to, cb){
    let fromDate = moment(to).format('YYYY-MM-DD')+' 00:00:00'
    let toDate = moment(from).format('YYYY-MM-DD')+' 23:59:59'
    let tStamp = moment(new Date())
    //console.log('fetchData is being....on api js file',)
    this.baseApi('/api/TransGuardBranch/DriverCheckInList?userID='+Cache.currentUser.user.UserID+`&dateFrom=${fromDate}&dateTo=${toDate}&t=${tStamp}`, 'GET', {}, cb)
  },
  checkIn(message, warehouseID, lat, lng, cb){
    //console.log('check-in is being done')
    let tStamp = moment(new Date())
    this.baseApi(`/api/TransGuardBranch/DriverCheckIn?userID=${Cache.currentUser.user.UserID}&message=${message}&warehouseID=${warehouseID}&lat=${lat}&lng=${lng}&t=${tStamp}`, 'POST', {}, cb)
  },
  checkOut(message, lat, lng, cb){
    //console.log('checkout is being done')
    let tStamp = moment(new Date())
    this.baseApi('/api/TransGuardBranch/DriverCheckOut?userID='+Cache.currentUser.user.UserID+'&message='+message+'&lat='+lat+'&lng='+lng+'&t='+tStamp, 'PUT', {}, cb)
  },
  setCheckResult(id, delivery, cb){
    this.baseApi(`/api/inventory/deliveryNotes/${id}/updateCheckResults`, 'PUT', delivery, cb)
  },
  getQacItemTemplate(cb){
    this.baseApi(`/api/TransGuardLabor/QacItemCreateOrUpdate`, 'GET', {}, cb)
  },
  getQacItemById(deliveryNoteItemID, cb){
    this.baseApi(`/api/TransGuardLabor/QacItemCreateOrUpdate?deliveryNoteItemID=${deliveryNoteItemID}`, 'GET', {}, cb)
  },
  async updateQacItem(data, cb){
    if ( Cache.hasInternetConnection ){
      this.baseApi('/api/TransGuardLabor/QacItemCreateOrUpdate', 'POST', data, cb)
    }else{
      let index = await this.pushApi('updateQacItem', null, data)
      cb(null, index)
    }
    
  },
  getQacItems(deliveryNoteID, cb){
    this.baseApi(`/api/TransGuardLabor/QacView?deliveryNoteID=`+deliveryNoteID, 'GET', {}, cb)
  },
  getUsers(page, pageSize, cb){
    this.baseApi(`/api/setting/users?page=${page}&pageSize=${pageSize}`, 'GET', {}, cb)
  },
  async uploadSign(deliveryNoteID, uri, cb){
    if ( Cache.hasInternetConnection ){
      this.baseApi(`/api/inventory/deliveryNotes/uploadSign/${deliveryNoteID}?signature=${uri}`, 'PUT', {}, cb)
    }else{
      let index = await this.pushApi('uploadSign', deliveryNoteID, uri)
      cb(null, index)
    }
    
  },
  async pushApi(sub_url, method, json_data){
    let apiList = await this.getStoredApis()
    apiList.push({sub_url, method, json_data})
    this.putApis(apiList)
    return apiList.length-1
  },
  async getStoredApis(){
    let stringData = await UtilService.getLocalStringData('apiList')
    //console.log('stringData', stringData)
    return (stringData == 'null' || stringData == undefined ||stringData==null) ? []:JSON.parse(stringData)
  },
  putApis(apiList){
    UtilService.saveLocalStringData("apiList", JSON.stringify(apiList));
    
  }
};
