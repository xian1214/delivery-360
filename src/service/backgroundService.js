
import Cache from '../utils/cache'
import api from '../service/api'

let patchWholeInfoIntervalHandler = null, patchWholeInfoInterval = 30000;

export function main(){
    Cache.deliveries = [];
    Cache.currentDelivery = null;

    patchWholeInfoIntervalHandler = setTimeout(()=>{
        api.getDeliveries((err, res)=>{
            if ( err == null ){
                Cache.deliveries = res.Results
            }
        })
    }, patchWholeInfoInterval)
}

export function refresh(){

}

export function close(){
    clearInterval(patchWholeInfoIntervalHandler)
}
