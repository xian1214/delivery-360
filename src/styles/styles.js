import { Dimensions } from "react-native";

export const { width: screenWidth, height: screenHeight } = Dimensions.get(
  "window"
);

export const NavNoneButton              = 0;
export const NavBackButton              = 1;
export const NavFilterButton            = 2;
export const NavOfflineButton           = 4;
export const NavCloseButton             = 8;
export const NavCloseTextButton         = 16;
export const NavMenuButton              = 32;
export const NavNotificationButton      = 64;
export const NavPinButton               = 128;

export const headerHeight = 80;
export const menuHeight = 60;
export const viewHeight = screenHeight - headerHeight - menuHeight;

export const background = require("../../public/images/back.jpg");
export const logo_black = require("../../public/images/logo.png");
export const logo_white = require("../../public/images/logo-white.png");
export const face = require("../../public/images/placeholder.jpg");
export const face1 = require("../../public/images/face1.jpg");
export const face2 = require("../../public/images/face2.jpg");
export const face3 = require("../../public/images/face3.jpg");
export const path = require("../../public/images/path.png");
export const path_theme = require("../../public/images/path_theme.png");
export const deliver = require("../../public/images/deliver.jpg");
export const noUser = require("../../public/images/noUser.png");


export const REROUTE = '010004'
export const IDLE = '010003'
export const GEO_FENCE = '010002'
export const PAUSE = '010005'
export const START = '010006'
export const CONFIRMED = '010007'
export const ARRIVAL = '010010'
export const RESUME = '010008'
export const Unloading = '010011'
export const Unloaded = '010012'
export const SignOff = '010013'
export const Returning = '010014'
export const Finish = '010015'
export const FORCE_ARRIVAL = '010017'


export const DELIVERY_STATUS_DRAFT = "024001";
export const DELIVERY_STATUS_READY = "024002";
export const DELIVERY_STATUS_START = "024003";
export const DELIVERY_STATUS_PAUSE = "024004";
export const DELIVERY_STATUS_RESUME = "024005";
export const DELIVERY_STATUS_ARRIVED = "024006";
export const DELIVERY_STATUS_UNLOADING = "024007";
export const DELIVERY_STATUS_UNLOADED = "024008";
export const DELIVERY_STATUS_SIGNOFF = "024009";
export const DELIVERY_STATUS_RETURNING = "024010";
export const DELIVERY_STATUS_FINISH = "024011";