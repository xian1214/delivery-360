export const theme = 'rgb(68,68,68)';
export const button = 'rgb(60,118,177)'
export const inputColor = 'rgb(221,241,248)';
export const cardColor1 = 'rgb(255, 249, 231)';
export const cardColor2 = 'rgb(233, 251, 241)';
export const cardColor3 = 'rgb(234, 240, 253)';

export const yellow = 'rgb(255, 197, 61)'
export const green = 'rgb(0, 195, 115)'
export const lightGreen = 'rgb(76, 214, 158)'
export const blue = 'rgba(105, 148, 226,1)';
export const lightBlue = 'rgb(123, 80, 234)'
export const brown = 'rgb(255, 110, 86)'
export const pink = 'rgb(181, 76, 231)'
export const lightPink = 'rgb(205, 128, 239)'
export const red = 'rgb(205, 0, 0)'


export const normalText = 'rgb(58,58,72)'
export const menuRight = 'rgb(205, 205, 211)'
export const menuBorder = 'rgb(169,169,171)'
export const borderColor = 'rgb(217,217,222)'
export const textColor1 = 'rgb(157, 157, 163)'
export const textColor2 = 'rgb(107, 107, 118)'
export const textColor3 = 'rgb(178, 178, 186)'
export const placeholderText = 'rgb(158,158,164)';
export const header = 'black'
export const lineBetween = '#d8d8da'