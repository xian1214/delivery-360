import { AsyncStorage } from "react-native";
import * as CONST from "./const";
import * as CONST2 from '../components/constant'
import moment from 'moment'
class UtilService {
  static getPastDateTime(ts) {
    if (ts == null || ts == "") return "";
    //console.log('ts on getting Past is ppppppppppppppppppp',ts)
    return Math.floor(ts/1000)
    //var mins = Math.floor((Date.now() / 1000 - ts / 1000) / 60);
    // var mins = Math.floor(ts/1000/60);
    // if (mins <= 0) {
    //   return "just now";
    // } else if (mins < 60) {
    //   if (mins == 1) return mins + " minute ago";
    //   else return mins + " minutes ago";
    // } else if (mins < 24 * 60) {
    //   var hours = Math.floor(mins / 60);

    //   if (hours == 1) return hours + " hour ago";
    //   else return hours + " hours ago";
    // } else if (mins >= 24 * 60) {
    //   var days = Math.floor(mins / (24 * 60));

    //   if (days == 1) return days + " day ago";
    //   else return days + " days ago";
    // }
  }
  static getPassTimeBySecondFrom(start){
    let past = (this.getCurrentLocalTimeObj().getTime() - (this.getLocalTime(start)).getTime())/1000
    ////console.log('start on getPassTimeBy', start)
    return past
  }
  static convertToSlug(Text) {
    return Text.toLowerCase()
      .replace(/[^\w ]+/g, "")
      .replace(/ +/g, "-");
  }

  static getPositionString(pos) {
    if (!pos) return "—";

    return this.ordinal_suffix_of(pos);
  }

  static deg2rad(angle) {
    return (angle * Math.PI) / 180;
  }

  
  

  static getDistanceFromLatLonInMile(lat1, lon1, lat2, lon2) {
    var radlat1 = Math.PI * lat1/180
    var radlat2 = Math.PI * lat2/180
    var theta = lon1-lon2
    var radtheta = Math.PI * theta/180
    var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
    if (dist > 1) {
      dist = 1;
    }
    dist = Math.acos(dist)
    dist = dist * 180/Math.PI
    dist = dist * 60 * 1.1515
    return dist * 1.609344
  }
  static getTimeSpanBySec(start,end){
      //console.log('start-end on gettimespanbysec',start,'--end--',end)
      let past = ((new Date(end)).getTime() - (new Date(start)).getTime())/1000
      //console.log('start on getPassTimeBy', start)
      return past
  }
  static getDistanceFromLatLonInMeter(lat1, lon1, lat2, lon2) {
    var radlat1 = Math.PI * lat1/180
    var radlat2 = Math.PI * lat2/180
    var theta = lon1-lon2
    var radtheta = Math.PI * theta/180
    var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
    if (dist > 1) {
      dist = 1;
    }
    dist = Math.acos(dist)
    dist = dist * 180/Math.PI
    dist = dist * 60 * 1.1515
    return dist * 1.609344 * 1604
  }

  static getBackColor(imageObj) {
    if (!imageObj) return "rgb(255,255,255)";

    var backgroundColor = imageObj._env
      ? "rgb(" +
        imageObj._env["input-md-average"].r +
        "," +
        imageObj._env["input-md-average"].g +
        "," +
        imageObj._env["input-md-average"].b +
        ")"
      : "rgb(255,255,255)";

    return backgroundColor;
  }

  static getPricesString(prices) {
    var p = prices || 1;
    var ret = "";
    for (i = 1; i <= p; i++) {
      ret += "₡";
    }

    return ret;
  }

  static capitalizeFirstLetter(string) {
    if (string === undefined) {
      return null;
    }

    return string.charAt(0).toUpperCase() + string.slice(1);
  }

  static isValid(data) {
    if (!data) return false;

    if (data == "") return false;

    return true;
  }

  static isValidURL(data) {
    if (!this.isValid(data)) return false;

    if (data == "http://") return false;

    return true;
  }

  static fixUrl(url) {
    if (this.isValidURL(url)) {
      url = url.toLowerCase();
      //if ((url.indexOf("http://") == -1) && (url.indexOf("https://") == -1)) {
      if (url.indexOf(":") == -1) {
        url = "http://" + url;
      }
      return url;
    }

    return null;
  }

  static ordinal_suffix_of(i) {
    var j = i % 10,
      k = i % 100;
    if (j == 1 && k != 11) {
      return i + "st";
    }
    if (j == 2 && k != 12) {
      return i + "nd";
    }
    if (j == 3 && k != 13) {
      return i + "rd";
    }
    return i + "th";
  }

  static async saveLocalStringData(key, strValue) {
    await AsyncStorage.setItem("@gogo:" + key, strValue);
    return true;
  }

  static async saveLocalObjectData(key, obj) {
    await AsyncStorage.setItem("@gogo:" + key, JSON.stringify(obj));
  }

  static async getLocalStringData(key) {
    let ret = await AsyncStorage.getItem("@gogo:" + key);

    return ret;
  }
  static getCurrentLocalTimeObj() {

    var d = new Date()
    d.setTime(d.getTime() - (new Date()).getTimezoneOffset() * 60 * 1000);
    return d

}
  static async getLocalObjectData(key) {
    let ret = await AsyncStorage.getItem("@gogo:" + key);
    if (ret != null) {
      return JSON.parse(ret);
    } else {
      return null;
    }
  }

  static async removeLocalObjectData(key) {
    let ret = await AsyncStorage.removeItem("@gogo:" + key);
    return true;
  }

  getHours(date) {
    let d = new Date(date);
  }

  static getAddress1(contact) {
    var rets = [];
    if (contact) {
      if (contact.Street) rets.push(contact.Street);
      if (contact.Street2) rets.push(contact.Street2);
      return rets.join(" ");
    }
  }
  static getAddress2(contact) {
    if (contact) {
      var rets = [];
      if (contact.City) rets.push(contact.City);
      if (contact.State) rets.push(contact.State);
      if (contact.Country)
        rets.push(this.getCountryNameFromAlpha3(contact.Country));
      return rets.join(" ");
    }
  }
  static getCountryNameFromAlpha3(alpha) {
    var exist = CONST.countries.find(o => {
      return o.alpha3 == alpha;
    });
    if (exist) {
      return exist.name;
    }
  }

  static getDateTime(date) {
    let d = new Date(date);
    let ap = (d.getHours()>12)?'PM':'AM'
    const padWithZero = number => {
      const string = number.toString();
      if (number < 10) {
        return "0" + string;
      }
      return string;
    };
    return d.getFullYear()+'-'+padWithZero(d.getMonth()+1)+'-'+padWithZero(d.getDate())+' '+padWithZero(d.getHours())+':'+padWithZero(d.getMinutes()) +" " + ap
  }

  static getCurrentLocalDateTime(){
    var tempDate = new Date();
    var offsetInMinutes = tempDate.getTimezoneOffset()
    var res = new Date(tempDate.getTime()-1000*offsetInMinutes*60)
    return res
  }
  static getDistanceOnMap(A, B, point){
    let a = this.getDistanceFromLatLonInMile(A.latitude, A.longitude, point.latitude, point.longitude)
    let b = this.getDistanceFromLatLonInMile(B.latitude, B.longitude, point.latitude, point.longitude)
    let c = this.getDistanceFromLatLonInMile(A.latitude, A.longitude, B.latitude, B.longitude)

    // //console.log(a,b)
    if ( a*a>b*b+c*c ) return b
    if ( b*b>a*a+c*c ) return a
    let p = (a+b+c)/2
    return 2*Math.sqrt(p*(p-a)*(p-b)*(p-c))/c
  }

  static getDistanceFromRoute(route, point){
    let res = 1e10
    let len = route.length
    for ( let i = 0; i < len-1; i ++){
      let dis = this.getDistanceOnMap(route[i], route[i+1], point)
      res = Math.min(res, dis)
    }
    return res
  }

  static getTimeSpanPercent(a,b){
     
    if ( a > b ) return 100
    return 100*a/b
  }
  static getOverPassPercent(issuedTime, shelfLife){
    if ( issuedTime == null ) return 0
    let passedTime = (this.getCurrentLocalTimeObj().getTime() - (new Date(issuedTime)).getTime())/60000
    let overTime = passedTime - shelfLife
    if ( overTime > shelfLife ) return 100
    return 100*overTime/shelfLife
  }
  static getDiffTime(start, duration){
    let past = (this.getCurrentLocalTimeObj().getTime() - (new Date(start)).getTime())/1000
    var diff = (past-duration)
    //console.log('getPastTime------render-------diff',diff)
    //if ( past > duration ) return '+'+(past-duration).toFixed(0)+':'+((past-duration)%60)
    var hourPart = (diff/60).toFixed(0)
    var minPart = (diff%60).toFixed(0)
    var paddedMin = this.zeroPad(Number(minPart),2)
    return (diff/60).toFixed(0)+'m:'+ paddedMin + 's'
  }
  static getHourMinutes(date){
    let dd = new Date(date)
    return moment(dd).format('hh:mm A')
  }
  static isSameDate(A, B) {
    if (
      A.getDate() == B.getDate() &&
      A.getMonth() == B.getMonth() &&
      A.getFullYear() == B.getFullYear()
    )
      return true;
    return false;
  }
  static getHourMinutesFromSecond(date){
    let dd = new Date(date)
    return moment(dd).format('MM/DD/YY hh:mm A')
  }

  static convertToHourMinSec(secs){
    ////console.log('-----convertToHourMinSec----',secs)
    var hour = this.zeroPad(Math.floor(secs/3600),2)
    var min = this.zeroPad(((secs/60)%60).toFixed(0),2)
    var sec = this.zeroPad((secs%60).toFixed(0),2)
    return hour+':'+min+':'+sec
  }
  static getLocalTime(date){
      var d = new Date(date)
      d.setTime(d.getTime() - (new Date()).getTimezoneOffset()*60*1000);
      return d
  }
  static zeroPad(num, places) {
    var zero = places - num.toString().length + 1;
    return Array(+(zero > 0 && zero)).join("0") + num;
  }
  static getAlarmText(deliveryMessage){
    
    switch(deliveryMessage.Type){
        case CONST2.ALARM_TYPE_GEOFENCE:
            return 'Truck is in geofence region'
        case CONST2.ALARM_TYPE_IDLE:
            return 'Truck is on idle'
        case CONST2.ALARM_TYPE_REROUTING:
            return 'Route is changed'
        case CONST2.ALARM_TYPE_PAUSE:
            return 'Delivery is paused'
        case CONST2.ALARM_TYPE_START:
            return 'Delivery is started'
        case CONST2.ALARM_TYPE_CONFIRM:
            return 'Delivery is confirmed'
        case CONST2.ALARM_TYPE_RESUME:
            return 'Delivery is resumed'
        case CONST2.ALARM_TYPE_ARRIVAL:
            return 'Truck is arrived'
        case CONST2.ALARM_TYPE_UNLOADING:
            return 'Unloading...'
        case CONST2.ALARM_TYPE_UNLOADED:
            return 'Unloaded!'
        case CONST2.ALARM_TYPE_READY:
            return 'Delivery is ready'
        case CONST2.ALARM_TYPE_RETURN:
            return 'Delivery is returning'
        case CONST2.ALARM_TYPE_RETURN_ARRIVAL:
            return 'Truck is returned back'
        case CONST2.ALARM_TYPE_SIGNOFF:
            return 'Delivery is completed'
        case CONST2.ALARM_TYPE_FORCE_ARRIVAL:
            return 'Delivery is arrived out of geofence'
        case CONST2.ALARM_TYPE_EXPIRED:
            return 'Delivery Item Expired'
        case CONST2.ALARM_TYPE_OUT_UNLOADING:
            return 'Unloading out of Geofence'
    }
  }
}

export default UtilService;
