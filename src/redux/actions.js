import * as types from './actionTypes';
import api from '../service/api'
import Cache from '../utils/cache'
import moment from 'moment'
import * as commonStyles from '../styles/styles'


export function setHeading(heading){
  return dispatch =>{
    dispatch({type:types.SET_HEADING, heading});
  }
}

export function setLocation(location){
  return dispatch =>{
    dispatch({type:types.SET_LOCATION, location, status:types.SUCCESS});
  }
}
export function setBackupLocation(location){
  return dispatch =>{
    dispatch({type:types.SET_BACKUP_LOCATION, location, status:types.SUCCESS});
    //setTimeout(()=>dispatch({type:types.SET_BACKUP_LOCATION, location, status:null}),20);
  }
}
// export function plusUnreadMsgCnt(){
//   return dispatch =>{
//     dispatch({type:types.PLUS_UNREAD_CNT, status:types.SUCCESS});
//     setTimeout(()=>dispatch({type:types.PLUS_UNREAD_CNT, location, status:null}),20);
//   }
// }
// export function resetUnreadMsgCnt(){
//   return dispatch =>{

//     dispatch({type:types.RESET_UNREAD_CNT, status:types.SUCCESS});
//     setTimeout(()=>dispatch({type:types.RESET_UNREAD_CNT, status:null}),20);
//   }
// }
export function setLocationMode(isMobileMode){
  //console.log('updating location on actions.....',isMobileMode)
  return dispatch =>{
    dispatch({type:types.SET_LOCATION_MODE, isMobileMode, status:types.SUCCESS});
  }
}
export function receivedNotification(notification){
  return dispatch=>{
    dispatch({type:types.RECEIVED_NOTIFICATION, notification});
  }
}

export function changeQueueState(value,fireNeeded){
  //console.log('change queue state on actions>>>>>')
  return dispatch=>{
    dispatch({type:types.CHANGE_QUEUE_STATE, value});
    if(fireNeeded){
      if(value){
        api.queueIn((err,res)=>{
          if (err){
            return 
          }
        })  
      }
      else{
        api.queueDrop((err,res)=>{
          if (err){
            return 
          }
        })
      }
    }
  }
}

// export function updateCurrentDelivery(id){
//   return dispatch=>{
//     api.getDelivery(id, (err, res)=>{
//       // //console.log('update delivery', err, res)
//       if ( err == null ){
//         if (res.Status==commonStyles.DELIVERY_STATUS_FINISH){
//           res = null
//         }
//         dispatch({type:types.UPDATE_CURRENT_DELIVERY, value:res, status:types.SUCCESS})
//         setTimeout(()=>dispatch({type:types.UPDATE_CURRENT_DELIVERY, value:res, status:null}),20)
//       }else{
//         dispatch({type:types.UPDATE_CURRENT_DELIVERY, value:res, status:types.FAILED})
//       }
//     })
//   }
// }

export function getTodayUpcomingDeliveryCount(){
  return dispatch=>{
    api.getDeliveries(
      Cache.currentUser.user.UserID, 
      false,
      1,
      1,
      moment().format('YYYY-MM-DD') + ' 00:00:00',
      moment().format('YYYY-MM-DD') + ' 23:59:59',
      (err, res)=>{
        // //console.log('todayCount', err, res)
        if ( err == null ){
          dispatch({type:types.GET_TODAY_UPCOMING_COUNT, count:res.TotalNumberOfRecords})
        }
      }
    )
  }
}

export function getFutureUpcomingDeliveryCount(){
  return dispatch=>{
    api.getDeliveries(
      Cache.currentUser.user.UserID, 
      false,
      1,
      1,
      moment().format('YYYY-MM-DD') + ' 00:00:00',
      null,
      (err, res)=>{
        // //console.log('otherCount', err, res)
        if ( err == null ){
          dispatch({type:types.GET_FUTURE_UPCOMING_COUNT, count:res.TotalNumberOfRecords})
        }
      }
    )
  }
}

export function getUpcomingDeliveries(page, pageSize){
  return dispatch=>{
    dispatch({type:types.GET_UPCOMING_DELIVERIES_MORE, status:types.LOADING})
    api.getDeliveries(
      Cache.currentUser.user.UserID, 
      false,
      page,
      pageSize,
      null,
      null,
      (err, res)=>{
        if ( err == null ){
          dispatch({type:types.GET_UPCOMING_DELIVERIES_MORE, items:res.Results, status: types.SUCCESS})
        }else{
          dispatch({type:types.GET_UPCOMING_DELIVERIES_MORE, status:types.FAILED})
        }
      }
    )
  }
}

export function getUpcomingAllDeliveries(){
 
  return dispatch=>{
    dispatch({type:types.GET_UPCOMING_DELIVERIES_REFRESH, status:types.LOADING})
    api.getDeliveries(
      Cache.currentUser.user.UserID, 
      false,
      null,
      null,
      null,
      null,
      (err, res)=>{
        // //console.log('upcoming Deliveries', err)
        if ( err == null ){
          dispatch({type:types.GET_UPCOMING_DELIVERIES_REFRESH, items:res.Results, status: types.SUCCESS})
        }else{
          dispatch({type:types.GET_UPCOMING_DELIVERIES_REFRESH, status:types.FAILED})
        }
      }
    )
  }
}

export function getCompletedDeliveries(page, pageSize){
  return dispatch=>{
    dispatch({type:types.GET_COMPLETED_DELIVERIES_MORE, status:types.LOADING})
    api.getDeliveries(
      Cache.currentUser.user.UserID, 
      true,
      page,
      pageSize,
      null,
      null,
      (err, res)=>{
        if ( err == null ){
          dispatch({type:types.GET_COMPLETED_DELIVERIES_MORE, items:res.Results, status: types.SUCCESS})
        }else{
          dispatch({type:types.GET_COMPLETED_DELIVERIES_MORE, status:types.FAILED})
        }
      }
    )
  }
}

export function getCompletedAllDeliveries(){
  return dispatch=>{
    dispatch({type:types.GET_COMPLETED_DELIVERIES_REFRESH, status:types.LOADING})
    api.getDeliveries(
      Cache.currentUser.user.UserID, 
      true,
      null,
      null,
      null,
      null,
      (err, res)=>{
        // //console.log('completedDeliveries', err)
        if ( err == null ){
          dispatch({type:types.GET_COMPLETED_DELIVERIES_REFRESH, items:res.Results, status: types.SUCCESS})
        }else{
          dispatch({type:types.GET_COMPLETED_DELIVERIES_REFRESH, status:types.FAILED})
        }
      }
    )
  }
}
// export function getDeliveries(){
//   return dispatch=>{
//     dispatch({type:types.GET_DELIVERIES, status:types.LOADING})
//     api.getDeliveries(( err, res) => {
//       if (err == null) {
//         Cache.deliveries = res.Results
//         let upcoming = [], completed=[], currentItem=null;
//         res.Results.map(item => {
//           if ((item.User && item.User.UserID == Cache.currentUser.user.UserID)&&item.ProjectID!=null) {
//             if ( item.Status=='004005' || item.Status == '004002'){
//               upcoming.push(item)
//               if ( item.OTWDatetime ){
//                 currentItem = item
//               }
//             }
//             if ( item.Status=='004003'){
//               completed.push(item)
//             }
//           }
//         });
//         completed.reverse()
//         dispatch({type:types.GET_DELIVERIES, upcomingDeliveries:upcoming, completedDeliveries:completed, currentItem, status:types.SUCCESS})
//       }else{
//         dispatch({type:types.GET_DELIVERIES, status:types.FAILED})
//       }
//     });
//   }
// }

