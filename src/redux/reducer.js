import * as types from "./actionTypes";
import * as commonStyles from '../styles/styles'

const initialState = {
  type: null,
  heading:null,
  location: null,
  backupLocation:null,
  notification: null,
  hasConnection: true,
  upcomingDeliveries:[],
  completedDeliveries:[],
  queueIn: false,
  currentDelivery:null,
  targetSpeed:0,
  todayUpcomingCount:0,
  otherUpcomingCount:0,
};

export default function common(state = initialState, action = {}) {
  ////console.log('actions.type on reducer ...',action.type)
  switch (action.type) {
    
    case types.SET_HEADING:
      state.type = types.SET_HEADING;
      state.heading = action.heading;
      return JSON.parse(JSON.stringify(state));

    case types.SET_LOCATION:
      state.type = types.SET_LOCATION;
      state.location = action.location;
      state.status = action.status;
      return JSON.parse(JSON.stringify(state));
    
    case types.SET_BACKUP_LOCATION:
      ////console.log('set_backup_location....on reducer',action.location)
      state.type = types.SET_BACKUP_LOCATION;
      state.backupLocation = action.location;
      state.status = action.status;
      return JSON.parse(JSON.stringify(state));

    case types.SET_TARGETSPEED:
      state.type = types.SET_TARGETSPEED;
      state.targetSpeed = action.targetSpeed;
      state.status = action.status;
      return JSON.parse(JSON.stringify(state));

    case types.RECEIVED_NOTIFICATION:
      state.type = types.RECEIVED_NOTIFICATION;
      state.notification = action.notification
      return JSON.parse(JSON.stringify(state));

    case types.GET_TODAY_UPCOMING_COUNT:
      state.type = types.GET_TODAY_UPCOMING_COUNT;
      state.todayUpcomingCount = action.count
      return JSON.parse(JSON.stringify(state))

    case types.GET_FUTURE_UPCOMING_COUNT:
      state.type = types.GET_FUTURE_UPCOMING_COUNT;
      state.otherUpcomingCount = action.count
      return JSON.parse(JSON.stringify(state))

    // case types.GET_UPCOMING_DELIVERIES_MORE:
    //   state.type = types.GET_UPCOMING_DELIVERIES
    //   state.status = action.status
    //   if ( action.status == types.SUCCESS ){
    //     let mapping = []
    //     state.upcomingDeliveries.map((item)=>mapping[item.DeliveryNoteID]=true)
    //     action.items.map((item)=>{
    //       if ( !mapping[item.DeliveryNoteID] ){
    //         state.upcomingDeliveries.push(item)
    //       }
    //     })
    //   }
    //   return JSON.parse(JSON.stringify(state))

    case types.GET_UPCOMING_DELIVERIES_REFRESH:
      state.type = types.GET_UPCOMING_DELIVERIES_REFRESH
      state.status = action.status
      if ( action.status == types.SUCCESS ){
        state.upcomingDeliveries = action.items
        action.items.map((item)=>{
          if ( item.OTWDatetime && item.Status != commonStyles.DELIVERY_STATUS_FINISH){
            if ( state.currentDelivery == null ) state.currentDelivery = item
          }
        })
      }
      return JSON.parse(JSON.stringify(state))

    // case types.GET_COMPLETED_DELIVERIES_MORE:
    //   state.type = types.GET_COMPLETED_DELIVERIES
    //   state.status = action.status
    //   if ( action.status == types.SUCCESS ){
    //     let mapping = []
    //     state.completedDeliveries.map((item)=>mapping[item.DeliveryNoteID]=true)
    //     action.items.map((item)=>{
    //       if ( !mapping[item.DeliveryNoteID] ){
    //         state.completedDeliveries.push(item)
    //       }
    //     })
    //   }
    //   return JSON.parse(JSON.stringify(state))

    case types.GET_COMPLETED_DELIVERIES_REFRESH:
      state.type = types.GET_COMPLETED_DELIVERIES_REFRESH
      state.status = action.status
      if ( action.status == types.SUCCESS ){
        state.completedDeliveries = action.items.reverse()
        action.items.map((item)=>{
          if ( item.OTWDatetime && item.Status != commonStyles.DELIVERY_STATUS_FINISH){
            if ( state.currentDelivery == null ) state.currentDelivery = item
          }
        })
        // //console.log('completed', action.items)
      }
      return JSON.parse(JSON.stringify(state))
      
    // case types.GET_DELIVERIES:
    //   state.type = types.GET_DELIVERIES;
    //   if ( action.status== types.SUCCESS ){
    //     state.upcomingDeliveries = action.upcomingDeliveries
    //     let mapping = []
    //     state.upcomingDeliveries.map((item)=>mapping[item.DeliveryNoteID]=true)
    //     action.upcomingDeliveries.map((item)=>{
    //       if ( !mapping[item.DeliveryNoteID] ){
    //         state.upcomingDeliveries.push(item)
    //       }
    //     })
    //     state.completedDeliveries = action.completedDeliveries
    //     state.currentDelivery = action.currentItem
    //   }
    //   state.status = action.status
    //   return JSON.parse(JSON.stringify(state))

    case types.CHANGE_QUEUE_STATE:
      state.type=types.CHANGE_QUEUE_STATE
      state.queueIn = action.value
      return JSON.parse(JSON.stringify(state))

    case types.UPDATE_CURRENT_DELIVERY:
      state.type=types.UPDATE_CURRENT_DELIVERY
      state.currentDelivery = action.value
      state.status = action.status;
      return JSON.parse(JSON.stringify(state))
      
    default:
      return state;
  }
}
