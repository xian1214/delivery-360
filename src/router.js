import React, { PureComponent } from "react";
import { Constants, Permissions, Notifications } from 'expo';

const PUSH_ENDPOINT = 'http://192.168.1.107/notification/send';

import { NetInfo, KeyboardAvoidingView, Platform, Alert,Modal} from "react-native";
import { Actions, ActionConst, Scene, Router } from "react-native-router-flux";
import { Asset, AppLoading } from 'expo';

import SignIn from "./screens/auth/login";
import ConfirmOTP from "./screens/auth/confirmOTP";
import Main from "./screens/main";
import Upcoming from "./screens/upcoming";
import Completed from "./screens/completed";
import Summary from "./screens/summary";
import DeliveryDetail from "./screens/deliveryDetail";
import CompletedDeliveryDetail from "./screens/completedDeliveryDetail";
import CompletedReport from './screens/completedReport'
import SetRoute from "./screens/setRoute";
import DeliverySecondReport from "./screens/deliverySecondReport";
import SelectRoute from "./screens/selectRoute";
import Navigation from "./screens/navigation";
import TotalOrders from "./screens/totalOrders";
import Chat from "./screens/chat";
import PolygonCreator from './screens/PolygonCreator'
import Offline from "./screens/offline";
import CompleteDelivery from "./screens/completeDelivery";
import DeliveryReport from "./screens/deliveryReport";
import ConfirmClient from "./screens/confirmClient";
import ScanQRCode from "./screens/qrScan";
import ScanFace from "./screens/faceScan";
import RegisterFace from "./screens/faceRegister";
import CameraView from "./screens/cameraView";
import Test from "./screens/test";
import ImagePicker from "./screens/imagePicker";
import Navigation2 from "./screens/navigation2";
import CheckList from "./screens/checkList";
import AlertList from "./screens/alertList";
import LastScreen from "./screens/lastScreen";
import Signature from "./screens/signature";
import BranchList from "./screens/branchList";
import CheckinList from './screens/checkInList'
import AddItem from './screens/addItem'
import ShowImage from './screens/showImage'
import DriverList from './screens/driverlist'
import AsyncScreen from './screens/asyncScreen'
import Profile from './screens/profile'
import Detail from "./screens/home/detail";
import CheckOut from "./screens/home/checkOut";
import ChangePassword from './screens/changePassword'
import ItemReturn from './screens/itemReturn'
import VehicleDetail from './screens/vehicleDetail'
import CompletedSummary from './screens/completedSummary'
import PlacePicker from './screens/placePicker'
import NavService from './screens/navService'
import GroupCheckIn from './screens/groupCheckIn';
import CompletedSecondReport from './screens/completedSecondReport'
import ChatList from './screens/chatList'
import { FontAwesome, Feather, Ionicons, MaterialCommunityIcons, MaterialIcons } from '@expo/vector-icons';
// import EditRouter from "./screens/home/editRouter";
import AlertListByDn from './screens/alertListByDn'
import api from "./service/api";
import Cache from './utils/cache'
import UtilService from './utils/utils'

import * as actionTypes from "./store/common/actionTypes";

import { bindActionCreators } from "redux";
import { connect } from "react-redux";
// import * as actions from "../redux/actions";
import * as actions from './store/common/actions'
import * as session from './store/session/actions'
//import DeviceBattery from 'react-native-device-battery';

class App extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      init: false,
      loggedIn: false,
      isReady: false,
    };
    console.ignoredYellowBox = [
      'Setting a timer'
    ];
    this.hasMounted = false
  }

  onBatteryStateChanged=(state)=> {
    // //console.log(state) // {level: 0.95, charging: true}
    this.props.actions.updateBatteryInfo(state)
  };

  async componentWillMount() {
    
    await this.registerForPushNotificationsAsync();
    Notifications.addListener((receivedNotification) => {
      ////console.log('receivedNotification ... on router....!!!',receivedNotification)
      this.props.actions.receivedNotification(receivedNotification)
    });
    //DeviceBattery.addListener(this.onBatteryStateChanged);
  }

  registerForPushNotificationsAsync = async () => {
    if(Constants.isDevice){
      const { status: existingStatus } = await Permissions.getAsync(
        Permissions.NOTIFICATIONS
      );
      let finalStatus = existingStatus;
      if (existingStatus !== 'granted') {
        const { status } = await Permissions.askAsync(Permissions.NOTIFICATIONS);
        finalStatus = status;
      }
      if (finalStatus !== 'granted') {
        return;
      }
      let token = await Notifications.getExpoPushTokenAsync();
      Cache.pushToken = token;
      if(!!!token){
        //Alert.alert("Failed to get push token")
      }
    }  
    else{
      Cache.pushToken = "sim_driver_token";
    }         
  };

  // uploadAll(cb) {
  //   let myInterval = setInterval(() => {
  //     if (this.state.init) {
  //       clearInterval(myInterval)
  //       api.uploadAllData(cb)
  //     }
  //   }, 200)
  // }

  componentDidMount() {
    // this.testCount = 0
    // setInterval(()=>{
    //   this.testCount ++;
    //   this.props.actions.receivedNotification('Test'+this.testCount)
    // }, 1000)
    this.hasMounted = true
    api.init((err, res) => {
      if (err == null) {
        this.setState({ loggedIn: true, init: true });
      } else {
        this.setState({ init: true });
      }
    });
    api.checkVersion((error,res)=>{
      //console.log('err,res on router init',error,res)
      if(error){

      }
    })
    NetInfo.isConnected.addEventListener(
      "connectionChange",
      hasInternetConnection => {
        this.props.actions.setInternetConnection(hasInternetConnection)
        if ( hasInternetConnection != Cache.hasInternetConnection && hasInternetConnection ){
          Cache.hasInternetConnection = hasInternetConnection
          Actions.AsyncScreen()
        }
        Cache.hasInternetConnection = hasInternetConnection
      }
    );
  }
  async _cacheResourcesAsync() {
    const images = [
      require('../public/images/back.jpg'),
      require('../public/images/logo.png'),
      require('../public/images/logo-white.png'),
      require('../public/images/logo-white.png'),
      require('../public/images/face1.jpg'),
      require("../public/images/face2.jpg"),
      require("../public/images/face3.jpg"),
      require("../public/images/path_theme.png"),
      require("../public/images/deliver.jpg"),
    ];

    const cacheImages = images.map((image) => {
      return Asset.fromModule(image).downloadAsync();
    });
    // const fontAssets = cacheFonts([FontAwesome.font, Feather.font, Ionicons.font, MaterialCommunityIcons.font, MaterialIcons.font]);
    return Promise.all(cacheImages)

  }

  render() {
    if (!this.state.isReady || !this.state.init) {
      return (
        <AppLoading
          startAsync={this._cacheResourcesAsync}
          onFinish={() => this.hasMounted && this.setState({ isReady: true })}
          onError={console.warn}
        />
      );
    }
    
    const scenes = Actions.create(
      <Scene key="root">
       
        <Scene key="SignIn" component={SignIn} hideNavBar />
        <Scene key="ConfirmOTP" component={ConfirmOTP} hideNavBar />
        <Scene key="Main" component={Main} hideNavBar initial={this.state.loggedIn}  />
        {/* <Scene key="Main" component={Main} hideNavBar initial={false}  /> */}
        <Scene key="GroupCheckIn" component={GroupCheckIn} hideNavBar />
        <Scene key="Offline" component={Offline} hideNavBar />
        <Scene key="Profile" component={Profile} hideNavBar />
        <Scene key="Upcoming" component={Upcoming} hideNavBar />
        <Scene key="Completed" component={Completed} hideNavBar />
        <Scene key="Summary" component={Summary} hideNavBar />
        <Scene key="DeliveryDetail" component={DeliveryDetail} hideNavBar />
        <Scene key="ChatList" component={ChatList} hideNavBar />
        <Scene key="CompletedDeliveryDetail" component={CompletedDeliveryDetail} hideNavBar />
        <Scene key="SetRoute" component={SetRoute} hideNavBar />
        <Scene key="SelectRoute" component={SelectRoute} hideNavBar />
        <Scene key="Navigation" component={Navigation} hideNavBar />
        <Scene key="Navigation2" component={Navigation2} hideNavBar />
        <Scene key="TotalOrders" component={TotalOrders} hideNavBar />
        <Scene key="Chat" component={Chat} hideNavBar />
        <Scene key="AlertListByDn" component={AlertListByDn} hideNavBar />
        <Scene key="NavService" component={NavService} hideNavBar />
        <Scene key="PolygonCreator" component={PolygonCreator} hideNavBar />
        <Scene key="DeliveryReport" component={DeliveryReport} hideNavBar />
        <Scene key="DeliverySecondReport" component={DeliverySecondReport} hideNavBar />
        <Scene key="CompleteDelivery" component={CompleteDelivery} hideNavBar />
        <Scene key="CompletedSummary" component={CompletedSummary} hideNavBar />
        <Scene key="ConfirmClient" component={ConfirmClient} hideNavBar />
        <Scene key="ScanFace" component={ScanFace} hideNavBar />
        <Scene key="RegisterFace" component={RegisterFace} hideNavBar />
        <Scene key="ScanQRCode" component={ScanQRCode} hideNavBar />
        <Scene key="PlacePicker" component={PlacePicker} hideNavBar />
        {/* <Scene key="CameraView" component={CameraView} hideNavBar /> */}
        <Scene key="Test" component={Test} hideNavBar />
        <Scene key="ImagePicker" component={ImagePicker} hideNavBar />
        <Scene key="CheckList" component={CheckList} hideNavBar />
        <Scene key="AlertList" component={AlertList} hideNavBar />
        <Scene key="LastScreen" component={LastScreen} hideNavBar />
        <Scene key="Signature" component={Signature} hideNavBar />
        <Scene key="CompletedReport" component={CompletedReport} hideNavBar />
        <Scene key="CompletedSecondReport" component={CompletedSecondReport} hideNavBar />
        <Scene key="BranchList" component={BranchList} hideNavBar />
        <Scene key="CheckinList" component={CheckinList} hideNavBar/>
        <Scene key="AddItem" component={AddItem} hideNavBar/>
        <Scene key="ShowImage" component={ShowImage} hideNavBar/>
        <Scene key="DriverList" component={DriverList} hideNavBar/>
        <Scene key="AsyncScreen" component={AsyncScreen} hideNavBar/>
        <Scene key="ChangePassword" component={ChangePassword} hideNavBar/>
        <Scene key="ItemReturn" component={ItemReturn} hideNavBar/>
        <Scene key="VehicleDetail" component={VehicleDetail} hideNavBar/>
        {/* Auth */}
        <Scene key="Detail" component={Detail} hideNavBar />
        <Scene key="CheckOut" component={CheckOut} hideNavBar />
        {/* <Scene key="EditRouter" component={EditRouter} hideNavBar /> */}
      </Scene>
    );

  
    return (
      <KeyboardAvoidingView
        behavior={'padding'}
        style={{ flex: 1 }}>
        <Router hideNavBar scenes={scenes} />
      </KeyboardAvoidingView>
    );
  }
}

export default connect(
  state => ({}),
  dispatch => ({
    actions: bindActionCreators(actions, dispatch)
  })
)(App);
